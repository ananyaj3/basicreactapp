# packt-clj.images

a folder containing all relevant files for a basic react app which displays images and has some interactivity

## Overview

Basic react app using clojurescript, figwheel and reagent

## Setup

Install lein and npm. CD to the folder then type lein fighweel, this app will open up in a default browser's window

## License

None
