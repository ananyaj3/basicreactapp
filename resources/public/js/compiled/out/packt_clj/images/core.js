// Compiled by ClojureScript 1.10.520 {}
goog.provide('packt_clj.images.core');
goog.require('cljs.core');
goog.require('reagent.core');
cljs.core.enable_console_print_BANG_.call(null);
if((typeof packt_clj !== 'undefined') && (typeof packt_clj.images !== 'undefined') && (typeof packt_clj.images.core !== 'undefined') && (typeof packt_clj.images.core.app_state !== 'undefined')){
} else {
packt_clj.images.core.app_state = reagent.core.atom.call(null,new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"images","images",1757475080),cljs.core.PersistentVector.EMPTY,new cljs.core.Keyword(null,"author-display","author-display",1863450609),true], null));
}
packt_clj.images.core.image = (function packt_clj$images$core$image(p__21452){
var map__21453 = p__21452;
var map__21453__$1 = (((((!((map__21453 == null))))?(((((map__21453.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__21453.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__21453):map__21453);
var download_url = cljs.core.get.call(null,map__21453__$1,new cljs.core.Keyword(null,"download_url","download_url",-2115471203));
var author = cljs.core.get.call(null,map__21453__$1,new cljs.core.Keyword(null,"author","author",2111686192));
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div","div",1057191632),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"img","img",1442687358),new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"src","src",-1651076051),download_url,new cljs.core.Keyword(null,"height","height",1025178622),"130px",new cljs.core.Keyword(null,"style","style",-496642736),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"border","border",1444987323),"solid gray 3px",new cljs.core.Keyword(null,"border-radius","border-radius",419594011),"10px"], null)], null)], null),(cljs.core.truth_(new cljs.core.Keyword(null,"author-display","author-display",1863450609).cljs$core$IFn$_invoke$arity$1(cljs.core.deref.call(null,packt_clj.images.core.app_state)))?new cljs.core.PersistentVector(null, 4, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div","div",1057191632),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"style","style",-496642736),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"font-size","font-size",-1847940346),"15px",new cljs.core.Keyword(null,"color","color",1011675173),"gray"], null)], null),"Image by ",author], null):null)], null);
});
packt_clj.images.core.image_grid = (function packt_clj$images$core$image_grid(images){
if(cljs.core.empty_QMARK_.call(null,images)){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div","div",1057191632),"Click the button to fetch images"], null);
} else {
return cljs.core.into.call(null,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div","div",1057191632)], null),cljs.core.map.call(null,(function (image_data){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div","div",1057191632),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"style","style",-496642736),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"float","float",-1732389368),"left",new cljs.core.Keyword(null,"margin-left","margin-left",2015598377),"20px"], null)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [packt_clj.images.core.image,image_data], null)], null);
}),images));
}
});
packt_clj.images.core.author_display_button = (function packt_clj$images$core$author_display_button(){
var text = (cljs.core.truth_(new cljs.core.Keyword(null,"author-display","author-display",1863450609).cljs$core$IFn$_invoke$arity$1(cljs.core.deref.call(null,packt_clj.images.core.app_state)))?"Hide author":"Show author");
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"button.btn","button.btn",-661984613),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"on-click","on-click",1632826543),((function (text){
return (function (){
return cljs.core.swap_BANG_.call(null,packt_clj.images.core.app_state,cljs.core.update_in,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"author-display","author-display",1863450609)], null),cljs.core.not);
});})(text))
], null),text], null);
});
packt_clj.images.core.fetch_images = (function packt_clj$images$core$fetch_images(){
return fetch("https://picsum.photos/v2/list?limit=6").then((function (response){
return response.json();
})).then((function (json){
return cljs.core.swap_BANG_.call(null,packt_clj.images.core.app_state,cljs.core.assoc_in,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"images","images",1757475080)], null),cljs.core.js__GT_clj.call(null,json,new cljs.core.Keyword(null,"keywordize-keys","keywordize-keys",1310784252),true));
}));
});
packt_clj.images.core.clear_images = (function packt_clj$images$core$clear_images(){
return cljs.core.swap_BANG_.call(null,packt_clj.images.core.app_state,cljs.core.assoc_in,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"images","images",1757475080)], null),cljs.core.PersistentVector.EMPTY);
});
packt_clj.images.core.fetch_or_clear_button = (function packt_clj$images$core$fetch_or_clear_button(){
var handler = ((cljs.core.empty_QMARK_.call(null,new cljs.core.Keyword(null,"images","images",1757475080).cljs$core$IFn$_invoke$arity$1(cljs.core.deref.call(null,packt_clj.images.core.app_state))))?packt_clj.images.core.fetch_images:packt_clj.images.core.clear_images);
var text = ((cljs.core.empty_QMARK_.call(null,new cljs.core.Keyword(null,"images","images",1757475080).cljs$core$IFn$_invoke$arity$1(cljs.core.deref.call(null,packt_clj.images.core.app_state))))?"Fetch Images":"Clear Images");
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"button.btn","button.btn",-661984613),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"on-click","on-click",1632826543),handler], null),text], null);
});
packt_clj.images.core.app = (function packt_clj$images$core$app(){
return new cljs.core.PersistentVector(null, 4, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div","div",1057191632),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [packt_clj.images.core.fetch_or_clear_button], null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [packt_clj.images.core.author_display_button], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [packt_clj.images.core.image_grid,new cljs.core.Keyword(null,"images","images",1757475080).cljs$core$IFn$_invoke$arity$1(cljs.core.deref.call(null,packt_clj.images.core.app_state))], null)], null);
});
reagent.core.render_component.call(null,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [packt_clj.images.core.app], null),document.getElementById("app"));

//# sourceMappingURL=core.js.map?rel=1591997111079
