// Compiled by ClojureScript 1.10.520 {}
goog.provide('cljs.core.async');
goog.require('cljs.core');
goog.require('cljs.core.async.impl.protocols');
goog.require('cljs.core.async.impl.channels');
goog.require('cljs.core.async.impl.buffers');
goog.require('cljs.core.async.impl.timers');
goog.require('cljs.core.async.impl.dispatch');
goog.require('cljs.core.async.impl.ioc_helpers');
goog.require('goog.array');
cljs.core.async.fn_handler = (function cljs$core$async$fn_handler(var_args){
var G__26369 = arguments.length;
switch (G__26369) {
case 1:
return cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$1 = (function (f){
return cljs.core.async.fn_handler.call(null,f,true);
});

cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$2 = (function (f,blockable){
if((typeof cljs !== 'undefined') && (typeof cljs.core !== 'undefined') && (typeof cljs.core.async !== 'undefined') && (typeof cljs.core.async.t_cljs$core$async26370 !== 'undefined')){
} else {

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Handler}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async26370 = (function (f,blockable,meta26371){
this.f = f;
this.blockable = blockable;
this.meta26371 = meta26371;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
cljs.core.async.t_cljs$core$async26370.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_26372,meta26371__$1){
var self__ = this;
var _26372__$1 = this;
return (new cljs.core.async.t_cljs$core$async26370(self__.f,self__.blockable,meta26371__$1));
});

cljs.core.async.t_cljs$core$async26370.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_26372){
var self__ = this;
var _26372__$1 = this;
return self__.meta26371;
});

cljs.core.async.t_cljs$core$async26370.prototype.cljs$core$async$impl$protocols$Handler$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async26370.prototype.cljs$core$async$impl$protocols$Handler$active_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return true;
});

cljs.core.async.t_cljs$core$async26370.prototype.cljs$core$async$impl$protocols$Handler$blockable_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return self__.blockable;
});

cljs.core.async.t_cljs$core$async26370.prototype.cljs$core$async$impl$protocols$Handler$commit$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return self__.f;
});

cljs.core.async.t_cljs$core$async26370.getBasis = (function (){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"f","f",43394975,null),new cljs.core.Symbol(null,"blockable","blockable",-28395259,null),new cljs.core.Symbol(null,"meta26371","meta26371",-1470178877,null)], null);
});

cljs.core.async.t_cljs$core$async26370.cljs$lang$type = true;

cljs.core.async.t_cljs$core$async26370.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async26370";

cljs.core.async.t_cljs$core$async26370.cljs$lang$ctorPrWriter = (function (this__4374__auto__,writer__4375__auto__,opt__4376__auto__){
return cljs.core._write.call(null,writer__4375__auto__,"cljs.core.async/t_cljs$core$async26370");
});

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async26370.
 */
cljs.core.async.__GT_t_cljs$core$async26370 = (function cljs$core$async$__GT_t_cljs$core$async26370(f__$1,blockable__$1,meta26371){
return (new cljs.core.async.t_cljs$core$async26370(f__$1,blockable__$1,meta26371));
});

}

return (new cljs.core.async.t_cljs$core$async26370(f,blockable,cljs.core.PersistentArrayMap.EMPTY));
});

cljs.core.async.fn_handler.cljs$lang$maxFixedArity = 2;

/**
 * Returns a fixed buffer of size n. When full, puts will block/park.
 */
cljs.core.async.buffer = (function cljs$core$async$buffer(n){
return cljs.core.async.impl.buffers.fixed_buffer.call(null,n);
});
/**
 * Returns a buffer of size n. When full, puts will complete but
 *   val will be dropped (no transfer).
 */
cljs.core.async.dropping_buffer = (function cljs$core$async$dropping_buffer(n){
return cljs.core.async.impl.buffers.dropping_buffer.call(null,n);
});
/**
 * Returns a buffer of size n. When full, puts will complete, and be
 *   buffered, but oldest elements in buffer will be dropped (not
 *   transferred).
 */
cljs.core.async.sliding_buffer = (function cljs$core$async$sliding_buffer(n){
return cljs.core.async.impl.buffers.sliding_buffer.call(null,n);
});
/**
 * Returns true if a channel created with buff will never block. That is to say,
 * puts into this buffer will never cause the buffer to be full. 
 */
cljs.core.async.unblocking_buffer_QMARK_ = (function cljs$core$async$unblocking_buffer_QMARK_(buff){
if((!((buff == null)))){
if(((false) || ((cljs.core.PROTOCOL_SENTINEL === buff.cljs$core$async$impl$protocols$UnblockingBuffer$)))){
return true;
} else {
if((!buff.cljs$lang$protocol_mask$partition$)){
return cljs.core.native_satisfies_QMARK_.call(null,cljs.core.async.impl.protocols.UnblockingBuffer,buff);
} else {
return false;
}
}
} else {
return cljs.core.native_satisfies_QMARK_.call(null,cljs.core.async.impl.protocols.UnblockingBuffer,buff);
}
});
/**
 * Creates a channel with an optional buffer, an optional transducer (like (map f),
 *   (filter p) etc or a composition thereof), and an optional exception handler.
 *   If buf-or-n is a number, will create and use a fixed buffer of that size. If a
 *   transducer is supplied a buffer must be specified. ex-handler must be a
 *   fn of one argument - if an exception occurs during transformation it will be called
 *   with the thrown value as an argument, and any non-nil return value will be placed
 *   in the channel.
 */
cljs.core.async.chan = (function cljs$core$async$chan(var_args){
var G__26376 = arguments.length;
switch (G__26376) {
case 0:
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$0();

break;
case 1:
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.chan.cljs$core$IFn$_invoke$arity$0 = (function (){
return cljs.core.async.chan.call(null,null);
});

cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1 = (function (buf_or_n){
return cljs.core.async.chan.call(null,buf_or_n,null,null);
});

cljs.core.async.chan.cljs$core$IFn$_invoke$arity$2 = (function (buf_or_n,xform){
return cljs.core.async.chan.call(null,buf_or_n,xform,null);
});

cljs.core.async.chan.cljs$core$IFn$_invoke$arity$3 = (function (buf_or_n,xform,ex_handler){
var buf_or_n__$1 = ((cljs.core._EQ_.call(null,buf_or_n,(0)))?null:buf_or_n);
if(cljs.core.truth_(xform)){
if(cljs.core.truth_(buf_or_n__$1)){
} else {
throw (new Error(["Assert failed: ","buffer must be supplied when transducer is","\n","buf-or-n"].join('')));
}
} else {
}

return cljs.core.async.impl.channels.chan.call(null,((typeof buf_or_n__$1 === 'number')?cljs.core.async.buffer.call(null,buf_or_n__$1):buf_or_n__$1),xform,ex_handler);
});

cljs.core.async.chan.cljs$lang$maxFixedArity = 3;

/**
 * Creates a promise channel with an optional transducer, and an optional
 *   exception-handler. A promise channel can take exactly one value that consumers
 *   will receive. Once full, puts complete but val is dropped (no transfer).
 *   Consumers will block until either a value is placed in the channel or the
 *   channel is closed. See chan for the semantics of xform and ex-handler.
 */
cljs.core.async.promise_chan = (function cljs$core$async$promise_chan(var_args){
var G__26379 = arguments.length;
switch (G__26379) {
case 0:
return cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$0();

break;
case 1:
return cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$0 = (function (){
return cljs.core.async.promise_chan.call(null,null);
});

cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$1 = (function (xform){
return cljs.core.async.promise_chan.call(null,xform,null);
});

cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$2 = (function (xform,ex_handler){
return cljs.core.async.chan.call(null,cljs.core.async.impl.buffers.promise_buffer.call(null),xform,ex_handler);
});

cljs.core.async.promise_chan.cljs$lang$maxFixedArity = 2;

/**
 * Returns a channel that will close after msecs
 */
cljs.core.async.timeout = (function cljs$core$async$timeout(msecs){
return cljs.core.async.impl.timers.timeout.call(null,msecs);
});
/**
 * takes a val from port. Must be called inside a (go ...) block. Will
 *   return nil if closed. Will park if nothing is available.
 *   Returns true unless port is already closed
 */
cljs.core.async._LT__BANG_ = (function cljs$core$async$_LT__BANG_(port){
throw (new Error("<! used not in (go ...) block"));
});
/**
 * Asynchronously takes a val from port, passing to fn1. Will pass nil
 * if closed. If on-caller? (default true) is true, and value is
 * immediately available, will call fn1 on calling thread.
 * Returns nil.
 */
cljs.core.async.take_BANG_ = (function cljs$core$async$take_BANG_(var_args){
var G__26382 = arguments.length;
switch (G__26382) {
case 2:
return cljs.core.async.take_BANG_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.take_BANG_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.take_BANG_.cljs$core$IFn$_invoke$arity$2 = (function (port,fn1){
return cljs.core.async.take_BANG_.call(null,port,fn1,true);
});

cljs.core.async.take_BANG_.cljs$core$IFn$_invoke$arity$3 = (function (port,fn1,on_caller_QMARK_){
var ret = cljs.core.async.impl.protocols.take_BANG_.call(null,port,cljs.core.async.fn_handler.call(null,fn1));
if(cljs.core.truth_(ret)){
var val_26384 = cljs.core.deref.call(null,ret);
if(cljs.core.truth_(on_caller_QMARK_)){
fn1.call(null,val_26384);
} else {
cljs.core.async.impl.dispatch.run.call(null,((function (val_26384,ret){
return (function (){
return fn1.call(null,val_26384);
});})(val_26384,ret))
);
}
} else {
}

return null;
});

cljs.core.async.take_BANG_.cljs$lang$maxFixedArity = 3;

cljs.core.async.nop = (function cljs$core$async$nop(_){
return null;
});
cljs.core.async.fhnop = cljs.core.async.fn_handler.call(null,cljs.core.async.nop);
/**
 * puts a val into port. nil values are not allowed. Must be called
 *   inside a (go ...) block. Will park if no buffer space is available.
 *   Returns true unless port is already closed.
 */
cljs.core.async._GT__BANG_ = (function cljs$core$async$_GT__BANG_(port,val){
throw (new Error(">! used not in (go ...) block"));
});
/**
 * Asynchronously puts a val into port, calling fn1 (if supplied) when
 * complete. nil values are not allowed. Will throw if closed. If
 * on-caller? (default true) is true, and the put is immediately
 * accepted, will call fn1 on calling thread.  Returns nil.
 */
cljs.core.async.put_BANG_ = (function cljs$core$async$put_BANG_(var_args){
var G__26386 = arguments.length;
switch (G__26386) {
case 2:
return cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
case 4:
return cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2 = (function (port,val){
var temp__5718__auto__ = cljs.core.async.impl.protocols.put_BANG_.call(null,port,val,cljs.core.async.fhnop);
if(cljs.core.truth_(temp__5718__auto__)){
var ret = temp__5718__auto__;
return cljs.core.deref.call(null,ret);
} else {
return true;
}
});

cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$3 = (function (port,val,fn1){
return cljs.core.async.put_BANG_.call(null,port,val,fn1,true);
});

cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$4 = (function (port,val,fn1,on_caller_QMARK_){
var temp__5718__auto__ = cljs.core.async.impl.protocols.put_BANG_.call(null,port,val,cljs.core.async.fn_handler.call(null,fn1));
if(cljs.core.truth_(temp__5718__auto__)){
var retb = temp__5718__auto__;
var ret = cljs.core.deref.call(null,retb);
if(cljs.core.truth_(on_caller_QMARK_)){
fn1.call(null,ret);
} else {
cljs.core.async.impl.dispatch.run.call(null,((function (ret,retb,temp__5718__auto__){
return (function (){
return fn1.call(null,ret);
});})(ret,retb,temp__5718__auto__))
);
}

return ret;
} else {
return true;
}
});

cljs.core.async.put_BANG_.cljs$lang$maxFixedArity = 4;

cljs.core.async.close_BANG_ = (function cljs$core$async$close_BANG_(port){
return cljs.core.async.impl.protocols.close_BANG_.call(null,port);
});
cljs.core.async.random_array = (function cljs$core$async$random_array(n){
var a = (new Array(n));
var n__4607__auto___26388 = n;
var x_26389 = (0);
while(true){
if((x_26389 < n__4607__auto___26388)){
(a[x_26389] = x_26389);

var G__26390 = (x_26389 + (1));
x_26389 = G__26390;
continue;
} else {
}
break;
}

goog.array.shuffle(a);

return a;
});
cljs.core.async.alt_flag = (function cljs$core$async$alt_flag(){
var flag = cljs.core.atom.call(null,true);
if((typeof cljs !== 'undefined') && (typeof cljs.core !== 'undefined') && (typeof cljs.core.async !== 'undefined') && (typeof cljs.core.async.t_cljs$core$async26391 !== 'undefined')){
} else {

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Handler}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async26391 = (function (flag,meta26392){
this.flag = flag;
this.meta26392 = meta26392;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
cljs.core.async.t_cljs$core$async26391.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = ((function (flag){
return (function (_26393,meta26392__$1){
var self__ = this;
var _26393__$1 = this;
return (new cljs.core.async.t_cljs$core$async26391(self__.flag,meta26392__$1));
});})(flag))
;

cljs.core.async.t_cljs$core$async26391.prototype.cljs$core$IMeta$_meta$arity$1 = ((function (flag){
return (function (_26393){
var self__ = this;
var _26393__$1 = this;
return self__.meta26392;
});})(flag))
;

cljs.core.async.t_cljs$core$async26391.prototype.cljs$core$async$impl$protocols$Handler$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async26391.prototype.cljs$core$async$impl$protocols$Handler$active_QMARK_$arity$1 = ((function (flag){
return (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.deref.call(null,self__.flag);
});})(flag))
;

cljs.core.async.t_cljs$core$async26391.prototype.cljs$core$async$impl$protocols$Handler$blockable_QMARK_$arity$1 = ((function (flag){
return (function (_){
var self__ = this;
var ___$1 = this;
return true;
});})(flag))
;

cljs.core.async.t_cljs$core$async26391.prototype.cljs$core$async$impl$protocols$Handler$commit$arity$1 = ((function (flag){
return (function (_){
var self__ = this;
var ___$1 = this;
cljs.core.reset_BANG_.call(null,self__.flag,null);

return true;
});})(flag))
;

cljs.core.async.t_cljs$core$async26391.getBasis = ((function (flag){
return (function (){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"flag","flag",-1565787888,null),new cljs.core.Symbol(null,"meta26392","meta26392",701553770,null)], null);
});})(flag))
;

cljs.core.async.t_cljs$core$async26391.cljs$lang$type = true;

cljs.core.async.t_cljs$core$async26391.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async26391";

cljs.core.async.t_cljs$core$async26391.cljs$lang$ctorPrWriter = ((function (flag){
return (function (this__4374__auto__,writer__4375__auto__,opt__4376__auto__){
return cljs.core._write.call(null,writer__4375__auto__,"cljs.core.async/t_cljs$core$async26391");
});})(flag))
;

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async26391.
 */
cljs.core.async.__GT_t_cljs$core$async26391 = ((function (flag){
return (function cljs$core$async$alt_flag_$___GT_t_cljs$core$async26391(flag__$1,meta26392){
return (new cljs.core.async.t_cljs$core$async26391(flag__$1,meta26392));
});})(flag))
;

}

return (new cljs.core.async.t_cljs$core$async26391(flag,cljs.core.PersistentArrayMap.EMPTY));
});
cljs.core.async.alt_handler = (function cljs$core$async$alt_handler(flag,cb){
if((typeof cljs !== 'undefined') && (typeof cljs.core !== 'undefined') && (typeof cljs.core.async !== 'undefined') && (typeof cljs.core.async.t_cljs$core$async26394 !== 'undefined')){
} else {

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Handler}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async26394 = (function (flag,cb,meta26395){
this.flag = flag;
this.cb = cb;
this.meta26395 = meta26395;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
cljs.core.async.t_cljs$core$async26394.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_26396,meta26395__$1){
var self__ = this;
var _26396__$1 = this;
return (new cljs.core.async.t_cljs$core$async26394(self__.flag,self__.cb,meta26395__$1));
});

cljs.core.async.t_cljs$core$async26394.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_26396){
var self__ = this;
var _26396__$1 = this;
return self__.meta26395;
});

cljs.core.async.t_cljs$core$async26394.prototype.cljs$core$async$impl$protocols$Handler$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async26394.prototype.cljs$core$async$impl$protocols$Handler$active_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.active_QMARK_.call(null,self__.flag);
});

cljs.core.async.t_cljs$core$async26394.prototype.cljs$core$async$impl$protocols$Handler$blockable_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return true;
});

cljs.core.async.t_cljs$core$async26394.prototype.cljs$core$async$impl$protocols$Handler$commit$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
cljs.core.async.impl.protocols.commit.call(null,self__.flag);

return self__.cb;
});

cljs.core.async.t_cljs$core$async26394.getBasis = (function (){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"flag","flag",-1565787888,null),new cljs.core.Symbol(null,"cb","cb",-2064487928,null),new cljs.core.Symbol(null,"meta26395","meta26395",660699347,null)], null);
});

cljs.core.async.t_cljs$core$async26394.cljs$lang$type = true;

cljs.core.async.t_cljs$core$async26394.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async26394";

cljs.core.async.t_cljs$core$async26394.cljs$lang$ctorPrWriter = (function (this__4374__auto__,writer__4375__auto__,opt__4376__auto__){
return cljs.core._write.call(null,writer__4375__auto__,"cljs.core.async/t_cljs$core$async26394");
});

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async26394.
 */
cljs.core.async.__GT_t_cljs$core$async26394 = (function cljs$core$async$alt_handler_$___GT_t_cljs$core$async26394(flag__$1,cb__$1,meta26395){
return (new cljs.core.async.t_cljs$core$async26394(flag__$1,cb__$1,meta26395));
});

}

return (new cljs.core.async.t_cljs$core$async26394(flag,cb,cljs.core.PersistentArrayMap.EMPTY));
});
/**
 * returns derefable [val port] if immediate, nil if enqueued
 */
cljs.core.async.do_alts = (function cljs$core$async$do_alts(fret,ports,opts){
if((cljs.core.count.call(null,ports) > (0))){
} else {
throw (new Error(["Assert failed: ","alts must have at least one channel operation","\n","(pos? (count ports))"].join('')));
}

var flag = cljs.core.async.alt_flag.call(null);
var n = cljs.core.count.call(null,ports);
var idxs = cljs.core.async.random_array.call(null,n);
var priority = new cljs.core.Keyword(null,"priority","priority",1431093715).cljs$core$IFn$_invoke$arity$1(opts);
var ret = (function (){var i = (0);
while(true){
if((i < n)){
var idx = (cljs.core.truth_(priority)?i:(idxs[i]));
var port = cljs.core.nth.call(null,ports,idx);
var wport = ((cljs.core.vector_QMARK_.call(null,port))?port.call(null,(0)):null);
var vbox = (cljs.core.truth_(wport)?(function (){var val = port.call(null,(1));
return cljs.core.async.impl.protocols.put_BANG_.call(null,wport,val,cljs.core.async.alt_handler.call(null,flag,((function (i,val,idx,port,wport,flag,n,idxs,priority){
return (function (p1__26397_SHARP_){
return fret.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [p1__26397_SHARP_,wport], null));
});})(i,val,idx,port,wport,flag,n,idxs,priority))
));
})():cljs.core.async.impl.protocols.take_BANG_.call(null,port,cljs.core.async.alt_handler.call(null,flag,((function (i,idx,port,wport,flag,n,idxs,priority){
return (function (p1__26398_SHARP_){
return fret.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [p1__26398_SHARP_,port], null));
});})(i,idx,port,wport,flag,n,idxs,priority))
)));
if(cljs.core.truth_(vbox)){
return cljs.core.async.impl.channels.box.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.deref.call(null,vbox),(function (){var or__4131__auto__ = wport;
if(cljs.core.truth_(or__4131__auto__)){
return or__4131__auto__;
} else {
return port;
}
})()], null));
} else {
var G__26399 = (i + (1));
i = G__26399;
continue;
}
} else {
return null;
}
break;
}
})();
var or__4131__auto__ = ret;
if(cljs.core.truth_(or__4131__auto__)){
return or__4131__auto__;
} else {
if(cljs.core.contains_QMARK_.call(null,opts,new cljs.core.Keyword(null,"default","default",-1987822328))){
var temp__5720__auto__ = (function (){var and__4120__auto__ = cljs.core.async.impl.protocols.active_QMARK_.call(null,flag);
if(cljs.core.truth_(and__4120__auto__)){
return cljs.core.async.impl.protocols.commit.call(null,flag);
} else {
return and__4120__auto__;
}
})();
if(cljs.core.truth_(temp__5720__auto__)){
var got = temp__5720__auto__;
return cljs.core.async.impl.channels.box.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"default","default",-1987822328).cljs$core$IFn$_invoke$arity$1(opts),new cljs.core.Keyword(null,"default","default",-1987822328)], null));
} else {
return null;
}
} else {
return null;
}
}
});
/**
 * Completes at most one of several channel operations. Must be called
 * inside a (go ...) block. ports is a vector of channel endpoints,
 * which can be either a channel to take from or a vector of
 *   [channel-to-put-to val-to-put], in any combination. Takes will be
 *   made as if by <!, and puts will be made as if by >!. Unless
 *   the :priority option is true, if more than one port operation is
 *   ready a non-deterministic choice will be made. If no operation is
 *   ready and a :default value is supplied, [default-val :default] will
 *   be returned, otherwise alts! will park until the first operation to
 *   become ready completes. Returns [val port] of the completed
 *   operation, where val is the value taken for takes, and a
 *   boolean (true unless already closed, as per put!) for puts.
 * 
 *   opts are passed as :key val ... Supported options:
 * 
 *   :default val - the value to use if none of the operations are immediately ready
 *   :priority true - (default nil) when true, the operations will be tried in order.
 * 
 *   Note: there is no guarantee that the port exps or val exprs will be
 *   used, nor in what order should they be, so they should not be
 *   depended upon for side effects.
 */
cljs.core.async.alts_BANG_ = (function cljs$core$async$alts_BANG_(var_args){
var args__4736__auto__ = [];
var len__4730__auto___26405 = arguments.length;
var i__4731__auto___26406 = (0);
while(true){
if((i__4731__auto___26406 < len__4730__auto___26405)){
args__4736__auto__.push((arguments[i__4731__auto___26406]));

var G__26407 = (i__4731__auto___26406 + (1));
i__4731__auto___26406 = G__26407;
continue;
} else {
}
break;
}

var argseq__4737__auto__ = ((((1) < args__4736__auto__.length))?(new cljs.core.IndexedSeq(args__4736__auto__.slice((1)),(0),null)):null);
return cljs.core.async.alts_BANG_.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),argseq__4737__auto__);
});

cljs.core.async.alts_BANG_.cljs$core$IFn$_invoke$arity$variadic = (function (ports,p__26402){
var map__26403 = p__26402;
var map__26403__$1 = (((((!((map__26403 == null))))?(((((map__26403.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__26403.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__26403):map__26403);
var opts = map__26403__$1;
throw (new Error("alts! used not in (go ...) block"));
});

cljs.core.async.alts_BANG_.cljs$lang$maxFixedArity = (1);

/** @this {Function} */
cljs.core.async.alts_BANG_.cljs$lang$applyTo = (function (seq26400){
var G__26401 = cljs.core.first.call(null,seq26400);
var seq26400__$1 = cljs.core.next.call(null,seq26400);
var self__4717__auto__ = this;
return self__4717__auto__.cljs$core$IFn$_invoke$arity$variadic(G__26401,seq26400__$1);
});

/**
 * Puts a val into port if it's possible to do so immediately.
 *   nil values are not allowed. Never blocks. Returns true if offer succeeds.
 */
cljs.core.async.offer_BANG_ = (function cljs$core$async$offer_BANG_(port,val){
var ret = cljs.core.async.impl.protocols.put_BANG_.call(null,port,val,cljs.core.async.fn_handler.call(null,cljs.core.async.nop,false));
if(cljs.core.truth_(ret)){
return cljs.core.deref.call(null,ret);
} else {
return null;
}
});
/**
 * Takes a val from port if it's possible to do so immediately.
 *   Never blocks. Returns value if successful, nil otherwise.
 */
cljs.core.async.poll_BANG_ = (function cljs$core$async$poll_BANG_(port){
var ret = cljs.core.async.impl.protocols.take_BANG_.call(null,port,cljs.core.async.fn_handler.call(null,cljs.core.async.nop,false));
if(cljs.core.truth_(ret)){
return cljs.core.deref.call(null,ret);
} else {
return null;
}
});
/**
 * Takes elements from the from channel and supplies them to the to
 * channel. By default, the to channel will be closed when the from
 * channel closes, but can be determined by the close?  parameter. Will
 * stop consuming the from channel if the to channel closes
 */
cljs.core.async.pipe = (function cljs$core$async$pipe(var_args){
var G__26409 = arguments.length;
switch (G__26409) {
case 2:
return cljs.core.async.pipe.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.pipe.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.pipe.cljs$core$IFn$_invoke$arity$2 = (function (from,to){
return cljs.core.async.pipe.call(null,from,to,true);
});

cljs.core.async.pipe.cljs$core$IFn$_invoke$arity$3 = (function (from,to,close_QMARK_){
var c__26309__auto___26455 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__26309__auto___26455){
return (function (){
var f__26310__auto__ = (function (){var switch__26214__auto__ = ((function (c__26309__auto___26455){
return (function (state_26433){
var state_val_26434 = (state_26433[(1)]);
if((state_val_26434 === (7))){
var inst_26429 = (state_26433[(2)]);
var state_26433__$1 = state_26433;
var statearr_26435_26456 = state_26433__$1;
(statearr_26435_26456[(2)] = inst_26429);

(statearr_26435_26456[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_26434 === (1))){
var state_26433__$1 = state_26433;
var statearr_26436_26457 = state_26433__$1;
(statearr_26436_26457[(2)] = null);

(statearr_26436_26457[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_26434 === (4))){
var inst_26412 = (state_26433[(7)]);
var inst_26412__$1 = (state_26433[(2)]);
var inst_26413 = (inst_26412__$1 == null);
var state_26433__$1 = (function (){var statearr_26437 = state_26433;
(statearr_26437[(7)] = inst_26412__$1);

return statearr_26437;
})();
if(cljs.core.truth_(inst_26413)){
var statearr_26438_26458 = state_26433__$1;
(statearr_26438_26458[(1)] = (5));

} else {
var statearr_26439_26459 = state_26433__$1;
(statearr_26439_26459[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_26434 === (13))){
var state_26433__$1 = state_26433;
var statearr_26440_26460 = state_26433__$1;
(statearr_26440_26460[(2)] = null);

(statearr_26440_26460[(1)] = (14));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_26434 === (6))){
var inst_26412 = (state_26433[(7)]);
var state_26433__$1 = state_26433;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_26433__$1,(11),to,inst_26412);
} else {
if((state_val_26434 === (3))){
var inst_26431 = (state_26433[(2)]);
var state_26433__$1 = state_26433;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_26433__$1,inst_26431);
} else {
if((state_val_26434 === (12))){
var state_26433__$1 = state_26433;
var statearr_26441_26461 = state_26433__$1;
(statearr_26441_26461[(2)] = null);

(statearr_26441_26461[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_26434 === (2))){
var state_26433__$1 = state_26433;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_26433__$1,(4),from);
} else {
if((state_val_26434 === (11))){
var inst_26422 = (state_26433[(2)]);
var state_26433__$1 = state_26433;
if(cljs.core.truth_(inst_26422)){
var statearr_26442_26462 = state_26433__$1;
(statearr_26442_26462[(1)] = (12));

} else {
var statearr_26443_26463 = state_26433__$1;
(statearr_26443_26463[(1)] = (13));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_26434 === (9))){
var state_26433__$1 = state_26433;
var statearr_26444_26464 = state_26433__$1;
(statearr_26444_26464[(2)] = null);

(statearr_26444_26464[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_26434 === (5))){
var state_26433__$1 = state_26433;
if(cljs.core.truth_(close_QMARK_)){
var statearr_26445_26465 = state_26433__$1;
(statearr_26445_26465[(1)] = (8));

} else {
var statearr_26446_26466 = state_26433__$1;
(statearr_26446_26466[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_26434 === (14))){
var inst_26427 = (state_26433[(2)]);
var state_26433__$1 = state_26433;
var statearr_26447_26467 = state_26433__$1;
(statearr_26447_26467[(2)] = inst_26427);

(statearr_26447_26467[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_26434 === (10))){
var inst_26419 = (state_26433[(2)]);
var state_26433__$1 = state_26433;
var statearr_26448_26468 = state_26433__$1;
(statearr_26448_26468[(2)] = inst_26419);

(statearr_26448_26468[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_26434 === (8))){
var inst_26416 = cljs.core.async.close_BANG_.call(null,to);
var state_26433__$1 = state_26433;
var statearr_26449_26469 = state_26433__$1;
(statearr_26449_26469[(2)] = inst_26416);

(statearr_26449_26469[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__26309__auto___26455))
;
return ((function (switch__26214__auto__,c__26309__auto___26455){
return (function() {
var cljs$core$async$state_machine__26215__auto__ = null;
var cljs$core$async$state_machine__26215__auto____0 = (function (){
var statearr_26450 = [null,null,null,null,null,null,null,null];
(statearr_26450[(0)] = cljs$core$async$state_machine__26215__auto__);

(statearr_26450[(1)] = (1));

return statearr_26450;
});
var cljs$core$async$state_machine__26215__auto____1 = (function (state_26433){
while(true){
var ret_value__26216__auto__ = (function (){try{while(true){
var result__26217__auto__ = switch__26214__auto__.call(null,state_26433);
if(cljs.core.keyword_identical_QMARK_.call(null,result__26217__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__26217__auto__;
}
break;
}
}catch (e26451){if((e26451 instanceof Object)){
var ex__26218__auto__ = e26451;
var statearr_26452_26470 = state_26433;
(statearr_26452_26470[(5)] = ex__26218__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_26433);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e26451;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__26216__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__26471 = state_26433;
state_26433 = G__26471;
continue;
} else {
return ret_value__26216__auto__;
}
break;
}
});
cljs$core$async$state_machine__26215__auto__ = function(state_26433){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__26215__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__26215__auto____1.call(this,state_26433);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__26215__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__26215__auto____0;
cljs$core$async$state_machine__26215__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__26215__auto____1;
return cljs$core$async$state_machine__26215__auto__;
})()
;})(switch__26214__auto__,c__26309__auto___26455))
})();
var state__26311__auto__ = (function (){var statearr_26453 = f__26310__auto__.call(null);
(statearr_26453[(6)] = c__26309__auto___26455);

return statearr_26453;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__26311__auto__);
});})(c__26309__auto___26455))
);


return to;
});

cljs.core.async.pipe.cljs$lang$maxFixedArity = 3;

cljs.core.async.pipeline_STAR_ = (function cljs$core$async$pipeline_STAR_(n,to,xf,from,close_QMARK_,ex_handler,type){
if((n > (0))){
} else {
throw (new Error("Assert failed: (pos? n)"));
}

var jobs = cljs.core.async.chan.call(null,n);
var results = cljs.core.async.chan.call(null,n);
var process = ((function (jobs,results){
return (function (p__26472){
var vec__26473 = p__26472;
var v = cljs.core.nth.call(null,vec__26473,(0),null);
var p = cljs.core.nth.call(null,vec__26473,(1),null);
var job = vec__26473;
if((job == null)){
cljs.core.async.close_BANG_.call(null,results);

return null;
} else {
var res = cljs.core.async.chan.call(null,(1),xf,ex_handler);
var c__26309__auto___26644 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__26309__auto___26644,res,vec__26473,v,p,job,jobs,results){
return (function (){
var f__26310__auto__ = (function (){var switch__26214__auto__ = ((function (c__26309__auto___26644,res,vec__26473,v,p,job,jobs,results){
return (function (state_26480){
var state_val_26481 = (state_26480[(1)]);
if((state_val_26481 === (1))){
var state_26480__$1 = state_26480;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_26480__$1,(2),res,v);
} else {
if((state_val_26481 === (2))){
var inst_26477 = (state_26480[(2)]);
var inst_26478 = cljs.core.async.close_BANG_.call(null,res);
var state_26480__$1 = (function (){var statearr_26482 = state_26480;
(statearr_26482[(7)] = inst_26477);

return statearr_26482;
})();
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_26480__$1,inst_26478);
} else {
return null;
}
}
});})(c__26309__auto___26644,res,vec__26473,v,p,job,jobs,results))
;
return ((function (switch__26214__auto__,c__26309__auto___26644,res,vec__26473,v,p,job,jobs,results){
return (function() {
var cljs$core$async$pipeline_STAR__$_state_machine__26215__auto__ = null;
var cljs$core$async$pipeline_STAR__$_state_machine__26215__auto____0 = (function (){
var statearr_26483 = [null,null,null,null,null,null,null,null];
(statearr_26483[(0)] = cljs$core$async$pipeline_STAR__$_state_machine__26215__auto__);

(statearr_26483[(1)] = (1));

return statearr_26483;
});
var cljs$core$async$pipeline_STAR__$_state_machine__26215__auto____1 = (function (state_26480){
while(true){
var ret_value__26216__auto__ = (function (){try{while(true){
var result__26217__auto__ = switch__26214__auto__.call(null,state_26480);
if(cljs.core.keyword_identical_QMARK_.call(null,result__26217__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__26217__auto__;
}
break;
}
}catch (e26484){if((e26484 instanceof Object)){
var ex__26218__auto__ = e26484;
var statearr_26485_26645 = state_26480;
(statearr_26485_26645[(5)] = ex__26218__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_26480);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e26484;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__26216__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__26646 = state_26480;
state_26480 = G__26646;
continue;
} else {
return ret_value__26216__auto__;
}
break;
}
});
cljs$core$async$pipeline_STAR__$_state_machine__26215__auto__ = function(state_26480){
switch(arguments.length){
case 0:
return cljs$core$async$pipeline_STAR__$_state_machine__26215__auto____0.call(this);
case 1:
return cljs$core$async$pipeline_STAR__$_state_machine__26215__auto____1.call(this,state_26480);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$pipeline_STAR__$_state_machine__26215__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$pipeline_STAR__$_state_machine__26215__auto____0;
cljs$core$async$pipeline_STAR__$_state_machine__26215__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$pipeline_STAR__$_state_machine__26215__auto____1;
return cljs$core$async$pipeline_STAR__$_state_machine__26215__auto__;
})()
;})(switch__26214__auto__,c__26309__auto___26644,res,vec__26473,v,p,job,jobs,results))
})();
var state__26311__auto__ = (function (){var statearr_26486 = f__26310__auto__.call(null);
(statearr_26486[(6)] = c__26309__auto___26644);

return statearr_26486;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__26311__auto__);
});})(c__26309__auto___26644,res,vec__26473,v,p,job,jobs,results))
);


cljs.core.async.put_BANG_.call(null,p,res);

return true;
}
});})(jobs,results))
;
var async = ((function (jobs,results,process){
return (function (p__26487){
var vec__26488 = p__26487;
var v = cljs.core.nth.call(null,vec__26488,(0),null);
var p = cljs.core.nth.call(null,vec__26488,(1),null);
var job = vec__26488;
if((job == null)){
cljs.core.async.close_BANG_.call(null,results);

return null;
} else {
var res = cljs.core.async.chan.call(null,(1));
xf.call(null,v,res);

cljs.core.async.put_BANG_.call(null,p,res);

return true;
}
});})(jobs,results,process))
;
var n__4607__auto___26647 = n;
var __26648 = (0);
while(true){
if((__26648 < n__4607__auto___26647)){
var G__26491_26649 = type;
var G__26491_26650__$1 = (((G__26491_26649 instanceof cljs.core.Keyword))?G__26491_26649.fqn:null);
switch (G__26491_26650__$1) {
case "compute":
var c__26309__auto___26652 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (__26648,c__26309__auto___26652,G__26491_26649,G__26491_26650__$1,n__4607__auto___26647,jobs,results,process,async){
return (function (){
var f__26310__auto__ = (function (){var switch__26214__auto__ = ((function (__26648,c__26309__auto___26652,G__26491_26649,G__26491_26650__$1,n__4607__auto___26647,jobs,results,process,async){
return (function (state_26504){
var state_val_26505 = (state_26504[(1)]);
if((state_val_26505 === (1))){
var state_26504__$1 = state_26504;
var statearr_26506_26653 = state_26504__$1;
(statearr_26506_26653[(2)] = null);

(statearr_26506_26653[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_26505 === (2))){
var state_26504__$1 = state_26504;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_26504__$1,(4),jobs);
} else {
if((state_val_26505 === (3))){
var inst_26502 = (state_26504[(2)]);
var state_26504__$1 = state_26504;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_26504__$1,inst_26502);
} else {
if((state_val_26505 === (4))){
var inst_26494 = (state_26504[(2)]);
var inst_26495 = process.call(null,inst_26494);
var state_26504__$1 = state_26504;
if(cljs.core.truth_(inst_26495)){
var statearr_26507_26654 = state_26504__$1;
(statearr_26507_26654[(1)] = (5));

} else {
var statearr_26508_26655 = state_26504__$1;
(statearr_26508_26655[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_26505 === (5))){
var state_26504__$1 = state_26504;
var statearr_26509_26656 = state_26504__$1;
(statearr_26509_26656[(2)] = null);

(statearr_26509_26656[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_26505 === (6))){
var state_26504__$1 = state_26504;
var statearr_26510_26657 = state_26504__$1;
(statearr_26510_26657[(2)] = null);

(statearr_26510_26657[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_26505 === (7))){
var inst_26500 = (state_26504[(2)]);
var state_26504__$1 = state_26504;
var statearr_26511_26658 = state_26504__$1;
(statearr_26511_26658[(2)] = inst_26500);

(statearr_26511_26658[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
});})(__26648,c__26309__auto___26652,G__26491_26649,G__26491_26650__$1,n__4607__auto___26647,jobs,results,process,async))
;
return ((function (__26648,switch__26214__auto__,c__26309__auto___26652,G__26491_26649,G__26491_26650__$1,n__4607__auto___26647,jobs,results,process,async){
return (function() {
var cljs$core$async$pipeline_STAR__$_state_machine__26215__auto__ = null;
var cljs$core$async$pipeline_STAR__$_state_machine__26215__auto____0 = (function (){
var statearr_26512 = [null,null,null,null,null,null,null];
(statearr_26512[(0)] = cljs$core$async$pipeline_STAR__$_state_machine__26215__auto__);

(statearr_26512[(1)] = (1));

return statearr_26512;
});
var cljs$core$async$pipeline_STAR__$_state_machine__26215__auto____1 = (function (state_26504){
while(true){
var ret_value__26216__auto__ = (function (){try{while(true){
var result__26217__auto__ = switch__26214__auto__.call(null,state_26504);
if(cljs.core.keyword_identical_QMARK_.call(null,result__26217__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__26217__auto__;
}
break;
}
}catch (e26513){if((e26513 instanceof Object)){
var ex__26218__auto__ = e26513;
var statearr_26514_26659 = state_26504;
(statearr_26514_26659[(5)] = ex__26218__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_26504);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e26513;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__26216__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__26660 = state_26504;
state_26504 = G__26660;
continue;
} else {
return ret_value__26216__auto__;
}
break;
}
});
cljs$core$async$pipeline_STAR__$_state_machine__26215__auto__ = function(state_26504){
switch(arguments.length){
case 0:
return cljs$core$async$pipeline_STAR__$_state_machine__26215__auto____0.call(this);
case 1:
return cljs$core$async$pipeline_STAR__$_state_machine__26215__auto____1.call(this,state_26504);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$pipeline_STAR__$_state_machine__26215__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$pipeline_STAR__$_state_machine__26215__auto____0;
cljs$core$async$pipeline_STAR__$_state_machine__26215__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$pipeline_STAR__$_state_machine__26215__auto____1;
return cljs$core$async$pipeline_STAR__$_state_machine__26215__auto__;
})()
;})(__26648,switch__26214__auto__,c__26309__auto___26652,G__26491_26649,G__26491_26650__$1,n__4607__auto___26647,jobs,results,process,async))
})();
var state__26311__auto__ = (function (){var statearr_26515 = f__26310__auto__.call(null);
(statearr_26515[(6)] = c__26309__auto___26652);

return statearr_26515;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__26311__auto__);
});})(__26648,c__26309__auto___26652,G__26491_26649,G__26491_26650__$1,n__4607__auto___26647,jobs,results,process,async))
);


break;
case "async":
var c__26309__auto___26661 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (__26648,c__26309__auto___26661,G__26491_26649,G__26491_26650__$1,n__4607__auto___26647,jobs,results,process,async){
return (function (){
var f__26310__auto__ = (function (){var switch__26214__auto__ = ((function (__26648,c__26309__auto___26661,G__26491_26649,G__26491_26650__$1,n__4607__auto___26647,jobs,results,process,async){
return (function (state_26528){
var state_val_26529 = (state_26528[(1)]);
if((state_val_26529 === (1))){
var state_26528__$1 = state_26528;
var statearr_26530_26662 = state_26528__$1;
(statearr_26530_26662[(2)] = null);

(statearr_26530_26662[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_26529 === (2))){
var state_26528__$1 = state_26528;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_26528__$1,(4),jobs);
} else {
if((state_val_26529 === (3))){
var inst_26526 = (state_26528[(2)]);
var state_26528__$1 = state_26528;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_26528__$1,inst_26526);
} else {
if((state_val_26529 === (4))){
var inst_26518 = (state_26528[(2)]);
var inst_26519 = async.call(null,inst_26518);
var state_26528__$1 = state_26528;
if(cljs.core.truth_(inst_26519)){
var statearr_26531_26663 = state_26528__$1;
(statearr_26531_26663[(1)] = (5));

} else {
var statearr_26532_26664 = state_26528__$1;
(statearr_26532_26664[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_26529 === (5))){
var state_26528__$1 = state_26528;
var statearr_26533_26665 = state_26528__$1;
(statearr_26533_26665[(2)] = null);

(statearr_26533_26665[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_26529 === (6))){
var state_26528__$1 = state_26528;
var statearr_26534_26666 = state_26528__$1;
(statearr_26534_26666[(2)] = null);

(statearr_26534_26666[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_26529 === (7))){
var inst_26524 = (state_26528[(2)]);
var state_26528__$1 = state_26528;
var statearr_26535_26667 = state_26528__$1;
(statearr_26535_26667[(2)] = inst_26524);

(statearr_26535_26667[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
});})(__26648,c__26309__auto___26661,G__26491_26649,G__26491_26650__$1,n__4607__auto___26647,jobs,results,process,async))
;
return ((function (__26648,switch__26214__auto__,c__26309__auto___26661,G__26491_26649,G__26491_26650__$1,n__4607__auto___26647,jobs,results,process,async){
return (function() {
var cljs$core$async$pipeline_STAR__$_state_machine__26215__auto__ = null;
var cljs$core$async$pipeline_STAR__$_state_machine__26215__auto____0 = (function (){
var statearr_26536 = [null,null,null,null,null,null,null];
(statearr_26536[(0)] = cljs$core$async$pipeline_STAR__$_state_machine__26215__auto__);

(statearr_26536[(1)] = (1));

return statearr_26536;
});
var cljs$core$async$pipeline_STAR__$_state_machine__26215__auto____1 = (function (state_26528){
while(true){
var ret_value__26216__auto__ = (function (){try{while(true){
var result__26217__auto__ = switch__26214__auto__.call(null,state_26528);
if(cljs.core.keyword_identical_QMARK_.call(null,result__26217__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__26217__auto__;
}
break;
}
}catch (e26537){if((e26537 instanceof Object)){
var ex__26218__auto__ = e26537;
var statearr_26538_26668 = state_26528;
(statearr_26538_26668[(5)] = ex__26218__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_26528);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e26537;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__26216__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__26669 = state_26528;
state_26528 = G__26669;
continue;
} else {
return ret_value__26216__auto__;
}
break;
}
});
cljs$core$async$pipeline_STAR__$_state_machine__26215__auto__ = function(state_26528){
switch(arguments.length){
case 0:
return cljs$core$async$pipeline_STAR__$_state_machine__26215__auto____0.call(this);
case 1:
return cljs$core$async$pipeline_STAR__$_state_machine__26215__auto____1.call(this,state_26528);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$pipeline_STAR__$_state_machine__26215__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$pipeline_STAR__$_state_machine__26215__auto____0;
cljs$core$async$pipeline_STAR__$_state_machine__26215__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$pipeline_STAR__$_state_machine__26215__auto____1;
return cljs$core$async$pipeline_STAR__$_state_machine__26215__auto__;
})()
;})(__26648,switch__26214__auto__,c__26309__auto___26661,G__26491_26649,G__26491_26650__$1,n__4607__auto___26647,jobs,results,process,async))
})();
var state__26311__auto__ = (function (){var statearr_26539 = f__26310__auto__.call(null);
(statearr_26539[(6)] = c__26309__auto___26661);

return statearr_26539;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__26311__auto__);
});})(__26648,c__26309__auto___26661,G__26491_26649,G__26491_26650__$1,n__4607__auto___26647,jobs,results,process,async))
);


break;
default:
throw (new Error(["No matching clause: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(G__26491_26650__$1)].join('')));

}

var G__26670 = (__26648 + (1));
__26648 = G__26670;
continue;
} else {
}
break;
}

var c__26309__auto___26671 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__26309__auto___26671,jobs,results,process,async){
return (function (){
var f__26310__auto__ = (function (){var switch__26214__auto__ = ((function (c__26309__auto___26671,jobs,results,process,async){
return (function (state_26561){
var state_val_26562 = (state_26561[(1)]);
if((state_val_26562 === (7))){
var inst_26557 = (state_26561[(2)]);
var state_26561__$1 = state_26561;
var statearr_26563_26672 = state_26561__$1;
(statearr_26563_26672[(2)] = inst_26557);

(statearr_26563_26672[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_26562 === (1))){
var state_26561__$1 = state_26561;
var statearr_26564_26673 = state_26561__$1;
(statearr_26564_26673[(2)] = null);

(statearr_26564_26673[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_26562 === (4))){
var inst_26542 = (state_26561[(7)]);
var inst_26542__$1 = (state_26561[(2)]);
var inst_26543 = (inst_26542__$1 == null);
var state_26561__$1 = (function (){var statearr_26565 = state_26561;
(statearr_26565[(7)] = inst_26542__$1);

return statearr_26565;
})();
if(cljs.core.truth_(inst_26543)){
var statearr_26566_26674 = state_26561__$1;
(statearr_26566_26674[(1)] = (5));

} else {
var statearr_26567_26675 = state_26561__$1;
(statearr_26567_26675[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_26562 === (6))){
var inst_26542 = (state_26561[(7)]);
var inst_26547 = (state_26561[(8)]);
var inst_26547__$1 = cljs.core.async.chan.call(null,(1));
var inst_26548 = cljs.core.PersistentVector.EMPTY_NODE;
var inst_26549 = [inst_26542,inst_26547__$1];
var inst_26550 = (new cljs.core.PersistentVector(null,2,(5),inst_26548,inst_26549,null));
var state_26561__$1 = (function (){var statearr_26568 = state_26561;
(statearr_26568[(8)] = inst_26547__$1);

return statearr_26568;
})();
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_26561__$1,(8),jobs,inst_26550);
} else {
if((state_val_26562 === (3))){
var inst_26559 = (state_26561[(2)]);
var state_26561__$1 = state_26561;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_26561__$1,inst_26559);
} else {
if((state_val_26562 === (2))){
var state_26561__$1 = state_26561;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_26561__$1,(4),from);
} else {
if((state_val_26562 === (9))){
var inst_26554 = (state_26561[(2)]);
var state_26561__$1 = (function (){var statearr_26569 = state_26561;
(statearr_26569[(9)] = inst_26554);

return statearr_26569;
})();
var statearr_26570_26676 = state_26561__$1;
(statearr_26570_26676[(2)] = null);

(statearr_26570_26676[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_26562 === (5))){
var inst_26545 = cljs.core.async.close_BANG_.call(null,jobs);
var state_26561__$1 = state_26561;
var statearr_26571_26677 = state_26561__$1;
(statearr_26571_26677[(2)] = inst_26545);

(statearr_26571_26677[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_26562 === (8))){
var inst_26547 = (state_26561[(8)]);
var inst_26552 = (state_26561[(2)]);
var state_26561__$1 = (function (){var statearr_26572 = state_26561;
(statearr_26572[(10)] = inst_26552);

return statearr_26572;
})();
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_26561__$1,(9),results,inst_26547);
} else {
return null;
}
}
}
}
}
}
}
}
}
});})(c__26309__auto___26671,jobs,results,process,async))
;
return ((function (switch__26214__auto__,c__26309__auto___26671,jobs,results,process,async){
return (function() {
var cljs$core$async$pipeline_STAR__$_state_machine__26215__auto__ = null;
var cljs$core$async$pipeline_STAR__$_state_machine__26215__auto____0 = (function (){
var statearr_26573 = [null,null,null,null,null,null,null,null,null,null,null];
(statearr_26573[(0)] = cljs$core$async$pipeline_STAR__$_state_machine__26215__auto__);

(statearr_26573[(1)] = (1));

return statearr_26573;
});
var cljs$core$async$pipeline_STAR__$_state_machine__26215__auto____1 = (function (state_26561){
while(true){
var ret_value__26216__auto__ = (function (){try{while(true){
var result__26217__auto__ = switch__26214__auto__.call(null,state_26561);
if(cljs.core.keyword_identical_QMARK_.call(null,result__26217__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__26217__auto__;
}
break;
}
}catch (e26574){if((e26574 instanceof Object)){
var ex__26218__auto__ = e26574;
var statearr_26575_26678 = state_26561;
(statearr_26575_26678[(5)] = ex__26218__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_26561);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e26574;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__26216__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__26679 = state_26561;
state_26561 = G__26679;
continue;
} else {
return ret_value__26216__auto__;
}
break;
}
});
cljs$core$async$pipeline_STAR__$_state_machine__26215__auto__ = function(state_26561){
switch(arguments.length){
case 0:
return cljs$core$async$pipeline_STAR__$_state_machine__26215__auto____0.call(this);
case 1:
return cljs$core$async$pipeline_STAR__$_state_machine__26215__auto____1.call(this,state_26561);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$pipeline_STAR__$_state_machine__26215__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$pipeline_STAR__$_state_machine__26215__auto____0;
cljs$core$async$pipeline_STAR__$_state_machine__26215__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$pipeline_STAR__$_state_machine__26215__auto____1;
return cljs$core$async$pipeline_STAR__$_state_machine__26215__auto__;
})()
;})(switch__26214__auto__,c__26309__auto___26671,jobs,results,process,async))
})();
var state__26311__auto__ = (function (){var statearr_26576 = f__26310__auto__.call(null);
(statearr_26576[(6)] = c__26309__auto___26671);

return statearr_26576;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__26311__auto__);
});})(c__26309__auto___26671,jobs,results,process,async))
);


var c__26309__auto__ = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__26309__auto__,jobs,results,process,async){
return (function (){
var f__26310__auto__ = (function (){var switch__26214__auto__ = ((function (c__26309__auto__,jobs,results,process,async){
return (function (state_26614){
var state_val_26615 = (state_26614[(1)]);
if((state_val_26615 === (7))){
var inst_26610 = (state_26614[(2)]);
var state_26614__$1 = state_26614;
var statearr_26616_26680 = state_26614__$1;
(statearr_26616_26680[(2)] = inst_26610);

(statearr_26616_26680[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_26615 === (20))){
var state_26614__$1 = state_26614;
var statearr_26617_26681 = state_26614__$1;
(statearr_26617_26681[(2)] = null);

(statearr_26617_26681[(1)] = (21));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_26615 === (1))){
var state_26614__$1 = state_26614;
var statearr_26618_26682 = state_26614__$1;
(statearr_26618_26682[(2)] = null);

(statearr_26618_26682[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_26615 === (4))){
var inst_26579 = (state_26614[(7)]);
var inst_26579__$1 = (state_26614[(2)]);
var inst_26580 = (inst_26579__$1 == null);
var state_26614__$1 = (function (){var statearr_26619 = state_26614;
(statearr_26619[(7)] = inst_26579__$1);

return statearr_26619;
})();
if(cljs.core.truth_(inst_26580)){
var statearr_26620_26683 = state_26614__$1;
(statearr_26620_26683[(1)] = (5));

} else {
var statearr_26621_26684 = state_26614__$1;
(statearr_26621_26684[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_26615 === (15))){
var inst_26592 = (state_26614[(8)]);
var state_26614__$1 = state_26614;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_26614__$1,(18),to,inst_26592);
} else {
if((state_val_26615 === (21))){
var inst_26605 = (state_26614[(2)]);
var state_26614__$1 = state_26614;
var statearr_26622_26685 = state_26614__$1;
(statearr_26622_26685[(2)] = inst_26605);

(statearr_26622_26685[(1)] = (13));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_26615 === (13))){
var inst_26607 = (state_26614[(2)]);
var state_26614__$1 = (function (){var statearr_26623 = state_26614;
(statearr_26623[(9)] = inst_26607);

return statearr_26623;
})();
var statearr_26624_26686 = state_26614__$1;
(statearr_26624_26686[(2)] = null);

(statearr_26624_26686[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_26615 === (6))){
var inst_26579 = (state_26614[(7)]);
var state_26614__$1 = state_26614;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_26614__$1,(11),inst_26579);
} else {
if((state_val_26615 === (17))){
var inst_26600 = (state_26614[(2)]);
var state_26614__$1 = state_26614;
if(cljs.core.truth_(inst_26600)){
var statearr_26625_26687 = state_26614__$1;
(statearr_26625_26687[(1)] = (19));

} else {
var statearr_26626_26688 = state_26614__$1;
(statearr_26626_26688[(1)] = (20));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_26615 === (3))){
var inst_26612 = (state_26614[(2)]);
var state_26614__$1 = state_26614;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_26614__$1,inst_26612);
} else {
if((state_val_26615 === (12))){
var inst_26589 = (state_26614[(10)]);
var state_26614__$1 = state_26614;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_26614__$1,(14),inst_26589);
} else {
if((state_val_26615 === (2))){
var state_26614__$1 = state_26614;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_26614__$1,(4),results);
} else {
if((state_val_26615 === (19))){
var state_26614__$1 = state_26614;
var statearr_26627_26689 = state_26614__$1;
(statearr_26627_26689[(2)] = null);

(statearr_26627_26689[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_26615 === (11))){
var inst_26589 = (state_26614[(2)]);
var state_26614__$1 = (function (){var statearr_26628 = state_26614;
(statearr_26628[(10)] = inst_26589);

return statearr_26628;
})();
var statearr_26629_26690 = state_26614__$1;
(statearr_26629_26690[(2)] = null);

(statearr_26629_26690[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_26615 === (9))){
var state_26614__$1 = state_26614;
var statearr_26630_26691 = state_26614__$1;
(statearr_26630_26691[(2)] = null);

(statearr_26630_26691[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_26615 === (5))){
var state_26614__$1 = state_26614;
if(cljs.core.truth_(close_QMARK_)){
var statearr_26631_26692 = state_26614__$1;
(statearr_26631_26692[(1)] = (8));

} else {
var statearr_26632_26693 = state_26614__$1;
(statearr_26632_26693[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_26615 === (14))){
var inst_26592 = (state_26614[(8)]);
var inst_26594 = (state_26614[(11)]);
var inst_26592__$1 = (state_26614[(2)]);
var inst_26593 = (inst_26592__$1 == null);
var inst_26594__$1 = cljs.core.not.call(null,inst_26593);
var state_26614__$1 = (function (){var statearr_26633 = state_26614;
(statearr_26633[(8)] = inst_26592__$1);

(statearr_26633[(11)] = inst_26594__$1);

return statearr_26633;
})();
if(inst_26594__$1){
var statearr_26634_26694 = state_26614__$1;
(statearr_26634_26694[(1)] = (15));

} else {
var statearr_26635_26695 = state_26614__$1;
(statearr_26635_26695[(1)] = (16));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_26615 === (16))){
var inst_26594 = (state_26614[(11)]);
var state_26614__$1 = state_26614;
var statearr_26636_26696 = state_26614__$1;
(statearr_26636_26696[(2)] = inst_26594);

(statearr_26636_26696[(1)] = (17));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_26615 === (10))){
var inst_26586 = (state_26614[(2)]);
var state_26614__$1 = state_26614;
var statearr_26637_26697 = state_26614__$1;
(statearr_26637_26697[(2)] = inst_26586);

(statearr_26637_26697[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_26615 === (18))){
var inst_26597 = (state_26614[(2)]);
var state_26614__$1 = state_26614;
var statearr_26638_26698 = state_26614__$1;
(statearr_26638_26698[(2)] = inst_26597);

(statearr_26638_26698[(1)] = (17));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_26615 === (8))){
var inst_26583 = cljs.core.async.close_BANG_.call(null,to);
var state_26614__$1 = state_26614;
var statearr_26639_26699 = state_26614__$1;
(statearr_26639_26699[(2)] = inst_26583);

(statearr_26639_26699[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__26309__auto__,jobs,results,process,async))
;
return ((function (switch__26214__auto__,c__26309__auto__,jobs,results,process,async){
return (function() {
var cljs$core$async$pipeline_STAR__$_state_machine__26215__auto__ = null;
var cljs$core$async$pipeline_STAR__$_state_machine__26215__auto____0 = (function (){
var statearr_26640 = [null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_26640[(0)] = cljs$core$async$pipeline_STAR__$_state_machine__26215__auto__);

(statearr_26640[(1)] = (1));

return statearr_26640;
});
var cljs$core$async$pipeline_STAR__$_state_machine__26215__auto____1 = (function (state_26614){
while(true){
var ret_value__26216__auto__ = (function (){try{while(true){
var result__26217__auto__ = switch__26214__auto__.call(null,state_26614);
if(cljs.core.keyword_identical_QMARK_.call(null,result__26217__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__26217__auto__;
}
break;
}
}catch (e26641){if((e26641 instanceof Object)){
var ex__26218__auto__ = e26641;
var statearr_26642_26700 = state_26614;
(statearr_26642_26700[(5)] = ex__26218__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_26614);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e26641;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__26216__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__26701 = state_26614;
state_26614 = G__26701;
continue;
} else {
return ret_value__26216__auto__;
}
break;
}
});
cljs$core$async$pipeline_STAR__$_state_machine__26215__auto__ = function(state_26614){
switch(arguments.length){
case 0:
return cljs$core$async$pipeline_STAR__$_state_machine__26215__auto____0.call(this);
case 1:
return cljs$core$async$pipeline_STAR__$_state_machine__26215__auto____1.call(this,state_26614);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$pipeline_STAR__$_state_machine__26215__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$pipeline_STAR__$_state_machine__26215__auto____0;
cljs$core$async$pipeline_STAR__$_state_machine__26215__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$pipeline_STAR__$_state_machine__26215__auto____1;
return cljs$core$async$pipeline_STAR__$_state_machine__26215__auto__;
})()
;})(switch__26214__auto__,c__26309__auto__,jobs,results,process,async))
})();
var state__26311__auto__ = (function (){var statearr_26643 = f__26310__auto__.call(null);
(statearr_26643[(6)] = c__26309__auto__);

return statearr_26643;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__26311__auto__);
});})(c__26309__auto__,jobs,results,process,async))
);

return c__26309__auto__;
});
/**
 * Takes elements from the from channel and supplies them to the to
 *   channel, subject to the async function af, with parallelism n. af
 *   must be a function of two arguments, the first an input value and
 *   the second a channel on which to place the result(s). af must close!
 *   the channel before returning.  The presumption is that af will
 *   return immediately, having launched some asynchronous operation
 *   whose completion/callback will manipulate the result channel. Outputs
 *   will be returned in order relative to  the inputs. By default, the to
 *   channel will be closed when the from channel closes, but can be
 *   determined by the close?  parameter. Will stop consuming the from
 *   channel if the to channel closes.
 */
cljs.core.async.pipeline_async = (function cljs$core$async$pipeline_async(var_args){
var G__26703 = arguments.length;
switch (G__26703) {
case 4:
return cljs.core.async.pipeline_async.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
case 5:
return cljs.core.async.pipeline_async.cljs$core$IFn$_invoke$arity$5((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]),(arguments[(4)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.pipeline_async.cljs$core$IFn$_invoke$arity$4 = (function (n,to,af,from){
return cljs.core.async.pipeline_async.call(null,n,to,af,from,true);
});

cljs.core.async.pipeline_async.cljs$core$IFn$_invoke$arity$5 = (function (n,to,af,from,close_QMARK_){
return cljs.core.async.pipeline_STAR_.call(null,n,to,af,from,close_QMARK_,null,new cljs.core.Keyword(null,"async","async",1050769601));
});

cljs.core.async.pipeline_async.cljs$lang$maxFixedArity = 5;

/**
 * Takes elements from the from channel and supplies them to the to
 *   channel, subject to the transducer xf, with parallelism n. Because
 *   it is parallel, the transducer will be applied independently to each
 *   element, not across elements, and may produce zero or more outputs
 *   per input.  Outputs will be returned in order relative to the
 *   inputs. By default, the to channel will be closed when the from
 *   channel closes, but can be determined by the close?  parameter. Will
 *   stop consuming the from channel if the to channel closes.
 * 
 *   Note this is supplied for API compatibility with the Clojure version.
 *   Values of N > 1 will not result in actual concurrency in a
 *   single-threaded runtime.
 */
cljs.core.async.pipeline = (function cljs$core$async$pipeline(var_args){
var G__26706 = arguments.length;
switch (G__26706) {
case 4:
return cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
case 5:
return cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$5((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]),(arguments[(4)]));

break;
case 6:
return cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$6((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]),(arguments[(4)]),(arguments[(5)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$4 = (function (n,to,xf,from){
return cljs.core.async.pipeline.call(null,n,to,xf,from,true);
});

cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$5 = (function (n,to,xf,from,close_QMARK_){
return cljs.core.async.pipeline.call(null,n,to,xf,from,close_QMARK_,null);
});

cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$6 = (function (n,to,xf,from,close_QMARK_,ex_handler){
return cljs.core.async.pipeline_STAR_.call(null,n,to,xf,from,close_QMARK_,ex_handler,new cljs.core.Keyword(null,"compute","compute",1555393130));
});

cljs.core.async.pipeline.cljs$lang$maxFixedArity = 6;

/**
 * Takes a predicate and a source channel and returns a vector of two
 *   channels, the first of which will contain the values for which the
 *   predicate returned true, the second those for which it returned
 *   false.
 * 
 *   The out channels will be unbuffered by default, or two buf-or-ns can
 *   be supplied. The channels will close after the source channel has
 *   closed.
 */
cljs.core.async.split = (function cljs$core$async$split(var_args){
var G__26709 = arguments.length;
switch (G__26709) {
case 2:
return cljs.core.async.split.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 4:
return cljs.core.async.split.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.split.cljs$core$IFn$_invoke$arity$2 = (function (p,ch){
return cljs.core.async.split.call(null,p,ch,null,null);
});

cljs.core.async.split.cljs$core$IFn$_invoke$arity$4 = (function (p,ch,t_buf_or_n,f_buf_or_n){
var tc = cljs.core.async.chan.call(null,t_buf_or_n);
var fc = cljs.core.async.chan.call(null,f_buf_or_n);
var c__26309__auto___26758 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__26309__auto___26758,tc,fc){
return (function (){
var f__26310__auto__ = (function (){var switch__26214__auto__ = ((function (c__26309__auto___26758,tc,fc){
return (function (state_26735){
var state_val_26736 = (state_26735[(1)]);
if((state_val_26736 === (7))){
var inst_26731 = (state_26735[(2)]);
var state_26735__$1 = state_26735;
var statearr_26737_26759 = state_26735__$1;
(statearr_26737_26759[(2)] = inst_26731);

(statearr_26737_26759[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_26736 === (1))){
var state_26735__$1 = state_26735;
var statearr_26738_26760 = state_26735__$1;
(statearr_26738_26760[(2)] = null);

(statearr_26738_26760[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_26736 === (4))){
var inst_26712 = (state_26735[(7)]);
var inst_26712__$1 = (state_26735[(2)]);
var inst_26713 = (inst_26712__$1 == null);
var state_26735__$1 = (function (){var statearr_26739 = state_26735;
(statearr_26739[(7)] = inst_26712__$1);

return statearr_26739;
})();
if(cljs.core.truth_(inst_26713)){
var statearr_26740_26761 = state_26735__$1;
(statearr_26740_26761[(1)] = (5));

} else {
var statearr_26741_26762 = state_26735__$1;
(statearr_26741_26762[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_26736 === (13))){
var state_26735__$1 = state_26735;
var statearr_26742_26763 = state_26735__$1;
(statearr_26742_26763[(2)] = null);

(statearr_26742_26763[(1)] = (14));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_26736 === (6))){
var inst_26712 = (state_26735[(7)]);
var inst_26718 = p.call(null,inst_26712);
var state_26735__$1 = state_26735;
if(cljs.core.truth_(inst_26718)){
var statearr_26743_26764 = state_26735__$1;
(statearr_26743_26764[(1)] = (9));

} else {
var statearr_26744_26765 = state_26735__$1;
(statearr_26744_26765[(1)] = (10));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_26736 === (3))){
var inst_26733 = (state_26735[(2)]);
var state_26735__$1 = state_26735;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_26735__$1,inst_26733);
} else {
if((state_val_26736 === (12))){
var state_26735__$1 = state_26735;
var statearr_26745_26766 = state_26735__$1;
(statearr_26745_26766[(2)] = null);

(statearr_26745_26766[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_26736 === (2))){
var state_26735__$1 = state_26735;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_26735__$1,(4),ch);
} else {
if((state_val_26736 === (11))){
var inst_26712 = (state_26735[(7)]);
var inst_26722 = (state_26735[(2)]);
var state_26735__$1 = state_26735;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_26735__$1,(8),inst_26722,inst_26712);
} else {
if((state_val_26736 === (9))){
var state_26735__$1 = state_26735;
var statearr_26746_26767 = state_26735__$1;
(statearr_26746_26767[(2)] = tc);

(statearr_26746_26767[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_26736 === (5))){
var inst_26715 = cljs.core.async.close_BANG_.call(null,tc);
var inst_26716 = cljs.core.async.close_BANG_.call(null,fc);
var state_26735__$1 = (function (){var statearr_26747 = state_26735;
(statearr_26747[(8)] = inst_26715);

return statearr_26747;
})();
var statearr_26748_26768 = state_26735__$1;
(statearr_26748_26768[(2)] = inst_26716);

(statearr_26748_26768[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_26736 === (14))){
var inst_26729 = (state_26735[(2)]);
var state_26735__$1 = state_26735;
var statearr_26749_26769 = state_26735__$1;
(statearr_26749_26769[(2)] = inst_26729);

(statearr_26749_26769[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_26736 === (10))){
var state_26735__$1 = state_26735;
var statearr_26750_26770 = state_26735__$1;
(statearr_26750_26770[(2)] = fc);

(statearr_26750_26770[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_26736 === (8))){
var inst_26724 = (state_26735[(2)]);
var state_26735__$1 = state_26735;
if(cljs.core.truth_(inst_26724)){
var statearr_26751_26771 = state_26735__$1;
(statearr_26751_26771[(1)] = (12));

} else {
var statearr_26752_26772 = state_26735__$1;
(statearr_26752_26772[(1)] = (13));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__26309__auto___26758,tc,fc))
;
return ((function (switch__26214__auto__,c__26309__auto___26758,tc,fc){
return (function() {
var cljs$core$async$state_machine__26215__auto__ = null;
var cljs$core$async$state_machine__26215__auto____0 = (function (){
var statearr_26753 = [null,null,null,null,null,null,null,null,null];
(statearr_26753[(0)] = cljs$core$async$state_machine__26215__auto__);

(statearr_26753[(1)] = (1));

return statearr_26753;
});
var cljs$core$async$state_machine__26215__auto____1 = (function (state_26735){
while(true){
var ret_value__26216__auto__ = (function (){try{while(true){
var result__26217__auto__ = switch__26214__auto__.call(null,state_26735);
if(cljs.core.keyword_identical_QMARK_.call(null,result__26217__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__26217__auto__;
}
break;
}
}catch (e26754){if((e26754 instanceof Object)){
var ex__26218__auto__ = e26754;
var statearr_26755_26773 = state_26735;
(statearr_26755_26773[(5)] = ex__26218__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_26735);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e26754;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__26216__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__26774 = state_26735;
state_26735 = G__26774;
continue;
} else {
return ret_value__26216__auto__;
}
break;
}
});
cljs$core$async$state_machine__26215__auto__ = function(state_26735){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__26215__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__26215__auto____1.call(this,state_26735);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__26215__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__26215__auto____0;
cljs$core$async$state_machine__26215__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__26215__auto____1;
return cljs$core$async$state_machine__26215__auto__;
})()
;})(switch__26214__auto__,c__26309__auto___26758,tc,fc))
})();
var state__26311__auto__ = (function (){var statearr_26756 = f__26310__auto__.call(null);
(statearr_26756[(6)] = c__26309__auto___26758);

return statearr_26756;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__26311__auto__);
});})(c__26309__auto___26758,tc,fc))
);


return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [tc,fc], null);
});

cljs.core.async.split.cljs$lang$maxFixedArity = 4;

/**
 * f should be a function of 2 arguments. Returns a channel containing
 *   the single result of applying f to init and the first item from the
 *   channel, then applying f to that result and the 2nd item, etc. If
 *   the channel closes without yielding items, returns init and f is not
 *   called. ch must close before reduce produces a result.
 */
cljs.core.async.reduce = (function cljs$core$async$reduce(f,init,ch){
var c__26309__auto__ = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__26309__auto__){
return (function (){
var f__26310__auto__ = (function (){var switch__26214__auto__ = ((function (c__26309__auto__){
return (function (state_26795){
var state_val_26796 = (state_26795[(1)]);
if((state_val_26796 === (7))){
var inst_26791 = (state_26795[(2)]);
var state_26795__$1 = state_26795;
var statearr_26797_26815 = state_26795__$1;
(statearr_26797_26815[(2)] = inst_26791);

(statearr_26797_26815[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_26796 === (1))){
var inst_26775 = init;
var state_26795__$1 = (function (){var statearr_26798 = state_26795;
(statearr_26798[(7)] = inst_26775);

return statearr_26798;
})();
var statearr_26799_26816 = state_26795__$1;
(statearr_26799_26816[(2)] = null);

(statearr_26799_26816[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_26796 === (4))){
var inst_26778 = (state_26795[(8)]);
var inst_26778__$1 = (state_26795[(2)]);
var inst_26779 = (inst_26778__$1 == null);
var state_26795__$1 = (function (){var statearr_26800 = state_26795;
(statearr_26800[(8)] = inst_26778__$1);

return statearr_26800;
})();
if(cljs.core.truth_(inst_26779)){
var statearr_26801_26817 = state_26795__$1;
(statearr_26801_26817[(1)] = (5));

} else {
var statearr_26802_26818 = state_26795__$1;
(statearr_26802_26818[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_26796 === (6))){
var inst_26782 = (state_26795[(9)]);
var inst_26778 = (state_26795[(8)]);
var inst_26775 = (state_26795[(7)]);
var inst_26782__$1 = f.call(null,inst_26775,inst_26778);
var inst_26783 = cljs.core.reduced_QMARK_.call(null,inst_26782__$1);
var state_26795__$1 = (function (){var statearr_26803 = state_26795;
(statearr_26803[(9)] = inst_26782__$1);

return statearr_26803;
})();
if(inst_26783){
var statearr_26804_26819 = state_26795__$1;
(statearr_26804_26819[(1)] = (8));

} else {
var statearr_26805_26820 = state_26795__$1;
(statearr_26805_26820[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_26796 === (3))){
var inst_26793 = (state_26795[(2)]);
var state_26795__$1 = state_26795;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_26795__$1,inst_26793);
} else {
if((state_val_26796 === (2))){
var state_26795__$1 = state_26795;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_26795__$1,(4),ch);
} else {
if((state_val_26796 === (9))){
var inst_26782 = (state_26795[(9)]);
var inst_26775 = inst_26782;
var state_26795__$1 = (function (){var statearr_26806 = state_26795;
(statearr_26806[(7)] = inst_26775);

return statearr_26806;
})();
var statearr_26807_26821 = state_26795__$1;
(statearr_26807_26821[(2)] = null);

(statearr_26807_26821[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_26796 === (5))){
var inst_26775 = (state_26795[(7)]);
var state_26795__$1 = state_26795;
var statearr_26808_26822 = state_26795__$1;
(statearr_26808_26822[(2)] = inst_26775);

(statearr_26808_26822[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_26796 === (10))){
var inst_26789 = (state_26795[(2)]);
var state_26795__$1 = state_26795;
var statearr_26809_26823 = state_26795__$1;
(statearr_26809_26823[(2)] = inst_26789);

(statearr_26809_26823[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_26796 === (8))){
var inst_26782 = (state_26795[(9)]);
var inst_26785 = cljs.core.deref.call(null,inst_26782);
var state_26795__$1 = state_26795;
var statearr_26810_26824 = state_26795__$1;
(statearr_26810_26824[(2)] = inst_26785);

(statearr_26810_26824[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
});})(c__26309__auto__))
;
return ((function (switch__26214__auto__,c__26309__auto__){
return (function() {
var cljs$core$async$reduce_$_state_machine__26215__auto__ = null;
var cljs$core$async$reduce_$_state_machine__26215__auto____0 = (function (){
var statearr_26811 = [null,null,null,null,null,null,null,null,null,null];
(statearr_26811[(0)] = cljs$core$async$reduce_$_state_machine__26215__auto__);

(statearr_26811[(1)] = (1));

return statearr_26811;
});
var cljs$core$async$reduce_$_state_machine__26215__auto____1 = (function (state_26795){
while(true){
var ret_value__26216__auto__ = (function (){try{while(true){
var result__26217__auto__ = switch__26214__auto__.call(null,state_26795);
if(cljs.core.keyword_identical_QMARK_.call(null,result__26217__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__26217__auto__;
}
break;
}
}catch (e26812){if((e26812 instanceof Object)){
var ex__26218__auto__ = e26812;
var statearr_26813_26825 = state_26795;
(statearr_26813_26825[(5)] = ex__26218__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_26795);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e26812;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__26216__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__26826 = state_26795;
state_26795 = G__26826;
continue;
} else {
return ret_value__26216__auto__;
}
break;
}
});
cljs$core$async$reduce_$_state_machine__26215__auto__ = function(state_26795){
switch(arguments.length){
case 0:
return cljs$core$async$reduce_$_state_machine__26215__auto____0.call(this);
case 1:
return cljs$core$async$reduce_$_state_machine__26215__auto____1.call(this,state_26795);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$reduce_$_state_machine__26215__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$reduce_$_state_machine__26215__auto____0;
cljs$core$async$reduce_$_state_machine__26215__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$reduce_$_state_machine__26215__auto____1;
return cljs$core$async$reduce_$_state_machine__26215__auto__;
})()
;})(switch__26214__auto__,c__26309__auto__))
})();
var state__26311__auto__ = (function (){var statearr_26814 = f__26310__auto__.call(null);
(statearr_26814[(6)] = c__26309__auto__);

return statearr_26814;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__26311__auto__);
});})(c__26309__auto__))
);

return c__26309__auto__;
});
/**
 * async/reduces a channel with a transformation (xform f).
 *   Returns a channel containing the result.  ch must close before
 *   transduce produces a result.
 */
cljs.core.async.transduce = (function cljs$core$async$transduce(xform,f,init,ch){
var f__$1 = xform.call(null,f);
var c__26309__auto__ = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__26309__auto__,f__$1){
return (function (){
var f__26310__auto__ = (function (){var switch__26214__auto__ = ((function (c__26309__auto__,f__$1){
return (function (state_26832){
var state_val_26833 = (state_26832[(1)]);
if((state_val_26833 === (1))){
var inst_26827 = cljs.core.async.reduce.call(null,f__$1,init,ch);
var state_26832__$1 = state_26832;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_26832__$1,(2),inst_26827);
} else {
if((state_val_26833 === (2))){
var inst_26829 = (state_26832[(2)]);
var inst_26830 = f__$1.call(null,inst_26829);
var state_26832__$1 = state_26832;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_26832__$1,inst_26830);
} else {
return null;
}
}
});})(c__26309__auto__,f__$1))
;
return ((function (switch__26214__auto__,c__26309__auto__,f__$1){
return (function() {
var cljs$core$async$transduce_$_state_machine__26215__auto__ = null;
var cljs$core$async$transduce_$_state_machine__26215__auto____0 = (function (){
var statearr_26834 = [null,null,null,null,null,null,null];
(statearr_26834[(0)] = cljs$core$async$transduce_$_state_machine__26215__auto__);

(statearr_26834[(1)] = (1));

return statearr_26834;
});
var cljs$core$async$transduce_$_state_machine__26215__auto____1 = (function (state_26832){
while(true){
var ret_value__26216__auto__ = (function (){try{while(true){
var result__26217__auto__ = switch__26214__auto__.call(null,state_26832);
if(cljs.core.keyword_identical_QMARK_.call(null,result__26217__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__26217__auto__;
}
break;
}
}catch (e26835){if((e26835 instanceof Object)){
var ex__26218__auto__ = e26835;
var statearr_26836_26838 = state_26832;
(statearr_26836_26838[(5)] = ex__26218__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_26832);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e26835;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__26216__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__26839 = state_26832;
state_26832 = G__26839;
continue;
} else {
return ret_value__26216__auto__;
}
break;
}
});
cljs$core$async$transduce_$_state_machine__26215__auto__ = function(state_26832){
switch(arguments.length){
case 0:
return cljs$core$async$transduce_$_state_machine__26215__auto____0.call(this);
case 1:
return cljs$core$async$transduce_$_state_machine__26215__auto____1.call(this,state_26832);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$transduce_$_state_machine__26215__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$transduce_$_state_machine__26215__auto____0;
cljs$core$async$transduce_$_state_machine__26215__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$transduce_$_state_machine__26215__auto____1;
return cljs$core$async$transduce_$_state_machine__26215__auto__;
})()
;})(switch__26214__auto__,c__26309__auto__,f__$1))
})();
var state__26311__auto__ = (function (){var statearr_26837 = f__26310__auto__.call(null);
(statearr_26837[(6)] = c__26309__auto__);

return statearr_26837;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__26311__auto__);
});})(c__26309__auto__,f__$1))
);

return c__26309__auto__;
});
/**
 * Puts the contents of coll into the supplied channel.
 * 
 *   By default the channel will be closed after the items are copied,
 *   but can be determined by the close? parameter.
 * 
 *   Returns a channel which will close after the items are copied.
 */
cljs.core.async.onto_chan = (function cljs$core$async$onto_chan(var_args){
var G__26841 = arguments.length;
switch (G__26841) {
case 2:
return cljs.core.async.onto_chan.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.onto_chan.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.onto_chan.cljs$core$IFn$_invoke$arity$2 = (function (ch,coll){
return cljs.core.async.onto_chan.call(null,ch,coll,true);
});

cljs.core.async.onto_chan.cljs$core$IFn$_invoke$arity$3 = (function (ch,coll,close_QMARK_){
var c__26309__auto__ = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__26309__auto__){
return (function (){
var f__26310__auto__ = (function (){var switch__26214__auto__ = ((function (c__26309__auto__){
return (function (state_26866){
var state_val_26867 = (state_26866[(1)]);
if((state_val_26867 === (7))){
var inst_26848 = (state_26866[(2)]);
var state_26866__$1 = state_26866;
var statearr_26868_26889 = state_26866__$1;
(statearr_26868_26889[(2)] = inst_26848);

(statearr_26868_26889[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_26867 === (1))){
var inst_26842 = cljs.core.seq.call(null,coll);
var inst_26843 = inst_26842;
var state_26866__$1 = (function (){var statearr_26869 = state_26866;
(statearr_26869[(7)] = inst_26843);

return statearr_26869;
})();
var statearr_26870_26890 = state_26866__$1;
(statearr_26870_26890[(2)] = null);

(statearr_26870_26890[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_26867 === (4))){
var inst_26843 = (state_26866[(7)]);
var inst_26846 = cljs.core.first.call(null,inst_26843);
var state_26866__$1 = state_26866;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_26866__$1,(7),ch,inst_26846);
} else {
if((state_val_26867 === (13))){
var inst_26860 = (state_26866[(2)]);
var state_26866__$1 = state_26866;
var statearr_26871_26891 = state_26866__$1;
(statearr_26871_26891[(2)] = inst_26860);

(statearr_26871_26891[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_26867 === (6))){
var inst_26851 = (state_26866[(2)]);
var state_26866__$1 = state_26866;
if(cljs.core.truth_(inst_26851)){
var statearr_26872_26892 = state_26866__$1;
(statearr_26872_26892[(1)] = (8));

} else {
var statearr_26873_26893 = state_26866__$1;
(statearr_26873_26893[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_26867 === (3))){
var inst_26864 = (state_26866[(2)]);
var state_26866__$1 = state_26866;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_26866__$1,inst_26864);
} else {
if((state_val_26867 === (12))){
var state_26866__$1 = state_26866;
var statearr_26874_26894 = state_26866__$1;
(statearr_26874_26894[(2)] = null);

(statearr_26874_26894[(1)] = (13));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_26867 === (2))){
var inst_26843 = (state_26866[(7)]);
var state_26866__$1 = state_26866;
if(cljs.core.truth_(inst_26843)){
var statearr_26875_26895 = state_26866__$1;
(statearr_26875_26895[(1)] = (4));

} else {
var statearr_26876_26896 = state_26866__$1;
(statearr_26876_26896[(1)] = (5));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_26867 === (11))){
var inst_26857 = cljs.core.async.close_BANG_.call(null,ch);
var state_26866__$1 = state_26866;
var statearr_26877_26897 = state_26866__$1;
(statearr_26877_26897[(2)] = inst_26857);

(statearr_26877_26897[(1)] = (13));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_26867 === (9))){
var state_26866__$1 = state_26866;
if(cljs.core.truth_(close_QMARK_)){
var statearr_26878_26898 = state_26866__$1;
(statearr_26878_26898[(1)] = (11));

} else {
var statearr_26879_26899 = state_26866__$1;
(statearr_26879_26899[(1)] = (12));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_26867 === (5))){
var inst_26843 = (state_26866[(7)]);
var state_26866__$1 = state_26866;
var statearr_26880_26900 = state_26866__$1;
(statearr_26880_26900[(2)] = inst_26843);

(statearr_26880_26900[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_26867 === (10))){
var inst_26862 = (state_26866[(2)]);
var state_26866__$1 = state_26866;
var statearr_26881_26901 = state_26866__$1;
(statearr_26881_26901[(2)] = inst_26862);

(statearr_26881_26901[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_26867 === (8))){
var inst_26843 = (state_26866[(7)]);
var inst_26853 = cljs.core.next.call(null,inst_26843);
var inst_26843__$1 = inst_26853;
var state_26866__$1 = (function (){var statearr_26882 = state_26866;
(statearr_26882[(7)] = inst_26843__$1);

return statearr_26882;
})();
var statearr_26883_26902 = state_26866__$1;
(statearr_26883_26902[(2)] = null);

(statearr_26883_26902[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__26309__auto__))
;
return ((function (switch__26214__auto__,c__26309__auto__){
return (function() {
var cljs$core$async$state_machine__26215__auto__ = null;
var cljs$core$async$state_machine__26215__auto____0 = (function (){
var statearr_26884 = [null,null,null,null,null,null,null,null];
(statearr_26884[(0)] = cljs$core$async$state_machine__26215__auto__);

(statearr_26884[(1)] = (1));

return statearr_26884;
});
var cljs$core$async$state_machine__26215__auto____1 = (function (state_26866){
while(true){
var ret_value__26216__auto__ = (function (){try{while(true){
var result__26217__auto__ = switch__26214__auto__.call(null,state_26866);
if(cljs.core.keyword_identical_QMARK_.call(null,result__26217__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__26217__auto__;
}
break;
}
}catch (e26885){if((e26885 instanceof Object)){
var ex__26218__auto__ = e26885;
var statearr_26886_26903 = state_26866;
(statearr_26886_26903[(5)] = ex__26218__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_26866);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e26885;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__26216__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__26904 = state_26866;
state_26866 = G__26904;
continue;
} else {
return ret_value__26216__auto__;
}
break;
}
});
cljs$core$async$state_machine__26215__auto__ = function(state_26866){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__26215__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__26215__auto____1.call(this,state_26866);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__26215__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__26215__auto____0;
cljs$core$async$state_machine__26215__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__26215__auto____1;
return cljs$core$async$state_machine__26215__auto__;
})()
;})(switch__26214__auto__,c__26309__auto__))
})();
var state__26311__auto__ = (function (){var statearr_26887 = f__26310__auto__.call(null);
(statearr_26887[(6)] = c__26309__auto__);

return statearr_26887;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__26311__auto__);
});})(c__26309__auto__))
);

return c__26309__auto__;
});

cljs.core.async.onto_chan.cljs$lang$maxFixedArity = 3;

/**
 * Creates and returns a channel which contains the contents of coll,
 *   closing when exhausted.
 */
cljs.core.async.to_chan = (function cljs$core$async$to_chan(coll){
var ch = cljs.core.async.chan.call(null,cljs.core.bounded_count.call(null,(100),coll));
cljs.core.async.onto_chan.call(null,ch,coll);

return ch;
});

/**
 * @interface
 */
cljs.core.async.Mux = function(){};

cljs.core.async.muxch_STAR_ = (function cljs$core$async$muxch_STAR_(_){
if((((!((_ == null)))) && ((!((_.cljs$core$async$Mux$muxch_STAR_$arity$1 == null)))))){
return _.cljs$core$async$Mux$muxch_STAR_$arity$1(_);
} else {
var x__4433__auto__ = (((_ == null))?null:_);
var m__4434__auto__ = (cljs.core.async.muxch_STAR_[goog.typeOf(x__4433__auto__)]);
if((!((m__4434__auto__ == null)))){
return m__4434__auto__.call(null,_);
} else {
var m__4431__auto__ = (cljs.core.async.muxch_STAR_["_"]);
if((!((m__4431__auto__ == null)))){
return m__4431__auto__.call(null,_);
} else {
throw cljs.core.missing_protocol.call(null,"Mux.muxch*",_);
}
}
}
});


/**
 * @interface
 */
cljs.core.async.Mult = function(){};

cljs.core.async.tap_STAR_ = (function cljs$core$async$tap_STAR_(m,ch,close_QMARK_){
if((((!((m == null)))) && ((!((m.cljs$core$async$Mult$tap_STAR_$arity$3 == null)))))){
return m.cljs$core$async$Mult$tap_STAR_$arity$3(m,ch,close_QMARK_);
} else {
var x__4433__auto__ = (((m == null))?null:m);
var m__4434__auto__ = (cljs.core.async.tap_STAR_[goog.typeOf(x__4433__auto__)]);
if((!((m__4434__auto__ == null)))){
return m__4434__auto__.call(null,m,ch,close_QMARK_);
} else {
var m__4431__auto__ = (cljs.core.async.tap_STAR_["_"]);
if((!((m__4431__auto__ == null)))){
return m__4431__auto__.call(null,m,ch,close_QMARK_);
} else {
throw cljs.core.missing_protocol.call(null,"Mult.tap*",m);
}
}
}
});

cljs.core.async.untap_STAR_ = (function cljs$core$async$untap_STAR_(m,ch){
if((((!((m == null)))) && ((!((m.cljs$core$async$Mult$untap_STAR_$arity$2 == null)))))){
return m.cljs$core$async$Mult$untap_STAR_$arity$2(m,ch);
} else {
var x__4433__auto__ = (((m == null))?null:m);
var m__4434__auto__ = (cljs.core.async.untap_STAR_[goog.typeOf(x__4433__auto__)]);
if((!((m__4434__auto__ == null)))){
return m__4434__auto__.call(null,m,ch);
} else {
var m__4431__auto__ = (cljs.core.async.untap_STAR_["_"]);
if((!((m__4431__auto__ == null)))){
return m__4431__auto__.call(null,m,ch);
} else {
throw cljs.core.missing_protocol.call(null,"Mult.untap*",m);
}
}
}
});

cljs.core.async.untap_all_STAR_ = (function cljs$core$async$untap_all_STAR_(m){
if((((!((m == null)))) && ((!((m.cljs$core$async$Mult$untap_all_STAR_$arity$1 == null)))))){
return m.cljs$core$async$Mult$untap_all_STAR_$arity$1(m);
} else {
var x__4433__auto__ = (((m == null))?null:m);
var m__4434__auto__ = (cljs.core.async.untap_all_STAR_[goog.typeOf(x__4433__auto__)]);
if((!((m__4434__auto__ == null)))){
return m__4434__auto__.call(null,m);
} else {
var m__4431__auto__ = (cljs.core.async.untap_all_STAR_["_"]);
if((!((m__4431__auto__ == null)))){
return m__4431__auto__.call(null,m);
} else {
throw cljs.core.missing_protocol.call(null,"Mult.untap-all*",m);
}
}
}
});

/**
 * Creates and returns a mult(iple) of the supplied channel. Channels
 *   containing copies of the channel can be created with 'tap', and
 *   detached with 'untap'.
 * 
 *   Each item is distributed to all taps in parallel and synchronously,
 *   i.e. each tap must accept before the next item is distributed. Use
 *   buffering/windowing to prevent slow taps from holding up the mult.
 * 
 *   Items received when there are no taps get dropped.
 * 
 *   If a tap puts to a closed channel, it will be removed from the mult.
 */
cljs.core.async.mult = (function cljs$core$async$mult(ch){
var cs = cljs.core.atom.call(null,cljs.core.PersistentArrayMap.EMPTY);
var m = (function (){
if((typeof cljs !== 'undefined') && (typeof cljs.core !== 'undefined') && (typeof cljs.core.async !== 'undefined') && (typeof cljs.core.async.t_cljs$core$async26905 !== 'undefined')){
} else {

/**
* @constructor
 * @implements {cljs.core.async.Mult}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.async.Mux}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async26905 = (function (ch,cs,meta26906){
this.ch = ch;
this.cs = cs;
this.meta26906 = meta26906;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
cljs.core.async.t_cljs$core$async26905.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = ((function (cs){
return (function (_26907,meta26906__$1){
var self__ = this;
var _26907__$1 = this;
return (new cljs.core.async.t_cljs$core$async26905(self__.ch,self__.cs,meta26906__$1));
});})(cs))
;

cljs.core.async.t_cljs$core$async26905.prototype.cljs$core$IMeta$_meta$arity$1 = ((function (cs){
return (function (_26907){
var self__ = this;
var _26907__$1 = this;
return self__.meta26906;
});})(cs))
;

cljs.core.async.t_cljs$core$async26905.prototype.cljs$core$async$Mux$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async26905.prototype.cljs$core$async$Mux$muxch_STAR_$arity$1 = ((function (cs){
return (function (_){
var self__ = this;
var ___$1 = this;
return self__.ch;
});})(cs))
;

cljs.core.async.t_cljs$core$async26905.prototype.cljs$core$async$Mult$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async26905.prototype.cljs$core$async$Mult$tap_STAR_$arity$3 = ((function (cs){
return (function (_,ch__$1,close_QMARK_){
var self__ = this;
var ___$1 = this;
cljs.core.swap_BANG_.call(null,self__.cs,cljs.core.assoc,ch__$1,close_QMARK_);

return null;
});})(cs))
;

cljs.core.async.t_cljs$core$async26905.prototype.cljs$core$async$Mult$untap_STAR_$arity$2 = ((function (cs){
return (function (_,ch__$1){
var self__ = this;
var ___$1 = this;
cljs.core.swap_BANG_.call(null,self__.cs,cljs.core.dissoc,ch__$1);

return null;
});})(cs))
;

cljs.core.async.t_cljs$core$async26905.prototype.cljs$core$async$Mult$untap_all_STAR_$arity$1 = ((function (cs){
return (function (_){
var self__ = this;
var ___$1 = this;
cljs.core.reset_BANG_.call(null,self__.cs,cljs.core.PersistentArrayMap.EMPTY);

return null;
});})(cs))
;

cljs.core.async.t_cljs$core$async26905.getBasis = ((function (cs){
return (function (){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"ch","ch",1085813622,null),new cljs.core.Symbol(null,"cs","cs",-117024463,null),new cljs.core.Symbol(null,"meta26906","meta26906",-479938785,null)], null);
});})(cs))
;

cljs.core.async.t_cljs$core$async26905.cljs$lang$type = true;

cljs.core.async.t_cljs$core$async26905.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async26905";

cljs.core.async.t_cljs$core$async26905.cljs$lang$ctorPrWriter = ((function (cs){
return (function (this__4374__auto__,writer__4375__auto__,opt__4376__auto__){
return cljs.core._write.call(null,writer__4375__auto__,"cljs.core.async/t_cljs$core$async26905");
});})(cs))
;

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async26905.
 */
cljs.core.async.__GT_t_cljs$core$async26905 = ((function (cs){
return (function cljs$core$async$mult_$___GT_t_cljs$core$async26905(ch__$1,cs__$1,meta26906){
return (new cljs.core.async.t_cljs$core$async26905(ch__$1,cs__$1,meta26906));
});})(cs))
;

}

return (new cljs.core.async.t_cljs$core$async26905(ch,cs,cljs.core.PersistentArrayMap.EMPTY));
})()
;
var dchan = cljs.core.async.chan.call(null,(1));
var dctr = cljs.core.atom.call(null,null);
var done = ((function (cs,m,dchan,dctr){
return (function (_){
if((cljs.core.swap_BANG_.call(null,dctr,cljs.core.dec) === (0))){
return cljs.core.async.put_BANG_.call(null,dchan,true);
} else {
return null;
}
});})(cs,m,dchan,dctr))
;
var c__26309__auto___27127 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__26309__auto___27127,cs,m,dchan,dctr,done){
return (function (){
var f__26310__auto__ = (function (){var switch__26214__auto__ = ((function (c__26309__auto___27127,cs,m,dchan,dctr,done){
return (function (state_27042){
var state_val_27043 = (state_27042[(1)]);
if((state_val_27043 === (7))){
var inst_27038 = (state_27042[(2)]);
var state_27042__$1 = state_27042;
var statearr_27044_27128 = state_27042__$1;
(statearr_27044_27128[(2)] = inst_27038);

(statearr_27044_27128[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27043 === (20))){
var inst_26941 = (state_27042[(7)]);
var inst_26953 = cljs.core.first.call(null,inst_26941);
var inst_26954 = cljs.core.nth.call(null,inst_26953,(0),null);
var inst_26955 = cljs.core.nth.call(null,inst_26953,(1),null);
var state_27042__$1 = (function (){var statearr_27045 = state_27042;
(statearr_27045[(8)] = inst_26954);

return statearr_27045;
})();
if(cljs.core.truth_(inst_26955)){
var statearr_27046_27129 = state_27042__$1;
(statearr_27046_27129[(1)] = (22));

} else {
var statearr_27047_27130 = state_27042__$1;
(statearr_27047_27130[(1)] = (23));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27043 === (27))){
var inst_26910 = (state_27042[(9)]);
var inst_26983 = (state_27042[(10)]);
var inst_26990 = (state_27042[(11)]);
var inst_26985 = (state_27042[(12)]);
var inst_26990__$1 = cljs.core._nth.call(null,inst_26983,inst_26985);
var inst_26991 = cljs.core.async.put_BANG_.call(null,inst_26990__$1,inst_26910,done);
var state_27042__$1 = (function (){var statearr_27048 = state_27042;
(statearr_27048[(11)] = inst_26990__$1);

return statearr_27048;
})();
if(cljs.core.truth_(inst_26991)){
var statearr_27049_27131 = state_27042__$1;
(statearr_27049_27131[(1)] = (30));

} else {
var statearr_27050_27132 = state_27042__$1;
(statearr_27050_27132[(1)] = (31));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27043 === (1))){
var state_27042__$1 = state_27042;
var statearr_27051_27133 = state_27042__$1;
(statearr_27051_27133[(2)] = null);

(statearr_27051_27133[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27043 === (24))){
var inst_26941 = (state_27042[(7)]);
var inst_26960 = (state_27042[(2)]);
var inst_26961 = cljs.core.next.call(null,inst_26941);
var inst_26919 = inst_26961;
var inst_26920 = null;
var inst_26921 = (0);
var inst_26922 = (0);
var state_27042__$1 = (function (){var statearr_27052 = state_27042;
(statearr_27052[(13)] = inst_26920);

(statearr_27052[(14)] = inst_26919);

(statearr_27052[(15)] = inst_26960);

(statearr_27052[(16)] = inst_26922);

(statearr_27052[(17)] = inst_26921);

return statearr_27052;
})();
var statearr_27053_27134 = state_27042__$1;
(statearr_27053_27134[(2)] = null);

(statearr_27053_27134[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27043 === (39))){
var state_27042__$1 = state_27042;
var statearr_27057_27135 = state_27042__$1;
(statearr_27057_27135[(2)] = null);

(statearr_27057_27135[(1)] = (41));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27043 === (4))){
var inst_26910 = (state_27042[(9)]);
var inst_26910__$1 = (state_27042[(2)]);
var inst_26911 = (inst_26910__$1 == null);
var state_27042__$1 = (function (){var statearr_27058 = state_27042;
(statearr_27058[(9)] = inst_26910__$1);

return statearr_27058;
})();
if(cljs.core.truth_(inst_26911)){
var statearr_27059_27136 = state_27042__$1;
(statearr_27059_27136[(1)] = (5));

} else {
var statearr_27060_27137 = state_27042__$1;
(statearr_27060_27137[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27043 === (15))){
var inst_26920 = (state_27042[(13)]);
var inst_26919 = (state_27042[(14)]);
var inst_26922 = (state_27042[(16)]);
var inst_26921 = (state_27042[(17)]);
var inst_26937 = (state_27042[(2)]);
var inst_26938 = (inst_26922 + (1));
var tmp27054 = inst_26920;
var tmp27055 = inst_26919;
var tmp27056 = inst_26921;
var inst_26919__$1 = tmp27055;
var inst_26920__$1 = tmp27054;
var inst_26921__$1 = tmp27056;
var inst_26922__$1 = inst_26938;
var state_27042__$1 = (function (){var statearr_27061 = state_27042;
(statearr_27061[(18)] = inst_26937);

(statearr_27061[(13)] = inst_26920__$1);

(statearr_27061[(14)] = inst_26919__$1);

(statearr_27061[(16)] = inst_26922__$1);

(statearr_27061[(17)] = inst_26921__$1);

return statearr_27061;
})();
var statearr_27062_27138 = state_27042__$1;
(statearr_27062_27138[(2)] = null);

(statearr_27062_27138[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27043 === (21))){
var inst_26964 = (state_27042[(2)]);
var state_27042__$1 = state_27042;
var statearr_27066_27139 = state_27042__$1;
(statearr_27066_27139[(2)] = inst_26964);

(statearr_27066_27139[(1)] = (18));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27043 === (31))){
var inst_26990 = (state_27042[(11)]);
var inst_26994 = done.call(null,null);
var inst_26995 = cljs.core.async.untap_STAR_.call(null,m,inst_26990);
var state_27042__$1 = (function (){var statearr_27067 = state_27042;
(statearr_27067[(19)] = inst_26994);

return statearr_27067;
})();
var statearr_27068_27140 = state_27042__$1;
(statearr_27068_27140[(2)] = inst_26995);

(statearr_27068_27140[(1)] = (32));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27043 === (32))){
var inst_26984 = (state_27042[(20)]);
var inst_26982 = (state_27042[(21)]);
var inst_26983 = (state_27042[(10)]);
var inst_26985 = (state_27042[(12)]);
var inst_26997 = (state_27042[(2)]);
var inst_26998 = (inst_26985 + (1));
var tmp27063 = inst_26984;
var tmp27064 = inst_26982;
var tmp27065 = inst_26983;
var inst_26982__$1 = tmp27064;
var inst_26983__$1 = tmp27065;
var inst_26984__$1 = tmp27063;
var inst_26985__$1 = inst_26998;
var state_27042__$1 = (function (){var statearr_27069 = state_27042;
(statearr_27069[(22)] = inst_26997);

(statearr_27069[(20)] = inst_26984__$1);

(statearr_27069[(21)] = inst_26982__$1);

(statearr_27069[(10)] = inst_26983__$1);

(statearr_27069[(12)] = inst_26985__$1);

return statearr_27069;
})();
var statearr_27070_27141 = state_27042__$1;
(statearr_27070_27141[(2)] = null);

(statearr_27070_27141[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27043 === (40))){
var inst_27010 = (state_27042[(23)]);
var inst_27014 = done.call(null,null);
var inst_27015 = cljs.core.async.untap_STAR_.call(null,m,inst_27010);
var state_27042__$1 = (function (){var statearr_27071 = state_27042;
(statearr_27071[(24)] = inst_27014);

return statearr_27071;
})();
var statearr_27072_27142 = state_27042__$1;
(statearr_27072_27142[(2)] = inst_27015);

(statearr_27072_27142[(1)] = (41));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27043 === (33))){
var inst_27001 = (state_27042[(25)]);
var inst_27003 = cljs.core.chunked_seq_QMARK_.call(null,inst_27001);
var state_27042__$1 = state_27042;
if(inst_27003){
var statearr_27073_27143 = state_27042__$1;
(statearr_27073_27143[(1)] = (36));

} else {
var statearr_27074_27144 = state_27042__$1;
(statearr_27074_27144[(1)] = (37));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27043 === (13))){
var inst_26931 = (state_27042[(26)]);
var inst_26934 = cljs.core.async.close_BANG_.call(null,inst_26931);
var state_27042__$1 = state_27042;
var statearr_27075_27145 = state_27042__$1;
(statearr_27075_27145[(2)] = inst_26934);

(statearr_27075_27145[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27043 === (22))){
var inst_26954 = (state_27042[(8)]);
var inst_26957 = cljs.core.async.close_BANG_.call(null,inst_26954);
var state_27042__$1 = state_27042;
var statearr_27076_27146 = state_27042__$1;
(statearr_27076_27146[(2)] = inst_26957);

(statearr_27076_27146[(1)] = (24));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27043 === (36))){
var inst_27001 = (state_27042[(25)]);
var inst_27005 = cljs.core.chunk_first.call(null,inst_27001);
var inst_27006 = cljs.core.chunk_rest.call(null,inst_27001);
var inst_27007 = cljs.core.count.call(null,inst_27005);
var inst_26982 = inst_27006;
var inst_26983 = inst_27005;
var inst_26984 = inst_27007;
var inst_26985 = (0);
var state_27042__$1 = (function (){var statearr_27077 = state_27042;
(statearr_27077[(20)] = inst_26984);

(statearr_27077[(21)] = inst_26982);

(statearr_27077[(10)] = inst_26983);

(statearr_27077[(12)] = inst_26985);

return statearr_27077;
})();
var statearr_27078_27147 = state_27042__$1;
(statearr_27078_27147[(2)] = null);

(statearr_27078_27147[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27043 === (41))){
var inst_27001 = (state_27042[(25)]);
var inst_27017 = (state_27042[(2)]);
var inst_27018 = cljs.core.next.call(null,inst_27001);
var inst_26982 = inst_27018;
var inst_26983 = null;
var inst_26984 = (0);
var inst_26985 = (0);
var state_27042__$1 = (function (){var statearr_27079 = state_27042;
(statearr_27079[(20)] = inst_26984);

(statearr_27079[(21)] = inst_26982);

(statearr_27079[(10)] = inst_26983);

(statearr_27079[(27)] = inst_27017);

(statearr_27079[(12)] = inst_26985);

return statearr_27079;
})();
var statearr_27080_27148 = state_27042__$1;
(statearr_27080_27148[(2)] = null);

(statearr_27080_27148[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27043 === (43))){
var state_27042__$1 = state_27042;
var statearr_27081_27149 = state_27042__$1;
(statearr_27081_27149[(2)] = null);

(statearr_27081_27149[(1)] = (44));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27043 === (29))){
var inst_27026 = (state_27042[(2)]);
var state_27042__$1 = state_27042;
var statearr_27082_27150 = state_27042__$1;
(statearr_27082_27150[(2)] = inst_27026);

(statearr_27082_27150[(1)] = (26));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27043 === (44))){
var inst_27035 = (state_27042[(2)]);
var state_27042__$1 = (function (){var statearr_27083 = state_27042;
(statearr_27083[(28)] = inst_27035);

return statearr_27083;
})();
var statearr_27084_27151 = state_27042__$1;
(statearr_27084_27151[(2)] = null);

(statearr_27084_27151[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27043 === (6))){
var inst_26974 = (state_27042[(29)]);
var inst_26973 = cljs.core.deref.call(null,cs);
var inst_26974__$1 = cljs.core.keys.call(null,inst_26973);
var inst_26975 = cljs.core.count.call(null,inst_26974__$1);
var inst_26976 = cljs.core.reset_BANG_.call(null,dctr,inst_26975);
var inst_26981 = cljs.core.seq.call(null,inst_26974__$1);
var inst_26982 = inst_26981;
var inst_26983 = null;
var inst_26984 = (0);
var inst_26985 = (0);
var state_27042__$1 = (function (){var statearr_27085 = state_27042;
(statearr_27085[(29)] = inst_26974__$1);

(statearr_27085[(30)] = inst_26976);

(statearr_27085[(20)] = inst_26984);

(statearr_27085[(21)] = inst_26982);

(statearr_27085[(10)] = inst_26983);

(statearr_27085[(12)] = inst_26985);

return statearr_27085;
})();
var statearr_27086_27152 = state_27042__$1;
(statearr_27086_27152[(2)] = null);

(statearr_27086_27152[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27043 === (28))){
var inst_26982 = (state_27042[(21)]);
var inst_27001 = (state_27042[(25)]);
var inst_27001__$1 = cljs.core.seq.call(null,inst_26982);
var state_27042__$1 = (function (){var statearr_27087 = state_27042;
(statearr_27087[(25)] = inst_27001__$1);

return statearr_27087;
})();
if(inst_27001__$1){
var statearr_27088_27153 = state_27042__$1;
(statearr_27088_27153[(1)] = (33));

} else {
var statearr_27089_27154 = state_27042__$1;
(statearr_27089_27154[(1)] = (34));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27043 === (25))){
var inst_26984 = (state_27042[(20)]);
var inst_26985 = (state_27042[(12)]);
var inst_26987 = (inst_26985 < inst_26984);
var inst_26988 = inst_26987;
var state_27042__$1 = state_27042;
if(cljs.core.truth_(inst_26988)){
var statearr_27090_27155 = state_27042__$1;
(statearr_27090_27155[(1)] = (27));

} else {
var statearr_27091_27156 = state_27042__$1;
(statearr_27091_27156[(1)] = (28));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27043 === (34))){
var state_27042__$1 = state_27042;
var statearr_27092_27157 = state_27042__$1;
(statearr_27092_27157[(2)] = null);

(statearr_27092_27157[(1)] = (35));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27043 === (17))){
var state_27042__$1 = state_27042;
var statearr_27093_27158 = state_27042__$1;
(statearr_27093_27158[(2)] = null);

(statearr_27093_27158[(1)] = (18));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27043 === (3))){
var inst_27040 = (state_27042[(2)]);
var state_27042__$1 = state_27042;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_27042__$1,inst_27040);
} else {
if((state_val_27043 === (12))){
var inst_26969 = (state_27042[(2)]);
var state_27042__$1 = state_27042;
var statearr_27094_27159 = state_27042__$1;
(statearr_27094_27159[(2)] = inst_26969);

(statearr_27094_27159[(1)] = (9));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27043 === (2))){
var state_27042__$1 = state_27042;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_27042__$1,(4),ch);
} else {
if((state_val_27043 === (23))){
var state_27042__$1 = state_27042;
var statearr_27095_27160 = state_27042__$1;
(statearr_27095_27160[(2)] = null);

(statearr_27095_27160[(1)] = (24));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27043 === (35))){
var inst_27024 = (state_27042[(2)]);
var state_27042__$1 = state_27042;
var statearr_27096_27161 = state_27042__$1;
(statearr_27096_27161[(2)] = inst_27024);

(statearr_27096_27161[(1)] = (29));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27043 === (19))){
var inst_26941 = (state_27042[(7)]);
var inst_26945 = cljs.core.chunk_first.call(null,inst_26941);
var inst_26946 = cljs.core.chunk_rest.call(null,inst_26941);
var inst_26947 = cljs.core.count.call(null,inst_26945);
var inst_26919 = inst_26946;
var inst_26920 = inst_26945;
var inst_26921 = inst_26947;
var inst_26922 = (0);
var state_27042__$1 = (function (){var statearr_27097 = state_27042;
(statearr_27097[(13)] = inst_26920);

(statearr_27097[(14)] = inst_26919);

(statearr_27097[(16)] = inst_26922);

(statearr_27097[(17)] = inst_26921);

return statearr_27097;
})();
var statearr_27098_27162 = state_27042__$1;
(statearr_27098_27162[(2)] = null);

(statearr_27098_27162[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27043 === (11))){
var inst_26919 = (state_27042[(14)]);
var inst_26941 = (state_27042[(7)]);
var inst_26941__$1 = cljs.core.seq.call(null,inst_26919);
var state_27042__$1 = (function (){var statearr_27099 = state_27042;
(statearr_27099[(7)] = inst_26941__$1);

return statearr_27099;
})();
if(inst_26941__$1){
var statearr_27100_27163 = state_27042__$1;
(statearr_27100_27163[(1)] = (16));

} else {
var statearr_27101_27164 = state_27042__$1;
(statearr_27101_27164[(1)] = (17));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27043 === (9))){
var inst_26971 = (state_27042[(2)]);
var state_27042__$1 = state_27042;
var statearr_27102_27165 = state_27042__$1;
(statearr_27102_27165[(2)] = inst_26971);

(statearr_27102_27165[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27043 === (5))){
var inst_26917 = cljs.core.deref.call(null,cs);
var inst_26918 = cljs.core.seq.call(null,inst_26917);
var inst_26919 = inst_26918;
var inst_26920 = null;
var inst_26921 = (0);
var inst_26922 = (0);
var state_27042__$1 = (function (){var statearr_27103 = state_27042;
(statearr_27103[(13)] = inst_26920);

(statearr_27103[(14)] = inst_26919);

(statearr_27103[(16)] = inst_26922);

(statearr_27103[(17)] = inst_26921);

return statearr_27103;
})();
var statearr_27104_27166 = state_27042__$1;
(statearr_27104_27166[(2)] = null);

(statearr_27104_27166[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27043 === (14))){
var state_27042__$1 = state_27042;
var statearr_27105_27167 = state_27042__$1;
(statearr_27105_27167[(2)] = null);

(statearr_27105_27167[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27043 === (45))){
var inst_27032 = (state_27042[(2)]);
var state_27042__$1 = state_27042;
var statearr_27106_27168 = state_27042__$1;
(statearr_27106_27168[(2)] = inst_27032);

(statearr_27106_27168[(1)] = (44));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27043 === (26))){
var inst_26974 = (state_27042[(29)]);
var inst_27028 = (state_27042[(2)]);
var inst_27029 = cljs.core.seq.call(null,inst_26974);
var state_27042__$1 = (function (){var statearr_27107 = state_27042;
(statearr_27107[(31)] = inst_27028);

return statearr_27107;
})();
if(inst_27029){
var statearr_27108_27169 = state_27042__$1;
(statearr_27108_27169[(1)] = (42));

} else {
var statearr_27109_27170 = state_27042__$1;
(statearr_27109_27170[(1)] = (43));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27043 === (16))){
var inst_26941 = (state_27042[(7)]);
var inst_26943 = cljs.core.chunked_seq_QMARK_.call(null,inst_26941);
var state_27042__$1 = state_27042;
if(inst_26943){
var statearr_27110_27171 = state_27042__$1;
(statearr_27110_27171[(1)] = (19));

} else {
var statearr_27111_27172 = state_27042__$1;
(statearr_27111_27172[(1)] = (20));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27043 === (38))){
var inst_27021 = (state_27042[(2)]);
var state_27042__$1 = state_27042;
var statearr_27112_27173 = state_27042__$1;
(statearr_27112_27173[(2)] = inst_27021);

(statearr_27112_27173[(1)] = (35));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27043 === (30))){
var state_27042__$1 = state_27042;
var statearr_27113_27174 = state_27042__$1;
(statearr_27113_27174[(2)] = null);

(statearr_27113_27174[(1)] = (32));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27043 === (10))){
var inst_26920 = (state_27042[(13)]);
var inst_26922 = (state_27042[(16)]);
var inst_26930 = cljs.core._nth.call(null,inst_26920,inst_26922);
var inst_26931 = cljs.core.nth.call(null,inst_26930,(0),null);
var inst_26932 = cljs.core.nth.call(null,inst_26930,(1),null);
var state_27042__$1 = (function (){var statearr_27114 = state_27042;
(statearr_27114[(26)] = inst_26931);

return statearr_27114;
})();
if(cljs.core.truth_(inst_26932)){
var statearr_27115_27175 = state_27042__$1;
(statearr_27115_27175[(1)] = (13));

} else {
var statearr_27116_27176 = state_27042__$1;
(statearr_27116_27176[(1)] = (14));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27043 === (18))){
var inst_26967 = (state_27042[(2)]);
var state_27042__$1 = state_27042;
var statearr_27117_27177 = state_27042__$1;
(statearr_27117_27177[(2)] = inst_26967);

(statearr_27117_27177[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27043 === (42))){
var state_27042__$1 = state_27042;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_27042__$1,(45),dchan);
} else {
if((state_val_27043 === (37))){
var inst_26910 = (state_27042[(9)]);
var inst_27010 = (state_27042[(23)]);
var inst_27001 = (state_27042[(25)]);
var inst_27010__$1 = cljs.core.first.call(null,inst_27001);
var inst_27011 = cljs.core.async.put_BANG_.call(null,inst_27010__$1,inst_26910,done);
var state_27042__$1 = (function (){var statearr_27118 = state_27042;
(statearr_27118[(23)] = inst_27010__$1);

return statearr_27118;
})();
if(cljs.core.truth_(inst_27011)){
var statearr_27119_27178 = state_27042__$1;
(statearr_27119_27178[(1)] = (39));

} else {
var statearr_27120_27179 = state_27042__$1;
(statearr_27120_27179[(1)] = (40));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27043 === (8))){
var inst_26922 = (state_27042[(16)]);
var inst_26921 = (state_27042[(17)]);
var inst_26924 = (inst_26922 < inst_26921);
var inst_26925 = inst_26924;
var state_27042__$1 = state_27042;
if(cljs.core.truth_(inst_26925)){
var statearr_27121_27180 = state_27042__$1;
(statearr_27121_27180[(1)] = (10));

} else {
var statearr_27122_27181 = state_27042__$1;
(statearr_27122_27181[(1)] = (11));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__26309__auto___27127,cs,m,dchan,dctr,done))
;
return ((function (switch__26214__auto__,c__26309__auto___27127,cs,m,dchan,dctr,done){
return (function() {
var cljs$core$async$mult_$_state_machine__26215__auto__ = null;
var cljs$core$async$mult_$_state_machine__26215__auto____0 = (function (){
var statearr_27123 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_27123[(0)] = cljs$core$async$mult_$_state_machine__26215__auto__);

(statearr_27123[(1)] = (1));

return statearr_27123;
});
var cljs$core$async$mult_$_state_machine__26215__auto____1 = (function (state_27042){
while(true){
var ret_value__26216__auto__ = (function (){try{while(true){
var result__26217__auto__ = switch__26214__auto__.call(null,state_27042);
if(cljs.core.keyword_identical_QMARK_.call(null,result__26217__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__26217__auto__;
}
break;
}
}catch (e27124){if((e27124 instanceof Object)){
var ex__26218__auto__ = e27124;
var statearr_27125_27182 = state_27042;
(statearr_27125_27182[(5)] = ex__26218__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_27042);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e27124;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__26216__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__27183 = state_27042;
state_27042 = G__27183;
continue;
} else {
return ret_value__26216__auto__;
}
break;
}
});
cljs$core$async$mult_$_state_machine__26215__auto__ = function(state_27042){
switch(arguments.length){
case 0:
return cljs$core$async$mult_$_state_machine__26215__auto____0.call(this);
case 1:
return cljs$core$async$mult_$_state_machine__26215__auto____1.call(this,state_27042);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$mult_$_state_machine__26215__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$mult_$_state_machine__26215__auto____0;
cljs$core$async$mult_$_state_machine__26215__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$mult_$_state_machine__26215__auto____1;
return cljs$core$async$mult_$_state_machine__26215__auto__;
})()
;})(switch__26214__auto__,c__26309__auto___27127,cs,m,dchan,dctr,done))
})();
var state__26311__auto__ = (function (){var statearr_27126 = f__26310__auto__.call(null);
(statearr_27126[(6)] = c__26309__auto___27127);

return statearr_27126;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__26311__auto__);
});})(c__26309__auto___27127,cs,m,dchan,dctr,done))
);


return m;
});
/**
 * Copies the mult source onto the supplied channel.
 * 
 *   By default the channel will be closed when the source closes,
 *   but can be determined by the close? parameter.
 */
cljs.core.async.tap = (function cljs$core$async$tap(var_args){
var G__27185 = arguments.length;
switch (G__27185) {
case 2:
return cljs.core.async.tap.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.tap.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.tap.cljs$core$IFn$_invoke$arity$2 = (function (mult,ch){
return cljs.core.async.tap.call(null,mult,ch,true);
});

cljs.core.async.tap.cljs$core$IFn$_invoke$arity$3 = (function (mult,ch,close_QMARK_){
cljs.core.async.tap_STAR_.call(null,mult,ch,close_QMARK_);

return ch;
});

cljs.core.async.tap.cljs$lang$maxFixedArity = 3;

/**
 * Disconnects a target channel from a mult
 */
cljs.core.async.untap = (function cljs$core$async$untap(mult,ch){
return cljs.core.async.untap_STAR_.call(null,mult,ch);
});
/**
 * Disconnects all target channels from a mult
 */
cljs.core.async.untap_all = (function cljs$core$async$untap_all(mult){
return cljs.core.async.untap_all_STAR_.call(null,mult);
});

/**
 * @interface
 */
cljs.core.async.Mix = function(){};

cljs.core.async.admix_STAR_ = (function cljs$core$async$admix_STAR_(m,ch){
if((((!((m == null)))) && ((!((m.cljs$core$async$Mix$admix_STAR_$arity$2 == null)))))){
return m.cljs$core$async$Mix$admix_STAR_$arity$2(m,ch);
} else {
var x__4433__auto__ = (((m == null))?null:m);
var m__4434__auto__ = (cljs.core.async.admix_STAR_[goog.typeOf(x__4433__auto__)]);
if((!((m__4434__auto__ == null)))){
return m__4434__auto__.call(null,m,ch);
} else {
var m__4431__auto__ = (cljs.core.async.admix_STAR_["_"]);
if((!((m__4431__auto__ == null)))){
return m__4431__auto__.call(null,m,ch);
} else {
throw cljs.core.missing_protocol.call(null,"Mix.admix*",m);
}
}
}
});

cljs.core.async.unmix_STAR_ = (function cljs$core$async$unmix_STAR_(m,ch){
if((((!((m == null)))) && ((!((m.cljs$core$async$Mix$unmix_STAR_$arity$2 == null)))))){
return m.cljs$core$async$Mix$unmix_STAR_$arity$2(m,ch);
} else {
var x__4433__auto__ = (((m == null))?null:m);
var m__4434__auto__ = (cljs.core.async.unmix_STAR_[goog.typeOf(x__4433__auto__)]);
if((!((m__4434__auto__ == null)))){
return m__4434__auto__.call(null,m,ch);
} else {
var m__4431__auto__ = (cljs.core.async.unmix_STAR_["_"]);
if((!((m__4431__auto__ == null)))){
return m__4431__auto__.call(null,m,ch);
} else {
throw cljs.core.missing_protocol.call(null,"Mix.unmix*",m);
}
}
}
});

cljs.core.async.unmix_all_STAR_ = (function cljs$core$async$unmix_all_STAR_(m){
if((((!((m == null)))) && ((!((m.cljs$core$async$Mix$unmix_all_STAR_$arity$1 == null)))))){
return m.cljs$core$async$Mix$unmix_all_STAR_$arity$1(m);
} else {
var x__4433__auto__ = (((m == null))?null:m);
var m__4434__auto__ = (cljs.core.async.unmix_all_STAR_[goog.typeOf(x__4433__auto__)]);
if((!((m__4434__auto__ == null)))){
return m__4434__auto__.call(null,m);
} else {
var m__4431__auto__ = (cljs.core.async.unmix_all_STAR_["_"]);
if((!((m__4431__auto__ == null)))){
return m__4431__auto__.call(null,m);
} else {
throw cljs.core.missing_protocol.call(null,"Mix.unmix-all*",m);
}
}
}
});

cljs.core.async.toggle_STAR_ = (function cljs$core$async$toggle_STAR_(m,state_map){
if((((!((m == null)))) && ((!((m.cljs$core$async$Mix$toggle_STAR_$arity$2 == null)))))){
return m.cljs$core$async$Mix$toggle_STAR_$arity$2(m,state_map);
} else {
var x__4433__auto__ = (((m == null))?null:m);
var m__4434__auto__ = (cljs.core.async.toggle_STAR_[goog.typeOf(x__4433__auto__)]);
if((!((m__4434__auto__ == null)))){
return m__4434__auto__.call(null,m,state_map);
} else {
var m__4431__auto__ = (cljs.core.async.toggle_STAR_["_"]);
if((!((m__4431__auto__ == null)))){
return m__4431__auto__.call(null,m,state_map);
} else {
throw cljs.core.missing_protocol.call(null,"Mix.toggle*",m);
}
}
}
});

cljs.core.async.solo_mode_STAR_ = (function cljs$core$async$solo_mode_STAR_(m,mode){
if((((!((m == null)))) && ((!((m.cljs$core$async$Mix$solo_mode_STAR_$arity$2 == null)))))){
return m.cljs$core$async$Mix$solo_mode_STAR_$arity$2(m,mode);
} else {
var x__4433__auto__ = (((m == null))?null:m);
var m__4434__auto__ = (cljs.core.async.solo_mode_STAR_[goog.typeOf(x__4433__auto__)]);
if((!((m__4434__auto__ == null)))){
return m__4434__auto__.call(null,m,mode);
} else {
var m__4431__auto__ = (cljs.core.async.solo_mode_STAR_["_"]);
if((!((m__4431__auto__ == null)))){
return m__4431__auto__.call(null,m,mode);
} else {
throw cljs.core.missing_protocol.call(null,"Mix.solo-mode*",m);
}
}
}
});

cljs.core.async.ioc_alts_BANG_ = (function cljs$core$async$ioc_alts_BANG_(var_args){
var args__4736__auto__ = [];
var len__4730__auto___27197 = arguments.length;
var i__4731__auto___27198 = (0);
while(true){
if((i__4731__auto___27198 < len__4730__auto___27197)){
args__4736__auto__.push((arguments[i__4731__auto___27198]));

var G__27199 = (i__4731__auto___27198 + (1));
i__4731__auto___27198 = G__27199;
continue;
} else {
}
break;
}

var argseq__4737__auto__ = ((((3) < args__4736__auto__.length))?(new cljs.core.IndexedSeq(args__4736__auto__.slice((3)),(0),null)):null);
return cljs.core.async.ioc_alts_BANG_.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),argseq__4737__auto__);
});

cljs.core.async.ioc_alts_BANG_.cljs$core$IFn$_invoke$arity$variadic = (function (state,cont_block,ports,p__27191){
var map__27192 = p__27191;
var map__27192__$1 = (((((!((map__27192 == null))))?(((((map__27192.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__27192.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__27192):map__27192);
var opts = map__27192__$1;
var statearr_27194_27200 = state;
(statearr_27194_27200[(1)] = cont_block);


var temp__5720__auto__ = cljs.core.async.do_alts.call(null,((function (map__27192,map__27192__$1,opts){
return (function (val){
var statearr_27195_27201 = state;
(statearr_27195_27201[(2)] = val);


return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state);
});})(map__27192,map__27192__$1,opts))
,ports,opts);
if(cljs.core.truth_(temp__5720__auto__)){
var cb = temp__5720__auto__;
var statearr_27196_27202 = state;
(statearr_27196_27202[(2)] = cljs.core.deref.call(null,cb));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
});

cljs.core.async.ioc_alts_BANG_.cljs$lang$maxFixedArity = (3);

/** @this {Function} */
cljs.core.async.ioc_alts_BANG_.cljs$lang$applyTo = (function (seq27187){
var G__27188 = cljs.core.first.call(null,seq27187);
var seq27187__$1 = cljs.core.next.call(null,seq27187);
var G__27189 = cljs.core.first.call(null,seq27187__$1);
var seq27187__$2 = cljs.core.next.call(null,seq27187__$1);
var G__27190 = cljs.core.first.call(null,seq27187__$2);
var seq27187__$3 = cljs.core.next.call(null,seq27187__$2);
var self__4717__auto__ = this;
return self__4717__auto__.cljs$core$IFn$_invoke$arity$variadic(G__27188,G__27189,G__27190,seq27187__$3);
});

/**
 * Creates and returns a mix of one or more input channels which will
 *   be put on the supplied out channel. Input sources can be added to
 *   the mix with 'admix', and removed with 'unmix'. A mix supports
 *   soloing, muting and pausing multiple inputs atomically using
 *   'toggle', and can solo using either muting or pausing as determined
 *   by 'solo-mode'.
 * 
 *   Each channel can have zero or more boolean modes set via 'toggle':
 * 
 *   :solo - when true, only this (ond other soloed) channel(s) will appear
 *        in the mix output channel. :mute and :pause states of soloed
 *        channels are ignored. If solo-mode is :mute, non-soloed
 *        channels are muted, if :pause, non-soloed channels are
 *        paused.
 * 
 *   :mute - muted channels will have their contents consumed but not included in the mix
 *   :pause - paused channels will not have their contents consumed (and thus also not included in the mix)
 */
cljs.core.async.mix = (function cljs$core$async$mix(out){
var cs = cljs.core.atom.call(null,cljs.core.PersistentArrayMap.EMPTY);
var solo_modes = new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"pause","pause",-2095325672),null,new cljs.core.Keyword(null,"mute","mute",1151223646),null], null), null);
var attrs = cljs.core.conj.call(null,solo_modes,new cljs.core.Keyword(null,"solo","solo",-316350075));
var solo_mode = cljs.core.atom.call(null,new cljs.core.Keyword(null,"mute","mute",1151223646));
var change = cljs.core.async.chan.call(null);
var changed = ((function (cs,solo_modes,attrs,solo_mode,change){
return (function (){
return cljs.core.async.put_BANG_.call(null,change,true);
});})(cs,solo_modes,attrs,solo_mode,change))
;
var pick = ((function (cs,solo_modes,attrs,solo_mode,change,changed){
return (function (attr,chs){
return cljs.core.reduce_kv.call(null,((function (cs,solo_modes,attrs,solo_mode,change,changed){
return (function (ret,c,v){
if(cljs.core.truth_(attr.call(null,v))){
return cljs.core.conj.call(null,ret,c);
} else {
return ret;
}
});})(cs,solo_modes,attrs,solo_mode,change,changed))
,cljs.core.PersistentHashSet.EMPTY,chs);
});})(cs,solo_modes,attrs,solo_mode,change,changed))
;
var calc_state = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick){
return (function (){
var chs = cljs.core.deref.call(null,cs);
var mode = cljs.core.deref.call(null,solo_mode);
var solos = pick.call(null,new cljs.core.Keyword(null,"solo","solo",-316350075),chs);
var pauses = pick.call(null,new cljs.core.Keyword(null,"pause","pause",-2095325672),chs);
return new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"solos","solos",1441458643),solos,new cljs.core.Keyword(null,"mutes","mutes",1068806309),pick.call(null,new cljs.core.Keyword(null,"mute","mute",1151223646),chs),new cljs.core.Keyword(null,"reads","reads",-1215067361),cljs.core.conj.call(null,((((cljs.core._EQ_.call(null,mode,new cljs.core.Keyword(null,"pause","pause",-2095325672))) && ((!(cljs.core.empty_QMARK_.call(null,solos))))))?cljs.core.vec.call(null,solos):cljs.core.vec.call(null,cljs.core.remove.call(null,pauses,cljs.core.keys.call(null,chs)))),change)], null);
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick))
;
var m = (function (){
if((typeof cljs !== 'undefined') && (typeof cljs.core !== 'undefined') && (typeof cljs.core.async !== 'undefined') && (typeof cljs.core.async.t_cljs$core$async27203 !== 'undefined')){
} else {

/**
* @constructor
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.async.Mix}
 * @implements {cljs.core.async.Mux}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async27203 = (function (change,solo_mode,pick,cs,calc_state,out,changed,solo_modes,attrs,meta27204){
this.change = change;
this.solo_mode = solo_mode;
this.pick = pick;
this.cs = cs;
this.calc_state = calc_state;
this.out = out;
this.changed = changed;
this.solo_modes = solo_modes;
this.attrs = attrs;
this.meta27204 = meta27204;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
cljs.core.async.t_cljs$core$async27203.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (_27205,meta27204__$1){
var self__ = this;
var _27205__$1 = this;
return (new cljs.core.async.t_cljs$core$async27203(self__.change,self__.solo_mode,self__.pick,self__.cs,self__.calc_state,self__.out,self__.changed,self__.solo_modes,self__.attrs,meta27204__$1));
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

cljs.core.async.t_cljs$core$async27203.prototype.cljs$core$IMeta$_meta$arity$1 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (_27205){
var self__ = this;
var _27205__$1 = this;
return self__.meta27204;
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

cljs.core.async.t_cljs$core$async27203.prototype.cljs$core$async$Mux$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async27203.prototype.cljs$core$async$Mux$muxch_STAR_$arity$1 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (_){
var self__ = this;
var ___$1 = this;
return self__.out;
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

cljs.core.async.t_cljs$core$async27203.prototype.cljs$core$async$Mix$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async27203.prototype.cljs$core$async$Mix$admix_STAR_$arity$2 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (_,ch){
var self__ = this;
var ___$1 = this;
cljs.core.swap_BANG_.call(null,self__.cs,cljs.core.assoc,ch,cljs.core.PersistentArrayMap.EMPTY);

return self__.changed.call(null);
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

cljs.core.async.t_cljs$core$async27203.prototype.cljs$core$async$Mix$unmix_STAR_$arity$2 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (_,ch){
var self__ = this;
var ___$1 = this;
cljs.core.swap_BANG_.call(null,self__.cs,cljs.core.dissoc,ch);

return self__.changed.call(null);
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

cljs.core.async.t_cljs$core$async27203.prototype.cljs$core$async$Mix$unmix_all_STAR_$arity$1 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (_){
var self__ = this;
var ___$1 = this;
cljs.core.reset_BANG_.call(null,self__.cs,cljs.core.PersistentArrayMap.EMPTY);

return self__.changed.call(null);
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

cljs.core.async.t_cljs$core$async27203.prototype.cljs$core$async$Mix$toggle_STAR_$arity$2 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (_,state_map){
var self__ = this;
var ___$1 = this;
cljs.core.swap_BANG_.call(null,self__.cs,cljs.core.partial.call(null,cljs.core.merge_with,cljs.core.merge),state_map);

return self__.changed.call(null);
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

cljs.core.async.t_cljs$core$async27203.prototype.cljs$core$async$Mix$solo_mode_STAR_$arity$2 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (_,mode){
var self__ = this;
var ___$1 = this;
if(cljs.core.truth_(self__.solo_modes.call(null,mode))){
} else {
throw (new Error(["Assert failed: ",["mode must be one of: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(self__.solo_modes)].join(''),"\n","(solo-modes mode)"].join('')));
}

cljs.core.reset_BANG_.call(null,self__.solo_mode,mode);

return self__.changed.call(null);
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

cljs.core.async.t_cljs$core$async27203.getBasis = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (){
return new cljs.core.PersistentVector(null, 10, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"change","change",477485025,null),new cljs.core.Symbol(null,"solo-mode","solo-mode",2031788074,null),new cljs.core.Symbol(null,"pick","pick",1300068175,null),new cljs.core.Symbol(null,"cs","cs",-117024463,null),new cljs.core.Symbol(null,"calc-state","calc-state",-349968968,null),new cljs.core.Symbol(null,"out","out",729986010,null),new cljs.core.Symbol(null,"changed","changed",-2083710852,null),new cljs.core.Symbol(null,"solo-modes","solo-modes",882180540,null),new cljs.core.Symbol(null,"attrs","attrs",-450137186,null),new cljs.core.Symbol(null,"meta27204","meta27204",-1563281429,null)], null);
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

cljs.core.async.t_cljs$core$async27203.cljs$lang$type = true;

cljs.core.async.t_cljs$core$async27203.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async27203";

cljs.core.async.t_cljs$core$async27203.cljs$lang$ctorPrWriter = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (this__4374__auto__,writer__4375__auto__,opt__4376__auto__){
return cljs.core._write.call(null,writer__4375__auto__,"cljs.core.async/t_cljs$core$async27203");
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async27203.
 */
cljs.core.async.__GT_t_cljs$core$async27203 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function cljs$core$async$mix_$___GT_t_cljs$core$async27203(change__$1,solo_mode__$1,pick__$1,cs__$1,calc_state__$1,out__$1,changed__$1,solo_modes__$1,attrs__$1,meta27204){
return (new cljs.core.async.t_cljs$core$async27203(change__$1,solo_mode__$1,pick__$1,cs__$1,calc_state__$1,out__$1,changed__$1,solo_modes__$1,attrs__$1,meta27204));
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

}

return (new cljs.core.async.t_cljs$core$async27203(change,solo_mode,pick,cs,calc_state,out,changed,solo_modes,attrs,cljs.core.PersistentArrayMap.EMPTY));
})()
;
var c__26309__auto___27367 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__26309__auto___27367,cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state,m){
return (function (){
var f__26310__auto__ = (function (){var switch__26214__auto__ = ((function (c__26309__auto___27367,cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state,m){
return (function (state_27307){
var state_val_27308 = (state_27307[(1)]);
if((state_val_27308 === (7))){
var inst_27222 = (state_27307[(2)]);
var state_27307__$1 = state_27307;
var statearr_27309_27368 = state_27307__$1;
(statearr_27309_27368[(2)] = inst_27222);

(statearr_27309_27368[(1)] = (4));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27308 === (20))){
var inst_27234 = (state_27307[(7)]);
var state_27307__$1 = state_27307;
var statearr_27310_27369 = state_27307__$1;
(statearr_27310_27369[(2)] = inst_27234);

(statearr_27310_27369[(1)] = (21));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27308 === (27))){
var state_27307__$1 = state_27307;
var statearr_27311_27370 = state_27307__$1;
(statearr_27311_27370[(2)] = null);

(statearr_27311_27370[(1)] = (28));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27308 === (1))){
var inst_27209 = (state_27307[(8)]);
var inst_27209__$1 = calc_state.call(null);
var inst_27211 = (inst_27209__$1 == null);
var inst_27212 = cljs.core.not.call(null,inst_27211);
var state_27307__$1 = (function (){var statearr_27312 = state_27307;
(statearr_27312[(8)] = inst_27209__$1);

return statearr_27312;
})();
if(inst_27212){
var statearr_27313_27371 = state_27307__$1;
(statearr_27313_27371[(1)] = (2));

} else {
var statearr_27314_27372 = state_27307__$1;
(statearr_27314_27372[(1)] = (3));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27308 === (24))){
var inst_27258 = (state_27307[(9)]);
var inst_27281 = (state_27307[(10)]);
var inst_27267 = (state_27307[(11)]);
var inst_27281__$1 = inst_27258.call(null,inst_27267);
var state_27307__$1 = (function (){var statearr_27315 = state_27307;
(statearr_27315[(10)] = inst_27281__$1);

return statearr_27315;
})();
if(cljs.core.truth_(inst_27281__$1)){
var statearr_27316_27373 = state_27307__$1;
(statearr_27316_27373[(1)] = (29));

} else {
var statearr_27317_27374 = state_27307__$1;
(statearr_27317_27374[(1)] = (30));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27308 === (4))){
var inst_27225 = (state_27307[(2)]);
var state_27307__$1 = state_27307;
if(cljs.core.truth_(inst_27225)){
var statearr_27318_27375 = state_27307__$1;
(statearr_27318_27375[(1)] = (8));

} else {
var statearr_27319_27376 = state_27307__$1;
(statearr_27319_27376[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27308 === (15))){
var inst_27252 = (state_27307[(2)]);
var state_27307__$1 = state_27307;
if(cljs.core.truth_(inst_27252)){
var statearr_27320_27377 = state_27307__$1;
(statearr_27320_27377[(1)] = (19));

} else {
var statearr_27321_27378 = state_27307__$1;
(statearr_27321_27378[(1)] = (20));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27308 === (21))){
var inst_27257 = (state_27307[(12)]);
var inst_27257__$1 = (state_27307[(2)]);
var inst_27258 = cljs.core.get.call(null,inst_27257__$1,new cljs.core.Keyword(null,"solos","solos",1441458643));
var inst_27259 = cljs.core.get.call(null,inst_27257__$1,new cljs.core.Keyword(null,"mutes","mutes",1068806309));
var inst_27260 = cljs.core.get.call(null,inst_27257__$1,new cljs.core.Keyword(null,"reads","reads",-1215067361));
var state_27307__$1 = (function (){var statearr_27322 = state_27307;
(statearr_27322[(13)] = inst_27259);

(statearr_27322[(12)] = inst_27257__$1);

(statearr_27322[(9)] = inst_27258);

return statearr_27322;
})();
return cljs.core.async.ioc_alts_BANG_.call(null,state_27307__$1,(22),inst_27260);
} else {
if((state_val_27308 === (31))){
var inst_27289 = (state_27307[(2)]);
var state_27307__$1 = state_27307;
if(cljs.core.truth_(inst_27289)){
var statearr_27323_27379 = state_27307__$1;
(statearr_27323_27379[(1)] = (32));

} else {
var statearr_27324_27380 = state_27307__$1;
(statearr_27324_27380[(1)] = (33));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27308 === (32))){
var inst_27266 = (state_27307[(14)]);
var state_27307__$1 = state_27307;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_27307__$1,(35),out,inst_27266);
} else {
if((state_val_27308 === (33))){
var inst_27257 = (state_27307[(12)]);
var inst_27234 = inst_27257;
var state_27307__$1 = (function (){var statearr_27325 = state_27307;
(statearr_27325[(7)] = inst_27234);

return statearr_27325;
})();
var statearr_27326_27381 = state_27307__$1;
(statearr_27326_27381[(2)] = null);

(statearr_27326_27381[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27308 === (13))){
var inst_27234 = (state_27307[(7)]);
var inst_27241 = inst_27234.cljs$lang$protocol_mask$partition0$;
var inst_27242 = (inst_27241 & (64));
var inst_27243 = inst_27234.cljs$core$ISeq$;
var inst_27244 = (cljs.core.PROTOCOL_SENTINEL === inst_27243);
var inst_27245 = ((inst_27242) || (inst_27244));
var state_27307__$1 = state_27307;
if(cljs.core.truth_(inst_27245)){
var statearr_27327_27382 = state_27307__$1;
(statearr_27327_27382[(1)] = (16));

} else {
var statearr_27328_27383 = state_27307__$1;
(statearr_27328_27383[(1)] = (17));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27308 === (22))){
var inst_27266 = (state_27307[(14)]);
var inst_27267 = (state_27307[(11)]);
var inst_27265 = (state_27307[(2)]);
var inst_27266__$1 = cljs.core.nth.call(null,inst_27265,(0),null);
var inst_27267__$1 = cljs.core.nth.call(null,inst_27265,(1),null);
var inst_27268 = (inst_27266__$1 == null);
var inst_27269 = cljs.core._EQ_.call(null,inst_27267__$1,change);
var inst_27270 = ((inst_27268) || (inst_27269));
var state_27307__$1 = (function (){var statearr_27329 = state_27307;
(statearr_27329[(14)] = inst_27266__$1);

(statearr_27329[(11)] = inst_27267__$1);

return statearr_27329;
})();
if(cljs.core.truth_(inst_27270)){
var statearr_27330_27384 = state_27307__$1;
(statearr_27330_27384[(1)] = (23));

} else {
var statearr_27331_27385 = state_27307__$1;
(statearr_27331_27385[(1)] = (24));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27308 === (36))){
var inst_27257 = (state_27307[(12)]);
var inst_27234 = inst_27257;
var state_27307__$1 = (function (){var statearr_27332 = state_27307;
(statearr_27332[(7)] = inst_27234);

return statearr_27332;
})();
var statearr_27333_27386 = state_27307__$1;
(statearr_27333_27386[(2)] = null);

(statearr_27333_27386[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27308 === (29))){
var inst_27281 = (state_27307[(10)]);
var state_27307__$1 = state_27307;
var statearr_27334_27387 = state_27307__$1;
(statearr_27334_27387[(2)] = inst_27281);

(statearr_27334_27387[(1)] = (31));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27308 === (6))){
var state_27307__$1 = state_27307;
var statearr_27335_27388 = state_27307__$1;
(statearr_27335_27388[(2)] = false);

(statearr_27335_27388[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27308 === (28))){
var inst_27277 = (state_27307[(2)]);
var inst_27278 = calc_state.call(null);
var inst_27234 = inst_27278;
var state_27307__$1 = (function (){var statearr_27336 = state_27307;
(statearr_27336[(15)] = inst_27277);

(statearr_27336[(7)] = inst_27234);

return statearr_27336;
})();
var statearr_27337_27389 = state_27307__$1;
(statearr_27337_27389[(2)] = null);

(statearr_27337_27389[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27308 === (25))){
var inst_27303 = (state_27307[(2)]);
var state_27307__$1 = state_27307;
var statearr_27338_27390 = state_27307__$1;
(statearr_27338_27390[(2)] = inst_27303);

(statearr_27338_27390[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27308 === (34))){
var inst_27301 = (state_27307[(2)]);
var state_27307__$1 = state_27307;
var statearr_27339_27391 = state_27307__$1;
(statearr_27339_27391[(2)] = inst_27301);

(statearr_27339_27391[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27308 === (17))){
var state_27307__$1 = state_27307;
var statearr_27340_27392 = state_27307__$1;
(statearr_27340_27392[(2)] = false);

(statearr_27340_27392[(1)] = (18));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27308 === (3))){
var state_27307__$1 = state_27307;
var statearr_27341_27393 = state_27307__$1;
(statearr_27341_27393[(2)] = false);

(statearr_27341_27393[(1)] = (4));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27308 === (12))){
var inst_27305 = (state_27307[(2)]);
var state_27307__$1 = state_27307;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_27307__$1,inst_27305);
} else {
if((state_val_27308 === (2))){
var inst_27209 = (state_27307[(8)]);
var inst_27214 = inst_27209.cljs$lang$protocol_mask$partition0$;
var inst_27215 = (inst_27214 & (64));
var inst_27216 = inst_27209.cljs$core$ISeq$;
var inst_27217 = (cljs.core.PROTOCOL_SENTINEL === inst_27216);
var inst_27218 = ((inst_27215) || (inst_27217));
var state_27307__$1 = state_27307;
if(cljs.core.truth_(inst_27218)){
var statearr_27342_27394 = state_27307__$1;
(statearr_27342_27394[(1)] = (5));

} else {
var statearr_27343_27395 = state_27307__$1;
(statearr_27343_27395[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27308 === (23))){
var inst_27266 = (state_27307[(14)]);
var inst_27272 = (inst_27266 == null);
var state_27307__$1 = state_27307;
if(cljs.core.truth_(inst_27272)){
var statearr_27344_27396 = state_27307__$1;
(statearr_27344_27396[(1)] = (26));

} else {
var statearr_27345_27397 = state_27307__$1;
(statearr_27345_27397[(1)] = (27));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27308 === (35))){
var inst_27292 = (state_27307[(2)]);
var state_27307__$1 = state_27307;
if(cljs.core.truth_(inst_27292)){
var statearr_27346_27398 = state_27307__$1;
(statearr_27346_27398[(1)] = (36));

} else {
var statearr_27347_27399 = state_27307__$1;
(statearr_27347_27399[(1)] = (37));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27308 === (19))){
var inst_27234 = (state_27307[(7)]);
var inst_27254 = cljs.core.apply.call(null,cljs.core.hash_map,inst_27234);
var state_27307__$1 = state_27307;
var statearr_27348_27400 = state_27307__$1;
(statearr_27348_27400[(2)] = inst_27254);

(statearr_27348_27400[(1)] = (21));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27308 === (11))){
var inst_27234 = (state_27307[(7)]);
var inst_27238 = (inst_27234 == null);
var inst_27239 = cljs.core.not.call(null,inst_27238);
var state_27307__$1 = state_27307;
if(inst_27239){
var statearr_27349_27401 = state_27307__$1;
(statearr_27349_27401[(1)] = (13));

} else {
var statearr_27350_27402 = state_27307__$1;
(statearr_27350_27402[(1)] = (14));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27308 === (9))){
var inst_27209 = (state_27307[(8)]);
var state_27307__$1 = state_27307;
var statearr_27351_27403 = state_27307__$1;
(statearr_27351_27403[(2)] = inst_27209);

(statearr_27351_27403[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27308 === (5))){
var state_27307__$1 = state_27307;
var statearr_27352_27404 = state_27307__$1;
(statearr_27352_27404[(2)] = true);

(statearr_27352_27404[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27308 === (14))){
var state_27307__$1 = state_27307;
var statearr_27353_27405 = state_27307__$1;
(statearr_27353_27405[(2)] = false);

(statearr_27353_27405[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27308 === (26))){
var inst_27267 = (state_27307[(11)]);
var inst_27274 = cljs.core.swap_BANG_.call(null,cs,cljs.core.dissoc,inst_27267);
var state_27307__$1 = state_27307;
var statearr_27354_27406 = state_27307__$1;
(statearr_27354_27406[(2)] = inst_27274);

(statearr_27354_27406[(1)] = (28));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27308 === (16))){
var state_27307__$1 = state_27307;
var statearr_27355_27407 = state_27307__$1;
(statearr_27355_27407[(2)] = true);

(statearr_27355_27407[(1)] = (18));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27308 === (38))){
var inst_27297 = (state_27307[(2)]);
var state_27307__$1 = state_27307;
var statearr_27356_27408 = state_27307__$1;
(statearr_27356_27408[(2)] = inst_27297);

(statearr_27356_27408[(1)] = (34));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27308 === (30))){
var inst_27259 = (state_27307[(13)]);
var inst_27258 = (state_27307[(9)]);
var inst_27267 = (state_27307[(11)]);
var inst_27284 = cljs.core.empty_QMARK_.call(null,inst_27258);
var inst_27285 = inst_27259.call(null,inst_27267);
var inst_27286 = cljs.core.not.call(null,inst_27285);
var inst_27287 = ((inst_27284) && (inst_27286));
var state_27307__$1 = state_27307;
var statearr_27357_27409 = state_27307__$1;
(statearr_27357_27409[(2)] = inst_27287);

(statearr_27357_27409[(1)] = (31));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27308 === (10))){
var inst_27209 = (state_27307[(8)]);
var inst_27230 = (state_27307[(2)]);
var inst_27231 = cljs.core.get.call(null,inst_27230,new cljs.core.Keyword(null,"solos","solos",1441458643));
var inst_27232 = cljs.core.get.call(null,inst_27230,new cljs.core.Keyword(null,"mutes","mutes",1068806309));
var inst_27233 = cljs.core.get.call(null,inst_27230,new cljs.core.Keyword(null,"reads","reads",-1215067361));
var inst_27234 = inst_27209;
var state_27307__$1 = (function (){var statearr_27358 = state_27307;
(statearr_27358[(16)] = inst_27233);

(statearr_27358[(17)] = inst_27231);

(statearr_27358[(18)] = inst_27232);

(statearr_27358[(7)] = inst_27234);

return statearr_27358;
})();
var statearr_27359_27410 = state_27307__$1;
(statearr_27359_27410[(2)] = null);

(statearr_27359_27410[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27308 === (18))){
var inst_27249 = (state_27307[(2)]);
var state_27307__$1 = state_27307;
var statearr_27360_27411 = state_27307__$1;
(statearr_27360_27411[(2)] = inst_27249);

(statearr_27360_27411[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27308 === (37))){
var state_27307__$1 = state_27307;
var statearr_27361_27412 = state_27307__$1;
(statearr_27361_27412[(2)] = null);

(statearr_27361_27412[(1)] = (38));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27308 === (8))){
var inst_27209 = (state_27307[(8)]);
var inst_27227 = cljs.core.apply.call(null,cljs.core.hash_map,inst_27209);
var state_27307__$1 = state_27307;
var statearr_27362_27413 = state_27307__$1;
(statearr_27362_27413[(2)] = inst_27227);

(statearr_27362_27413[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__26309__auto___27367,cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state,m))
;
return ((function (switch__26214__auto__,c__26309__auto___27367,cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state,m){
return (function() {
var cljs$core$async$mix_$_state_machine__26215__auto__ = null;
var cljs$core$async$mix_$_state_machine__26215__auto____0 = (function (){
var statearr_27363 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_27363[(0)] = cljs$core$async$mix_$_state_machine__26215__auto__);

(statearr_27363[(1)] = (1));

return statearr_27363;
});
var cljs$core$async$mix_$_state_machine__26215__auto____1 = (function (state_27307){
while(true){
var ret_value__26216__auto__ = (function (){try{while(true){
var result__26217__auto__ = switch__26214__auto__.call(null,state_27307);
if(cljs.core.keyword_identical_QMARK_.call(null,result__26217__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__26217__auto__;
}
break;
}
}catch (e27364){if((e27364 instanceof Object)){
var ex__26218__auto__ = e27364;
var statearr_27365_27414 = state_27307;
(statearr_27365_27414[(5)] = ex__26218__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_27307);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e27364;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__26216__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__27415 = state_27307;
state_27307 = G__27415;
continue;
} else {
return ret_value__26216__auto__;
}
break;
}
});
cljs$core$async$mix_$_state_machine__26215__auto__ = function(state_27307){
switch(arguments.length){
case 0:
return cljs$core$async$mix_$_state_machine__26215__auto____0.call(this);
case 1:
return cljs$core$async$mix_$_state_machine__26215__auto____1.call(this,state_27307);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$mix_$_state_machine__26215__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$mix_$_state_machine__26215__auto____0;
cljs$core$async$mix_$_state_machine__26215__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$mix_$_state_machine__26215__auto____1;
return cljs$core$async$mix_$_state_machine__26215__auto__;
})()
;})(switch__26214__auto__,c__26309__auto___27367,cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state,m))
})();
var state__26311__auto__ = (function (){var statearr_27366 = f__26310__auto__.call(null);
(statearr_27366[(6)] = c__26309__auto___27367);

return statearr_27366;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__26311__auto__);
});})(c__26309__auto___27367,cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state,m))
);


return m;
});
/**
 * Adds ch as an input to the mix
 */
cljs.core.async.admix = (function cljs$core$async$admix(mix,ch){
return cljs.core.async.admix_STAR_.call(null,mix,ch);
});
/**
 * Removes ch as an input to the mix
 */
cljs.core.async.unmix = (function cljs$core$async$unmix(mix,ch){
return cljs.core.async.unmix_STAR_.call(null,mix,ch);
});
/**
 * removes all inputs from the mix
 */
cljs.core.async.unmix_all = (function cljs$core$async$unmix_all(mix){
return cljs.core.async.unmix_all_STAR_.call(null,mix);
});
/**
 * Atomically sets the state(s) of one or more channels in a mix. The
 *   state map is a map of channels -> channel-state-map. A
 *   channel-state-map is a map of attrs -> boolean, where attr is one or
 *   more of :mute, :pause or :solo. Any states supplied are merged with
 *   the current state.
 * 
 *   Note that channels can be added to a mix via toggle, which can be
 *   used to add channels in a particular (e.g. paused) state.
 */
cljs.core.async.toggle = (function cljs$core$async$toggle(mix,state_map){
return cljs.core.async.toggle_STAR_.call(null,mix,state_map);
});
/**
 * Sets the solo mode of the mix. mode must be one of :mute or :pause
 */
cljs.core.async.solo_mode = (function cljs$core$async$solo_mode(mix,mode){
return cljs.core.async.solo_mode_STAR_.call(null,mix,mode);
});

/**
 * @interface
 */
cljs.core.async.Pub = function(){};

cljs.core.async.sub_STAR_ = (function cljs$core$async$sub_STAR_(p,v,ch,close_QMARK_){
if((((!((p == null)))) && ((!((p.cljs$core$async$Pub$sub_STAR_$arity$4 == null)))))){
return p.cljs$core$async$Pub$sub_STAR_$arity$4(p,v,ch,close_QMARK_);
} else {
var x__4433__auto__ = (((p == null))?null:p);
var m__4434__auto__ = (cljs.core.async.sub_STAR_[goog.typeOf(x__4433__auto__)]);
if((!((m__4434__auto__ == null)))){
return m__4434__auto__.call(null,p,v,ch,close_QMARK_);
} else {
var m__4431__auto__ = (cljs.core.async.sub_STAR_["_"]);
if((!((m__4431__auto__ == null)))){
return m__4431__auto__.call(null,p,v,ch,close_QMARK_);
} else {
throw cljs.core.missing_protocol.call(null,"Pub.sub*",p);
}
}
}
});

cljs.core.async.unsub_STAR_ = (function cljs$core$async$unsub_STAR_(p,v,ch){
if((((!((p == null)))) && ((!((p.cljs$core$async$Pub$unsub_STAR_$arity$3 == null)))))){
return p.cljs$core$async$Pub$unsub_STAR_$arity$3(p,v,ch);
} else {
var x__4433__auto__ = (((p == null))?null:p);
var m__4434__auto__ = (cljs.core.async.unsub_STAR_[goog.typeOf(x__4433__auto__)]);
if((!((m__4434__auto__ == null)))){
return m__4434__auto__.call(null,p,v,ch);
} else {
var m__4431__auto__ = (cljs.core.async.unsub_STAR_["_"]);
if((!((m__4431__auto__ == null)))){
return m__4431__auto__.call(null,p,v,ch);
} else {
throw cljs.core.missing_protocol.call(null,"Pub.unsub*",p);
}
}
}
});

cljs.core.async.unsub_all_STAR_ = (function cljs$core$async$unsub_all_STAR_(var_args){
var G__27417 = arguments.length;
switch (G__27417) {
case 1:
return cljs.core.async.unsub_all_STAR_.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.unsub_all_STAR_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.unsub_all_STAR_.cljs$core$IFn$_invoke$arity$1 = (function (p){
if((((!((p == null)))) && ((!((p.cljs$core$async$Pub$unsub_all_STAR_$arity$1 == null)))))){
return p.cljs$core$async$Pub$unsub_all_STAR_$arity$1(p);
} else {
var x__4433__auto__ = (((p == null))?null:p);
var m__4434__auto__ = (cljs.core.async.unsub_all_STAR_[goog.typeOf(x__4433__auto__)]);
if((!((m__4434__auto__ == null)))){
return m__4434__auto__.call(null,p);
} else {
var m__4431__auto__ = (cljs.core.async.unsub_all_STAR_["_"]);
if((!((m__4431__auto__ == null)))){
return m__4431__auto__.call(null,p);
} else {
throw cljs.core.missing_protocol.call(null,"Pub.unsub-all*",p);
}
}
}
});

cljs.core.async.unsub_all_STAR_.cljs$core$IFn$_invoke$arity$2 = (function (p,v){
if((((!((p == null)))) && ((!((p.cljs$core$async$Pub$unsub_all_STAR_$arity$2 == null)))))){
return p.cljs$core$async$Pub$unsub_all_STAR_$arity$2(p,v);
} else {
var x__4433__auto__ = (((p == null))?null:p);
var m__4434__auto__ = (cljs.core.async.unsub_all_STAR_[goog.typeOf(x__4433__auto__)]);
if((!((m__4434__auto__ == null)))){
return m__4434__auto__.call(null,p,v);
} else {
var m__4431__auto__ = (cljs.core.async.unsub_all_STAR_["_"]);
if((!((m__4431__auto__ == null)))){
return m__4431__auto__.call(null,p,v);
} else {
throw cljs.core.missing_protocol.call(null,"Pub.unsub-all*",p);
}
}
}
});

cljs.core.async.unsub_all_STAR_.cljs$lang$maxFixedArity = 2;


/**
 * Creates and returns a pub(lication) of the supplied channel,
 *   partitioned into topics by the topic-fn. topic-fn will be applied to
 *   each value on the channel and the result will determine the 'topic'
 *   on which that value will be put. Channels can be subscribed to
 *   receive copies of topics using 'sub', and unsubscribed using
 *   'unsub'. Each topic will be handled by an internal mult on a
 *   dedicated channel. By default these internal channels are
 *   unbuffered, but a buf-fn can be supplied which, given a topic,
 *   creates a buffer with desired properties.
 * 
 *   Each item is distributed to all subs in parallel and synchronously,
 *   i.e. each sub must accept before the next item is distributed. Use
 *   buffering/windowing to prevent slow subs from holding up the pub.
 * 
 *   Items received when there are no matching subs get dropped.
 * 
 *   Note that if buf-fns are used then each topic is handled
 *   asynchronously, i.e. if a channel is subscribed to more than one
 *   topic it should not expect them to be interleaved identically with
 *   the source.
 */
cljs.core.async.pub = (function cljs$core$async$pub(var_args){
var G__27421 = arguments.length;
switch (G__27421) {
case 2:
return cljs.core.async.pub.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.pub.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.pub.cljs$core$IFn$_invoke$arity$2 = (function (ch,topic_fn){
return cljs.core.async.pub.call(null,ch,topic_fn,cljs.core.constantly.call(null,null));
});

cljs.core.async.pub.cljs$core$IFn$_invoke$arity$3 = (function (ch,topic_fn,buf_fn){
var mults = cljs.core.atom.call(null,cljs.core.PersistentArrayMap.EMPTY);
var ensure_mult = ((function (mults){
return (function (topic){
var or__4131__auto__ = cljs.core.get.call(null,cljs.core.deref.call(null,mults),topic);
if(cljs.core.truth_(or__4131__auto__)){
return or__4131__auto__;
} else {
return cljs.core.get.call(null,cljs.core.swap_BANG_.call(null,mults,((function (or__4131__auto__,mults){
return (function (p1__27419_SHARP_){
if(cljs.core.truth_(p1__27419_SHARP_.call(null,topic))){
return p1__27419_SHARP_;
} else {
return cljs.core.assoc.call(null,p1__27419_SHARP_,topic,cljs.core.async.mult.call(null,cljs.core.async.chan.call(null,buf_fn.call(null,topic))));
}
});})(or__4131__auto__,mults))
),topic);
}
});})(mults))
;
var p = (function (){
if((typeof cljs !== 'undefined') && (typeof cljs.core !== 'undefined') && (typeof cljs.core.async !== 'undefined') && (typeof cljs.core.async.t_cljs$core$async27422 !== 'undefined')){
} else {

/**
* @constructor
 * @implements {cljs.core.async.Pub}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.async.Mux}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async27422 = (function (ch,topic_fn,buf_fn,mults,ensure_mult,meta27423){
this.ch = ch;
this.topic_fn = topic_fn;
this.buf_fn = buf_fn;
this.mults = mults;
this.ensure_mult = ensure_mult;
this.meta27423 = meta27423;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
cljs.core.async.t_cljs$core$async27422.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = ((function (mults,ensure_mult){
return (function (_27424,meta27423__$1){
var self__ = this;
var _27424__$1 = this;
return (new cljs.core.async.t_cljs$core$async27422(self__.ch,self__.topic_fn,self__.buf_fn,self__.mults,self__.ensure_mult,meta27423__$1));
});})(mults,ensure_mult))
;

cljs.core.async.t_cljs$core$async27422.prototype.cljs$core$IMeta$_meta$arity$1 = ((function (mults,ensure_mult){
return (function (_27424){
var self__ = this;
var _27424__$1 = this;
return self__.meta27423;
});})(mults,ensure_mult))
;

cljs.core.async.t_cljs$core$async27422.prototype.cljs$core$async$Mux$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async27422.prototype.cljs$core$async$Mux$muxch_STAR_$arity$1 = ((function (mults,ensure_mult){
return (function (_){
var self__ = this;
var ___$1 = this;
return self__.ch;
});})(mults,ensure_mult))
;

cljs.core.async.t_cljs$core$async27422.prototype.cljs$core$async$Pub$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async27422.prototype.cljs$core$async$Pub$sub_STAR_$arity$4 = ((function (mults,ensure_mult){
return (function (p,topic,ch__$1,close_QMARK_){
var self__ = this;
var p__$1 = this;
var m = self__.ensure_mult.call(null,topic);
return cljs.core.async.tap.call(null,m,ch__$1,close_QMARK_);
});})(mults,ensure_mult))
;

cljs.core.async.t_cljs$core$async27422.prototype.cljs$core$async$Pub$unsub_STAR_$arity$3 = ((function (mults,ensure_mult){
return (function (p,topic,ch__$1){
var self__ = this;
var p__$1 = this;
var temp__5720__auto__ = cljs.core.get.call(null,cljs.core.deref.call(null,self__.mults),topic);
if(cljs.core.truth_(temp__5720__auto__)){
var m = temp__5720__auto__;
return cljs.core.async.untap.call(null,m,ch__$1);
} else {
return null;
}
});})(mults,ensure_mult))
;

cljs.core.async.t_cljs$core$async27422.prototype.cljs$core$async$Pub$unsub_all_STAR_$arity$1 = ((function (mults,ensure_mult){
return (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.reset_BANG_.call(null,self__.mults,cljs.core.PersistentArrayMap.EMPTY);
});})(mults,ensure_mult))
;

cljs.core.async.t_cljs$core$async27422.prototype.cljs$core$async$Pub$unsub_all_STAR_$arity$2 = ((function (mults,ensure_mult){
return (function (_,topic){
var self__ = this;
var ___$1 = this;
return cljs.core.swap_BANG_.call(null,self__.mults,cljs.core.dissoc,topic);
});})(mults,ensure_mult))
;

cljs.core.async.t_cljs$core$async27422.getBasis = ((function (mults,ensure_mult){
return (function (){
return new cljs.core.PersistentVector(null, 6, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"ch","ch",1085813622,null),new cljs.core.Symbol(null,"topic-fn","topic-fn",-862449736,null),new cljs.core.Symbol(null,"buf-fn","buf-fn",-1200281591,null),new cljs.core.Symbol(null,"mults","mults",-461114485,null),new cljs.core.Symbol(null,"ensure-mult","ensure-mult",1796584816,null),new cljs.core.Symbol(null,"meta27423","meta27423",-1003495258,null)], null);
});})(mults,ensure_mult))
;

cljs.core.async.t_cljs$core$async27422.cljs$lang$type = true;

cljs.core.async.t_cljs$core$async27422.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async27422";

cljs.core.async.t_cljs$core$async27422.cljs$lang$ctorPrWriter = ((function (mults,ensure_mult){
return (function (this__4374__auto__,writer__4375__auto__,opt__4376__auto__){
return cljs.core._write.call(null,writer__4375__auto__,"cljs.core.async/t_cljs$core$async27422");
});})(mults,ensure_mult))
;

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async27422.
 */
cljs.core.async.__GT_t_cljs$core$async27422 = ((function (mults,ensure_mult){
return (function cljs$core$async$__GT_t_cljs$core$async27422(ch__$1,topic_fn__$1,buf_fn__$1,mults__$1,ensure_mult__$1,meta27423){
return (new cljs.core.async.t_cljs$core$async27422(ch__$1,topic_fn__$1,buf_fn__$1,mults__$1,ensure_mult__$1,meta27423));
});})(mults,ensure_mult))
;

}

return (new cljs.core.async.t_cljs$core$async27422(ch,topic_fn,buf_fn,mults,ensure_mult,cljs.core.PersistentArrayMap.EMPTY));
})()
;
var c__26309__auto___27542 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__26309__auto___27542,mults,ensure_mult,p){
return (function (){
var f__26310__auto__ = (function (){var switch__26214__auto__ = ((function (c__26309__auto___27542,mults,ensure_mult,p){
return (function (state_27496){
var state_val_27497 = (state_27496[(1)]);
if((state_val_27497 === (7))){
var inst_27492 = (state_27496[(2)]);
var state_27496__$1 = state_27496;
var statearr_27498_27543 = state_27496__$1;
(statearr_27498_27543[(2)] = inst_27492);

(statearr_27498_27543[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27497 === (20))){
var state_27496__$1 = state_27496;
var statearr_27499_27544 = state_27496__$1;
(statearr_27499_27544[(2)] = null);

(statearr_27499_27544[(1)] = (21));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27497 === (1))){
var state_27496__$1 = state_27496;
var statearr_27500_27545 = state_27496__$1;
(statearr_27500_27545[(2)] = null);

(statearr_27500_27545[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27497 === (24))){
var inst_27475 = (state_27496[(7)]);
var inst_27484 = cljs.core.swap_BANG_.call(null,mults,cljs.core.dissoc,inst_27475);
var state_27496__$1 = state_27496;
var statearr_27501_27546 = state_27496__$1;
(statearr_27501_27546[(2)] = inst_27484);

(statearr_27501_27546[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27497 === (4))){
var inst_27427 = (state_27496[(8)]);
var inst_27427__$1 = (state_27496[(2)]);
var inst_27428 = (inst_27427__$1 == null);
var state_27496__$1 = (function (){var statearr_27502 = state_27496;
(statearr_27502[(8)] = inst_27427__$1);

return statearr_27502;
})();
if(cljs.core.truth_(inst_27428)){
var statearr_27503_27547 = state_27496__$1;
(statearr_27503_27547[(1)] = (5));

} else {
var statearr_27504_27548 = state_27496__$1;
(statearr_27504_27548[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27497 === (15))){
var inst_27469 = (state_27496[(2)]);
var state_27496__$1 = state_27496;
var statearr_27505_27549 = state_27496__$1;
(statearr_27505_27549[(2)] = inst_27469);

(statearr_27505_27549[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27497 === (21))){
var inst_27489 = (state_27496[(2)]);
var state_27496__$1 = (function (){var statearr_27506 = state_27496;
(statearr_27506[(9)] = inst_27489);

return statearr_27506;
})();
var statearr_27507_27550 = state_27496__$1;
(statearr_27507_27550[(2)] = null);

(statearr_27507_27550[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27497 === (13))){
var inst_27451 = (state_27496[(10)]);
var inst_27453 = cljs.core.chunked_seq_QMARK_.call(null,inst_27451);
var state_27496__$1 = state_27496;
if(inst_27453){
var statearr_27508_27551 = state_27496__$1;
(statearr_27508_27551[(1)] = (16));

} else {
var statearr_27509_27552 = state_27496__$1;
(statearr_27509_27552[(1)] = (17));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27497 === (22))){
var inst_27481 = (state_27496[(2)]);
var state_27496__$1 = state_27496;
if(cljs.core.truth_(inst_27481)){
var statearr_27510_27553 = state_27496__$1;
(statearr_27510_27553[(1)] = (23));

} else {
var statearr_27511_27554 = state_27496__$1;
(statearr_27511_27554[(1)] = (24));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27497 === (6))){
var inst_27427 = (state_27496[(8)]);
var inst_27477 = (state_27496[(11)]);
var inst_27475 = (state_27496[(7)]);
var inst_27475__$1 = topic_fn.call(null,inst_27427);
var inst_27476 = cljs.core.deref.call(null,mults);
var inst_27477__$1 = cljs.core.get.call(null,inst_27476,inst_27475__$1);
var state_27496__$1 = (function (){var statearr_27512 = state_27496;
(statearr_27512[(11)] = inst_27477__$1);

(statearr_27512[(7)] = inst_27475__$1);

return statearr_27512;
})();
if(cljs.core.truth_(inst_27477__$1)){
var statearr_27513_27555 = state_27496__$1;
(statearr_27513_27555[(1)] = (19));

} else {
var statearr_27514_27556 = state_27496__$1;
(statearr_27514_27556[(1)] = (20));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27497 === (25))){
var inst_27486 = (state_27496[(2)]);
var state_27496__$1 = state_27496;
var statearr_27515_27557 = state_27496__$1;
(statearr_27515_27557[(2)] = inst_27486);

(statearr_27515_27557[(1)] = (21));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27497 === (17))){
var inst_27451 = (state_27496[(10)]);
var inst_27460 = cljs.core.first.call(null,inst_27451);
var inst_27461 = cljs.core.async.muxch_STAR_.call(null,inst_27460);
var inst_27462 = cljs.core.async.close_BANG_.call(null,inst_27461);
var inst_27463 = cljs.core.next.call(null,inst_27451);
var inst_27437 = inst_27463;
var inst_27438 = null;
var inst_27439 = (0);
var inst_27440 = (0);
var state_27496__$1 = (function (){var statearr_27516 = state_27496;
(statearr_27516[(12)] = inst_27440);

(statearr_27516[(13)] = inst_27437);

(statearr_27516[(14)] = inst_27438);

(statearr_27516[(15)] = inst_27462);

(statearr_27516[(16)] = inst_27439);

return statearr_27516;
})();
var statearr_27517_27558 = state_27496__$1;
(statearr_27517_27558[(2)] = null);

(statearr_27517_27558[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27497 === (3))){
var inst_27494 = (state_27496[(2)]);
var state_27496__$1 = state_27496;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_27496__$1,inst_27494);
} else {
if((state_val_27497 === (12))){
var inst_27471 = (state_27496[(2)]);
var state_27496__$1 = state_27496;
var statearr_27518_27559 = state_27496__$1;
(statearr_27518_27559[(2)] = inst_27471);

(statearr_27518_27559[(1)] = (9));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27497 === (2))){
var state_27496__$1 = state_27496;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_27496__$1,(4),ch);
} else {
if((state_val_27497 === (23))){
var state_27496__$1 = state_27496;
var statearr_27519_27560 = state_27496__$1;
(statearr_27519_27560[(2)] = null);

(statearr_27519_27560[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27497 === (19))){
var inst_27427 = (state_27496[(8)]);
var inst_27477 = (state_27496[(11)]);
var inst_27479 = cljs.core.async.muxch_STAR_.call(null,inst_27477);
var state_27496__$1 = state_27496;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_27496__$1,(22),inst_27479,inst_27427);
} else {
if((state_val_27497 === (11))){
var inst_27437 = (state_27496[(13)]);
var inst_27451 = (state_27496[(10)]);
var inst_27451__$1 = cljs.core.seq.call(null,inst_27437);
var state_27496__$1 = (function (){var statearr_27520 = state_27496;
(statearr_27520[(10)] = inst_27451__$1);

return statearr_27520;
})();
if(inst_27451__$1){
var statearr_27521_27561 = state_27496__$1;
(statearr_27521_27561[(1)] = (13));

} else {
var statearr_27522_27562 = state_27496__$1;
(statearr_27522_27562[(1)] = (14));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27497 === (9))){
var inst_27473 = (state_27496[(2)]);
var state_27496__$1 = state_27496;
var statearr_27523_27563 = state_27496__$1;
(statearr_27523_27563[(2)] = inst_27473);

(statearr_27523_27563[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27497 === (5))){
var inst_27434 = cljs.core.deref.call(null,mults);
var inst_27435 = cljs.core.vals.call(null,inst_27434);
var inst_27436 = cljs.core.seq.call(null,inst_27435);
var inst_27437 = inst_27436;
var inst_27438 = null;
var inst_27439 = (0);
var inst_27440 = (0);
var state_27496__$1 = (function (){var statearr_27524 = state_27496;
(statearr_27524[(12)] = inst_27440);

(statearr_27524[(13)] = inst_27437);

(statearr_27524[(14)] = inst_27438);

(statearr_27524[(16)] = inst_27439);

return statearr_27524;
})();
var statearr_27525_27564 = state_27496__$1;
(statearr_27525_27564[(2)] = null);

(statearr_27525_27564[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27497 === (14))){
var state_27496__$1 = state_27496;
var statearr_27529_27565 = state_27496__$1;
(statearr_27529_27565[(2)] = null);

(statearr_27529_27565[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27497 === (16))){
var inst_27451 = (state_27496[(10)]);
var inst_27455 = cljs.core.chunk_first.call(null,inst_27451);
var inst_27456 = cljs.core.chunk_rest.call(null,inst_27451);
var inst_27457 = cljs.core.count.call(null,inst_27455);
var inst_27437 = inst_27456;
var inst_27438 = inst_27455;
var inst_27439 = inst_27457;
var inst_27440 = (0);
var state_27496__$1 = (function (){var statearr_27530 = state_27496;
(statearr_27530[(12)] = inst_27440);

(statearr_27530[(13)] = inst_27437);

(statearr_27530[(14)] = inst_27438);

(statearr_27530[(16)] = inst_27439);

return statearr_27530;
})();
var statearr_27531_27566 = state_27496__$1;
(statearr_27531_27566[(2)] = null);

(statearr_27531_27566[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27497 === (10))){
var inst_27440 = (state_27496[(12)]);
var inst_27437 = (state_27496[(13)]);
var inst_27438 = (state_27496[(14)]);
var inst_27439 = (state_27496[(16)]);
var inst_27445 = cljs.core._nth.call(null,inst_27438,inst_27440);
var inst_27446 = cljs.core.async.muxch_STAR_.call(null,inst_27445);
var inst_27447 = cljs.core.async.close_BANG_.call(null,inst_27446);
var inst_27448 = (inst_27440 + (1));
var tmp27526 = inst_27437;
var tmp27527 = inst_27438;
var tmp27528 = inst_27439;
var inst_27437__$1 = tmp27526;
var inst_27438__$1 = tmp27527;
var inst_27439__$1 = tmp27528;
var inst_27440__$1 = inst_27448;
var state_27496__$1 = (function (){var statearr_27532 = state_27496;
(statearr_27532[(12)] = inst_27440__$1);

(statearr_27532[(13)] = inst_27437__$1);

(statearr_27532[(14)] = inst_27438__$1);

(statearr_27532[(16)] = inst_27439__$1);

(statearr_27532[(17)] = inst_27447);

return statearr_27532;
})();
var statearr_27533_27567 = state_27496__$1;
(statearr_27533_27567[(2)] = null);

(statearr_27533_27567[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27497 === (18))){
var inst_27466 = (state_27496[(2)]);
var state_27496__$1 = state_27496;
var statearr_27534_27568 = state_27496__$1;
(statearr_27534_27568[(2)] = inst_27466);

(statearr_27534_27568[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27497 === (8))){
var inst_27440 = (state_27496[(12)]);
var inst_27439 = (state_27496[(16)]);
var inst_27442 = (inst_27440 < inst_27439);
var inst_27443 = inst_27442;
var state_27496__$1 = state_27496;
if(cljs.core.truth_(inst_27443)){
var statearr_27535_27569 = state_27496__$1;
(statearr_27535_27569[(1)] = (10));

} else {
var statearr_27536_27570 = state_27496__$1;
(statearr_27536_27570[(1)] = (11));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__26309__auto___27542,mults,ensure_mult,p))
;
return ((function (switch__26214__auto__,c__26309__auto___27542,mults,ensure_mult,p){
return (function() {
var cljs$core$async$state_machine__26215__auto__ = null;
var cljs$core$async$state_machine__26215__auto____0 = (function (){
var statearr_27537 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_27537[(0)] = cljs$core$async$state_machine__26215__auto__);

(statearr_27537[(1)] = (1));

return statearr_27537;
});
var cljs$core$async$state_machine__26215__auto____1 = (function (state_27496){
while(true){
var ret_value__26216__auto__ = (function (){try{while(true){
var result__26217__auto__ = switch__26214__auto__.call(null,state_27496);
if(cljs.core.keyword_identical_QMARK_.call(null,result__26217__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__26217__auto__;
}
break;
}
}catch (e27538){if((e27538 instanceof Object)){
var ex__26218__auto__ = e27538;
var statearr_27539_27571 = state_27496;
(statearr_27539_27571[(5)] = ex__26218__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_27496);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e27538;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__26216__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__27572 = state_27496;
state_27496 = G__27572;
continue;
} else {
return ret_value__26216__auto__;
}
break;
}
});
cljs$core$async$state_machine__26215__auto__ = function(state_27496){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__26215__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__26215__auto____1.call(this,state_27496);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__26215__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__26215__auto____0;
cljs$core$async$state_machine__26215__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__26215__auto____1;
return cljs$core$async$state_machine__26215__auto__;
})()
;})(switch__26214__auto__,c__26309__auto___27542,mults,ensure_mult,p))
})();
var state__26311__auto__ = (function (){var statearr_27540 = f__26310__auto__.call(null);
(statearr_27540[(6)] = c__26309__auto___27542);

return statearr_27540;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__26311__auto__);
});})(c__26309__auto___27542,mults,ensure_mult,p))
);


return p;
});

cljs.core.async.pub.cljs$lang$maxFixedArity = 3;

/**
 * Subscribes a channel to a topic of a pub.
 * 
 *   By default the channel will be closed when the source closes,
 *   but can be determined by the close? parameter.
 */
cljs.core.async.sub = (function cljs$core$async$sub(var_args){
var G__27574 = arguments.length;
switch (G__27574) {
case 3:
return cljs.core.async.sub.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
case 4:
return cljs.core.async.sub.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.sub.cljs$core$IFn$_invoke$arity$3 = (function (p,topic,ch){
return cljs.core.async.sub.call(null,p,topic,ch,true);
});

cljs.core.async.sub.cljs$core$IFn$_invoke$arity$4 = (function (p,topic,ch,close_QMARK_){
return cljs.core.async.sub_STAR_.call(null,p,topic,ch,close_QMARK_);
});

cljs.core.async.sub.cljs$lang$maxFixedArity = 4;

/**
 * Unsubscribes a channel from a topic of a pub
 */
cljs.core.async.unsub = (function cljs$core$async$unsub(p,topic,ch){
return cljs.core.async.unsub_STAR_.call(null,p,topic,ch);
});
/**
 * Unsubscribes all channels from a pub, or a topic of a pub
 */
cljs.core.async.unsub_all = (function cljs$core$async$unsub_all(var_args){
var G__27577 = arguments.length;
switch (G__27577) {
case 1:
return cljs.core.async.unsub_all.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.unsub_all.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.unsub_all.cljs$core$IFn$_invoke$arity$1 = (function (p){
return cljs.core.async.unsub_all_STAR_.call(null,p);
});

cljs.core.async.unsub_all.cljs$core$IFn$_invoke$arity$2 = (function (p,topic){
return cljs.core.async.unsub_all_STAR_.call(null,p,topic);
});

cljs.core.async.unsub_all.cljs$lang$maxFixedArity = 2;

/**
 * Takes a function and a collection of source channels, and returns a
 *   channel which contains the values produced by applying f to the set
 *   of first items taken from each source channel, followed by applying
 *   f to the set of second items from each channel, until any one of the
 *   channels is closed, at which point the output channel will be
 *   closed. The returned channel will be unbuffered by default, or a
 *   buf-or-n can be supplied
 */
cljs.core.async.map = (function cljs$core$async$map(var_args){
var G__27580 = arguments.length;
switch (G__27580) {
case 2:
return cljs.core.async.map.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.map.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.map.cljs$core$IFn$_invoke$arity$2 = (function (f,chs){
return cljs.core.async.map.call(null,f,chs,null);
});

cljs.core.async.map.cljs$core$IFn$_invoke$arity$3 = (function (f,chs,buf_or_n){
var chs__$1 = cljs.core.vec.call(null,chs);
var out = cljs.core.async.chan.call(null,buf_or_n);
var cnt = cljs.core.count.call(null,chs__$1);
var rets = cljs.core.object_array.call(null,cnt);
var dchan = cljs.core.async.chan.call(null,(1));
var dctr = cljs.core.atom.call(null,null);
var done = cljs.core.mapv.call(null,((function (chs__$1,out,cnt,rets,dchan,dctr){
return (function (i){
return ((function (chs__$1,out,cnt,rets,dchan,dctr){
return (function (ret){
(rets[i] = ret);

if((cljs.core.swap_BANG_.call(null,dctr,cljs.core.dec) === (0))){
return cljs.core.async.put_BANG_.call(null,dchan,rets.slice((0)));
} else {
return null;
}
});
;})(chs__$1,out,cnt,rets,dchan,dctr))
});})(chs__$1,out,cnt,rets,dchan,dctr))
,cljs.core.range.call(null,cnt));
var c__26309__auto___27647 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__26309__auto___27647,chs__$1,out,cnt,rets,dchan,dctr,done){
return (function (){
var f__26310__auto__ = (function (){var switch__26214__auto__ = ((function (c__26309__auto___27647,chs__$1,out,cnt,rets,dchan,dctr,done){
return (function (state_27619){
var state_val_27620 = (state_27619[(1)]);
if((state_val_27620 === (7))){
var state_27619__$1 = state_27619;
var statearr_27621_27648 = state_27619__$1;
(statearr_27621_27648[(2)] = null);

(statearr_27621_27648[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27620 === (1))){
var state_27619__$1 = state_27619;
var statearr_27622_27649 = state_27619__$1;
(statearr_27622_27649[(2)] = null);

(statearr_27622_27649[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27620 === (4))){
var inst_27583 = (state_27619[(7)]);
var inst_27585 = (inst_27583 < cnt);
var state_27619__$1 = state_27619;
if(cljs.core.truth_(inst_27585)){
var statearr_27623_27650 = state_27619__$1;
(statearr_27623_27650[(1)] = (6));

} else {
var statearr_27624_27651 = state_27619__$1;
(statearr_27624_27651[(1)] = (7));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27620 === (15))){
var inst_27615 = (state_27619[(2)]);
var state_27619__$1 = state_27619;
var statearr_27625_27652 = state_27619__$1;
(statearr_27625_27652[(2)] = inst_27615);

(statearr_27625_27652[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27620 === (13))){
var inst_27608 = cljs.core.async.close_BANG_.call(null,out);
var state_27619__$1 = state_27619;
var statearr_27626_27653 = state_27619__$1;
(statearr_27626_27653[(2)] = inst_27608);

(statearr_27626_27653[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27620 === (6))){
var state_27619__$1 = state_27619;
var statearr_27627_27654 = state_27619__$1;
(statearr_27627_27654[(2)] = null);

(statearr_27627_27654[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27620 === (3))){
var inst_27617 = (state_27619[(2)]);
var state_27619__$1 = state_27619;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_27619__$1,inst_27617);
} else {
if((state_val_27620 === (12))){
var inst_27605 = (state_27619[(8)]);
var inst_27605__$1 = (state_27619[(2)]);
var inst_27606 = cljs.core.some.call(null,cljs.core.nil_QMARK_,inst_27605__$1);
var state_27619__$1 = (function (){var statearr_27628 = state_27619;
(statearr_27628[(8)] = inst_27605__$1);

return statearr_27628;
})();
if(cljs.core.truth_(inst_27606)){
var statearr_27629_27655 = state_27619__$1;
(statearr_27629_27655[(1)] = (13));

} else {
var statearr_27630_27656 = state_27619__$1;
(statearr_27630_27656[(1)] = (14));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27620 === (2))){
var inst_27582 = cljs.core.reset_BANG_.call(null,dctr,cnt);
var inst_27583 = (0);
var state_27619__$1 = (function (){var statearr_27631 = state_27619;
(statearr_27631[(9)] = inst_27582);

(statearr_27631[(7)] = inst_27583);

return statearr_27631;
})();
var statearr_27632_27657 = state_27619__$1;
(statearr_27632_27657[(2)] = null);

(statearr_27632_27657[(1)] = (4));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27620 === (11))){
var inst_27583 = (state_27619[(7)]);
var _ = cljs.core.async.impl.ioc_helpers.add_exception_frame.call(null,state_27619,(10),Object,null,(9));
var inst_27592 = chs__$1.call(null,inst_27583);
var inst_27593 = done.call(null,inst_27583);
var inst_27594 = cljs.core.async.take_BANG_.call(null,inst_27592,inst_27593);
var state_27619__$1 = state_27619;
var statearr_27633_27658 = state_27619__$1;
(statearr_27633_27658[(2)] = inst_27594);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_27619__$1);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27620 === (9))){
var inst_27583 = (state_27619[(7)]);
var inst_27596 = (state_27619[(2)]);
var inst_27597 = (inst_27583 + (1));
var inst_27583__$1 = inst_27597;
var state_27619__$1 = (function (){var statearr_27634 = state_27619;
(statearr_27634[(7)] = inst_27583__$1);

(statearr_27634[(10)] = inst_27596);

return statearr_27634;
})();
var statearr_27635_27659 = state_27619__$1;
(statearr_27635_27659[(2)] = null);

(statearr_27635_27659[(1)] = (4));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27620 === (5))){
var inst_27603 = (state_27619[(2)]);
var state_27619__$1 = (function (){var statearr_27636 = state_27619;
(statearr_27636[(11)] = inst_27603);

return statearr_27636;
})();
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_27619__$1,(12),dchan);
} else {
if((state_val_27620 === (14))){
var inst_27605 = (state_27619[(8)]);
var inst_27610 = cljs.core.apply.call(null,f,inst_27605);
var state_27619__$1 = state_27619;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_27619__$1,(16),out,inst_27610);
} else {
if((state_val_27620 === (16))){
var inst_27612 = (state_27619[(2)]);
var state_27619__$1 = (function (){var statearr_27637 = state_27619;
(statearr_27637[(12)] = inst_27612);

return statearr_27637;
})();
var statearr_27638_27660 = state_27619__$1;
(statearr_27638_27660[(2)] = null);

(statearr_27638_27660[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27620 === (10))){
var inst_27587 = (state_27619[(2)]);
var inst_27588 = cljs.core.swap_BANG_.call(null,dctr,cljs.core.dec);
var state_27619__$1 = (function (){var statearr_27639 = state_27619;
(statearr_27639[(13)] = inst_27587);

return statearr_27639;
})();
var statearr_27640_27661 = state_27619__$1;
(statearr_27640_27661[(2)] = inst_27588);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_27619__$1);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27620 === (8))){
var inst_27601 = (state_27619[(2)]);
var state_27619__$1 = state_27619;
var statearr_27641_27662 = state_27619__$1;
(statearr_27641_27662[(2)] = inst_27601);

(statearr_27641_27662[(1)] = (5));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__26309__auto___27647,chs__$1,out,cnt,rets,dchan,dctr,done))
;
return ((function (switch__26214__auto__,c__26309__auto___27647,chs__$1,out,cnt,rets,dchan,dctr,done){
return (function() {
var cljs$core$async$state_machine__26215__auto__ = null;
var cljs$core$async$state_machine__26215__auto____0 = (function (){
var statearr_27642 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_27642[(0)] = cljs$core$async$state_machine__26215__auto__);

(statearr_27642[(1)] = (1));

return statearr_27642;
});
var cljs$core$async$state_machine__26215__auto____1 = (function (state_27619){
while(true){
var ret_value__26216__auto__ = (function (){try{while(true){
var result__26217__auto__ = switch__26214__auto__.call(null,state_27619);
if(cljs.core.keyword_identical_QMARK_.call(null,result__26217__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__26217__auto__;
}
break;
}
}catch (e27643){if((e27643 instanceof Object)){
var ex__26218__auto__ = e27643;
var statearr_27644_27663 = state_27619;
(statearr_27644_27663[(5)] = ex__26218__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_27619);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e27643;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__26216__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__27664 = state_27619;
state_27619 = G__27664;
continue;
} else {
return ret_value__26216__auto__;
}
break;
}
});
cljs$core$async$state_machine__26215__auto__ = function(state_27619){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__26215__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__26215__auto____1.call(this,state_27619);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__26215__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__26215__auto____0;
cljs$core$async$state_machine__26215__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__26215__auto____1;
return cljs$core$async$state_machine__26215__auto__;
})()
;})(switch__26214__auto__,c__26309__auto___27647,chs__$1,out,cnt,rets,dchan,dctr,done))
})();
var state__26311__auto__ = (function (){var statearr_27645 = f__26310__auto__.call(null);
(statearr_27645[(6)] = c__26309__auto___27647);

return statearr_27645;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__26311__auto__);
});})(c__26309__auto___27647,chs__$1,out,cnt,rets,dchan,dctr,done))
);


return out;
});

cljs.core.async.map.cljs$lang$maxFixedArity = 3;

/**
 * Takes a collection of source channels and returns a channel which
 *   contains all values taken from them. The returned channel will be
 *   unbuffered by default, or a buf-or-n can be supplied. The channel
 *   will close after all the source channels have closed.
 */
cljs.core.async.merge = (function cljs$core$async$merge(var_args){
var G__27667 = arguments.length;
switch (G__27667) {
case 1:
return cljs.core.async.merge.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.merge.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.merge.cljs$core$IFn$_invoke$arity$1 = (function (chs){
return cljs.core.async.merge.call(null,chs,null);
});

cljs.core.async.merge.cljs$core$IFn$_invoke$arity$2 = (function (chs,buf_or_n){
var out = cljs.core.async.chan.call(null,buf_or_n);
var c__26309__auto___27721 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__26309__auto___27721,out){
return (function (){
var f__26310__auto__ = (function (){var switch__26214__auto__ = ((function (c__26309__auto___27721,out){
return (function (state_27699){
var state_val_27700 = (state_27699[(1)]);
if((state_val_27700 === (7))){
var inst_27679 = (state_27699[(7)]);
var inst_27678 = (state_27699[(8)]);
var inst_27678__$1 = (state_27699[(2)]);
var inst_27679__$1 = cljs.core.nth.call(null,inst_27678__$1,(0),null);
var inst_27680 = cljs.core.nth.call(null,inst_27678__$1,(1),null);
var inst_27681 = (inst_27679__$1 == null);
var state_27699__$1 = (function (){var statearr_27701 = state_27699;
(statearr_27701[(7)] = inst_27679__$1);

(statearr_27701[(8)] = inst_27678__$1);

(statearr_27701[(9)] = inst_27680);

return statearr_27701;
})();
if(cljs.core.truth_(inst_27681)){
var statearr_27702_27722 = state_27699__$1;
(statearr_27702_27722[(1)] = (8));

} else {
var statearr_27703_27723 = state_27699__$1;
(statearr_27703_27723[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27700 === (1))){
var inst_27668 = cljs.core.vec.call(null,chs);
var inst_27669 = inst_27668;
var state_27699__$1 = (function (){var statearr_27704 = state_27699;
(statearr_27704[(10)] = inst_27669);

return statearr_27704;
})();
var statearr_27705_27724 = state_27699__$1;
(statearr_27705_27724[(2)] = null);

(statearr_27705_27724[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27700 === (4))){
var inst_27669 = (state_27699[(10)]);
var state_27699__$1 = state_27699;
return cljs.core.async.ioc_alts_BANG_.call(null,state_27699__$1,(7),inst_27669);
} else {
if((state_val_27700 === (6))){
var inst_27695 = (state_27699[(2)]);
var state_27699__$1 = state_27699;
var statearr_27706_27725 = state_27699__$1;
(statearr_27706_27725[(2)] = inst_27695);

(statearr_27706_27725[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27700 === (3))){
var inst_27697 = (state_27699[(2)]);
var state_27699__$1 = state_27699;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_27699__$1,inst_27697);
} else {
if((state_val_27700 === (2))){
var inst_27669 = (state_27699[(10)]);
var inst_27671 = cljs.core.count.call(null,inst_27669);
var inst_27672 = (inst_27671 > (0));
var state_27699__$1 = state_27699;
if(cljs.core.truth_(inst_27672)){
var statearr_27708_27726 = state_27699__$1;
(statearr_27708_27726[(1)] = (4));

} else {
var statearr_27709_27727 = state_27699__$1;
(statearr_27709_27727[(1)] = (5));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27700 === (11))){
var inst_27669 = (state_27699[(10)]);
var inst_27688 = (state_27699[(2)]);
var tmp27707 = inst_27669;
var inst_27669__$1 = tmp27707;
var state_27699__$1 = (function (){var statearr_27710 = state_27699;
(statearr_27710[(11)] = inst_27688);

(statearr_27710[(10)] = inst_27669__$1);

return statearr_27710;
})();
var statearr_27711_27728 = state_27699__$1;
(statearr_27711_27728[(2)] = null);

(statearr_27711_27728[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27700 === (9))){
var inst_27679 = (state_27699[(7)]);
var state_27699__$1 = state_27699;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_27699__$1,(11),out,inst_27679);
} else {
if((state_val_27700 === (5))){
var inst_27693 = cljs.core.async.close_BANG_.call(null,out);
var state_27699__$1 = state_27699;
var statearr_27712_27729 = state_27699__$1;
(statearr_27712_27729[(2)] = inst_27693);

(statearr_27712_27729[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27700 === (10))){
var inst_27691 = (state_27699[(2)]);
var state_27699__$1 = state_27699;
var statearr_27713_27730 = state_27699__$1;
(statearr_27713_27730[(2)] = inst_27691);

(statearr_27713_27730[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27700 === (8))){
var inst_27679 = (state_27699[(7)]);
var inst_27669 = (state_27699[(10)]);
var inst_27678 = (state_27699[(8)]);
var inst_27680 = (state_27699[(9)]);
var inst_27683 = (function (){var cs = inst_27669;
var vec__27674 = inst_27678;
var v = inst_27679;
var c = inst_27680;
return ((function (cs,vec__27674,v,c,inst_27679,inst_27669,inst_27678,inst_27680,state_val_27700,c__26309__auto___27721,out){
return (function (p1__27665_SHARP_){
return cljs.core.not_EQ_.call(null,c,p1__27665_SHARP_);
});
;})(cs,vec__27674,v,c,inst_27679,inst_27669,inst_27678,inst_27680,state_val_27700,c__26309__auto___27721,out))
})();
var inst_27684 = cljs.core.filterv.call(null,inst_27683,inst_27669);
var inst_27669__$1 = inst_27684;
var state_27699__$1 = (function (){var statearr_27714 = state_27699;
(statearr_27714[(10)] = inst_27669__$1);

return statearr_27714;
})();
var statearr_27715_27731 = state_27699__$1;
(statearr_27715_27731[(2)] = null);

(statearr_27715_27731[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
});})(c__26309__auto___27721,out))
;
return ((function (switch__26214__auto__,c__26309__auto___27721,out){
return (function() {
var cljs$core$async$state_machine__26215__auto__ = null;
var cljs$core$async$state_machine__26215__auto____0 = (function (){
var statearr_27716 = [null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_27716[(0)] = cljs$core$async$state_machine__26215__auto__);

(statearr_27716[(1)] = (1));

return statearr_27716;
});
var cljs$core$async$state_machine__26215__auto____1 = (function (state_27699){
while(true){
var ret_value__26216__auto__ = (function (){try{while(true){
var result__26217__auto__ = switch__26214__auto__.call(null,state_27699);
if(cljs.core.keyword_identical_QMARK_.call(null,result__26217__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__26217__auto__;
}
break;
}
}catch (e27717){if((e27717 instanceof Object)){
var ex__26218__auto__ = e27717;
var statearr_27718_27732 = state_27699;
(statearr_27718_27732[(5)] = ex__26218__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_27699);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e27717;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__26216__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__27733 = state_27699;
state_27699 = G__27733;
continue;
} else {
return ret_value__26216__auto__;
}
break;
}
});
cljs$core$async$state_machine__26215__auto__ = function(state_27699){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__26215__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__26215__auto____1.call(this,state_27699);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__26215__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__26215__auto____0;
cljs$core$async$state_machine__26215__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__26215__auto____1;
return cljs$core$async$state_machine__26215__auto__;
})()
;})(switch__26214__auto__,c__26309__auto___27721,out))
})();
var state__26311__auto__ = (function (){var statearr_27719 = f__26310__auto__.call(null);
(statearr_27719[(6)] = c__26309__auto___27721);

return statearr_27719;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__26311__auto__);
});})(c__26309__auto___27721,out))
);


return out;
});

cljs.core.async.merge.cljs$lang$maxFixedArity = 2;

/**
 * Returns a channel containing the single (collection) result of the
 *   items taken from the channel conjoined to the supplied
 *   collection. ch must close before into produces a result.
 */
cljs.core.async.into = (function cljs$core$async$into(coll,ch){
return cljs.core.async.reduce.call(null,cljs.core.conj,coll,ch);
});
/**
 * Returns a channel that will return, at most, n items from ch. After n items
 * have been returned, or ch has been closed, the return chanel will close.
 * 
 *   The output channel is unbuffered by default, unless buf-or-n is given.
 */
cljs.core.async.take = (function cljs$core$async$take(var_args){
var G__27735 = arguments.length;
switch (G__27735) {
case 2:
return cljs.core.async.take.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.take.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.take.cljs$core$IFn$_invoke$arity$2 = (function (n,ch){
return cljs.core.async.take.call(null,n,ch,null);
});

cljs.core.async.take.cljs$core$IFn$_invoke$arity$3 = (function (n,ch,buf_or_n){
var out = cljs.core.async.chan.call(null,buf_or_n);
var c__26309__auto___27780 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__26309__auto___27780,out){
return (function (){
var f__26310__auto__ = (function (){var switch__26214__auto__ = ((function (c__26309__auto___27780,out){
return (function (state_27759){
var state_val_27760 = (state_27759[(1)]);
if((state_val_27760 === (7))){
var inst_27741 = (state_27759[(7)]);
var inst_27741__$1 = (state_27759[(2)]);
var inst_27742 = (inst_27741__$1 == null);
var inst_27743 = cljs.core.not.call(null,inst_27742);
var state_27759__$1 = (function (){var statearr_27761 = state_27759;
(statearr_27761[(7)] = inst_27741__$1);

return statearr_27761;
})();
if(inst_27743){
var statearr_27762_27781 = state_27759__$1;
(statearr_27762_27781[(1)] = (8));

} else {
var statearr_27763_27782 = state_27759__$1;
(statearr_27763_27782[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27760 === (1))){
var inst_27736 = (0);
var state_27759__$1 = (function (){var statearr_27764 = state_27759;
(statearr_27764[(8)] = inst_27736);

return statearr_27764;
})();
var statearr_27765_27783 = state_27759__$1;
(statearr_27765_27783[(2)] = null);

(statearr_27765_27783[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27760 === (4))){
var state_27759__$1 = state_27759;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_27759__$1,(7),ch);
} else {
if((state_val_27760 === (6))){
var inst_27754 = (state_27759[(2)]);
var state_27759__$1 = state_27759;
var statearr_27766_27784 = state_27759__$1;
(statearr_27766_27784[(2)] = inst_27754);

(statearr_27766_27784[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27760 === (3))){
var inst_27756 = (state_27759[(2)]);
var inst_27757 = cljs.core.async.close_BANG_.call(null,out);
var state_27759__$1 = (function (){var statearr_27767 = state_27759;
(statearr_27767[(9)] = inst_27756);

return statearr_27767;
})();
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_27759__$1,inst_27757);
} else {
if((state_val_27760 === (2))){
var inst_27736 = (state_27759[(8)]);
var inst_27738 = (inst_27736 < n);
var state_27759__$1 = state_27759;
if(cljs.core.truth_(inst_27738)){
var statearr_27768_27785 = state_27759__$1;
(statearr_27768_27785[(1)] = (4));

} else {
var statearr_27769_27786 = state_27759__$1;
(statearr_27769_27786[(1)] = (5));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27760 === (11))){
var inst_27736 = (state_27759[(8)]);
var inst_27746 = (state_27759[(2)]);
var inst_27747 = (inst_27736 + (1));
var inst_27736__$1 = inst_27747;
var state_27759__$1 = (function (){var statearr_27770 = state_27759;
(statearr_27770[(10)] = inst_27746);

(statearr_27770[(8)] = inst_27736__$1);

return statearr_27770;
})();
var statearr_27771_27787 = state_27759__$1;
(statearr_27771_27787[(2)] = null);

(statearr_27771_27787[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27760 === (9))){
var state_27759__$1 = state_27759;
var statearr_27772_27788 = state_27759__$1;
(statearr_27772_27788[(2)] = null);

(statearr_27772_27788[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27760 === (5))){
var state_27759__$1 = state_27759;
var statearr_27773_27789 = state_27759__$1;
(statearr_27773_27789[(2)] = null);

(statearr_27773_27789[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27760 === (10))){
var inst_27751 = (state_27759[(2)]);
var state_27759__$1 = state_27759;
var statearr_27774_27790 = state_27759__$1;
(statearr_27774_27790[(2)] = inst_27751);

(statearr_27774_27790[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27760 === (8))){
var inst_27741 = (state_27759[(7)]);
var state_27759__$1 = state_27759;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_27759__$1,(11),out,inst_27741);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
});})(c__26309__auto___27780,out))
;
return ((function (switch__26214__auto__,c__26309__auto___27780,out){
return (function() {
var cljs$core$async$state_machine__26215__auto__ = null;
var cljs$core$async$state_machine__26215__auto____0 = (function (){
var statearr_27775 = [null,null,null,null,null,null,null,null,null,null,null];
(statearr_27775[(0)] = cljs$core$async$state_machine__26215__auto__);

(statearr_27775[(1)] = (1));

return statearr_27775;
});
var cljs$core$async$state_machine__26215__auto____1 = (function (state_27759){
while(true){
var ret_value__26216__auto__ = (function (){try{while(true){
var result__26217__auto__ = switch__26214__auto__.call(null,state_27759);
if(cljs.core.keyword_identical_QMARK_.call(null,result__26217__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__26217__auto__;
}
break;
}
}catch (e27776){if((e27776 instanceof Object)){
var ex__26218__auto__ = e27776;
var statearr_27777_27791 = state_27759;
(statearr_27777_27791[(5)] = ex__26218__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_27759);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e27776;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__26216__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__27792 = state_27759;
state_27759 = G__27792;
continue;
} else {
return ret_value__26216__auto__;
}
break;
}
});
cljs$core$async$state_machine__26215__auto__ = function(state_27759){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__26215__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__26215__auto____1.call(this,state_27759);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__26215__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__26215__auto____0;
cljs$core$async$state_machine__26215__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__26215__auto____1;
return cljs$core$async$state_machine__26215__auto__;
})()
;})(switch__26214__auto__,c__26309__auto___27780,out))
})();
var state__26311__auto__ = (function (){var statearr_27778 = f__26310__auto__.call(null);
(statearr_27778[(6)] = c__26309__auto___27780);

return statearr_27778;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__26311__auto__);
});})(c__26309__auto___27780,out))
);


return out;
});

cljs.core.async.take.cljs$lang$maxFixedArity = 3;

/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.map_LT_ = (function cljs$core$async$map_LT_(f,ch){
if((typeof cljs !== 'undefined') && (typeof cljs.core !== 'undefined') && (typeof cljs.core.async !== 'undefined') && (typeof cljs.core.async.t_cljs$core$async27794 !== 'undefined')){
} else {

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Channel}
 * @implements {cljs.core.async.impl.protocols.WritePort}
 * @implements {cljs.core.async.impl.protocols.ReadPort}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async27794 = (function (f,ch,meta27795){
this.f = f;
this.ch = ch;
this.meta27795 = meta27795;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
cljs.core.async.t_cljs$core$async27794.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_27796,meta27795__$1){
var self__ = this;
var _27796__$1 = this;
return (new cljs.core.async.t_cljs$core$async27794(self__.f,self__.ch,meta27795__$1));
});

cljs.core.async.t_cljs$core$async27794.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_27796){
var self__ = this;
var _27796__$1 = this;
return self__.meta27795;
});

cljs.core.async.t_cljs$core$async27794.prototype.cljs$core$async$impl$protocols$Channel$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async27794.prototype.cljs$core$async$impl$protocols$Channel$close_BANG_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.close_BANG_.call(null,self__.ch);
});

cljs.core.async.t_cljs$core$async27794.prototype.cljs$core$async$impl$protocols$Channel$closed_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.closed_QMARK_.call(null,self__.ch);
});

cljs.core.async.t_cljs$core$async27794.prototype.cljs$core$async$impl$protocols$ReadPort$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async27794.prototype.cljs$core$async$impl$protocols$ReadPort$take_BANG_$arity$2 = (function (_,fn1){
var self__ = this;
var ___$1 = this;
var ret = cljs.core.async.impl.protocols.take_BANG_.call(null,self__.ch,(function (){
if((typeof cljs !== 'undefined') && (typeof cljs.core !== 'undefined') && (typeof cljs.core.async !== 'undefined') && (typeof cljs.core.async.t_cljs$core$async27797 !== 'undefined')){
} else {

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Handler}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async27797 = (function (f,ch,meta27795,_,fn1,meta27798){
this.f = f;
this.ch = ch;
this.meta27795 = meta27795;
this._ = _;
this.fn1 = fn1;
this.meta27798 = meta27798;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
cljs.core.async.t_cljs$core$async27797.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = ((function (___$1){
return (function (_27799,meta27798__$1){
var self__ = this;
var _27799__$1 = this;
return (new cljs.core.async.t_cljs$core$async27797(self__.f,self__.ch,self__.meta27795,self__._,self__.fn1,meta27798__$1));
});})(___$1))
;

cljs.core.async.t_cljs$core$async27797.prototype.cljs$core$IMeta$_meta$arity$1 = ((function (___$1){
return (function (_27799){
var self__ = this;
var _27799__$1 = this;
return self__.meta27798;
});})(___$1))
;

cljs.core.async.t_cljs$core$async27797.prototype.cljs$core$async$impl$protocols$Handler$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async27797.prototype.cljs$core$async$impl$protocols$Handler$active_QMARK_$arity$1 = ((function (___$1){
return (function (___$1){
var self__ = this;
var ___$2 = this;
return cljs.core.async.impl.protocols.active_QMARK_.call(null,self__.fn1);
});})(___$1))
;

cljs.core.async.t_cljs$core$async27797.prototype.cljs$core$async$impl$protocols$Handler$blockable_QMARK_$arity$1 = ((function (___$1){
return (function (___$1){
var self__ = this;
var ___$2 = this;
return true;
});})(___$1))
;

cljs.core.async.t_cljs$core$async27797.prototype.cljs$core$async$impl$protocols$Handler$commit$arity$1 = ((function (___$1){
return (function (___$1){
var self__ = this;
var ___$2 = this;
var f1 = cljs.core.async.impl.protocols.commit.call(null,self__.fn1);
return ((function (f1,___$2,___$1){
return (function (p1__27793_SHARP_){
return f1.call(null,(((p1__27793_SHARP_ == null))?null:self__.f.call(null,p1__27793_SHARP_)));
});
;})(f1,___$2,___$1))
});})(___$1))
;

cljs.core.async.t_cljs$core$async27797.getBasis = ((function (___$1){
return (function (){
return new cljs.core.PersistentVector(null, 6, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"f","f",43394975,null),new cljs.core.Symbol(null,"ch","ch",1085813622,null),new cljs.core.Symbol(null,"meta27795","meta27795",-1632178026,null),cljs.core.with_meta(new cljs.core.Symbol(null,"_","_",-1201019570,null),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"tag","tag",-1290361223),new cljs.core.Symbol("cljs.core.async","t_cljs$core$async27794","cljs.core.async/t_cljs$core$async27794",1073370663,null)], null)),new cljs.core.Symbol(null,"fn1","fn1",895834444,null),new cljs.core.Symbol(null,"meta27798","meta27798",-119505924,null)], null);
});})(___$1))
;

cljs.core.async.t_cljs$core$async27797.cljs$lang$type = true;

cljs.core.async.t_cljs$core$async27797.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async27797";

cljs.core.async.t_cljs$core$async27797.cljs$lang$ctorPrWriter = ((function (___$1){
return (function (this__4374__auto__,writer__4375__auto__,opt__4376__auto__){
return cljs.core._write.call(null,writer__4375__auto__,"cljs.core.async/t_cljs$core$async27797");
});})(___$1))
;

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async27797.
 */
cljs.core.async.__GT_t_cljs$core$async27797 = ((function (___$1){
return (function cljs$core$async$map_LT__$___GT_t_cljs$core$async27797(f__$1,ch__$1,meta27795__$1,___$2,fn1__$1,meta27798){
return (new cljs.core.async.t_cljs$core$async27797(f__$1,ch__$1,meta27795__$1,___$2,fn1__$1,meta27798));
});})(___$1))
;

}

return (new cljs.core.async.t_cljs$core$async27797(self__.f,self__.ch,self__.meta27795,___$1,fn1,cljs.core.PersistentArrayMap.EMPTY));
})()
);
if(cljs.core.truth_((function (){var and__4120__auto__ = ret;
if(cljs.core.truth_(and__4120__auto__)){
return (!((cljs.core.deref.call(null,ret) == null)));
} else {
return and__4120__auto__;
}
})())){
return cljs.core.async.impl.channels.box.call(null,self__.f.call(null,cljs.core.deref.call(null,ret)));
} else {
return ret;
}
});

cljs.core.async.t_cljs$core$async27794.prototype.cljs$core$async$impl$protocols$WritePort$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async27794.prototype.cljs$core$async$impl$protocols$WritePort$put_BANG_$arity$3 = (function (_,val,fn1){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.put_BANG_.call(null,self__.ch,val,fn1);
});

cljs.core.async.t_cljs$core$async27794.getBasis = (function (){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"f","f",43394975,null),new cljs.core.Symbol(null,"ch","ch",1085813622,null),new cljs.core.Symbol(null,"meta27795","meta27795",-1632178026,null)], null);
});

cljs.core.async.t_cljs$core$async27794.cljs$lang$type = true;

cljs.core.async.t_cljs$core$async27794.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async27794";

cljs.core.async.t_cljs$core$async27794.cljs$lang$ctorPrWriter = (function (this__4374__auto__,writer__4375__auto__,opt__4376__auto__){
return cljs.core._write.call(null,writer__4375__auto__,"cljs.core.async/t_cljs$core$async27794");
});

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async27794.
 */
cljs.core.async.__GT_t_cljs$core$async27794 = (function cljs$core$async$map_LT__$___GT_t_cljs$core$async27794(f__$1,ch__$1,meta27795){
return (new cljs.core.async.t_cljs$core$async27794(f__$1,ch__$1,meta27795));
});

}

return (new cljs.core.async.t_cljs$core$async27794(f,ch,cljs.core.PersistentArrayMap.EMPTY));
});
/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.map_GT_ = (function cljs$core$async$map_GT_(f,ch){
if((typeof cljs !== 'undefined') && (typeof cljs.core !== 'undefined') && (typeof cljs.core.async !== 'undefined') && (typeof cljs.core.async.t_cljs$core$async27800 !== 'undefined')){
} else {

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Channel}
 * @implements {cljs.core.async.impl.protocols.WritePort}
 * @implements {cljs.core.async.impl.protocols.ReadPort}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async27800 = (function (f,ch,meta27801){
this.f = f;
this.ch = ch;
this.meta27801 = meta27801;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
cljs.core.async.t_cljs$core$async27800.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_27802,meta27801__$1){
var self__ = this;
var _27802__$1 = this;
return (new cljs.core.async.t_cljs$core$async27800(self__.f,self__.ch,meta27801__$1));
});

cljs.core.async.t_cljs$core$async27800.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_27802){
var self__ = this;
var _27802__$1 = this;
return self__.meta27801;
});

cljs.core.async.t_cljs$core$async27800.prototype.cljs$core$async$impl$protocols$Channel$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async27800.prototype.cljs$core$async$impl$protocols$Channel$close_BANG_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.close_BANG_.call(null,self__.ch);
});

cljs.core.async.t_cljs$core$async27800.prototype.cljs$core$async$impl$protocols$ReadPort$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async27800.prototype.cljs$core$async$impl$protocols$ReadPort$take_BANG_$arity$2 = (function (_,fn1){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.take_BANG_.call(null,self__.ch,fn1);
});

cljs.core.async.t_cljs$core$async27800.prototype.cljs$core$async$impl$protocols$WritePort$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async27800.prototype.cljs$core$async$impl$protocols$WritePort$put_BANG_$arity$3 = (function (_,val,fn1){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.put_BANG_.call(null,self__.ch,self__.f.call(null,val),fn1);
});

cljs.core.async.t_cljs$core$async27800.getBasis = (function (){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"f","f",43394975,null),new cljs.core.Symbol(null,"ch","ch",1085813622,null),new cljs.core.Symbol(null,"meta27801","meta27801",-1903482712,null)], null);
});

cljs.core.async.t_cljs$core$async27800.cljs$lang$type = true;

cljs.core.async.t_cljs$core$async27800.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async27800";

cljs.core.async.t_cljs$core$async27800.cljs$lang$ctorPrWriter = (function (this__4374__auto__,writer__4375__auto__,opt__4376__auto__){
return cljs.core._write.call(null,writer__4375__auto__,"cljs.core.async/t_cljs$core$async27800");
});

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async27800.
 */
cljs.core.async.__GT_t_cljs$core$async27800 = (function cljs$core$async$map_GT__$___GT_t_cljs$core$async27800(f__$1,ch__$1,meta27801){
return (new cljs.core.async.t_cljs$core$async27800(f__$1,ch__$1,meta27801));
});

}

return (new cljs.core.async.t_cljs$core$async27800(f,ch,cljs.core.PersistentArrayMap.EMPTY));
});
/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.filter_GT_ = (function cljs$core$async$filter_GT_(p,ch){
if((typeof cljs !== 'undefined') && (typeof cljs.core !== 'undefined') && (typeof cljs.core.async !== 'undefined') && (typeof cljs.core.async.t_cljs$core$async27803 !== 'undefined')){
} else {

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Channel}
 * @implements {cljs.core.async.impl.protocols.WritePort}
 * @implements {cljs.core.async.impl.protocols.ReadPort}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async27803 = (function (p,ch,meta27804){
this.p = p;
this.ch = ch;
this.meta27804 = meta27804;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
cljs.core.async.t_cljs$core$async27803.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_27805,meta27804__$1){
var self__ = this;
var _27805__$1 = this;
return (new cljs.core.async.t_cljs$core$async27803(self__.p,self__.ch,meta27804__$1));
});

cljs.core.async.t_cljs$core$async27803.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_27805){
var self__ = this;
var _27805__$1 = this;
return self__.meta27804;
});

cljs.core.async.t_cljs$core$async27803.prototype.cljs$core$async$impl$protocols$Channel$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async27803.prototype.cljs$core$async$impl$protocols$Channel$close_BANG_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.close_BANG_.call(null,self__.ch);
});

cljs.core.async.t_cljs$core$async27803.prototype.cljs$core$async$impl$protocols$Channel$closed_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.closed_QMARK_.call(null,self__.ch);
});

cljs.core.async.t_cljs$core$async27803.prototype.cljs$core$async$impl$protocols$ReadPort$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async27803.prototype.cljs$core$async$impl$protocols$ReadPort$take_BANG_$arity$2 = (function (_,fn1){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.take_BANG_.call(null,self__.ch,fn1);
});

cljs.core.async.t_cljs$core$async27803.prototype.cljs$core$async$impl$protocols$WritePort$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async27803.prototype.cljs$core$async$impl$protocols$WritePort$put_BANG_$arity$3 = (function (_,val,fn1){
var self__ = this;
var ___$1 = this;
if(cljs.core.truth_(self__.p.call(null,val))){
return cljs.core.async.impl.protocols.put_BANG_.call(null,self__.ch,val,fn1);
} else {
return cljs.core.async.impl.channels.box.call(null,cljs.core.not.call(null,cljs.core.async.impl.protocols.closed_QMARK_.call(null,self__.ch)));
}
});

cljs.core.async.t_cljs$core$async27803.getBasis = (function (){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"p","p",1791580836,null),new cljs.core.Symbol(null,"ch","ch",1085813622,null),new cljs.core.Symbol(null,"meta27804","meta27804",-44422893,null)], null);
});

cljs.core.async.t_cljs$core$async27803.cljs$lang$type = true;

cljs.core.async.t_cljs$core$async27803.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async27803";

cljs.core.async.t_cljs$core$async27803.cljs$lang$ctorPrWriter = (function (this__4374__auto__,writer__4375__auto__,opt__4376__auto__){
return cljs.core._write.call(null,writer__4375__auto__,"cljs.core.async/t_cljs$core$async27803");
});

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async27803.
 */
cljs.core.async.__GT_t_cljs$core$async27803 = (function cljs$core$async$filter_GT__$___GT_t_cljs$core$async27803(p__$1,ch__$1,meta27804){
return (new cljs.core.async.t_cljs$core$async27803(p__$1,ch__$1,meta27804));
});

}

return (new cljs.core.async.t_cljs$core$async27803(p,ch,cljs.core.PersistentArrayMap.EMPTY));
});
/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.remove_GT_ = (function cljs$core$async$remove_GT_(p,ch){
return cljs.core.async.filter_GT_.call(null,cljs.core.complement.call(null,p),ch);
});
/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.filter_LT_ = (function cljs$core$async$filter_LT_(var_args){
var G__27807 = arguments.length;
switch (G__27807) {
case 2:
return cljs.core.async.filter_LT_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.filter_LT_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.filter_LT_.cljs$core$IFn$_invoke$arity$2 = (function (p,ch){
return cljs.core.async.filter_LT_.call(null,p,ch,null);
});

cljs.core.async.filter_LT_.cljs$core$IFn$_invoke$arity$3 = (function (p,ch,buf_or_n){
var out = cljs.core.async.chan.call(null,buf_or_n);
var c__26309__auto___27847 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__26309__auto___27847,out){
return (function (){
var f__26310__auto__ = (function (){var switch__26214__auto__ = ((function (c__26309__auto___27847,out){
return (function (state_27828){
var state_val_27829 = (state_27828[(1)]);
if((state_val_27829 === (7))){
var inst_27824 = (state_27828[(2)]);
var state_27828__$1 = state_27828;
var statearr_27830_27848 = state_27828__$1;
(statearr_27830_27848[(2)] = inst_27824);

(statearr_27830_27848[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27829 === (1))){
var state_27828__$1 = state_27828;
var statearr_27831_27849 = state_27828__$1;
(statearr_27831_27849[(2)] = null);

(statearr_27831_27849[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27829 === (4))){
var inst_27810 = (state_27828[(7)]);
var inst_27810__$1 = (state_27828[(2)]);
var inst_27811 = (inst_27810__$1 == null);
var state_27828__$1 = (function (){var statearr_27832 = state_27828;
(statearr_27832[(7)] = inst_27810__$1);

return statearr_27832;
})();
if(cljs.core.truth_(inst_27811)){
var statearr_27833_27850 = state_27828__$1;
(statearr_27833_27850[(1)] = (5));

} else {
var statearr_27834_27851 = state_27828__$1;
(statearr_27834_27851[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27829 === (6))){
var inst_27810 = (state_27828[(7)]);
var inst_27815 = p.call(null,inst_27810);
var state_27828__$1 = state_27828;
if(cljs.core.truth_(inst_27815)){
var statearr_27835_27852 = state_27828__$1;
(statearr_27835_27852[(1)] = (8));

} else {
var statearr_27836_27853 = state_27828__$1;
(statearr_27836_27853[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27829 === (3))){
var inst_27826 = (state_27828[(2)]);
var state_27828__$1 = state_27828;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_27828__$1,inst_27826);
} else {
if((state_val_27829 === (2))){
var state_27828__$1 = state_27828;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_27828__$1,(4),ch);
} else {
if((state_val_27829 === (11))){
var inst_27818 = (state_27828[(2)]);
var state_27828__$1 = state_27828;
var statearr_27837_27854 = state_27828__$1;
(statearr_27837_27854[(2)] = inst_27818);

(statearr_27837_27854[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27829 === (9))){
var state_27828__$1 = state_27828;
var statearr_27838_27855 = state_27828__$1;
(statearr_27838_27855[(2)] = null);

(statearr_27838_27855[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27829 === (5))){
var inst_27813 = cljs.core.async.close_BANG_.call(null,out);
var state_27828__$1 = state_27828;
var statearr_27839_27856 = state_27828__$1;
(statearr_27839_27856[(2)] = inst_27813);

(statearr_27839_27856[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27829 === (10))){
var inst_27821 = (state_27828[(2)]);
var state_27828__$1 = (function (){var statearr_27840 = state_27828;
(statearr_27840[(8)] = inst_27821);

return statearr_27840;
})();
var statearr_27841_27857 = state_27828__$1;
(statearr_27841_27857[(2)] = null);

(statearr_27841_27857[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27829 === (8))){
var inst_27810 = (state_27828[(7)]);
var state_27828__$1 = state_27828;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_27828__$1,(11),out,inst_27810);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
});})(c__26309__auto___27847,out))
;
return ((function (switch__26214__auto__,c__26309__auto___27847,out){
return (function() {
var cljs$core$async$state_machine__26215__auto__ = null;
var cljs$core$async$state_machine__26215__auto____0 = (function (){
var statearr_27842 = [null,null,null,null,null,null,null,null,null];
(statearr_27842[(0)] = cljs$core$async$state_machine__26215__auto__);

(statearr_27842[(1)] = (1));

return statearr_27842;
});
var cljs$core$async$state_machine__26215__auto____1 = (function (state_27828){
while(true){
var ret_value__26216__auto__ = (function (){try{while(true){
var result__26217__auto__ = switch__26214__auto__.call(null,state_27828);
if(cljs.core.keyword_identical_QMARK_.call(null,result__26217__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__26217__auto__;
}
break;
}
}catch (e27843){if((e27843 instanceof Object)){
var ex__26218__auto__ = e27843;
var statearr_27844_27858 = state_27828;
(statearr_27844_27858[(5)] = ex__26218__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_27828);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e27843;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__26216__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__27859 = state_27828;
state_27828 = G__27859;
continue;
} else {
return ret_value__26216__auto__;
}
break;
}
});
cljs$core$async$state_machine__26215__auto__ = function(state_27828){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__26215__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__26215__auto____1.call(this,state_27828);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__26215__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__26215__auto____0;
cljs$core$async$state_machine__26215__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__26215__auto____1;
return cljs$core$async$state_machine__26215__auto__;
})()
;})(switch__26214__auto__,c__26309__auto___27847,out))
})();
var state__26311__auto__ = (function (){var statearr_27845 = f__26310__auto__.call(null);
(statearr_27845[(6)] = c__26309__auto___27847);

return statearr_27845;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__26311__auto__);
});})(c__26309__auto___27847,out))
);


return out;
});

cljs.core.async.filter_LT_.cljs$lang$maxFixedArity = 3;

/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.remove_LT_ = (function cljs$core$async$remove_LT_(var_args){
var G__27861 = arguments.length;
switch (G__27861) {
case 2:
return cljs.core.async.remove_LT_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.remove_LT_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.remove_LT_.cljs$core$IFn$_invoke$arity$2 = (function (p,ch){
return cljs.core.async.remove_LT_.call(null,p,ch,null);
});

cljs.core.async.remove_LT_.cljs$core$IFn$_invoke$arity$3 = (function (p,ch,buf_or_n){
return cljs.core.async.filter_LT_.call(null,cljs.core.complement.call(null,p),ch,buf_or_n);
});

cljs.core.async.remove_LT_.cljs$lang$maxFixedArity = 3;

cljs.core.async.mapcat_STAR_ = (function cljs$core$async$mapcat_STAR_(f,in$,out){
var c__26309__auto__ = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__26309__auto__){
return (function (){
var f__26310__auto__ = (function (){var switch__26214__auto__ = ((function (c__26309__auto__){
return (function (state_27924){
var state_val_27925 = (state_27924[(1)]);
if((state_val_27925 === (7))){
var inst_27920 = (state_27924[(2)]);
var state_27924__$1 = state_27924;
var statearr_27926_27964 = state_27924__$1;
(statearr_27926_27964[(2)] = inst_27920);

(statearr_27926_27964[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27925 === (20))){
var inst_27890 = (state_27924[(7)]);
var inst_27901 = (state_27924[(2)]);
var inst_27902 = cljs.core.next.call(null,inst_27890);
var inst_27876 = inst_27902;
var inst_27877 = null;
var inst_27878 = (0);
var inst_27879 = (0);
var state_27924__$1 = (function (){var statearr_27927 = state_27924;
(statearr_27927[(8)] = inst_27879);

(statearr_27927[(9)] = inst_27901);

(statearr_27927[(10)] = inst_27876);

(statearr_27927[(11)] = inst_27877);

(statearr_27927[(12)] = inst_27878);

return statearr_27927;
})();
var statearr_27928_27965 = state_27924__$1;
(statearr_27928_27965[(2)] = null);

(statearr_27928_27965[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27925 === (1))){
var state_27924__$1 = state_27924;
var statearr_27929_27966 = state_27924__$1;
(statearr_27929_27966[(2)] = null);

(statearr_27929_27966[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27925 === (4))){
var inst_27865 = (state_27924[(13)]);
var inst_27865__$1 = (state_27924[(2)]);
var inst_27866 = (inst_27865__$1 == null);
var state_27924__$1 = (function (){var statearr_27930 = state_27924;
(statearr_27930[(13)] = inst_27865__$1);

return statearr_27930;
})();
if(cljs.core.truth_(inst_27866)){
var statearr_27931_27967 = state_27924__$1;
(statearr_27931_27967[(1)] = (5));

} else {
var statearr_27932_27968 = state_27924__$1;
(statearr_27932_27968[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27925 === (15))){
var state_27924__$1 = state_27924;
var statearr_27936_27969 = state_27924__$1;
(statearr_27936_27969[(2)] = null);

(statearr_27936_27969[(1)] = (16));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27925 === (21))){
var state_27924__$1 = state_27924;
var statearr_27937_27970 = state_27924__$1;
(statearr_27937_27970[(2)] = null);

(statearr_27937_27970[(1)] = (23));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27925 === (13))){
var inst_27879 = (state_27924[(8)]);
var inst_27876 = (state_27924[(10)]);
var inst_27877 = (state_27924[(11)]);
var inst_27878 = (state_27924[(12)]);
var inst_27886 = (state_27924[(2)]);
var inst_27887 = (inst_27879 + (1));
var tmp27933 = inst_27876;
var tmp27934 = inst_27877;
var tmp27935 = inst_27878;
var inst_27876__$1 = tmp27933;
var inst_27877__$1 = tmp27934;
var inst_27878__$1 = tmp27935;
var inst_27879__$1 = inst_27887;
var state_27924__$1 = (function (){var statearr_27938 = state_27924;
(statearr_27938[(8)] = inst_27879__$1);

(statearr_27938[(14)] = inst_27886);

(statearr_27938[(10)] = inst_27876__$1);

(statearr_27938[(11)] = inst_27877__$1);

(statearr_27938[(12)] = inst_27878__$1);

return statearr_27938;
})();
var statearr_27939_27971 = state_27924__$1;
(statearr_27939_27971[(2)] = null);

(statearr_27939_27971[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27925 === (22))){
var state_27924__$1 = state_27924;
var statearr_27940_27972 = state_27924__$1;
(statearr_27940_27972[(2)] = null);

(statearr_27940_27972[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27925 === (6))){
var inst_27865 = (state_27924[(13)]);
var inst_27874 = f.call(null,inst_27865);
var inst_27875 = cljs.core.seq.call(null,inst_27874);
var inst_27876 = inst_27875;
var inst_27877 = null;
var inst_27878 = (0);
var inst_27879 = (0);
var state_27924__$1 = (function (){var statearr_27941 = state_27924;
(statearr_27941[(8)] = inst_27879);

(statearr_27941[(10)] = inst_27876);

(statearr_27941[(11)] = inst_27877);

(statearr_27941[(12)] = inst_27878);

return statearr_27941;
})();
var statearr_27942_27973 = state_27924__$1;
(statearr_27942_27973[(2)] = null);

(statearr_27942_27973[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27925 === (17))){
var inst_27890 = (state_27924[(7)]);
var inst_27894 = cljs.core.chunk_first.call(null,inst_27890);
var inst_27895 = cljs.core.chunk_rest.call(null,inst_27890);
var inst_27896 = cljs.core.count.call(null,inst_27894);
var inst_27876 = inst_27895;
var inst_27877 = inst_27894;
var inst_27878 = inst_27896;
var inst_27879 = (0);
var state_27924__$1 = (function (){var statearr_27943 = state_27924;
(statearr_27943[(8)] = inst_27879);

(statearr_27943[(10)] = inst_27876);

(statearr_27943[(11)] = inst_27877);

(statearr_27943[(12)] = inst_27878);

return statearr_27943;
})();
var statearr_27944_27974 = state_27924__$1;
(statearr_27944_27974[(2)] = null);

(statearr_27944_27974[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27925 === (3))){
var inst_27922 = (state_27924[(2)]);
var state_27924__$1 = state_27924;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_27924__$1,inst_27922);
} else {
if((state_val_27925 === (12))){
var inst_27910 = (state_27924[(2)]);
var state_27924__$1 = state_27924;
var statearr_27945_27975 = state_27924__$1;
(statearr_27945_27975[(2)] = inst_27910);

(statearr_27945_27975[(1)] = (9));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27925 === (2))){
var state_27924__$1 = state_27924;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_27924__$1,(4),in$);
} else {
if((state_val_27925 === (23))){
var inst_27918 = (state_27924[(2)]);
var state_27924__$1 = state_27924;
var statearr_27946_27976 = state_27924__$1;
(statearr_27946_27976[(2)] = inst_27918);

(statearr_27946_27976[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27925 === (19))){
var inst_27905 = (state_27924[(2)]);
var state_27924__$1 = state_27924;
var statearr_27947_27977 = state_27924__$1;
(statearr_27947_27977[(2)] = inst_27905);

(statearr_27947_27977[(1)] = (16));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27925 === (11))){
var inst_27890 = (state_27924[(7)]);
var inst_27876 = (state_27924[(10)]);
var inst_27890__$1 = cljs.core.seq.call(null,inst_27876);
var state_27924__$1 = (function (){var statearr_27948 = state_27924;
(statearr_27948[(7)] = inst_27890__$1);

return statearr_27948;
})();
if(inst_27890__$1){
var statearr_27949_27978 = state_27924__$1;
(statearr_27949_27978[(1)] = (14));

} else {
var statearr_27950_27979 = state_27924__$1;
(statearr_27950_27979[(1)] = (15));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27925 === (9))){
var inst_27912 = (state_27924[(2)]);
var inst_27913 = cljs.core.async.impl.protocols.closed_QMARK_.call(null,out);
var state_27924__$1 = (function (){var statearr_27951 = state_27924;
(statearr_27951[(15)] = inst_27912);

return statearr_27951;
})();
if(cljs.core.truth_(inst_27913)){
var statearr_27952_27980 = state_27924__$1;
(statearr_27952_27980[(1)] = (21));

} else {
var statearr_27953_27981 = state_27924__$1;
(statearr_27953_27981[(1)] = (22));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27925 === (5))){
var inst_27868 = cljs.core.async.close_BANG_.call(null,out);
var state_27924__$1 = state_27924;
var statearr_27954_27982 = state_27924__$1;
(statearr_27954_27982[(2)] = inst_27868);

(statearr_27954_27982[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27925 === (14))){
var inst_27890 = (state_27924[(7)]);
var inst_27892 = cljs.core.chunked_seq_QMARK_.call(null,inst_27890);
var state_27924__$1 = state_27924;
if(inst_27892){
var statearr_27955_27983 = state_27924__$1;
(statearr_27955_27983[(1)] = (17));

} else {
var statearr_27956_27984 = state_27924__$1;
(statearr_27956_27984[(1)] = (18));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27925 === (16))){
var inst_27908 = (state_27924[(2)]);
var state_27924__$1 = state_27924;
var statearr_27957_27985 = state_27924__$1;
(statearr_27957_27985[(2)] = inst_27908);

(statearr_27957_27985[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_27925 === (10))){
var inst_27879 = (state_27924[(8)]);
var inst_27877 = (state_27924[(11)]);
var inst_27884 = cljs.core._nth.call(null,inst_27877,inst_27879);
var state_27924__$1 = state_27924;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_27924__$1,(13),out,inst_27884);
} else {
if((state_val_27925 === (18))){
var inst_27890 = (state_27924[(7)]);
var inst_27899 = cljs.core.first.call(null,inst_27890);
var state_27924__$1 = state_27924;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_27924__$1,(20),out,inst_27899);
} else {
if((state_val_27925 === (8))){
var inst_27879 = (state_27924[(8)]);
var inst_27878 = (state_27924[(12)]);
var inst_27881 = (inst_27879 < inst_27878);
var inst_27882 = inst_27881;
var state_27924__$1 = state_27924;
if(cljs.core.truth_(inst_27882)){
var statearr_27958_27986 = state_27924__$1;
(statearr_27958_27986[(1)] = (10));

} else {
var statearr_27959_27987 = state_27924__$1;
(statearr_27959_27987[(1)] = (11));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__26309__auto__))
;
return ((function (switch__26214__auto__,c__26309__auto__){
return (function() {
var cljs$core$async$mapcat_STAR__$_state_machine__26215__auto__ = null;
var cljs$core$async$mapcat_STAR__$_state_machine__26215__auto____0 = (function (){
var statearr_27960 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_27960[(0)] = cljs$core$async$mapcat_STAR__$_state_machine__26215__auto__);

(statearr_27960[(1)] = (1));

return statearr_27960;
});
var cljs$core$async$mapcat_STAR__$_state_machine__26215__auto____1 = (function (state_27924){
while(true){
var ret_value__26216__auto__ = (function (){try{while(true){
var result__26217__auto__ = switch__26214__auto__.call(null,state_27924);
if(cljs.core.keyword_identical_QMARK_.call(null,result__26217__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__26217__auto__;
}
break;
}
}catch (e27961){if((e27961 instanceof Object)){
var ex__26218__auto__ = e27961;
var statearr_27962_27988 = state_27924;
(statearr_27962_27988[(5)] = ex__26218__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_27924);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e27961;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__26216__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__27989 = state_27924;
state_27924 = G__27989;
continue;
} else {
return ret_value__26216__auto__;
}
break;
}
});
cljs$core$async$mapcat_STAR__$_state_machine__26215__auto__ = function(state_27924){
switch(arguments.length){
case 0:
return cljs$core$async$mapcat_STAR__$_state_machine__26215__auto____0.call(this);
case 1:
return cljs$core$async$mapcat_STAR__$_state_machine__26215__auto____1.call(this,state_27924);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$mapcat_STAR__$_state_machine__26215__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$mapcat_STAR__$_state_machine__26215__auto____0;
cljs$core$async$mapcat_STAR__$_state_machine__26215__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$mapcat_STAR__$_state_machine__26215__auto____1;
return cljs$core$async$mapcat_STAR__$_state_machine__26215__auto__;
})()
;})(switch__26214__auto__,c__26309__auto__))
})();
var state__26311__auto__ = (function (){var statearr_27963 = f__26310__auto__.call(null);
(statearr_27963[(6)] = c__26309__auto__);

return statearr_27963;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__26311__auto__);
});})(c__26309__auto__))
);

return c__26309__auto__;
});
/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.mapcat_LT_ = (function cljs$core$async$mapcat_LT_(var_args){
var G__27991 = arguments.length;
switch (G__27991) {
case 2:
return cljs.core.async.mapcat_LT_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.mapcat_LT_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.mapcat_LT_.cljs$core$IFn$_invoke$arity$2 = (function (f,in$){
return cljs.core.async.mapcat_LT_.call(null,f,in$,null);
});

cljs.core.async.mapcat_LT_.cljs$core$IFn$_invoke$arity$3 = (function (f,in$,buf_or_n){
var out = cljs.core.async.chan.call(null,buf_or_n);
cljs.core.async.mapcat_STAR_.call(null,f,in$,out);

return out;
});

cljs.core.async.mapcat_LT_.cljs$lang$maxFixedArity = 3;

/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.mapcat_GT_ = (function cljs$core$async$mapcat_GT_(var_args){
var G__27994 = arguments.length;
switch (G__27994) {
case 2:
return cljs.core.async.mapcat_GT_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.mapcat_GT_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.mapcat_GT_.cljs$core$IFn$_invoke$arity$2 = (function (f,out){
return cljs.core.async.mapcat_GT_.call(null,f,out,null);
});

cljs.core.async.mapcat_GT_.cljs$core$IFn$_invoke$arity$3 = (function (f,out,buf_or_n){
var in$ = cljs.core.async.chan.call(null,buf_or_n);
cljs.core.async.mapcat_STAR_.call(null,f,in$,out);

return in$;
});

cljs.core.async.mapcat_GT_.cljs$lang$maxFixedArity = 3;

/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.unique = (function cljs$core$async$unique(var_args){
var G__27997 = arguments.length;
switch (G__27997) {
case 1:
return cljs.core.async.unique.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.unique.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.unique.cljs$core$IFn$_invoke$arity$1 = (function (ch){
return cljs.core.async.unique.call(null,ch,null);
});

cljs.core.async.unique.cljs$core$IFn$_invoke$arity$2 = (function (ch,buf_or_n){
var out = cljs.core.async.chan.call(null,buf_or_n);
var c__26309__auto___28044 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__26309__auto___28044,out){
return (function (){
var f__26310__auto__ = (function (){var switch__26214__auto__ = ((function (c__26309__auto___28044,out){
return (function (state_28021){
var state_val_28022 = (state_28021[(1)]);
if((state_val_28022 === (7))){
var inst_28016 = (state_28021[(2)]);
var state_28021__$1 = state_28021;
var statearr_28023_28045 = state_28021__$1;
(statearr_28023_28045[(2)] = inst_28016);

(statearr_28023_28045[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28022 === (1))){
var inst_27998 = null;
var state_28021__$1 = (function (){var statearr_28024 = state_28021;
(statearr_28024[(7)] = inst_27998);

return statearr_28024;
})();
var statearr_28025_28046 = state_28021__$1;
(statearr_28025_28046[(2)] = null);

(statearr_28025_28046[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28022 === (4))){
var inst_28001 = (state_28021[(8)]);
var inst_28001__$1 = (state_28021[(2)]);
var inst_28002 = (inst_28001__$1 == null);
var inst_28003 = cljs.core.not.call(null,inst_28002);
var state_28021__$1 = (function (){var statearr_28026 = state_28021;
(statearr_28026[(8)] = inst_28001__$1);

return statearr_28026;
})();
if(inst_28003){
var statearr_28027_28047 = state_28021__$1;
(statearr_28027_28047[(1)] = (5));

} else {
var statearr_28028_28048 = state_28021__$1;
(statearr_28028_28048[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28022 === (6))){
var state_28021__$1 = state_28021;
var statearr_28029_28049 = state_28021__$1;
(statearr_28029_28049[(2)] = null);

(statearr_28029_28049[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28022 === (3))){
var inst_28018 = (state_28021[(2)]);
var inst_28019 = cljs.core.async.close_BANG_.call(null,out);
var state_28021__$1 = (function (){var statearr_28030 = state_28021;
(statearr_28030[(9)] = inst_28018);

return statearr_28030;
})();
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_28021__$1,inst_28019);
} else {
if((state_val_28022 === (2))){
var state_28021__$1 = state_28021;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_28021__$1,(4),ch);
} else {
if((state_val_28022 === (11))){
var inst_28001 = (state_28021[(8)]);
var inst_28010 = (state_28021[(2)]);
var inst_27998 = inst_28001;
var state_28021__$1 = (function (){var statearr_28031 = state_28021;
(statearr_28031[(7)] = inst_27998);

(statearr_28031[(10)] = inst_28010);

return statearr_28031;
})();
var statearr_28032_28050 = state_28021__$1;
(statearr_28032_28050[(2)] = null);

(statearr_28032_28050[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28022 === (9))){
var inst_28001 = (state_28021[(8)]);
var state_28021__$1 = state_28021;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_28021__$1,(11),out,inst_28001);
} else {
if((state_val_28022 === (5))){
var inst_27998 = (state_28021[(7)]);
var inst_28001 = (state_28021[(8)]);
var inst_28005 = cljs.core._EQ_.call(null,inst_28001,inst_27998);
var state_28021__$1 = state_28021;
if(inst_28005){
var statearr_28034_28051 = state_28021__$1;
(statearr_28034_28051[(1)] = (8));

} else {
var statearr_28035_28052 = state_28021__$1;
(statearr_28035_28052[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28022 === (10))){
var inst_28013 = (state_28021[(2)]);
var state_28021__$1 = state_28021;
var statearr_28036_28053 = state_28021__$1;
(statearr_28036_28053[(2)] = inst_28013);

(statearr_28036_28053[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28022 === (8))){
var inst_27998 = (state_28021[(7)]);
var tmp28033 = inst_27998;
var inst_27998__$1 = tmp28033;
var state_28021__$1 = (function (){var statearr_28037 = state_28021;
(statearr_28037[(7)] = inst_27998__$1);

return statearr_28037;
})();
var statearr_28038_28054 = state_28021__$1;
(statearr_28038_28054[(2)] = null);

(statearr_28038_28054[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
});})(c__26309__auto___28044,out))
;
return ((function (switch__26214__auto__,c__26309__auto___28044,out){
return (function() {
var cljs$core$async$state_machine__26215__auto__ = null;
var cljs$core$async$state_machine__26215__auto____0 = (function (){
var statearr_28039 = [null,null,null,null,null,null,null,null,null,null,null];
(statearr_28039[(0)] = cljs$core$async$state_machine__26215__auto__);

(statearr_28039[(1)] = (1));

return statearr_28039;
});
var cljs$core$async$state_machine__26215__auto____1 = (function (state_28021){
while(true){
var ret_value__26216__auto__ = (function (){try{while(true){
var result__26217__auto__ = switch__26214__auto__.call(null,state_28021);
if(cljs.core.keyword_identical_QMARK_.call(null,result__26217__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__26217__auto__;
}
break;
}
}catch (e28040){if((e28040 instanceof Object)){
var ex__26218__auto__ = e28040;
var statearr_28041_28055 = state_28021;
(statearr_28041_28055[(5)] = ex__26218__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_28021);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e28040;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__26216__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__28056 = state_28021;
state_28021 = G__28056;
continue;
} else {
return ret_value__26216__auto__;
}
break;
}
});
cljs$core$async$state_machine__26215__auto__ = function(state_28021){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__26215__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__26215__auto____1.call(this,state_28021);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__26215__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__26215__auto____0;
cljs$core$async$state_machine__26215__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__26215__auto____1;
return cljs$core$async$state_machine__26215__auto__;
})()
;})(switch__26214__auto__,c__26309__auto___28044,out))
})();
var state__26311__auto__ = (function (){var statearr_28042 = f__26310__auto__.call(null);
(statearr_28042[(6)] = c__26309__auto___28044);

return statearr_28042;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__26311__auto__);
});})(c__26309__auto___28044,out))
);


return out;
});

cljs.core.async.unique.cljs$lang$maxFixedArity = 2;

/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.partition = (function cljs$core$async$partition(var_args){
var G__28058 = arguments.length;
switch (G__28058) {
case 2:
return cljs.core.async.partition.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.partition.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.partition.cljs$core$IFn$_invoke$arity$2 = (function (n,ch){
return cljs.core.async.partition.call(null,n,ch,null);
});

cljs.core.async.partition.cljs$core$IFn$_invoke$arity$3 = (function (n,ch,buf_or_n){
var out = cljs.core.async.chan.call(null,buf_or_n);
var c__26309__auto___28124 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__26309__auto___28124,out){
return (function (){
var f__26310__auto__ = (function (){var switch__26214__auto__ = ((function (c__26309__auto___28124,out){
return (function (state_28096){
var state_val_28097 = (state_28096[(1)]);
if((state_val_28097 === (7))){
var inst_28092 = (state_28096[(2)]);
var state_28096__$1 = state_28096;
var statearr_28098_28125 = state_28096__$1;
(statearr_28098_28125[(2)] = inst_28092);

(statearr_28098_28125[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28097 === (1))){
var inst_28059 = (new Array(n));
var inst_28060 = inst_28059;
var inst_28061 = (0);
var state_28096__$1 = (function (){var statearr_28099 = state_28096;
(statearr_28099[(7)] = inst_28060);

(statearr_28099[(8)] = inst_28061);

return statearr_28099;
})();
var statearr_28100_28126 = state_28096__$1;
(statearr_28100_28126[(2)] = null);

(statearr_28100_28126[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28097 === (4))){
var inst_28064 = (state_28096[(9)]);
var inst_28064__$1 = (state_28096[(2)]);
var inst_28065 = (inst_28064__$1 == null);
var inst_28066 = cljs.core.not.call(null,inst_28065);
var state_28096__$1 = (function (){var statearr_28101 = state_28096;
(statearr_28101[(9)] = inst_28064__$1);

return statearr_28101;
})();
if(inst_28066){
var statearr_28102_28127 = state_28096__$1;
(statearr_28102_28127[(1)] = (5));

} else {
var statearr_28103_28128 = state_28096__$1;
(statearr_28103_28128[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28097 === (15))){
var inst_28086 = (state_28096[(2)]);
var state_28096__$1 = state_28096;
var statearr_28104_28129 = state_28096__$1;
(statearr_28104_28129[(2)] = inst_28086);

(statearr_28104_28129[(1)] = (14));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28097 === (13))){
var state_28096__$1 = state_28096;
var statearr_28105_28130 = state_28096__$1;
(statearr_28105_28130[(2)] = null);

(statearr_28105_28130[(1)] = (14));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28097 === (6))){
var inst_28061 = (state_28096[(8)]);
var inst_28082 = (inst_28061 > (0));
var state_28096__$1 = state_28096;
if(cljs.core.truth_(inst_28082)){
var statearr_28106_28131 = state_28096__$1;
(statearr_28106_28131[(1)] = (12));

} else {
var statearr_28107_28132 = state_28096__$1;
(statearr_28107_28132[(1)] = (13));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28097 === (3))){
var inst_28094 = (state_28096[(2)]);
var state_28096__$1 = state_28096;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_28096__$1,inst_28094);
} else {
if((state_val_28097 === (12))){
var inst_28060 = (state_28096[(7)]);
var inst_28084 = cljs.core.vec.call(null,inst_28060);
var state_28096__$1 = state_28096;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_28096__$1,(15),out,inst_28084);
} else {
if((state_val_28097 === (2))){
var state_28096__$1 = state_28096;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_28096__$1,(4),ch);
} else {
if((state_val_28097 === (11))){
var inst_28076 = (state_28096[(2)]);
var inst_28077 = (new Array(n));
var inst_28060 = inst_28077;
var inst_28061 = (0);
var state_28096__$1 = (function (){var statearr_28108 = state_28096;
(statearr_28108[(7)] = inst_28060);

(statearr_28108[(8)] = inst_28061);

(statearr_28108[(10)] = inst_28076);

return statearr_28108;
})();
var statearr_28109_28133 = state_28096__$1;
(statearr_28109_28133[(2)] = null);

(statearr_28109_28133[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28097 === (9))){
var inst_28060 = (state_28096[(7)]);
var inst_28074 = cljs.core.vec.call(null,inst_28060);
var state_28096__$1 = state_28096;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_28096__$1,(11),out,inst_28074);
} else {
if((state_val_28097 === (5))){
var inst_28060 = (state_28096[(7)]);
var inst_28061 = (state_28096[(8)]);
var inst_28069 = (state_28096[(11)]);
var inst_28064 = (state_28096[(9)]);
var inst_28068 = (inst_28060[inst_28061] = inst_28064);
var inst_28069__$1 = (inst_28061 + (1));
var inst_28070 = (inst_28069__$1 < n);
var state_28096__$1 = (function (){var statearr_28110 = state_28096;
(statearr_28110[(11)] = inst_28069__$1);

(statearr_28110[(12)] = inst_28068);

return statearr_28110;
})();
if(cljs.core.truth_(inst_28070)){
var statearr_28111_28134 = state_28096__$1;
(statearr_28111_28134[(1)] = (8));

} else {
var statearr_28112_28135 = state_28096__$1;
(statearr_28112_28135[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28097 === (14))){
var inst_28089 = (state_28096[(2)]);
var inst_28090 = cljs.core.async.close_BANG_.call(null,out);
var state_28096__$1 = (function (){var statearr_28114 = state_28096;
(statearr_28114[(13)] = inst_28089);

return statearr_28114;
})();
var statearr_28115_28136 = state_28096__$1;
(statearr_28115_28136[(2)] = inst_28090);

(statearr_28115_28136[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28097 === (10))){
var inst_28080 = (state_28096[(2)]);
var state_28096__$1 = state_28096;
var statearr_28116_28137 = state_28096__$1;
(statearr_28116_28137[(2)] = inst_28080);

(statearr_28116_28137[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28097 === (8))){
var inst_28060 = (state_28096[(7)]);
var inst_28069 = (state_28096[(11)]);
var tmp28113 = inst_28060;
var inst_28060__$1 = tmp28113;
var inst_28061 = inst_28069;
var state_28096__$1 = (function (){var statearr_28117 = state_28096;
(statearr_28117[(7)] = inst_28060__$1);

(statearr_28117[(8)] = inst_28061);

return statearr_28117;
})();
var statearr_28118_28138 = state_28096__$1;
(statearr_28118_28138[(2)] = null);

(statearr_28118_28138[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__26309__auto___28124,out))
;
return ((function (switch__26214__auto__,c__26309__auto___28124,out){
return (function() {
var cljs$core$async$state_machine__26215__auto__ = null;
var cljs$core$async$state_machine__26215__auto____0 = (function (){
var statearr_28119 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_28119[(0)] = cljs$core$async$state_machine__26215__auto__);

(statearr_28119[(1)] = (1));

return statearr_28119;
});
var cljs$core$async$state_machine__26215__auto____1 = (function (state_28096){
while(true){
var ret_value__26216__auto__ = (function (){try{while(true){
var result__26217__auto__ = switch__26214__auto__.call(null,state_28096);
if(cljs.core.keyword_identical_QMARK_.call(null,result__26217__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__26217__auto__;
}
break;
}
}catch (e28120){if((e28120 instanceof Object)){
var ex__26218__auto__ = e28120;
var statearr_28121_28139 = state_28096;
(statearr_28121_28139[(5)] = ex__26218__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_28096);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e28120;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__26216__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__28140 = state_28096;
state_28096 = G__28140;
continue;
} else {
return ret_value__26216__auto__;
}
break;
}
});
cljs$core$async$state_machine__26215__auto__ = function(state_28096){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__26215__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__26215__auto____1.call(this,state_28096);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__26215__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__26215__auto____0;
cljs$core$async$state_machine__26215__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__26215__auto____1;
return cljs$core$async$state_machine__26215__auto__;
})()
;})(switch__26214__auto__,c__26309__auto___28124,out))
})();
var state__26311__auto__ = (function (){var statearr_28122 = f__26310__auto__.call(null);
(statearr_28122[(6)] = c__26309__auto___28124);

return statearr_28122;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__26311__auto__);
});})(c__26309__auto___28124,out))
);


return out;
});

cljs.core.async.partition.cljs$lang$maxFixedArity = 3;

/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.partition_by = (function cljs$core$async$partition_by(var_args){
var G__28142 = arguments.length;
switch (G__28142) {
case 2:
return cljs.core.async.partition_by.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.partition_by.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.partition_by.cljs$core$IFn$_invoke$arity$2 = (function (f,ch){
return cljs.core.async.partition_by.call(null,f,ch,null);
});

cljs.core.async.partition_by.cljs$core$IFn$_invoke$arity$3 = (function (f,ch,buf_or_n){
var out = cljs.core.async.chan.call(null,buf_or_n);
var c__26309__auto___28212 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__26309__auto___28212,out){
return (function (){
var f__26310__auto__ = (function (){var switch__26214__auto__ = ((function (c__26309__auto___28212,out){
return (function (state_28184){
var state_val_28185 = (state_28184[(1)]);
if((state_val_28185 === (7))){
var inst_28180 = (state_28184[(2)]);
var state_28184__$1 = state_28184;
var statearr_28186_28213 = state_28184__$1;
(statearr_28186_28213[(2)] = inst_28180);

(statearr_28186_28213[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28185 === (1))){
var inst_28143 = [];
var inst_28144 = inst_28143;
var inst_28145 = new cljs.core.Keyword("cljs.core.async","nothing","cljs.core.async/nothing",-69252123);
var state_28184__$1 = (function (){var statearr_28187 = state_28184;
(statearr_28187[(7)] = inst_28144);

(statearr_28187[(8)] = inst_28145);

return statearr_28187;
})();
var statearr_28188_28214 = state_28184__$1;
(statearr_28188_28214[(2)] = null);

(statearr_28188_28214[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28185 === (4))){
var inst_28148 = (state_28184[(9)]);
var inst_28148__$1 = (state_28184[(2)]);
var inst_28149 = (inst_28148__$1 == null);
var inst_28150 = cljs.core.not.call(null,inst_28149);
var state_28184__$1 = (function (){var statearr_28189 = state_28184;
(statearr_28189[(9)] = inst_28148__$1);

return statearr_28189;
})();
if(inst_28150){
var statearr_28190_28215 = state_28184__$1;
(statearr_28190_28215[(1)] = (5));

} else {
var statearr_28191_28216 = state_28184__$1;
(statearr_28191_28216[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28185 === (15))){
var inst_28174 = (state_28184[(2)]);
var state_28184__$1 = state_28184;
var statearr_28192_28217 = state_28184__$1;
(statearr_28192_28217[(2)] = inst_28174);

(statearr_28192_28217[(1)] = (14));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28185 === (13))){
var state_28184__$1 = state_28184;
var statearr_28193_28218 = state_28184__$1;
(statearr_28193_28218[(2)] = null);

(statearr_28193_28218[(1)] = (14));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28185 === (6))){
var inst_28144 = (state_28184[(7)]);
var inst_28169 = inst_28144.length;
var inst_28170 = (inst_28169 > (0));
var state_28184__$1 = state_28184;
if(cljs.core.truth_(inst_28170)){
var statearr_28194_28219 = state_28184__$1;
(statearr_28194_28219[(1)] = (12));

} else {
var statearr_28195_28220 = state_28184__$1;
(statearr_28195_28220[(1)] = (13));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28185 === (3))){
var inst_28182 = (state_28184[(2)]);
var state_28184__$1 = state_28184;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_28184__$1,inst_28182);
} else {
if((state_val_28185 === (12))){
var inst_28144 = (state_28184[(7)]);
var inst_28172 = cljs.core.vec.call(null,inst_28144);
var state_28184__$1 = state_28184;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_28184__$1,(15),out,inst_28172);
} else {
if((state_val_28185 === (2))){
var state_28184__$1 = state_28184;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_28184__$1,(4),ch);
} else {
if((state_val_28185 === (11))){
var inst_28148 = (state_28184[(9)]);
var inst_28152 = (state_28184[(10)]);
var inst_28162 = (state_28184[(2)]);
var inst_28163 = [];
var inst_28164 = inst_28163.push(inst_28148);
var inst_28144 = inst_28163;
var inst_28145 = inst_28152;
var state_28184__$1 = (function (){var statearr_28196 = state_28184;
(statearr_28196[(7)] = inst_28144);

(statearr_28196[(11)] = inst_28162);

(statearr_28196[(12)] = inst_28164);

(statearr_28196[(8)] = inst_28145);

return statearr_28196;
})();
var statearr_28197_28221 = state_28184__$1;
(statearr_28197_28221[(2)] = null);

(statearr_28197_28221[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28185 === (9))){
var inst_28144 = (state_28184[(7)]);
var inst_28160 = cljs.core.vec.call(null,inst_28144);
var state_28184__$1 = state_28184;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_28184__$1,(11),out,inst_28160);
} else {
if((state_val_28185 === (5))){
var inst_28148 = (state_28184[(9)]);
var inst_28152 = (state_28184[(10)]);
var inst_28145 = (state_28184[(8)]);
var inst_28152__$1 = f.call(null,inst_28148);
var inst_28153 = cljs.core._EQ_.call(null,inst_28152__$1,inst_28145);
var inst_28154 = cljs.core.keyword_identical_QMARK_.call(null,inst_28145,new cljs.core.Keyword("cljs.core.async","nothing","cljs.core.async/nothing",-69252123));
var inst_28155 = ((inst_28153) || (inst_28154));
var state_28184__$1 = (function (){var statearr_28198 = state_28184;
(statearr_28198[(10)] = inst_28152__$1);

return statearr_28198;
})();
if(cljs.core.truth_(inst_28155)){
var statearr_28199_28222 = state_28184__$1;
(statearr_28199_28222[(1)] = (8));

} else {
var statearr_28200_28223 = state_28184__$1;
(statearr_28200_28223[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28185 === (14))){
var inst_28177 = (state_28184[(2)]);
var inst_28178 = cljs.core.async.close_BANG_.call(null,out);
var state_28184__$1 = (function (){var statearr_28202 = state_28184;
(statearr_28202[(13)] = inst_28177);

return statearr_28202;
})();
var statearr_28203_28224 = state_28184__$1;
(statearr_28203_28224[(2)] = inst_28178);

(statearr_28203_28224[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28185 === (10))){
var inst_28167 = (state_28184[(2)]);
var state_28184__$1 = state_28184;
var statearr_28204_28225 = state_28184__$1;
(statearr_28204_28225[(2)] = inst_28167);

(statearr_28204_28225[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28185 === (8))){
var inst_28148 = (state_28184[(9)]);
var inst_28152 = (state_28184[(10)]);
var inst_28144 = (state_28184[(7)]);
var inst_28157 = inst_28144.push(inst_28148);
var tmp28201 = inst_28144;
var inst_28144__$1 = tmp28201;
var inst_28145 = inst_28152;
var state_28184__$1 = (function (){var statearr_28205 = state_28184;
(statearr_28205[(14)] = inst_28157);

(statearr_28205[(7)] = inst_28144__$1);

(statearr_28205[(8)] = inst_28145);

return statearr_28205;
})();
var statearr_28206_28226 = state_28184__$1;
(statearr_28206_28226[(2)] = null);

(statearr_28206_28226[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__26309__auto___28212,out))
;
return ((function (switch__26214__auto__,c__26309__auto___28212,out){
return (function() {
var cljs$core$async$state_machine__26215__auto__ = null;
var cljs$core$async$state_machine__26215__auto____0 = (function (){
var statearr_28207 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_28207[(0)] = cljs$core$async$state_machine__26215__auto__);

(statearr_28207[(1)] = (1));

return statearr_28207;
});
var cljs$core$async$state_machine__26215__auto____1 = (function (state_28184){
while(true){
var ret_value__26216__auto__ = (function (){try{while(true){
var result__26217__auto__ = switch__26214__auto__.call(null,state_28184);
if(cljs.core.keyword_identical_QMARK_.call(null,result__26217__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__26217__auto__;
}
break;
}
}catch (e28208){if((e28208 instanceof Object)){
var ex__26218__auto__ = e28208;
var statearr_28209_28227 = state_28184;
(statearr_28209_28227[(5)] = ex__26218__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_28184);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e28208;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__26216__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__28228 = state_28184;
state_28184 = G__28228;
continue;
} else {
return ret_value__26216__auto__;
}
break;
}
});
cljs$core$async$state_machine__26215__auto__ = function(state_28184){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__26215__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__26215__auto____1.call(this,state_28184);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__26215__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__26215__auto____0;
cljs$core$async$state_machine__26215__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__26215__auto____1;
return cljs$core$async$state_machine__26215__auto__;
})()
;})(switch__26214__auto__,c__26309__auto___28212,out))
})();
var state__26311__auto__ = (function (){var statearr_28210 = f__26310__auto__.call(null);
(statearr_28210[(6)] = c__26309__auto___28212);

return statearr_28210;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__26311__auto__);
});})(c__26309__auto___28212,out))
);


return out;
});

cljs.core.async.partition_by.cljs$lang$maxFixedArity = 3;


//# sourceMappingURL=async.js.map?rel=1591997116422
