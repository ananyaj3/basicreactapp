// Compiled by ClojureScript 1.10.520 {}
goog.provide('figwheel.client.file_reloading');
goog.require('cljs.core');
goog.require('figwheel.client.utils');
goog.require('goog.Uri');
goog.require('goog.string');
goog.require('goog.object');
goog.require('goog.net.jsloader');
goog.require('goog.html.legacyconversions');
goog.require('clojure.string');
goog.require('clojure.set');
goog.require('cljs.core.async');
goog.require('goog.async.Deferred');
if((typeof figwheel !== 'undefined') && (typeof figwheel.client !== 'undefined') && (typeof figwheel.client.file_reloading !== 'undefined') && (typeof figwheel.client.file_reloading.figwheel_meta_pragmas !== 'undefined')){
} else {
figwheel.client.file_reloading.figwheel_meta_pragmas = cljs.core.atom.call(null,cljs.core.PersistentArrayMap.EMPTY);
}
figwheel.client.file_reloading.on_jsload_custom_event = (function figwheel$client$file_reloading$on_jsload_custom_event(url){
return figwheel.client.utils.dispatch_custom_event.call(null,"figwheel.js-reload",url);
});
figwheel.client.file_reloading.before_jsload_custom_event = (function figwheel$client$file_reloading$before_jsload_custom_event(files){
return figwheel.client.utils.dispatch_custom_event.call(null,"figwheel.before-js-reload",files);
});
figwheel.client.file_reloading.on_cssload_custom_event = (function figwheel$client$file_reloading$on_cssload_custom_event(files){
return figwheel.client.utils.dispatch_custom_event.call(null,"figwheel.css-reload",files);
});
figwheel.client.file_reloading.namespace_file_map_QMARK_ = (function figwheel$client$file_reloading$namespace_file_map_QMARK_(m){
var or__4131__auto__ = ((cljs.core.map_QMARK_.call(null,m)) && (typeof new cljs.core.Keyword(null,"namespace","namespace",-377510372).cljs$core$IFn$_invoke$arity$1(m) === 'string') && ((((new cljs.core.Keyword(null,"file","file",-1269645878).cljs$core$IFn$_invoke$arity$1(m) == null)) || (typeof new cljs.core.Keyword(null,"file","file",-1269645878).cljs$core$IFn$_invoke$arity$1(m) === 'string'))) && (cljs.core._EQ_.call(null,new cljs.core.Keyword(null,"type","type",1174270348).cljs$core$IFn$_invoke$arity$1(m),new cljs.core.Keyword(null,"namespace","namespace",-377510372))));
if(or__4131__auto__){
return or__4131__auto__;
} else {
cljs.core.println.call(null,"Error not namespace-file-map",cljs.core.pr_str.call(null,m));

return false;
}
});
figwheel.client.file_reloading.add_cache_buster = (function figwheel$client$file_reloading$add_cache_buster(url){

return goog.Uri.parse(url).makeUnique();
});
figwheel.client.file_reloading.name__GT_path = (function figwheel$client$file_reloading$name__GT_path(ns){

return goog.object.get(goog.dependencies_.nameToPath,ns);
});
figwheel.client.file_reloading.provided_QMARK_ = (function figwheel$client$file_reloading$provided_QMARK_(ns){
return goog.object.get(goog.dependencies_.written,figwheel.client.file_reloading.name__GT_path.call(null,ns));
});
figwheel.client.file_reloading.immutable_ns_QMARK_ = (function figwheel$client$file_reloading$immutable_ns_QMARK_(name){
var or__4131__auto__ = new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 3, ["cljs.nodejs",null,"goog",null,"cljs.core",null], null), null).call(null,name);
if(cljs.core.truth_(or__4131__auto__)){
return or__4131__auto__;
} else {
var or__4131__auto____$1 = goog.string.startsWith("clojure.",name);
if(cljs.core.truth_(or__4131__auto____$1)){
return or__4131__auto____$1;
} else {
return goog.string.startsWith("goog.",name);
}
}
});
figwheel.client.file_reloading.get_requires = (function figwheel$client$file_reloading$get_requires(ns){
return cljs.core.set.call(null,cljs.core.filter.call(null,(function (p1__28423_SHARP_){
return cljs.core.not.call(null,figwheel.client.file_reloading.immutable_ns_QMARK_.call(null,p1__28423_SHARP_));
}),goog.object.getKeys(goog.object.get(goog.dependencies_.requires,figwheel.client.file_reloading.name__GT_path.call(null,ns)))));
});
if((typeof figwheel !== 'undefined') && (typeof figwheel.client !== 'undefined') && (typeof figwheel.client.file_reloading !== 'undefined') && (typeof figwheel.client.file_reloading.dependency_data !== 'undefined')){
} else {
figwheel.client.file_reloading.dependency_data = cljs.core.atom.call(null,new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"pathToName","pathToName",-1236616181),cljs.core.PersistentArrayMap.EMPTY,new cljs.core.Keyword(null,"dependents","dependents",136812837),cljs.core.PersistentArrayMap.EMPTY], null));
}
figwheel.client.file_reloading.path_to_name_BANG_ = (function figwheel$client$file_reloading$path_to_name_BANG_(path,name){
return cljs.core.swap_BANG_.call(null,figwheel.client.file_reloading.dependency_data,cljs.core.update_in,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"pathToName","pathToName",-1236616181),path], null),cljs.core.fnil.call(null,clojure.set.union,cljs.core.PersistentHashSet.EMPTY),cljs.core.PersistentHashSet.createAsIfByAssoc([name]));
});
/**
 * Setup a path to name dependencies map.
 * That goes from path -> #{ ns-names }
 */
figwheel.client.file_reloading.setup_path__GT_name_BANG_ = (function figwheel$client$file_reloading$setup_path__GT_name_BANG_(){
var nameToPath = goog.object.filter(goog.dependencies_.nameToPath,(function (v,k,o){
return goog.string.startsWith(v,"../");
}));
return goog.object.forEach(nameToPath,((function (nameToPath){
return (function (v,k,o){
return figwheel.client.file_reloading.path_to_name_BANG_.call(null,v,k);
});})(nameToPath))
);
});
/**
 * returns a set of namespaces defined by a path
 */
figwheel.client.file_reloading.path__GT_name = (function figwheel$client$file_reloading$path__GT_name(path){
return cljs.core.get_in.call(null,cljs.core.deref.call(null,figwheel.client.file_reloading.dependency_data),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"pathToName","pathToName",-1236616181),path], null));
});
figwheel.client.file_reloading.name_to_parent_BANG_ = (function figwheel$client$file_reloading$name_to_parent_BANG_(ns,parent_ns){
return cljs.core.swap_BANG_.call(null,figwheel.client.file_reloading.dependency_data,cljs.core.update_in,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"dependents","dependents",136812837),ns], null),cljs.core.fnil.call(null,clojure.set.union,cljs.core.PersistentHashSet.EMPTY),cljs.core.PersistentHashSet.createAsIfByAssoc([parent_ns]));
});
/**
 * This reverses the goog.dependencies_.requires for looking up ns-dependents.
 */
figwheel.client.file_reloading.setup_ns__GT_dependents_BANG_ = (function figwheel$client$file_reloading$setup_ns__GT_dependents_BANG_(){
var requires = goog.object.filter(goog.dependencies_.requires,(function (v,k,o){
return goog.string.startsWith(k,"../");
}));
return goog.object.forEach(requires,((function (requires){
return (function (v,k,_){
return goog.object.forEach(v,((function (requires){
return (function (v_SINGLEQUOTE_,k_SINGLEQUOTE_,___$1){
var seq__28424 = cljs.core.seq.call(null,figwheel.client.file_reloading.path__GT_name.call(null,k));
var chunk__28425 = null;
var count__28426 = (0);
var i__28427 = (0);
while(true){
if((i__28427 < count__28426)){
var n = cljs.core._nth.call(null,chunk__28425,i__28427);
figwheel.client.file_reloading.name_to_parent_BANG_.call(null,k_SINGLEQUOTE_,n);


var G__28428 = seq__28424;
var G__28429 = chunk__28425;
var G__28430 = count__28426;
var G__28431 = (i__28427 + (1));
seq__28424 = G__28428;
chunk__28425 = G__28429;
count__28426 = G__28430;
i__28427 = G__28431;
continue;
} else {
var temp__5720__auto__ = cljs.core.seq.call(null,seq__28424);
if(temp__5720__auto__){
var seq__28424__$1 = temp__5720__auto__;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__28424__$1)){
var c__4550__auto__ = cljs.core.chunk_first.call(null,seq__28424__$1);
var G__28432 = cljs.core.chunk_rest.call(null,seq__28424__$1);
var G__28433 = c__4550__auto__;
var G__28434 = cljs.core.count.call(null,c__4550__auto__);
var G__28435 = (0);
seq__28424 = G__28432;
chunk__28425 = G__28433;
count__28426 = G__28434;
i__28427 = G__28435;
continue;
} else {
var n = cljs.core.first.call(null,seq__28424__$1);
figwheel.client.file_reloading.name_to_parent_BANG_.call(null,k_SINGLEQUOTE_,n);


var G__28436 = cljs.core.next.call(null,seq__28424__$1);
var G__28437 = null;
var G__28438 = (0);
var G__28439 = (0);
seq__28424 = G__28436;
chunk__28425 = G__28437;
count__28426 = G__28438;
i__28427 = G__28439;
continue;
}
} else {
return null;
}
}
break;
}
});})(requires))
);
});})(requires))
);
});
figwheel.client.file_reloading.ns__GT_dependents = (function figwheel$client$file_reloading$ns__GT_dependents(ns){
return cljs.core.get_in.call(null,cljs.core.deref.call(null,figwheel.client.file_reloading.dependency_data),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"dependents","dependents",136812837),ns], null));
});
figwheel.client.file_reloading.in_upper_level_QMARK_ = (function figwheel$client$file_reloading$in_upper_level_QMARK_(topo_state,current_depth,dep){
return cljs.core.some.call(null,(function (p__28440){
var vec__28441 = p__28440;
var _ = cljs.core.nth.call(null,vec__28441,(0),null);
var v = cljs.core.nth.call(null,vec__28441,(1),null);
var and__4120__auto__ = v;
if(cljs.core.truth_(and__4120__auto__)){
return v.call(null,dep);
} else {
return and__4120__auto__;
}
}),cljs.core.filter.call(null,(function (p__28444){
var vec__28445 = p__28444;
var k = cljs.core.nth.call(null,vec__28445,(0),null);
var v = cljs.core.nth.call(null,vec__28445,(1),null);
return (k > current_depth);
}),topo_state));
});
figwheel.client.file_reloading.build_topo_sort = (function figwheel$client$file_reloading$build_topo_sort(get_deps){
var get_deps__$1 = cljs.core.memoize.call(null,get_deps);
var topo_sort_helper_STAR_ = ((function (get_deps__$1){
return (function figwheel$client$file_reloading$build_topo_sort_$_topo_sort_helper_STAR_(x,depth,state){
var deps = get_deps__$1.call(null,x);
if(cljs.core.empty_QMARK_.call(null,deps)){
return null;
} else {
return topo_sort_STAR_.call(null,deps,depth,state);
}
});})(get_deps__$1))
;
var topo_sort_STAR_ = ((function (get_deps__$1){
return (function() {
var figwheel$client$file_reloading$build_topo_sort_$_topo_sort_STAR_ = null;
var figwheel$client$file_reloading$build_topo_sort_$_topo_sort_STAR___1 = (function (deps){
return figwheel$client$file_reloading$build_topo_sort_$_topo_sort_STAR_.call(null,deps,(0),cljs.core.atom.call(null,cljs.core.sorted_map.call(null)));
});
var figwheel$client$file_reloading$build_topo_sort_$_topo_sort_STAR___3 = (function (deps,depth,state){
cljs.core.swap_BANG_.call(null,state,cljs.core.update_in,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [depth], null),cljs.core.fnil.call(null,cljs.core.into,cljs.core.PersistentHashSet.EMPTY),deps);

var seq__28457_28465 = cljs.core.seq.call(null,deps);
var chunk__28458_28466 = null;
var count__28459_28467 = (0);
var i__28460_28468 = (0);
while(true){
if((i__28460_28468 < count__28459_28467)){
var dep_28469 = cljs.core._nth.call(null,chunk__28458_28466,i__28460_28468);
if(cljs.core.truth_((function (){var and__4120__auto__ = dep_28469;
if(cljs.core.truth_(and__4120__auto__)){
return cljs.core.not.call(null,figwheel.client.file_reloading.in_upper_level_QMARK_.call(null,cljs.core.deref.call(null,state),depth,dep_28469));
} else {
return and__4120__auto__;
}
})())){
topo_sort_helper_STAR_.call(null,dep_28469,(depth + (1)),state);
} else {
}


var G__28470 = seq__28457_28465;
var G__28471 = chunk__28458_28466;
var G__28472 = count__28459_28467;
var G__28473 = (i__28460_28468 + (1));
seq__28457_28465 = G__28470;
chunk__28458_28466 = G__28471;
count__28459_28467 = G__28472;
i__28460_28468 = G__28473;
continue;
} else {
var temp__5720__auto___28474 = cljs.core.seq.call(null,seq__28457_28465);
if(temp__5720__auto___28474){
var seq__28457_28475__$1 = temp__5720__auto___28474;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__28457_28475__$1)){
var c__4550__auto___28476 = cljs.core.chunk_first.call(null,seq__28457_28475__$1);
var G__28477 = cljs.core.chunk_rest.call(null,seq__28457_28475__$1);
var G__28478 = c__4550__auto___28476;
var G__28479 = cljs.core.count.call(null,c__4550__auto___28476);
var G__28480 = (0);
seq__28457_28465 = G__28477;
chunk__28458_28466 = G__28478;
count__28459_28467 = G__28479;
i__28460_28468 = G__28480;
continue;
} else {
var dep_28481 = cljs.core.first.call(null,seq__28457_28475__$1);
if(cljs.core.truth_((function (){var and__4120__auto__ = dep_28481;
if(cljs.core.truth_(and__4120__auto__)){
return cljs.core.not.call(null,figwheel.client.file_reloading.in_upper_level_QMARK_.call(null,cljs.core.deref.call(null,state),depth,dep_28481));
} else {
return and__4120__auto__;
}
})())){
topo_sort_helper_STAR_.call(null,dep_28481,(depth + (1)),state);
} else {
}


var G__28482 = cljs.core.next.call(null,seq__28457_28475__$1);
var G__28483 = null;
var G__28484 = (0);
var G__28485 = (0);
seq__28457_28465 = G__28482;
chunk__28458_28466 = G__28483;
count__28459_28467 = G__28484;
i__28460_28468 = G__28485;
continue;
}
} else {
}
}
break;
}

if(cljs.core._EQ_.call(null,depth,(0))){
return elim_dups_STAR_.call(null,cljs.core.reverse.call(null,cljs.core.vals.call(null,cljs.core.deref.call(null,state))));
} else {
return null;
}
});
figwheel$client$file_reloading$build_topo_sort_$_topo_sort_STAR_ = function(deps,depth,state){
switch(arguments.length){
case 1:
return figwheel$client$file_reloading$build_topo_sort_$_topo_sort_STAR___1.call(this,deps);
case 3:
return figwheel$client$file_reloading$build_topo_sort_$_topo_sort_STAR___3.call(this,deps,depth,state);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
figwheel$client$file_reloading$build_topo_sort_$_topo_sort_STAR_.cljs$core$IFn$_invoke$arity$1 = figwheel$client$file_reloading$build_topo_sort_$_topo_sort_STAR___1;
figwheel$client$file_reloading$build_topo_sort_$_topo_sort_STAR_.cljs$core$IFn$_invoke$arity$3 = figwheel$client$file_reloading$build_topo_sort_$_topo_sort_STAR___3;
return figwheel$client$file_reloading$build_topo_sort_$_topo_sort_STAR_;
})()
;})(get_deps__$1))
;
var elim_dups_STAR_ = ((function (get_deps__$1){
return (function figwheel$client$file_reloading$build_topo_sort_$_elim_dups_STAR_(p__28461){
var vec__28462 = p__28461;
var seq__28463 = cljs.core.seq.call(null,vec__28462);
var first__28464 = cljs.core.first.call(null,seq__28463);
var seq__28463__$1 = cljs.core.next.call(null,seq__28463);
var x = first__28464;
var xs = seq__28463__$1;
if((x == null)){
return cljs.core.List.EMPTY;
} else {
return cljs.core.cons.call(null,x,figwheel$client$file_reloading$build_topo_sort_$_elim_dups_STAR_.call(null,cljs.core.map.call(null,((function (vec__28462,seq__28463,first__28464,seq__28463__$1,x,xs,get_deps__$1){
return (function (p1__28448_SHARP_){
return clojure.set.difference.call(null,p1__28448_SHARP_,x);
});})(vec__28462,seq__28463,first__28464,seq__28463__$1,x,xs,get_deps__$1))
,xs)));
}
});})(get_deps__$1))
;
return topo_sort_STAR_;
});
figwheel.client.file_reloading.get_all_dependencies = (function figwheel$client$file_reloading$get_all_dependencies(ns){
var topo_sort_SINGLEQUOTE_ = figwheel.client.file_reloading.build_topo_sort.call(null,figwheel.client.file_reloading.get_requires);
return cljs.core.apply.call(null,cljs.core.concat,topo_sort_SINGLEQUOTE_.call(null,cljs.core.set.call(null,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [ns], null))));
});
figwheel.client.file_reloading.get_all_dependents = (function figwheel$client$file_reloading$get_all_dependents(nss){
var topo_sort_SINGLEQUOTE_ = figwheel.client.file_reloading.build_topo_sort.call(null,figwheel.client.file_reloading.ns__GT_dependents);
return cljs.core.filter.call(null,cljs.core.comp.call(null,cljs.core.not,figwheel.client.file_reloading.immutable_ns_QMARK_),cljs.core.reverse.call(null,cljs.core.apply.call(null,cljs.core.concat,topo_sort_SINGLEQUOTE_.call(null,cljs.core.set.call(null,nss)))));
});
figwheel.client.file_reloading.unprovide_BANG_ = (function figwheel$client$file_reloading$unprovide_BANG_(ns){
var path = figwheel.client.file_reloading.name__GT_path.call(null,ns);
goog.object.remove(goog.dependencies_.visited,path);

goog.object.remove(goog.dependencies_.written,path);

return goog.object.remove(goog.dependencies_.written,[cljs.core.str.cljs$core$IFn$_invoke$arity$1(goog.basePath),cljs.core.str.cljs$core$IFn$_invoke$arity$1(path)].join(''));
});
figwheel.client.file_reloading.resolve_ns = (function figwheel$client$file_reloading$resolve_ns(ns){
return [cljs.core.str.cljs$core$IFn$_invoke$arity$1(goog.basePath),cljs.core.str.cljs$core$IFn$_invoke$arity$1(figwheel.client.file_reloading.name__GT_path.call(null,ns))].join('');
});
figwheel.client.file_reloading.addDependency = (function figwheel$client$file_reloading$addDependency(path,provides,requires){
var seq__28486 = cljs.core.seq.call(null,provides);
var chunk__28487 = null;
var count__28488 = (0);
var i__28489 = (0);
while(true){
if((i__28489 < count__28488)){
var prov = cljs.core._nth.call(null,chunk__28487,i__28489);
figwheel.client.file_reloading.path_to_name_BANG_.call(null,path,prov);

var seq__28498_28506 = cljs.core.seq.call(null,requires);
var chunk__28499_28507 = null;
var count__28500_28508 = (0);
var i__28501_28509 = (0);
while(true){
if((i__28501_28509 < count__28500_28508)){
var req_28510 = cljs.core._nth.call(null,chunk__28499_28507,i__28501_28509);
figwheel.client.file_reloading.name_to_parent_BANG_.call(null,req_28510,prov);


var G__28511 = seq__28498_28506;
var G__28512 = chunk__28499_28507;
var G__28513 = count__28500_28508;
var G__28514 = (i__28501_28509 + (1));
seq__28498_28506 = G__28511;
chunk__28499_28507 = G__28512;
count__28500_28508 = G__28513;
i__28501_28509 = G__28514;
continue;
} else {
var temp__5720__auto___28515 = cljs.core.seq.call(null,seq__28498_28506);
if(temp__5720__auto___28515){
var seq__28498_28516__$1 = temp__5720__auto___28515;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__28498_28516__$1)){
var c__4550__auto___28517 = cljs.core.chunk_first.call(null,seq__28498_28516__$1);
var G__28518 = cljs.core.chunk_rest.call(null,seq__28498_28516__$1);
var G__28519 = c__4550__auto___28517;
var G__28520 = cljs.core.count.call(null,c__4550__auto___28517);
var G__28521 = (0);
seq__28498_28506 = G__28518;
chunk__28499_28507 = G__28519;
count__28500_28508 = G__28520;
i__28501_28509 = G__28521;
continue;
} else {
var req_28522 = cljs.core.first.call(null,seq__28498_28516__$1);
figwheel.client.file_reloading.name_to_parent_BANG_.call(null,req_28522,prov);


var G__28523 = cljs.core.next.call(null,seq__28498_28516__$1);
var G__28524 = null;
var G__28525 = (0);
var G__28526 = (0);
seq__28498_28506 = G__28523;
chunk__28499_28507 = G__28524;
count__28500_28508 = G__28525;
i__28501_28509 = G__28526;
continue;
}
} else {
}
}
break;
}


var G__28527 = seq__28486;
var G__28528 = chunk__28487;
var G__28529 = count__28488;
var G__28530 = (i__28489 + (1));
seq__28486 = G__28527;
chunk__28487 = G__28528;
count__28488 = G__28529;
i__28489 = G__28530;
continue;
} else {
var temp__5720__auto__ = cljs.core.seq.call(null,seq__28486);
if(temp__5720__auto__){
var seq__28486__$1 = temp__5720__auto__;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__28486__$1)){
var c__4550__auto__ = cljs.core.chunk_first.call(null,seq__28486__$1);
var G__28531 = cljs.core.chunk_rest.call(null,seq__28486__$1);
var G__28532 = c__4550__auto__;
var G__28533 = cljs.core.count.call(null,c__4550__auto__);
var G__28534 = (0);
seq__28486 = G__28531;
chunk__28487 = G__28532;
count__28488 = G__28533;
i__28489 = G__28534;
continue;
} else {
var prov = cljs.core.first.call(null,seq__28486__$1);
figwheel.client.file_reloading.path_to_name_BANG_.call(null,path,prov);

var seq__28502_28535 = cljs.core.seq.call(null,requires);
var chunk__28503_28536 = null;
var count__28504_28537 = (0);
var i__28505_28538 = (0);
while(true){
if((i__28505_28538 < count__28504_28537)){
var req_28539 = cljs.core._nth.call(null,chunk__28503_28536,i__28505_28538);
figwheel.client.file_reloading.name_to_parent_BANG_.call(null,req_28539,prov);


var G__28540 = seq__28502_28535;
var G__28541 = chunk__28503_28536;
var G__28542 = count__28504_28537;
var G__28543 = (i__28505_28538 + (1));
seq__28502_28535 = G__28540;
chunk__28503_28536 = G__28541;
count__28504_28537 = G__28542;
i__28505_28538 = G__28543;
continue;
} else {
var temp__5720__auto___28544__$1 = cljs.core.seq.call(null,seq__28502_28535);
if(temp__5720__auto___28544__$1){
var seq__28502_28545__$1 = temp__5720__auto___28544__$1;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__28502_28545__$1)){
var c__4550__auto___28546 = cljs.core.chunk_first.call(null,seq__28502_28545__$1);
var G__28547 = cljs.core.chunk_rest.call(null,seq__28502_28545__$1);
var G__28548 = c__4550__auto___28546;
var G__28549 = cljs.core.count.call(null,c__4550__auto___28546);
var G__28550 = (0);
seq__28502_28535 = G__28547;
chunk__28503_28536 = G__28548;
count__28504_28537 = G__28549;
i__28505_28538 = G__28550;
continue;
} else {
var req_28551 = cljs.core.first.call(null,seq__28502_28545__$1);
figwheel.client.file_reloading.name_to_parent_BANG_.call(null,req_28551,prov);


var G__28552 = cljs.core.next.call(null,seq__28502_28545__$1);
var G__28553 = null;
var G__28554 = (0);
var G__28555 = (0);
seq__28502_28535 = G__28552;
chunk__28503_28536 = G__28553;
count__28504_28537 = G__28554;
i__28505_28538 = G__28555;
continue;
}
} else {
}
}
break;
}


var G__28556 = cljs.core.next.call(null,seq__28486__$1);
var G__28557 = null;
var G__28558 = (0);
var G__28559 = (0);
seq__28486 = G__28556;
chunk__28487 = G__28557;
count__28488 = G__28558;
i__28489 = G__28559;
continue;
}
} else {
return null;
}
}
break;
}
});
figwheel.client.file_reloading.figwheel_require = (function figwheel$client$file_reloading$figwheel_require(src,reload){
goog.require = figwheel.client.file_reloading.figwheel_require;

if(cljs.core._EQ_.call(null,reload,"reload-all")){
var seq__28560_28564 = cljs.core.seq.call(null,figwheel.client.file_reloading.get_all_dependencies.call(null,src));
var chunk__28561_28565 = null;
var count__28562_28566 = (0);
var i__28563_28567 = (0);
while(true){
if((i__28563_28567 < count__28562_28566)){
var ns_28568 = cljs.core._nth.call(null,chunk__28561_28565,i__28563_28567);
figwheel.client.file_reloading.unprovide_BANG_.call(null,ns_28568);


var G__28569 = seq__28560_28564;
var G__28570 = chunk__28561_28565;
var G__28571 = count__28562_28566;
var G__28572 = (i__28563_28567 + (1));
seq__28560_28564 = G__28569;
chunk__28561_28565 = G__28570;
count__28562_28566 = G__28571;
i__28563_28567 = G__28572;
continue;
} else {
var temp__5720__auto___28573 = cljs.core.seq.call(null,seq__28560_28564);
if(temp__5720__auto___28573){
var seq__28560_28574__$1 = temp__5720__auto___28573;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__28560_28574__$1)){
var c__4550__auto___28575 = cljs.core.chunk_first.call(null,seq__28560_28574__$1);
var G__28576 = cljs.core.chunk_rest.call(null,seq__28560_28574__$1);
var G__28577 = c__4550__auto___28575;
var G__28578 = cljs.core.count.call(null,c__4550__auto___28575);
var G__28579 = (0);
seq__28560_28564 = G__28576;
chunk__28561_28565 = G__28577;
count__28562_28566 = G__28578;
i__28563_28567 = G__28579;
continue;
} else {
var ns_28580 = cljs.core.first.call(null,seq__28560_28574__$1);
figwheel.client.file_reloading.unprovide_BANG_.call(null,ns_28580);


var G__28581 = cljs.core.next.call(null,seq__28560_28574__$1);
var G__28582 = null;
var G__28583 = (0);
var G__28584 = (0);
seq__28560_28564 = G__28581;
chunk__28561_28565 = G__28582;
count__28562_28566 = G__28583;
i__28563_28567 = G__28584;
continue;
}
} else {
}
}
break;
}
} else {
}

if(cljs.core.truth_(reload)){
figwheel.client.file_reloading.unprovide_BANG_.call(null,src);
} else {
}

return goog.require_figwheel_backup_(src);
});
/**
 * Reusable browser REPL bootstrapping. Patches the essential functions
 *   in goog.base to support re-loading of namespaces after page load.
 */
figwheel.client.file_reloading.bootstrap_goog_base = (function figwheel$client$file_reloading$bootstrap_goog_base(){
if(cljs.core.truth_(COMPILED)){
return null;
} else {
goog.require_figwheel_backup_ = (function (){var or__4131__auto__ = goog.require__;
if(cljs.core.truth_(or__4131__auto__)){
return or__4131__auto__;
} else {
return goog.require;
}
})();

goog.isProvided_ = (function (name){
return false;
});

figwheel.client.file_reloading.setup_path__GT_name_BANG_.call(null);

figwheel.client.file_reloading.setup_ns__GT_dependents_BANG_.call(null);

goog.addDependency_figwheel_backup_ = goog.addDependency;

goog.addDependency = (function() { 
var G__28585__delegate = function (args){
cljs.core.apply.call(null,figwheel.client.file_reloading.addDependency,args);

return cljs.core.apply.call(null,goog.addDependency_figwheel_backup_,args);
};
var G__28585 = function (var_args){
var args = null;
if (arguments.length > 0) {
var G__28586__i = 0, G__28586__a = new Array(arguments.length -  0);
while (G__28586__i < G__28586__a.length) {G__28586__a[G__28586__i] = arguments[G__28586__i + 0]; ++G__28586__i;}
  args = new cljs.core.IndexedSeq(G__28586__a,0,null);
} 
return G__28585__delegate.call(this,args);};
G__28585.cljs$lang$maxFixedArity = 0;
G__28585.cljs$lang$applyTo = (function (arglist__28587){
var args = cljs.core.seq(arglist__28587);
return G__28585__delegate(args);
});
G__28585.cljs$core$IFn$_invoke$arity$variadic = G__28585__delegate;
return G__28585;
})()
;

goog.constructNamespace_("cljs.user");

goog.global.CLOSURE_IMPORT_SCRIPT = figwheel.client.file_reloading.queued_file_reload;

return goog.require = figwheel.client.file_reloading.figwheel_require;
}
});
figwheel.client.file_reloading.patch_goog_base = (function figwheel$client$file_reloading$patch_goog_base(){
if((typeof figwheel !== 'undefined') && (typeof figwheel.client !== 'undefined') && (typeof figwheel.client.file_reloading !== 'undefined') && (typeof figwheel.client.file_reloading.bootstrapped_cljs !== 'undefined')){
return null;
} else {
return (
figwheel.client.file_reloading.bootstrapped_cljs = (function (){
figwheel.client.file_reloading.bootstrap_goog_base.call(null);

return true;
})()
)
;
}
});
figwheel.client.file_reloading.gloader = (((typeof goog !== 'undefined') && (typeof goog.net !== 'undefined') && (typeof goog.net.jsloader !== 'undefined') && (typeof goog.net.jsloader.safeLoad !== 'undefined'))?(function (p1__28588_SHARP_,p2__28589_SHARP_){
return goog.net.jsloader.safeLoad(goog.html.legacyconversions.trustedResourceUrlFromString(cljs.core.str.cljs$core$IFn$_invoke$arity$1(p1__28588_SHARP_)),p2__28589_SHARP_);
}):(((typeof goog !== 'undefined') && (typeof goog.net !== 'undefined') && (typeof goog.net.jsloader !== 'undefined') && (typeof goog.net.jsloader.load !== 'undefined'))?(function (p1__28590_SHARP_,p2__28591_SHARP_){
return goog.net.jsloader.load(cljs.core.str.cljs$core$IFn$_invoke$arity$1(p1__28590_SHARP_),p2__28591_SHARP_);
}):(function(){throw cljs.core.ex_info.call(null,"No remote script loading function found.",cljs.core.PersistentArrayMap.EMPTY)})()
));
figwheel.client.file_reloading.reload_file_in_html_env = (function figwheel$client$file_reloading$reload_file_in_html_env(request_url,callback){

var G__28592 = figwheel.client.file_reloading.gloader.call(null,figwheel.client.file_reloading.add_cache_buster.call(null,request_url),({"cleanupWhenDone": true}));
G__28592.addCallback(((function (G__28592){
return (function (){
return cljs.core.apply.call(null,callback,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [true], null));
});})(G__28592))
);

G__28592.addErrback(((function (G__28592){
return (function (){
return cljs.core.apply.call(null,callback,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [false], null));
});})(G__28592))
);

return G__28592;
});
figwheel.client.file_reloading.write_script_tag_import = figwheel.client.file_reloading.reload_file_in_html_env;
goog.exportSymbol('figwheel.client.file_reloading.write_script_tag_import', figwheel.client.file_reloading.write_script_tag_import);
figwheel.client.file_reloading.worker_import_script = (function figwheel$client$file_reloading$worker_import_script(request_url,callback){

return callback.call(null,(function (){try{self.importScripts(figwheel.client.file_reloading.add_cache_buster.call(null,request_url));

return true;
}catch (e28593){if((e28593 instanceof Error)){
var e = e28593;
figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"error","error",-978969032),["Figwheel: Error loading file ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(request_url)].join(''));

figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"error","error",-978969032),e.stack);

return false;
} else {
throw e28593;

}
}})());
});
goog.exportSymbol('figwheel.client.file_reloading.worker_import_script', figwheel.client.file_reloading.worker_import_script);
figwheel.client.file_reloading.create_node_script_import_fn = (function figwheel$client$file_reloading$create_node_script_import_fn(){
var node_path_lib = require("path");
var util_pattern = [cljs.core.str.cljs$core$IFn$_invoke$arity$1(node_path_lib.sep),cljs.core.str.cljs$core$IFn$_invoke$arity$1(node_path_lib.join("goog","bootstrap","nodejs.js"))].join('');
var util_path = goog.object.findKey(require.cache,((function (node_path_lib,util_pattern){
return (function (v,k,o){
return goog.string.endsWith(k,util_pattern);
});})(node_path_lib,util_pattern))
);
var parts = cljs.core.pop.call(null,cljs.core.pop.call(null,clojure.string.split.call(null,util_path,/[\/\\]/)));
var root_path = clojure.string.join.call(null,node_path_lib.sep,parts);
return ((function (node_path_lib,util_pattern,util_path,parts,root_path){
return (function (request_url,callback){

var cache_path = node_path_lib.resolve(root_path,request_url);
goog.object.remove(require.cache,cache_path);

return callback.call(null,(function (){try{return require(cache_path);
}catch (e28594){if((e28594 instanceof Error)){
var e = e28594;
figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"error","error",-978969032),["Figwheel: Error loading file ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(cache_path)].join(''));

figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"error","error",-978969032),e.stack);

return false;
} else {
throw e28594;

}
}})());
});
;})(node_path_lib,util_pattern,util_path,parts,root_path))
});
goog.exportSymbol('figwheel.client.file_reloading.create_node_script_import_fn', figwheel.client.file_reloading.create_node_script_import_fn);
figwheel.client.file_reloading.reload_file_STAR_ = (function (){var pred__28595 = cljs.core._EQ_;
var expr__28596 = figwheel.client.utils.host_env_QMARK_.call(null);
if(cljs.core.truth_(pred__28595.call(null,new cljs.core.Keyword(null,"node","node",581201198),expr__28596))){
return figwheel.client.file_reloading.create_node_script_import_fn.call(null);
} else {
if(cljs.core.truth_(pred__28595.call(null,new cljs.core.Keyword(null,"html","html",-998796897),expr__28596))){
return figwheel.client.file_reloading.write_script_tag_import;
} else {
if(cljs.core.truth_(pred__28595.call(null,new cljs.core.Keyword(null,"worker","worker",938239996),expr__28596))){
return figwheel.client.file_reloading.worker_import_script;
} else {
return ((function (pred__28595,expr__28596){
return (function (a,b){
throw "Reload not defined for this platform";
});
;})(pred__28595,expr__28596))
}
}
}
})();
figwheel.client.file_reloading.reload_file = (function figwheel$client$file_reloading$reload_file(p__28598,callback){
var map__28599 = p__28598;
var map__28599__$1 = (((((!((map__28599 == null))))?(((((map__28599.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__28599.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__28599):map__28599);
var file_msg = map__28599__$1;
var request_url = cljs.core.get.call(null,map__28599__$1,new cljs.core.Keyword(null,"request-url","request-url",2100346596));

figwheel.client.utils.debug_prn.call(null,["FigWheel: Attempting to load ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(request_url)].join(''));

return (function (){var or__4131__auto__ = goog.object.get(goog.global,"FIGWHEEL_IMPORT_SCRIPT");
if(cljs.core.truth_(or__4131__auto__)){
return or__4131__auto__;
} else {
return figwheel.client.file_reloading.reload_file_STAR_;
}
})().call(null,request_url,((function (map__28599,map__28599__$1,file_msg,request_url){
return (function (success_QMARK_){
if(cljs.core.truth_(success_QMARK_)){
figwheel.client.utils.debug_prn.call(null,["FigWheel: Successfully loaded ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(request_url)].join(''));

return cljs.core.apply.call(null,callback,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.assoc.call(null,file_msg,new cljs.core.Keyword(null,"loaded-file","loaded-file",-168399375),true)], null));
} else {
figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"error","error",-978969032),["Figwheel: Error loading file ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(request_url)].join(''));

return cljs.core.apply.call(null,callback,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [file_msg], null));
}
});})(map__28599,map__28599__$1,file_msg,request_url))
);
});
if((typeof figwheel !== 'undefined') && (typeof figwheel.client !== 'undefined') && (typeof figwheel.client.file_reloading !== 'undefined') && (typeof figwheel.client.file_reloading.reload_chan !== 'undefined')){
} else {
figwheel.client.file_reloading.reload_chan = cljs.core.async.chan.call(null);
}
if((typeof figwheel !== 'undefined') && (typeof figwheel.client !== 'undefined') && (typeof figwheel.client.file_reloading !== 'undefined') && (typeof figwheel.client.file_reloading.on_load_callbacks !== 'undefined')){
} else {
figwheel.client.file_reloading.on_load_callbacks = cljs.core.atom.call(null,cljs.core.PersistentArrayMap.EMPTY);
}
if((typeof figwheel !== 'undefined') && (typeof figwheel.client !== 'undefined') && (typeof figwheel.client.file_reloading !== 'undefined') && (typeof figwheel.client.file_reloading.dependencies_loaded !== 'undefined')){
} else {
figwheel.client.file_reloading.dependencies_loaded = cljs.core.atom.call(null,cljs.core.PersistentVector.EMPTY);
}
figwheel.client.file_reloading.blocking_load = (function figwheel$client$file_reloading$blocking_load(url){
var out = cljs.core.async.chan.call(null);
figwheel.client.file_reloading.reload_file.call(null,new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"request-url","request-url",2100346596),url], null),((function (out){
return (function (file_msg){
cljs.core.async.put_BANG_.call(null,out,file_msg);

return cljs.core.async.close_BANG_.call(null,out);
});})(out))
);

return out;
});
if((typeof figwheel !== 'undefined') && (typeof figwheel.client !== 'undefined') && (typeof figwheel.client.file_reloading !== 'undefined') && (typeof figwheel.client.file_reloading.reloader_loop !== 'undefined')){
} else {
figwheel.client.file_reloading.reloader_loop = (function (){var c__26309__auto__ = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__26309__auto__){
return (function (){
var f__26310__auto__ = (function (){var switch__26214__auto__ = ((function (c__26309__auto__){
return (function (state_28637){
var state_val_28638 = (state_28637[(1)]);
if((state_val_28638 === (7))){
var inst_28633 = (state_28637[(2)]);
var state_28637__$1 = state_28637;
var statearr_28639_28665 = state_28637__$1;
(statearr_28639_28665[(2)] = inst_28633);

(statearr_28639_28665[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28638 === (1))){
var state_28637__$1 = state_28637;
var statearr_28640_28666 = state_28637__$1;
(statearr_28640_28666[(2)] = null);

(statearr_28640_28666[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28638 === (4))){
var inst_28603 = (state_28637[(7)]);
var inst_28603__$1 = (state_28637[(2)]);
var state_28637__$1 = (function (){var statearr_28641 = state_28637;
(statearr_28641[(7)] = inst_28603__$1);

return statearr_28641;
})();
if(cljs.core.truth_(inst_28603__$1)){
var statearr_28642_28667 = state_28637__$1;
(statearr_28642_28667[(1)] = (5));

} else {
var statearr_28643_28668 = state_28637__$1;
(statearr_28643_28668[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28638 === (15))){
var inst_28616 = (state_28637[(8)]);
var inst_28618 = (state_28637[(9)]);
var inst_28620 = inst_28618.call(null,inst_28616);
var state_28637__$1 = state_28637;
var statearr_28644_28669 = state_28637__$1;
(statearr_28644_28669[(2)] = inst_28620);

(statearr_28644_28669[(1)] = (17));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28638 === (13))){
var inst_28627 = (state_28637[(2)]);
var state_28637__$1 = state_28637;
var statearr_28645_28670 = state_28637__$1;
(statearr_28645_28670[(2)] = inst_28627);

(statearr_28645_28670[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28638 === (6))){
var state_28637__$1 = state_28637;
var statearr_28646_28671 = state_28637__$1;
(statearr_28646_28671[(2)] = null);

(statearr_28646_28671[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28638 === (17))){
var inst_28624 = (state_28637[(2)]);
var state_28637__$1 = state_28637;
var statearr_28647_28672 = state_28637__$1;
(statearr_28647_28672[(2)] = inst_28624);

(statearr_28647_28672[(1)] = (13));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28638 === (3))){
var inst_28635 = (state_28637[(2)]);
var state_28637__$1 = state_28637;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_28637__$1,inst_28635);
} else {
if((state_val_28638 === (12))){
var state_28637__$1 = state_28637;
var statearr_28648_28673 = state_28637__$1;
(statearr_28648_28673[(2)] = null);

(statearr_28648_28673[(1)] = (13));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28638 === (2))){
var state_28637__$1 = state_28637;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_28637__$1,(4),figwheel.client.file_reloading.reload_chan);
} else {
if((state_val_28638 === (11))){
var inst_28608 = (state_28637[(10)]);
var inst_28614 = figwheel.client.file_reloading.blocking_load.call(null,inst_28608);
var state_28637__$1 = state_28637;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_28637__$1,(14),inst_28614);
} else {
if((state_val_28638 === (9))){
var inst_28608 = (state_28637[(10)]);
var state_28637__$1 = state_28637;
if(cljs.core.truth_(inst_28608)){
var statearr_28649_28674 = state_28637__$1;
(statearr_28649_28674[(1)] = (11));

} else {
var statearr_28650_28675 = state_28637__$1;
(statearr_28650_28675[(1)] = (12));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28638 === (5))){
var inst_28603 = (state_28637[(7)]);
var inst_28609 = (state_28637[(11)]);
var inst_28608 = cljs.core.nth.call(null,inst_28603,(0),null);
var inst_28609__$1 = cljs.core.nth.call(null,inst_28603,(1),null);
var state_28637__$1 = (function (){var statearr_28651 = state_28637;
(statearr_28651[(10)] = inst_28608);

(statearr_28651[(11)] = inst_28609__$1);

return statearr_28651;
})();
if(cljs.core.truth_(inst_28609__$1)){
var statearr_28652_28676 = state_28637__$1;
(statearr_28652_28676[(1)] = (8));

} else {
var statearr_28653_28677 = state_28637__$1;
(statearr_28653_28677[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28638 === (14))){
var inst_28608 = (state_28637[(10)]);
var inst_28618 = (state_28637[(9)]);
var inst_28616 = (state_28637[(2)]);
var inst_28617 = cljs.core.deref.call(null,figwheel.client.file_reloading.on_load_callbacks);
var inst_28618__$1 = cljs.core.get.call(null,inst_28617,inst_28608);
var state_28637__$1 = (function (){var statearr_28654 = state_28637;
(statearr_28654[(8)] = inst_28616);

(statearr_28654[(9)] = inst_28618__$1);

return statearr_28654;
})();
if(cljs.core.truth_(inst_28618__$1)){
var statearr_28655_28678 = state_28637__$1;
(statearr_28655_28678[(1)] = (15));

} else {
var statearr_28656_28679 = state_28637__$1;
(statearr_28656_28679[(1)] = (16));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28638 === (16))){
var inst_28616 = (state_28637[(8)]);
var inst_28622 = cljs.core.swap_BANG_.call(null,figwheel.client.file_reloading.dependencies_loaded,cljs.core.conj,inst_28616);
var state_28637__$1 = state_28637;
var statearr_28657_28680 = state_28637__$1;
(statearr_28657_28680[(2)] = inst_28622);

(statearr_28657_28680[(1)] = (17));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28638 === (10))){
var inst_28629 = (state_28637[(2)]);
var state_28637__$1 = (function (){var statearr_28658 = state_28637;
(statearr_28658[(12)] = inst_28629);

return statearr_28658;
})();
var statearr_28659_28681 = state_28637__$1;
(statearr_28659_28681[(2)] = null);

(statearr_28659_28681[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28638 === (8))){
var inst_28609 = (state_28637[(11)]);
var inst_28611 = eval(inst_28609);
var state_28637__$1 = state_28637;
var statearr_28660_28682 = state_28637__$1;
(statearr_28660_28682[(2)] = inst_28611);

(statearr_28660_28682[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__26309__auto__))
;
return ((function (switch__26214__auto__,c__26309__auto__){
return (function() {
var figwheel$client$file_reloading$state_machine__26215__auto__ = null;
var figwheel$client$file_reloading$state_machine__26215__auto____0 = (function (){
var statearr_28661 = [null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_28661[(0)] = figwheel$client$file_reloading$state_machine__26215__auto__);

(statearr_28661[(1)] = (1));

return statearr_28661;
});
var figwheel$client$file_reloading$state_machine__26215__auto____1 = (function (state_28637){
while(true){
var ret_value__26216__auto__ = (function (){try{while(true){
var result__26217__auto__ = switch__26214__auto__.call(null,state_28637);
if(cljs.core.keyword_identical_QMARK_.call(null,result__26217__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__26217__auto__;
}
break;
}
}catch (e28662){if((e28662 instanceof Object)){
var ex__26218__auto__ = e28662;
var statearr_28663_28683 = state_28637;
(statearr_28663_28683[(5)] = ex__26218__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_28637);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e28662;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__26216__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__28684 = state_28637;
state_28637 = G__28684;
continue;
} else {
return ret_value__26216__auto__;
}
break;
}
});
figwheel$client$file_reloading$state_machine__26215__auto__ = function(state_28637){
switch(arguments.length){
case 0:
return figwheel$client$file_reloading$state_machine__26215__auto____0.call(this);
case 1:
return figwheel$client$file_reloading$state_machine__26215__auto____1.call(this,state_28637);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
figwheel$client$file_reloading$state_machine__26215__auto__.cljs$core$IFn$_invoke$arity$0 = figwheel$client$file_reloading$state_machine__26215__auto____0;
figwheel$client$file_reloading$state_machine__26215__auto__.cljs$core$IFn$_invoke$arity$1 = figwheel$client$file_reloading$state_machine__26215__auto____1;
return figwheel$client$file_reloading$state_machine__26215__auto__;
})()
;})(switch__26214__auto__,c__26309__auto__))
})();
var state__26311__auto__ = (function (){var statearr_28664 = f__26310__auto__.call(null);
(statearr_28664[(6)] = c__26309__auto__);

return statearr_28664;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__26311__auto__);
});})(c__26309__auto__))
);

return c__26309__auto__;
})();
}
figwheel.client.file_reloading.queued_file_reload = (function figwheel$client$file_reloading$queued_file_reload(var_args){
var G__28686 = arguments.length;
switch (G__28686) {
case 1:
return figwheel.client.file_reloading.queued_file_reload.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return figwheel.client.file_reloading.queued_file_reload.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

figwheel.client.file_reloading.queued_file_reload.cljs$core$IFn$_invoke$arity$1 = (function (url){
return figwheel.client.file_reloading.queued_file_reload.call(null,url,null);
});

figwheel.client.file_reloading.queued_file_reload.cljs$core$IFn$_invoke$arity$2 = (function (url,opt_source_text){
return cljs.core.async.put_BANG_.call(null,figwheel.client.file_reloading.reload_chan,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [url,opt_source_text], null));
});

figwheel.client.file_reloading.queued_file_reload.cljs$lang$maxFixedArity = 2;

figwheel.client.file_reloading.require_with_callback = (function figwheel$client$file_reloading$require_with_callback(p__28688,callback){
var map__28689 = p__28688;
var map__28689__$1 = (((((!((map__28689 == null))))?(((((map__28689.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__28689.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__28689):map__28689);
var file_msg = map__28689__$1;
var namespace = cljs.core.get.call(null,map__28689__$1,new cljs.core.Keyword(null,"namespace","namespace",-377510372));
var request_url = figwheel.client.file_reloading.resolve_ns.call(null,namespace);
cljs.core.swap_BANG_.call(null,figwheel.client.file_reloading.on_load_callbacks,cljs.core.assoc,request_url,((function (request_url,map__28689,map__28689__$1,file_msg,namespace){
return (function (file_msg_SINGLEQUOTE_){
cljs.core.swap_BANG_.call(null,figwheel.client.file_reloading.on_load_callbacks,cljs.core.dissoc,request_url);

return cljs.core.apply.call(null,callback,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.merge.call(null,file_msg,cljs.core.select_keys.call(null,file_msg_SINGLEQUOTE_,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"loaded-file","loaded-file",-168399375)], null)))], null));
});})(request_url,map__28689,map__28689__$1,file_msg,namespace))
);

return figwheel.client.file_reloading.figwheel_require.call(null,cljs.core.name.call(null,namespace),true);
});
figwheel.client.file_reloading.figwheel_no_load_QMARK_ = (function figwheel$client$file_reloading$figwheel_no_load_QMARK_(p__28691){
var map__28692 = p__28691;
var map__28692__$1 = (((((!((map__28692 == null))))?(((((map__28692.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__28692.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__28692):map__28692);
var file_msg = map__28692__$1;
var namespace = cljs.core.get.call(null,map__28692__$1,new cljs.core.Keyword(null,"namespace","namespace",-377510372));
var meta_pragmas = cljs.core.get.call(null,cljs.core.deref.call(null,figwheel.client.file_reloading.figwheel_meta_pragmas),cljs.core.name.call(null,namespace));
return new cljs.core.Keyword(null,"figwheel-no-load","figwheel-no-load",-555840179).cljs$core$IFn$_invoke$arity$1(meta_pragmas);
});
figwheel.client.file_reloading.ns_exists_QMARK_ = (function figwheel$client$file_reloading$ns_exists_QMARK_(namespace){
return (!((cljs.core.reduce.call(null,cljs.core.fnil.call(null,goog.object.get,({})),goog.global,clojure.string.split.call(null,cljs.core.name.call(null,namespace),".")) == null)));
});
figwheel.client.file_reloading.reload_file_QMARK_ = (function figwheel$client$file_reloading$reload_file_QMARK_(p__28694){
var map__28695 = p__28694;
var map__28695__$1 = (((((!((map__28695 == null))))?(((((map__28695.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__28695.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__28695):map__28695);
var file_msg = map__28695__$1;
var namespace = cljs.core.get.call(null,map__28695__$1,new cljs.core.Keyword(null,"namespace","namespace",-377510372));

var meta_pragmas = cljs.core.get.call(null,cljs.core.deref.call(null,figwheel.client.file_reloading.figwheel_meta_pragmas),cljs.core.name.call(null,namespace));
var and__4120__auto__ = cljs.core.not.call(null,figwheel.client.file_reloading.figwheel_no_load_QMARK_.call(null,file_msg));
if(and__4120__auto__){
var or__4131__auto__ = new cljs.core.Keyword(null,"figwheel-always","figwheel-always",799819691).cljs$core$IFn$_invoke$arity$1(meta_pragmas);
if(cljs.core.truth_(or__4131__auto__)){
return or__4131__auto__;
} else {
var or__4131__auto____$1 = new cljs.core.Keyword(null,"figwheel-load","figwheel-load",1316089175).cljs$core$IFn$_invoke$arity$1(meta_pragmas);
if(cljs.core.truth_(or__4131__auto____$1)){
return or__4131__auto____$1;
} else {
var or__4131__auto____$2 = figwheel.client.file_reloading.provided_QMARK_.call(null,cljs.core.name.call(null,namespace));
if(cljs.core.truth_(or__4131__auto____$2)){
return or__4131__auto____$2;
} else {
return figwheel.client.file_reloading.ns_exists_QMARK_.call(null,namespace);
}
}
}
} else {
return and__4120__auto__;
}
});
figwheel.client.file_reloading.js_reload = (function figwheel$client$file_reloading$js_reload(p__28697,callback){
var map__28698 = p__28697;
var map__28698__$1 = (((((!((map__28698 == null))))?(((((map__28698.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__28698.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__28698):map__28698);
var file_msg = map__28698__$1;
var request_url = cljs.core.get.call(null,map__28698__$1,new cljs.core.Keyword(null,"request-url","request-url",2100346596));
var namespace = cljs.core.get.call(null,map__28698__$1,new cljs.core.Keyword(null,"namespace","namespace",-377510372));

if(cljs.core.truth_(figwheel.client.file_reloading.reload_file_QMARK_.call(null,file_msg))){
return figwheel.client.file_reloading.require_with_callback.call(null,file_msg,callback);
} else {
figwheel.client.utils.debug_prn.call(null,["Figwheel: Not trying to load file ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(request_url)].join(''));

return cljs.core.apply.call(null,callback,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [file_msg], null));
}
});
figwheel.client.file_reloading.reload_js_file = (function figwheel$client$file_reloading$reload_js_file(file_msg){
var out = cljs.core.async.chan.call(null);
figwheel.client.file_reloading.js_reload.call(null,file_msg,((function (out){
return (function (url){
cljs.core.async.put_BANG_.call(null,out,url);

return cljs.core.async.close_BANG_.call(null,out);
});})(out))
);

return out;
});
/**
 * Returns a chanel with one collection of loaded filenames on it.
 */
figwheel.client.file_reloading.load_all_js_files = (function figwheel$client$file_reloading$load_all_js_files(files){
var out = cljs.core.async.chan.call(null);
var c__26309__auto___28748 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__26309__auto___28748,out){
return (function (){
var f__26310__auto__ = (function (){var switch__26214__auto__ = ((function (c__26309__auto___28748,out){
return (function (state_28733){
var state_val_28734 = (state_28733[(1)]);
if((state_val_28734 === (1))){
var inst_28707 = cljs.core.seq.call(null,files);
var inst_28708 = cljs.core.first.call(null,inst_28707);
var inst_28709 = cljs.core.next.call(null,inst_28707);
var inst_28710 = files;
var state_28733__$1 = (function (){var statearr_28735 = state_28733;
(statearr_28735[(7)] = inst_28709);

(statearr_28735[(8)] = inst_28708);

(statearr_28735[(9)] = inst_28710);

return statearr_28735;
})();
var statearr_28736_28749 = state_28733__$1;
(statearr_28736_28749[(2)] = null);

(statearr_28736_28749[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28734 === (2))){
var inst_28710 = (state_28733[(9)]);
var inst_28716 = (state_28733[(10)]);
var inst_28715 = cljs.core.seq.call(null,inst_28710);
var inst_28716__$1 = cljs.core.first.call(null,inst_28715);
var inst_28717 = cljs.core.next.call(null,inst_28715);
var inst_28718 = (inst_28716__$1 == null);
var inst_28719 = cljs.core.not.call(null,inst_28718);
var state_28733__$1 = (function (){var statearr_28737 = state_28733;
(statearr_28737[(11)] = inst_28717);

(statearr_28737[(10)] = inst_28716__$1);

return statearr_28737;
})();
if(inst_28719){
var statearr_28738_28750 = state_28733__$1;
(statearr_28738_28750[(1)] = (4));

} else {
var statearr_28739_28751 = state_28733__$1;
(statearr_28739_28751[(1)] = (5));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28734 === (3))){
var inst_28731 = (state_28733[(2)]);
var state_28733__$1 = state_28733;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_28733__$1,inst_28731);
} else {
if((state_val_28734 === (4))){
var inst_28716 = (state_28733[(10)]);
var inst_28721 = figwheel.client.file_reloading.reload_js_file.call(null,inst_28716);
var state_28733__$1 = state_28733;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_28733__$1,(7),inst_28721);
} else {
if((state_val_28734 === (5))){
var inst_28727 = cljs.core.async.close_BANG_.call(null,out);
var state_28733__$1 = state_28733;
var statearr_28740_28752 = state_28733__$1;
(statearr_28740_28752[(2)] = inst_28727);

(statearr_28740_28752[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28734 === (6))){
var inst_28729 = (state_28733[(2)]);
var state_28733__$1 = state_28733;
var statearr_28741_28753 = state_28733__$1;
(statearr_28741_28753[(2)] = inst_28729);

(statearr_28741_28753[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28734 === (7))){
var inst_28717 = (state_28733[(11)]);
var inst_28723 = (state_28733[(2)]);
var inst_28724 = cljs.core.async.put_BANG_.call(null,out,inst_28723);
var inst_28710 = inst_28717;
var state_28733__$1 = (function (){var statearr_28742 = state_28733;
(statearr_28742[(12)] = inst_28724);

(statearr_28742[(9)] = inst_28710);

return statearr_28742;
})();
var statearr_28743_28754 = state_28733__$1;
(statearr_28743_28754[(2)] = null);

(statearr_28743_28754[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
});})(c__26309__auto___28748,out))
;
return ((function (switch__26214__auto__,c__26309__auto___28748,out){
return (function() {
var figwheel$client$file_reloading$load_all_js_files_$_state_machine__26215__auto__ = null;
var figwheel$client$file_reloading$load_all_js_files_$_state_machine__26215__auto____0 = (function (){
var statearr_28744 = [null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_28744[(0)] = figwheel$client$file_reloading$load_all_js_files_$_state_machine__26215__auto__);

(statearr_28744[(1)] = (1));

return statearr_28744;
});
var figwheel$client$file_reloading$load_all_js_files_$_state_machine__26215__auto____1 = (function (state_28733){
while(true){
var ret_value__26216__auto__ = (function (){try{while(true){
var result__26217__auto__ = switch__26214__auto__.call(null,state_28733);
if(cljs.core.keyword_identical_QMARK_.call(null,result__26217__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__26217__auto__;
}
break;
}
}catch (e28745){if((e28745 instanceof Object)){
var ex__26218__auto__ = e28745;
var statearr_28746_28755 = state_28733;
(statearr_28746_28755[(5)] = ex__26218__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_28733);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e28745;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__26216__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__28756 = state_28733;
state_28733 = G__28756;
continue;
} else {
return ret_value__26216__auto__;
}
break;
}
});
figwheel$client$file_reloading$load_all_js_files_$_state_machine__26215__auto__ = function(state_28733){
switch(arguments.length){
case 0:
return figwheel$client$file_reloading$load_all_js_files_$_state_machine__26215__auto____0.call(this);
case 1:
return figwheel$client$file_reloading$load_all_js_files_$_state_machine__26215__auto____1.call(this,state_28733);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
figwheel$client$file_reloading$load_all_js_files_$_state_machine__26215__auto__.cljs$core$IFn$_invoke$arity$0 = figwheel$client$file_reloading$load_all_js_files_$_state_machine__26215__auto____0;
figwheel$client$file_reloading$load_all_js_files_$_state_machine__26215__auto__.cljs$core$IFn$_invoke$arity$1 = figwheel$client$file_reloading$load_all_js_files_$_state_machine__26215__auto____1;
return figwheel$client$file_reloading$load_all_js_files_$_state_machine__26215__auto__;
})()
;})(switch__26214__auto__,c__26309__auto___28748,out))
})();
var state__26311__auto__ = (function (){var statearr_28747 = f__26310__auto__.call(null);
(statearr_28747[(6)] = c__26309__auto___28748);

return statearr_28747;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__26311__auto__);
});})(c__26309__auto___28748,out))
);


return cljs.core.async.into.call(null,cljs.core.PersistentVector.EMPTY,out);
});
figwheel.client.file_reloading.eval_body = (function figwheel$client$file_reloading$eval_body(p__28757,opts){
var map__28758 = p__28757;
var map__28758__$1 = (((((!((map__28758 == null))))?(((((map__28758.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__28758.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__28758):map__28758);
var eval_body = cljs.core.get.call(null,map__28758__$1,new cljs.core.Keyword(null,"eval-body","eval-body",-907279883));
var file = cljs.core.get.call(null,map__28758__$1,new cljs.core.Keyword(null,"file","file",-1269645878));
if(cljs.core.truth_((function (){var and__4120__auto__ = eval_body;
if(cljs.core.truth_(and__4120__auto__)){
return typeof eval_body === 'string';
} else {
return and__4120__auto__;
}
})())){
var code = eval_body;
try{figwheel.client.utils.debug_prn.call(null,["Evaling file ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(file)].join(''));

return figwheel.client.utils.eval_helper.call(null,code,opts);
}catch (e28760){var e = e28760;
return figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"error","error",-978969032),["Unable to evaluate ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(file)].join(''));
}} else {
return null;
}
});
figwheel.client.file_reloading.expand_files = (function figwheel$client$file_reloading$expand_files(files){
var deps = figwheel.client.file_reloading.get_all_dependents.call(null,cljs.core.map.call(null,new cljs.core.Keyword(null,"namespace","namespace",-377510372),files));
return cljs.core.filter.call(null,cljs.core.comp.call(null,cljs.core.not,cljs.core.partial.call(null,cljs.core.re_matches,/figwheel\.connect.*/),new cljs.core.Keyword(null,"namespace","namespace",-377510372)),cljs.core.map.call(null,((function (deps){
return (function (n){
var temp__5718__auto__ = cljs.core.first.call(null,cljs.core.filter.call(null,((function (deps){
return (function (p1__28761_SHARP_){
return cljs.core._EQ_.call(null,new cljs.core.Keyword(null,"namespace","namespace",-377510372).cljs$core$IFn$_invoke$arity$1(p1__28761_SHARP_),n);
});})(deps))
,files));
if(cljs.core.truth_(temp__5718__auto__)){
var file_msg = temp__5718__auto__;
return file_msg;
} else {
return new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"type","type",1174270348),new cljs.core.Keyword(null,"namespace","namespace",-377510372),new cljs.core.Keyword(null,"namespace","namespace",-377510372),n], null);
}
});})(deps))
,deps));
});
figwheel.client.file_reloading.sort_files = (function figwheel$client$file_reloading$sort_files(files){
if((cljs.core.count.call(null,files) <= (1))){
return files;
} else {
var keep_files = cljs.core.set.call(null,cljs.core.keep.call(null,new cljs.core.Keyword(null,"namespace","namespace",-377510372),files));
return cljs.core.filter.call(null,cljs.core.comp.call(null,keep_files,new cljs.core.Keyword(null,"namespace","namespace",-377510372)),figwheel.client.file_reloading.expand_files.call(null,files));
}
});
figwheel.client.file_reloading.get_figwheel_always = (function figwheel$client$file_reloading$get_figwheel_always(){
return cljs.core.map.call(null,(function (p__28762){
var vec__28763 = p__28762;
var k = cljs.core.nth.call(null,vec__28763,(0),null);
var v = cljs.core.nth.call(null,vec__28763,(1),null);
return new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"namespace","namespace",-377510372),k,new cljs.core.Keyword(null,"type","type",1174270348),new cljs.core.Keyword(null,"namespace","namespace",-377510372)], null);
}),cljs.core.filter.call(null,(function (p__28766){
var vec__28767 = p__28766;
var k = cljs.core.nth.call(null,vec__28767,(0),null);
var v = cljs.core.nth.call(null,vec__28767,(1),null);
return new cljs.core.Keyword(null,"figwheel-always","figwheel-always",799819691).cljs$core$IFn$_invoke$arity$1(v);
}),cljs.core.deref.call(null,figwheel.client.file_reloading.figwheel_meta_pragmas)));
});
figwheel.client.file_reloading.reload_js_files = (function figwheel$client$file_reloading$reload_js_files(p__28773,p__28774){
var map__28775 = p__28773;
var map__28775__$1 = (((((!((map__28775 == null))))?(((((map__28775.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__28775.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__28775):map__28775);
var opts = map__28775__$1;
var before_jsload = cljs.core.get.call(null,map__28775__$1,new cljs.core.Keyword(null,"before-jsload","before-jsload",-847513128));
var on_jsload = cljs.core.get.call(null,map__28775__$1,new cljs.core.Keyword(null,"on-jsload","on-jsload",-395756602));
var reload_dependents = cljs.core.get.call(null,map__28775__$1,new cljs.core.Keyword(null,"reload-dependents","reload-dependents",-956865430));
var map__28776 = p__28774;
var map__28776__$1 = (((((!((map__28776 == null))))?(((((map__28776.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__28776.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__28776):map__28776);
var msg = map__28776__$1;
var files = cljs.core.get.call(null,map__28776__$1,new cljs.core.Keyword(null,"files","files",-472457450));
var figwheel_meta = cljs.core.get.call(null,map__28776__$1,new cljs.core.Keyword(null,"figwheel-meta","figwheel-meta",-225970237));
var recompile_dependents = cljs.core.get.call(null,map__28776__$1,new cljs.core.Keyword(null,"recompile-dependents","recompile-dependents",523804171));
if(cljs.core.empty_QMARK_.call(null,figwheel_meta)){
} else {
cljs.core.reset_BANG_.call(null,figwheel.client.file_reloading.figwheel_meta_pragmas,figwheel_meta);
}

var c__26309__auto__ = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__26309__auto__,map__28775,map__28775__$1,opts,before_jsload,on_jsload,reload_dependents,map__28776,map__28776__$1,msg,files,figwheel_meta,recompile_dependents){
return (function (){
var f__26310__auto__ = (function (){var switch__26214__auto__ = ((function (c__26309__auto__,map__28775,map__28775__$1,opts,before_jsload,on_jsload,reload_dependents,map__28776,map__28776__$1,msg,files,figwheel_meta,recompile_dependents){
return (function (state_28930){
var state_val_28931 = (state_28930[(1)]);
if((state_val_28931 === (7))){
var inst_28790 = (state_28930[(7)]);
var inst_28793 = (state_28930[(8)]);
var inst_28791 = (state_28930[(9)]);
var inst_28792 = (state_28930[(10)]);
var inst_28798 = cljs.core._nth.call(null,inst_28791,inst_28793);
var inst_28799 = figwheel.client.file_reloading.eval_body.call(null,inst_28798,opts);
var inst_28800 = (inst_28793 + (1));
var tmp28932 = inst_28790;
var tmp28933 = inst_28791;
var tmp28934 = inst_28792;
var inst_28790__$1 = tmp28932;
var inst_28791__$1 = tmp28933;
var inst_28792__$1 = tmp28934;
var inst_28793__$1 = inst_28800;
var state_28930__$1 = (function (){var statearr_28935 = state_28930;
(statearr_28935[(11)] = inst_28799);

(statearr_28935[(7)] = inst_28790__$1);

(statearr_28935[(8)] = inst_28793__$1);

(statearr_28935[(9)] = inst_28791__$1);

(statearr_28935[(10)] = inst_28792__$1);

return statearr_28935;
})();
var statearr_28936_29019 = state_28930__$1;
(statearr_28936_29019[(2)] = null);

(statearr_28936_29019[(1)] = (5));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28931 === (20))){
var inst_28833 = (state_28930[(12)]);
var inst_28841 = figwheel.client.file_reloading.sort_files.call(null,inst_28833);
var state_28930__$1 = state_28930;
var statearr_28937_29020 = state_28930__$1;
(statearr_28937_29020[(2)] = inst_28841);

(statearr_28937_29020[(1)] = (21));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28931 === (27))){
var state_28930__$1 = state_28930;
var statearr_28938_29021 = state_28930__$1;
(statearr_28938_29021[(2)] = null);

(statearr_28938_29021[(1)] = (28));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28931 === (1))){
var inst_28782 = (state_28930[(13)]);
var inst_28779 = before_jsload.call(null,files);
var inst_28780 = figwheel.client.file_reloading.before_jsload_custom_event.call(null,files);
var inst_28781 = (function (){return ((function (inst_28782,inst_28779,inst_28780,state_val_28931,c__26309__auto__,map__28775,map__28775__$1,opts,before_jsload,on_jsload,reload_dependents,map__28776,map__28776__$1,msg,files,figwheel_meta,recompile_dependents){
return (function (p1__28770_SHARP_){
return new cljs.core.Keyword(null,"eval-body","eval-body",-907279883).cljs$core$IFn$_invoke$arity$1(p1__28770_SHARP_);
});
;})(inst_28782,inst_28779,inst_28780,state_val_28931,c__26309__auto__,map__28775,map__28775__$1,opts,before_jsload,on_jsload,reload_dependents,map__28776,map__28776__$1,msg,files,figwheel_meta,recompile_dependents))
})();
var inst_28782__$1 = cljs.core.filter.call(null,inst_28781,files);
var inst_28783 = cljs.core.not_empty.call(null,inst_28782__$1);
var state_28930__$1 = (function (){var statearr_28939 = state_28930;
(statearr_28939[(14)] = inst_28780);

(statearr_28939[(13)] = inst_28782__$1);

(statearr_28939[(15)] = inst_28779);

return statearr_28939;
})();
if(cljs.core.truth_(inst_28783)){
var statearr_28940_29022 = state_28930__$1;
(statearr_28940_29022[(1)] = (2));

} else {
var statearr_28941_29023 = state_28930__$1;
(statearr_28941_29023[(1)] = (3));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28931 === (24))){
var state_28930__$1 = state_28930;
var statearr_28942_29024 = state_28930__$1;
(statearr_28942_29024[(2)] = null);

(statearr_28942_29024[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28931 === (39))){
var inst_28883 = (state_28930[(16)]);
var state_28930__$1 = state_28930;
var statearr_28943_29025 = state_28930__$1;
(statearr_28943_29025[(2)] = inst_28883);

(statearr_28943_29025[(1)] = (40));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28931 === (46))){
var inst_28925 = (state_28930[(2)]);
var state_28930__$1 = state_28930;
var statearr_28944_29026 = state_28930__$1;
(statearr_28944_29026[(2)] = inst_28925);

(statearr_28944_29026[(1)] = (31));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28931 === (4))){
var inst_28827 = (state_28930[(2)]);
var inst_28828 = cljs.core.List.EMPTY;
var inst_28829 = cljs.core.reset_BANG_.call(null,figwheel.client.file_reloading.dependencies_loaded,inst_28828);
var inst_28830 = (function (){return ((function (inst_28827,inst_28828,inst_28829,state_val_28931,c__26309__auto__,map__28775,map__28775__$1,opts,before_jsload,on_jsload,reload_dependents,map__28776,map__28776__$1,msg,files,figwheel_meta,recompile_dependents){
return (function (p1__28771_SHARP_){
var and__4120__auto__ = new cljs.core.Keyword(null,"namespace","namespace",-377510372).cljs$core$IFn$_invoke$arity$1(p1__28771_SHARP_);
if(cljs.core.truth_(and__4120__auto__)){
return ((cljs.core.not.call(null,new cljs.core.Keyword(null,"eval-body","eval-body",-907279883).cljs$core$IFn$_invoke$arity$1(p1__28771_SHARP_))) && (cljs.core.not.call(null,figwheel.client.file_reloading.figwheel_no_load_QMARK_.call(null,p1__28771_SHARP_))));
} else {
return and__4120__auto__;
}
});
;})(inst_28827,inst_28828,inst_28829,state_val_28931,c__26309__auto__,map__28775,map__28775__$1,opts,before_jsload,on_jsload,reload_dependents,map__28776,map__28776__$1,msg,files,figwheel_meta,recompile_dependents))
})();
var inst_28831 = cljs.core.filter.call(null,inst_28830,files);
var inst_28832 = figwheel.client.file_reloading.get_figwheel_always.call(null);
var inst_28833 = cljs.core.concat.call(null,inst_28831,inst_28832);
var state_28930__$1 = (function (){var statearr_28945 = state_28930;
(statearr_28945[(17)] = inst_28829);

(statearr_28945[(12)] = inst_28833);

(statearr_28945[(18)] = inst_28827);

return statearr_28945;
})();
if(cljs.core.truth_(reload_dependents)){
var statearr_28946_29027 = state_28930__$1;
(statearr_28946_29027[(1)] = (16));

} else {
var statearr_28947_29028 = state_28930__$1;
(statearr_28947_29028[(1)] = (17));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28931 === (15))){
var inst_28817 = (state_28930[(2)]);
var state_28930__$1 = state_28930;
var statearr_28948_29029 = state_28930__$1;
(statearr_28948_29029[(2)] = inst_28817);

(statearr_28948_29029[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28931 === (21))){
var inst_28843 = (state_28930[(19)]);
var inst_28843__$1 = (state_28930[(2)]);
var inst_28844 = figwheel.client.file_reloading.load_all_js_files.call(null,inst_28843__$1);
var state_28930__$1 = (function (){var statearr_28949 = state_28930;
(statearr_28949[(19)] = inst_28843__$1);

return statearr_28949;
})();
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_28930__$1,(22),inst_28844);
} else {
if((state_val_28931 === (31))){
var inst_28928 = (state_28930[(2)]);
var state_28930__$1 = state_28930;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_28930__$1,inst_28928);
} else {
if((state_val_28931 === (32))){
var inst_28883 = (state_28930[(16)]);
var inst_28888 = inst_28883.cljs$lang$protocol_mask$partition0$;
var inst_28889 = (inst_28888 & (64));
var inst_28890 = inst_28883.cljs$core$ISeq$;
var inst_28891 = (cljs.core.PROTOCOL_SENTINEL === inst_28890);
var inst_28892 = ((inst_28889) || (inst_28891));
var state_28930__$1 = state_28930;
if(cljs.core.truth_(inst_28892)){
var statearr_28950_29030 = state_28930__$1;
(statearr_28950_29030[(1)] = (35));

} else {
var statearr_28951_29031 = state_28930__$1;
(statearr_28951_29031[(1)] = (36));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28931 === (40))){
var inst_28905 = (state_28930[(20)]);
var inst_28904 = (state_28930[(2)]);
var inst_28905__$1 = cljs.core.get.call(null,inst_28904,new cljs.core.Keyword(null,"figwheel-no-load","figwheel-no-load",-555840179));
var inst_28906 = cljs.core.get.call(null,inst_28904,new cljs.core.Keyword(null,"not-required","not-required",-950359114));
var inst_28907 = cljs.core.not_empty.call(null,inst_28905__$1);
var state_28930__$1 = (function (){var statearr_28952 = state_28930;
(statearr_28952[(20)] = inst_28905__$1);

(statearr_28952[(21)] = inst_28906);

return statearr_28952;
})();
if(cljs.core.truth_(inst_28907)){
var statearr_28953_29032 = state_28930__$1;
(statearr_28953_29032[(1)] = (41));

} else {
var statearr_28954_29033 = state_28930__$1;
(statearr_28954_29033[(1)] = (42));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28931 === (33))){
var state_28930__$1 = state_28930;
var statearr_28955_29034 = state_28930__$1;
(statearr_28955_29034[(2)] = false);

(statearr_28955_29034[(1)] = (34));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28931 === (13))){
var inst_28803 = (state_28930[(22)]);
var inst_28807 = cljs.core.chunk_first.call(null,inst_28803);
var inst_28808 = cljs.core.chunk_rest.call(null,inst_28803);
var inst_28809 = cljs.core.count.call(null,inst_28807);
var inst_28790 = inst_28808;
var inst_28791 = inst_28807;
var inst_28792 = inst_28809;
var inst_28793 = (0);
var state_28930__$1 = (function (){var statearr_28956 = state_28930;
(statearr_28956[(7)] = inst_28790);

(statearr_28956[(8)] = inst_28793);

(statearr_28956[(9)] = inst_28791);

(statearr_28956[(10)] = inst_28792);

return statearr_28956;
})();
var statearr_28957_29035 = state_28930__$1;
(statearr_28957_29035[(2)] = null);

(statearr_28957_29035[(1)] = (5));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28931 === (22))){
var inst_28847 = (state_28930[(23)]);
var inst_28846 = (state_28930[(24)]);
var inst_28843 = (state_28930[(19)]);
var inst_28851 = (state_28930[(25)]);
var inst_28846__$1 = (state_28930[(2)]);
var inst_28847__$1 = cljs.core.filter.call(null,new cljs.core.Keyword(null,"loaded-file","loaded-file",-168399375),inst_28846__$1);
var inst_28848 = (function (){var all_files = inst_28843;
var res_SINGLEQUOTE_ = inst_28846__$1;
var res = inst_28847__$1;
return ((function (all_files,res_SINGLEQUOTE_,res,inst_28847,inst_28846,inst_28843,inst_28851,inst_28846__$1,inst_28847__$1,state_val_28931,c__26309__auto__,map__28775,map__28775__$1,opts,before_jsload,on_jsload,reload_dependents,map__28776,map__28776__$1,msg,files,figwheel_meta,recompile_dependents){
return (function (p1__28772_SHARP_){
return cljs.core.not.call(null,new cljs.core.Keyword(null,"loaded-file","loaded-file",-168399375).cljs$core$IFn$_invoke$arity$1(p1__28772_SHARP_));
});
;})(all_files,res_SINGLEQUOTE_,res,inst_28847,inst_28846,inst_28843,inst_28851,inst_28846__$1,inst_28847__$1,state_val_28931,c__26309__auto__,map__28775,map__28775__$1,opts,before_jsload,on_jsload,reload_dependents,map__28776,map__28776__$1,msg,files,figwheel_meta,recompile_dependents))
})();
var inst_28849 = cljs.core.filter.call(null,inst_28848,inst_28846__$1);
var inst_28850 = cljs.core.deref.call(null,figwheel.client.file_reloading.dependencies_loaded);
var inst_28851__$1 = cljs.core.filter.call(null,new cljs.core.Keyword(null,"loaded-file","loaded-file",-168399375),inst_28850);
var inst_28852 = cljs.core.not_empty.call(null,inst_28851__$1);
var state_28930__$1 = (function (){var statearr_28958 = state_28930;
(statearr_28958[(26)] = inst_28849);

(statearr_28958[(23)] = inst_28847__$1);

(statearr_28958[(24)] = inst_28846__$1);

(statearr_28958[(25)] = inst_28851__$1);

return statearr_28958;
})();
if(cljs.core.truth_(inst_28852)){
var statearr_28959_29036 = state_28930__$1;
(statearr_28959_29036[(1)] = (23));

} else {
var statearr_28960_29037 = state_28930__$1;
(statearr_28960_29037[(1)] = (24));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28931 === (36))){
var state_28930__$1 = state_28930;
var statearr_28961_29038 = state_28930__$1;
(statearr_28961_29038[(2)] = false);

(statearr_28961_29038[(1)] = (37));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28931 === (41))){
var inst_28905 = (state_28930[(20)]);
var inst_28909 = cljs.core.comp.call(null,figwheel.client.file_reloading.name__GT_path,new cljs.core.Keyword(null,"namespace","namespace",-377510372));
var inst_28910 = cljs.core.map.call(null,inst_28909,inst_28905);
var inst_28911 = cljs.core.pr_str.call(null,inst_28910);
var inst_28912 = ["figwheel-no-load meta-data: ",inst_28911].join('');
var inst_28913 = figwheel.client.utils.log.call(null,inst_28912);
var state_28930__$1 = state_28930;
var statearr_28962_29039 = state_28930__$1;
(statearr_28962_29039[(2)] = inst_28913);

(statearr_28962_29039[(1)] = (43));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28931 === (43))){
var inst_28906 = (state_28930[(21)]);
var inst_28916 = (state_28930[(2)]);
var inst_28917 = cljs.core.not_empty.call(null,inst_28906);
var state_28930__$1 = (function (){var statearr_28963 = state_28930;
(statearr_28963[(27)] = inst_28916);

return statearr_28963;
})();
if(cljs.core.truth_(inst_28917)){
var statearr_28964_29040 = state_28930__$1;
(statearr_28964_29040[(1)] = (44));

} else {
var statearr_28965_29041 = state_28930__$1;
(statearr_28965_29041[(1)] = (45));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28931 === (29))){
var inst_28849 = (state_28930[(26)]);
var inst_28883 = (state_28930[(16)]);
var inst_28847 = (state_28930[(23)]);
var inst_28846 = (state_28930[(24)]);
var inst_28843 = (state_28930[(19)]);
var inst_28851 = (state_28930[(25)]);
var inst_28879 = figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"debug","debug",-1608172596),"Figwheel: NOT loading these files ");
var inst_28882 = (function (){var all_files = inst_28843;
var res_SINGLEQUOTE_ = inst_28846;
var res = inst_28847;
var files_not_loaded = inst_28849;
var dependencies_that_loaded = inst_28851;
return ((function (all_files,res_SINGLEQUOTE_,res,files_not_loaded,dependencies_that_loaded,inst_28849,inst_28883,inst_28847,inst_28846,inst_28843,inst_28851,inst_28879,state_val_28931,c__26309__auto__,map__28775,map__28775__$1,opts,before_jsload,on_jsload,reload_dependents,map__28776,map__28776__$1,msg,files,figwheel_meta,recompile_dependents){
return (function (p__28881){
var map__28966 = p__28881;
var map__28966__$1 = (((((!((map__28966 == null))))?(((((map__28966.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__28966.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__28966):map__28966);
var namespace = cljs.core.get.call(null,map__28966__$1,new cljs.core.Keyword(null,"namespace","namespace",-377510372));
var meta_data = cljs.core.get.call(null,cljs.core.deref.call(null,figwheel.client.file_reloading.figwheel_meta_pragmas),cljs.core.name.call(null,namespace));
if((meta_data == null)){
return new cljs.core.Keyword(null,"not-required","not-required",-950359114);
} else {
if(cljs.core.truth_(meta_data.call(null,new cljs.core.Keyword(null,"figwheel-no-load","figwheel-no-load",-555840179)))){
return new cljs.core.Keyword(null,"figwheel-no-load","figwheel-no-load",-555840179);
} else {
return new cljs.core.Keyword(null,"not-required","not-required",-950359114);

}
}
});
;})(all_files,res_SINGLEQUOTE_,res,files_not_loaded,dependencies_that_loaded,inst_28849,inst_28883,inst_28847,inst_28846,inst_28843,inst_28851,inst_28879,state_val_28931,c__26309__auto__,map__28775,map__28775__$1,opts,before_jsload,on_jsload,reload_dependents,map__28776,map__28776__$1,msg,files,figwheel_meta,recompile_dependents))
})();
var inst_28883__$1 = cljs.core.group_by.call(null,inst_28882,inst_28849);
var inst_28885 = (inst_28883__$1 == null);
var inst_28886 = cljs.core.not.call(null,inst_28885);
var state_28930__$1 = (function (){var statearr_28968 = state_28930;
(statearr_28968[(16)] = inst_28883__$1);

(statearr_28968[(28)] = inst_28879);

return statearr_28968;
})();
if(inst_28886){
var statearr_28969_29042 = state_28930__$1;
(statearr_28969_29042[(1)] = (32));

} else {
var statearr_28970_29043 = state_28930__$1;
(statearr_28970_29043[(1)] = (33));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28931 === (44))){
var inst_28906 = (state_28930[(21)]);
var inst_28919 = cljs.core.map.call(null,new cljs.core.Keyword(null,"file","file",-1269645878),inst_28906);
var inst_28920 = cljs.core.pr_str.call(null,inst_28919);
var inst_28921 = ["not required: ",inst_28920].join('');
var inst_28922 = figwheel.client.utils.log.call(null,inst_28921);
var state_28930__$1 = state_28930;
var statearr_28971_29044 = state_28930__$1;
(statearr_28971_29044[(2)] = inst_28922);

(statearr_28971_29044[(1)] = (46));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28931 === (6))){
var inst_28824 = (state_28930[(2)]);
var state_28930__$1 = state_28930;
var statearr_28972_29045 = state_28930__$1;
(statearr_28972_29045[(2)] = inst_28824);

(statearr_28972_29045[(1)] = (4));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28931 === (28))){
var inst_28849 = (state_28930[(26)]);
var inst_28876 = (state_28930[(2)]);
var inst_28877 = cljs.core.not_empty.call(null,inst_28849);
var state_28930__$1 = (function (){var statearr_28973 = state_28930;
(statearr_28973[(29)] = inst_28876);

return statearr_28973;
})();
if(cljs.core.truth_(inst_28877)){
var statearr_28974_29046 = state_28930__$1;
(statearr_28974_29046[(1)] = (29));

} else {
var statearr_28975_29047 = state_28930__$1;
(statearr_28975_29047[(1)] = (30));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28931 === (25))){
var inst_28847 = (state_28930[(23)]);
var inst_28863 = (state_28930[(2)]);
var inst_28864 = cljs.core.not_empty.call(null,inst_28847);
var state_28930__$1 = (function (){var statearr_28976 = state_28930;
(statearr_28976[(30)] = inst_28863);

return statearr_28976;
})();
if(cljs.core.truth_(inst_28864)){
var statearr_28977_29048 = state_28930__$1;
(statearr_28977_29048[(1)] = (26));

} else {
var statearr_28978_29049 = state_28930__$1;
(statearr_28978_29049[(1)] = (27));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28931 === (34))){
var inst_28899 = (state_28930[(2)]);
var state_28930__$1 = state_28930;
if(cljs.core.truth_(inst_28899)){
var statearr_28979_29050 = state_28930__$1;
(statearr_28979_29050[(1)] = (38));

} else {
var statearr_28980_29051 = state_28930__$1;
(statearr_28980_29051[(1)] = (39));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28931 === (17))){
var state_28930__$1 = state_28930;
var statearr_28981_29052 = state_28930__$1;
(statearr_28981_29052[(2)] = recompile_dependents);

(statearr_28981_29052[(1)] = (18));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28931 === (3))){
var state_28930__$1 = state_28930;
var statearr_28982_29053 = state_28930__$1;
(statearr_28982_29053[(2)] = null);

(statearr_28982_29053[(1)] = (4));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28931 === (12))){
var inst_28820 = (state_28930[(2)]);
var state_28930__$1 = state_28930;
var statearr_28983_29054 = state_28930__$1;
(statearr_28983_29054[(2)] = inst_28820);

(statearr_28983_29054[(1)] = (9));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28931 === (2))){
var inst_28782 = (state_28930[(13)]);
var inst_28789 = cljs.core.seq.call(null,inst_28782);
var inst_28790 = inst_28789;
var inst_28791 = null;
var inst_28792 = (0);
var inst_28793 = (0);
var state_28930__$1 = (function (){var statearr_28984 = state_28930;
(statearr_28984[(7)] = inst_28790);

(statearr_28984[(8)] = inst_28793);

(statearr_28984[(9)] = inst_28791);

(statearr_28984[(10)] = inst_28792);

return statearr_28984;
})();
var statearr_28985_29055 = state_28930__$1;
(statearr_28985_29055[(2)] = null);

(statearr_28985_29055[(1)] = (5));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28931 === (23))){
var inst_28849 = (state_28930[(26)]);
var inst_28847 = (state_28930[(23)]);
var inst_28846 = (state_28930[(24)]);
var inst_28843 = (state_28930[(19)]);
var inst_28851 = (state_28930[(25)]);
var inst_28854 = figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"debug","debug",-1608172596),"Figwheel: loaded these dependencies");
var inst_28856 = (function (){var all_files = inst_28843;
var res_SINGLEQUOTE_ = inst_28846;
var res = inst_28847;
var files_not_loaded = inst_28849;
var dependencies_that_loaded = inst_28851;
return ((function (all_files,res_SINGLEQUOTE_,res,files_not_loaded,dependencies_that_loaded,inst_28849,inst_28847,inst_28846,inst_28843,inst_28851,inst_28854,state_val_28931,c__26309__auto__,map__28775,map__28775__$1,opts,before_jsload,on_jsload,reload_dependents,map__28776,map__28776__$1,msg,files,figwheel_meta,recompile_dependents){
return (function (p__28855){
var map__28986 = p__28855;
var map__28986__$1 = (((((!((map__28986 == null))))?(((((map__28986.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__28986.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__28986):map__28986);
var request_url = cljs.core.get.call(null,map__28986__$1,new cljs.core.Keyword(null,"request-url","request-url",2100346596));
return clojure.string.replace.call(null,request_url,goog.basePath,"");
});
;})(all_files,res_SINGLEQUOTE_,res,files_not_loaded,dependencies_that_loaded,inst_28849,inst_28847,inst_28846,inst_28843,inst_28851,inst_28854,state_val_28931,c__26309__auto__,map__28775,map__28775__$1,opts,before_jsload,on_jsload,reload_dependents,map__28776,map__28776__$1,msg,files,figwheel_meta,recompile_dependents))
})();
var inst_28857 = cljs.core.reverse.call(null,inst_28851);
var inst_28858 = cljs.core.map.call(null,inst_28856,inst_28857);
var inst_28859 = cljs.core.pr_str.call(null,inst_28858);
var inst_28860 = figwheel.client.utils.log.call(null,inst_28859);
var state_28930__$1 = (function (){var statearr_28988 = state_28930;
(statearr_28988[(31)] = inst_28854);

return statearr_28988;
})();
var statearr_28989_29056 = state_28930__$1;
(statearr_28989_29056[(2)] = inst_28860);

(statearr_28989_29056[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28931 === (35))){
var state_28930__$1 = state_28930;
var statearr_28990_29057 = state_28930__$1;
(statearr_28990_29057[(2)] = true);

(statearr_28990_29057[(1)] = (37));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28931 === (19))){
var inst_28833 = (state_28930[(12)]);
var inst_28839 = figwheel.client.file_reloading.expand_files.call(null,inst_28833);
var state_28930__$1 = state_28930;
var statearr_28991_29058 = state_28930__$1;
(statearr_28991_29058[(2)] = inst_28839);

(statearr_28991_29058[(1)] = (21));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28931 === (11))){
var state_28930__$1 = state_28930;
var statearr_28992_29059 = state_28930__$1;
(statearr_28992_29059[(2)] = null);

(statearr_28992_29059[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28931 === (9))){
var inst_28822 = (state_28930[(2)]);
var state_28930__$1 = state_28930;
var statearr_28993_29060 = state_28930__$1;
(statearr_28993_29060[(2)] = inst_28822);

(statearr_28993_29060[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28931 === (5))){
var inst_28793 = (state_28930[(8)]);
var inst_28792 = (state_28930[(10)]);
var inst_28795 = (inst_28793 < inst_28792);
var inst_28796 = inst_28795;
var state_28930__$1 = state_28930;
if(cljs.core.truth_(inst_28796)){
var statearr_28994_29061 = state_28930__$1;
(statearr_28994_29061[(1)] = (7));

} else {
var statearr_28995_29062 = state_28930__$1;
(statearr_28995_29062[(1)] = (8));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28931 === (14))){
var inst_28803 = (state_28930[(22)]);
var inst_28812 = cljs.core.first.call(null,inst_28803);
var inst_28813 = figwheel.client.file_reloading.eval_body.call(null,inst_28812,opts);
var inst_28814 = cljs.core.next.call(null,inst_28803);
var inst_28790 = inst_28814;
var inst_28791 = null;
var inst_28792 = (0);
var inst_28793 = (0);
var state_28930__$1 = (function (){var statearr_28996 = state_28930;
(statearr_28996[(7)] = inst_28790);

(statearr_28996[(8)] = inst_28793);

(statearr_28996[(32)] = inst_28813);

(statearr_28996[(9)] = inst_28791);

(statearr_28996[(10)] = inst_28792);

return statearr_28996;
})();
var statearr_28997_29063 = state_28930__$1;
(statearr_28997_29063[(2)] = null);

(statearr_28997_29063[(1)] = (5));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28931 === (45))){
var state_28930__$1 = state_28930;
var statearr_28998_29064 = state_28930__$1;
(statearr_28998_29064[(2)] = null);

(statearr_28998_29064[(1)] = (46));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28931 === (26))){
var inst_28849 = (state_28930[(26)]);
var inst_28847 = (state_28930[(23)]);
var inst_28846 = (state_28930[(24)]);
var inst_28843 = (state_28930[(19)]);
var inst_28851 = (state_28930[(25)]);
var inst_28866 = figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"debug","debug",-1608172596),"Figwheel: loaded these files");
var inst_28868 = (function (){var all_files = inst_28843;
var res_SINGLEQUOTE_ = inst_28846;
var res = inst_28847;
var files_not_loaded = inst_28849;
var dependencies_that_loaded = inst_28851;
return ((function (all_files,res_SINGLEQUOTE_,res,files_not_loaded,dependencies_that_loaded,inst_28849,inst_28847,inst_28846,inst_28843,inst_28851,inst_28866,state_val_28931,c__26309__auto__,map__28775,map__28775__$1,opts,before_jsload,on_jsload,reload_dependents,map__28776,map__28776__$1,msg,files,figwheel_meta,recompile_dependents){
return (function (p__28867){
var map__28999 = p__28867;
var map__28999__$1 = (((((!((map__28999 == null))))?(((((map__28999.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__28999.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__28999):map__28999);
var namespace = cljs.core.get.call(null,map__28999__$1,new cljs.core.Keyword(null,"namespace","namespace",-377510372));
var file = cljs.core.get.call(null,map__28999__$1,new cljs.core.Keyword(null,"file","file",-1269645878));
if(cljs.core.truth_(namespace)){
return figwheel.client.file_reloading.name__GT_path.call(null,cljs.core.name.call(null,namespace));
} else {
return file;
}
});
;})(all_files,res_SINGLEQUOTE_,res,files_not_loaded,dependencies_that_loaded,inst_28849,inst_28847,inst_28846,inst_28843,inst_28851,inst_28866,state_val_28931,c__26309__auto__,map__28775,map__28775__$1,opts,before_jsload,on_jsload,reload_dependents,map__28776,map__28776__$1,msg,files,figwheel_meta,recompile_dependents))
})();
var inst_28869 = cljs.core.map.call(null,inst_28868,inst_28847);
var inst_28870 = cljs.core.pr_str.call(null,inst_28869);
var inst_28871 = figwheel.client.utils.log.call(null,inst_28870);
var inst_28872 = (function (){var all_files = inst_28843;
var res_SINGLEQUOTE_ = inst_28846;
var res = inst_28847;
var files_not_loaded = inst_28849;
var dependencies_that_loaded = inst_28851;
return ((function (all_files,res_SINGLEQUOTE_,res,files_not_loaded,dependencies_that_loaded,inst_28849,inst_28847,inst_28846,inst_28843,inst_28851,inst_28866,inst_28868,inst_28869,inst_28870,inst_28871,state_val_28931,c__26309__auto__,map__28775,map__28775__$1,opts,before_jsload,on_jsload,reload_dependents,map__28776,map__28776__$1,msg,files,figwheel_meta,recompile_dependents){
return (function (){
figwheel.client.file_reloading.on_jsload_custom_event.call(null,res);

return cljs.core.apply.call(null,on_jsload,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [res], null));
});
;})(all_files,res_SINGLEQUOTE_,res,files_not_loaded,dependencies_that_loaded,inst_28849,inst_28847,inst_28846,inst_28843,inst_28851,inst_28866,inst_28868,inst_28869,inst_28870,inst_28871,state_val_28931,c__26309__auto__,map__28775,map__28775__$1,opts,before_jsload,on_jsload,reload_dependents,map__28776,map__28776__$1,msg,files,figwheel_meta,recompile_dependents))
})();
var inst_28873 = setTimeout(inst_28872,(10));
var state_28930__$1 = (function (){var statearr_29001 = state_28930;
(statearr_29001[(33)] = inst_28871);

(statearr_29001[(34)] = inst_28866);

return statearr_29001;
})();
var statearr_29002_29065 = state_28930__$1;
(statearr_29002_29065[(2)] = inst_28873);

(statearr_29002_29065[(1)] = (28));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28931 === (16))){
var state_28930__$1 = state_28930;
var statearr_29003_29066 = state_28930__$1;
(statearr_29003_29066[(2)] = reload_dependents);

(statearr_29003_29066[(1)] = (18));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28931 === (38))){
var inst_28883 = (state_28930[(16)]);
var inst_28901 = cljs.core.apply.call(null,cljs.core.hash_map,inst_28883);
var state_28930__$1 = state_28930;
var statearr_29004_29067 = state_28930__$1;
(statearr_29004_29067[(2)] = inst_28901);

(statearr_29004_29067[(1)] = (40));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28931 === (30))){
var state_28930__$1 = state_28930;
var statearr_29005_29068 = state_28930__$1;
(statearr_29005_29068[(2)] = null);

(statearr_29005_29068[(1)] = (31));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28931 === (10))){
var inst_28803 = (state_28930[(22)]);
var inst_28805 = cljs.core.chunked_seq_QMARK_.call(null,inst_28803);
var state_28930__$1 = state_28930;
if(inst_28805){
var statearr_29006_29069 = state_28930__$1;
(statearr_29006_29069[(1)] = (13));

} else {
var statearr_29007_29070 = state_28930__$1;
(statearr_29007_29070[(1)] = (14));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28931 === (18))){
var inst_28837 = (state_28930[(2)]);
var state_28930__$1 = state_28930;
if(cljs.core.truth_(inst_28837)){
var statearr_29008_29071 = state_28930__$1;
(statearr_29008_29071[(1)] = (19));

} else {
var statearr_29009_29072 = state_28930__$1;
(statearr_29009_29072[(1)] = (20));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28931 === (42))){
var state_28930__$1 = state_28930;
var statearr_29010_29073 = state_28930__$1;
(statearr_29010_29073[(2)] = null);

(statearr_29010_29073[(1)] = (43));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28931 === (37))){
var inst_28896 = (state_28930[(2)]);
var state_28930__$1 = state_28930;
var statearr_29011_29074 = state_28930__$1;
(statearr_29011_29074[(2)] = inst_28896);

(statearr_29011_29074[(1)] = (34));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28931 === (8))){
var inst_28790 = (state_28930[(7)]);
var inst_28803 = (state_28930[(22)]);
var inst_28803__$1 = cljs.core.seq.call(null,inst_28790);
var state_28930__$1 = (function (){var statearr_29012 = state_28930;
(statearr_29012[(22)] = inst_28803__$1);

return statearr_29012;
})();
if(inst_28803__$1){
var statearr_29013_29075 = state_28930__$1;
(statearr_29013_29075[(1)] = (10));

} else {
var statearr_29014_29076 = state_28930__$1;
(statearr_29014_29076[(1)] = (11));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__26309__auto__,map__28775,map__28775__$1,opts,before_jsload,on_jsload,reload_dependents,map__28776,map__28776__$1,msg,files,figwheel_meta,recompile_dependents))
;
return ((function (switch__26214__auto__,c__26309__auto__,map__28775,map__28775__$1,opts,before_jsload,on_jsload,reload_dependents,map__28776,map__28776__$1,msg,files,figwheel_meta,recompile_dependents){
return (function() {
var figwheel$client$file_reloading$reload_js_files_$_state_machine__26215__auto__ = null;
var figwheel$client$file_reloading$reload_js_files_$_state_machine__26215__auto____0 = (function (){
var statearr_29015 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_29015[(0)] = figwheel$client$file_reloading$reload_js_files_$_state_machine__26215__auto__);

(statearr_29015[(1)] = (1));

return statearr_29015;
});
var figwheel$client$file_reloading$reload_js_files_$_state_machine__26215__auto____1 = (function (state_28930){
while(true){
var ret_value__26216__auto__ = (function (){try{while(true){
var result__26217__auto__ = switch__26214__auto__.call(null,state_28930);
if(cljs.core.keyword_identical_QMARK_.call(null,result__26217__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__26217__auto__;
}
break;
}
}catch (e29016){if((e29016 instanceof Object)){
var ex__26218__auto__ = e29016;
var statearr_29017_29077 = state_28930;
(statearr_29017_29077[(5)] = ex__26218__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_28930);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e29016;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__26216__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__29078 = state_28930;
state_28930 = G__29078;
continue;
} else {
return ret_value__26216__auto__;
}
break;
}
});
figwheel$client$file_reloading$reload_js_files_$_state_machine__26215__auto__ = function(state_28930){
switch(arguments.length){
case 0:
return figwheel$client$file_reloading$reload_js_files_$_state_machine__26215__auto____0.call(this);
case 1:
return figwheel$client$file_reloading$reload_js_files_$_state_machine__26215__auto____1.call(this,state_28930);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
figwheel$client$file_reloading$reload_js_files_$_state_machine__26215__auto__.cljs$core$IFn$_invoke$arity$0 = figwheel$client$file_reloading$reload_js_files_$_state_machine__26215__auto____0;
figwheel$client$file_reloading$reload_js_files_$_state_machine__26215__auto__.cljs$core$IFn$_invoke$arity$1 = figwheel$client$file_reloading$reload_js_files_$_state_machine__26215__auto____1;
return figwheel$client$file_reloading$reload_js_files_$_state_machine__26215__auto__;
})()
;})(switch__26214__auto__,c__26309__auto__,map__28775,map__28775__$1,opts,before_jsload,on_jsload,reload_dependents,map__28776,map__28776__$1,msg,files,figwheel_meta,recompile_dependents))
})();
var state__26311__auto__ = (function (){var statearr_29018 = f__26310__auto__.call(null);
(statearr_29018[(6)] = c__26309__auto__);

return statearr_29018;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__26311__auto__);
});})(c__26309__auto__,map__28775,map__28775__$1,opts,before_jsload,on_jsload,reload_dependents,map__28776,map__28776__$1,msg,files,figwheel_meta,recompile_dependents))
);

return c__26309__auto__;
});
figwheel.client.file_reloading.current_links = (function figwheel$client$file_reloading$current_links(){
return Array.prototype.slice.call(document.getElementsByTagName("link"));
});
figwheel.client.file_reloading.truncate_url = (function figwheel$client$file_reloading$truncate_url(url){
return clojure.string.replace_first.call(null,clojure.string.replace_first.call(null,clojure.string.replace_first.call(null,clojure.string.replace_first.call(null,cljs.core.first.call(null,clojure.string.split.call(null,url,/\?/)),[cljs.core.str.cljs$core$IFn$_invoke$arity$1(location.protocol),"//"].join(''),""),".*://",""),/^\/\//,""),/[^\\/]*/,"");
});
figwheel.client.file_reloading.matches_file_QMARK_ = (function figwheel$client$file_reloading$matches_file_QMARK_(p__29081,link){
var map__29082 = p__29081;
var map__29082__$1 = (((((!((map__29082 == null))))?(((((map__29082.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__29082.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__29082):map__29082);
var file = cljs.core.get.call(null,map__29082__$1,new cljs.core.Keyword(null,"file","file",-1269645878));
var temp__5720__auto__ = link.href;
if(cljs.core.truth_(temp__5720__auto__)){
var link_href = temp__5720__auto__;
var match = clojure.string.join.call(null,"/",cljs.core.take_while.call(null,cljs.core.identity,cljs.core.map.call(null,((function (link_href,temp__5720__auto__,map__29082,map__29082__$1,file){
return (function (p1__29079_SHARP_,p2__29080_SHARP_){
if(cljs.core._EQ_.call(null,p1__29079_SHARP_,p2__29080_SHARP_)){
return p1__29079_SHARP_;
} else {
return false;
}
});})(link_href,temp__5720__auto__,map__29082,map__29082__$1,file))
,cljs.core.reverse.call(null,clojure.string.split.call(null,file,"/")),cljs.core.reverse.call(null,clojure.string.split.call(null,figwheel.client.file_reloading.truncate_url.call(null,link_href),"/")))));
var match_length = cljs.core.count.call(null,match);
var file_name_length = cljs.core.count.call(null,cljs.core.last.call(null,clojure.string.split.call(null,file,"/")));
if((match_length >= file_name_length)){
return new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"link","link",-1769163468),link,new cljs.core.Keyword(null,"link-href","link-href",-250644450),link_href,new cljs.core.Keyword(null,"match-length","match-length",1101537310),match_length,new cljs.core.Keyword(null,"current-url-length","current-url-length",380404083),cljs.core.count.call(null,figwheel.client.file_reloading.truncate_url.call(null,link_href))], null);
} else {
return null;
}
} else {
return null;
}
});
figwheel.client.file_reloading.get_correct_link = (function figwheel$client$file_reloading$get_correct_link(f_data){
var temp__5720__auto__ = cljs.core.first.call(null,cljs.core.sort_by.call(null,(function (p__29085){
var map__29086 = p__29085;
var map__29086__$1 = (((((!((map__29086 == null))))?(((((map__29086.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__29086.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__29086):map__29086);
var match_length = cljs.core.get.call(null,map__29086__$1,new cljs.core.Keyword(null,"match-length","match-length",1101537310));
var current_url_length = cljs.core.get.call(null,map__29086__$1,new cljs.core.Keyword(null,"current-url-length","current-url-length",380404083));
return (current_url_length - match_length);
}),cljs.core.keep.call(null,(function (p1__29084_SHARP_){
return figwheel.client.file_reloading.matches_file_QMARK_.call(null,f_data,p1__29084_SHARP_);
}),figwheel.client.file_reloading.current_links.call(null))));
if(cljs.core.truth_(temp__5720__auto__)){
var res = temp__5720__auto__;
return new cljs.core.Keyword(null,"link","link",-1769163468).cljs$core$IFn$_invoke$arity$1(res);
} else {
return null;
}
});
figwheel.client.file_reloading.clone_link = (function figwheel$client$file_reloading$clone_link(link,url){
var clone = document.createElement("link");
clone.rel = "stylesheet";

clone.media = link.media;

clone.disabled = link.disabled;

clone.href = figwheel.client.file_reloading.add_cache_buster.call(null,url);

return clone;
});
figwheel.client.file_reloading.create_link = (function figwheel$client$file_reloading$create_link(url){
var link = document.createElement("link");
link.rel = "stylesheet";

link.href = figwheel.client.file_reloading.add_cache_buster.call(null,url);

return link;
});
figwheel.client.file_reloading.distinctify = (function figwheel$client$file_reloading$distinctify(key,seqq){
return cljs.core.vals.call(null,cljs.core.reduce.call(null,(function (p1__29088_SHARP_,p2__29089_SHARP_){
return cljs.core.assoc.call(null,p1__29088_SHARP_,cljs.core.get.call(null,p2__29089_SHARP_,key),p2__29089_SHARP_);
}),cljs.core.PersistentArrayMap.EMPTY,seqq));
});
figwheel.client.file_reloading.add_link_to_document = (function figwheel$client$file_reloading$add_link_to_document(orig_link,klone,finished_fn){
var parent = orig_link.parentNode;
if(cljs.core._EQ_.call(null,orig_link,parent.lastChild)){
parent.appendChild(klone);
} else {
parent.insertBefore(klone,orig_link.nextSibling);
}

return setTimeout(((function (parent){
return (function (){
parent.removeChild(orig_link);

return finished_fn.call(null);
});})(parent))
,(300));
});
if((typeof figwheel !== 'undefined') && (typeof figwheel.client !== 'undefined') && (typeof figwheel.client.file_reloading !== 'undefined') && (typeof figwheel.client.file_reloading.reload_css_deferred_chain !== 'undefined')){
} else {
figwheel.client.file_reloading.reload_css_deferred_chain = cljs.core.atom.call(null,goog.async.Deferred.succeed());
}
figwheel.client.file_reloading.reload_css_file = (function figwheel$client$file_reloading$reload_css_file(f_data,fin){
var temp__5718__auto__ = figwheel.client.file_reloading.get_correct_link.call(null,f_data);
if(cljs.core.truth_(temp__5718__auto__)){
var link = temp__5718__auto__;
return figwheel.client.file_reloading.add_link_to_document.call(null,link,figwheel.client.file_reloading.clone_link.call(null,link,link.href),((function (link,temp__5718__auto__){
return (function (){
return fin.call(null,cljs.core.assoc.call(null,f_data,new cljs.core.Keyword(null,"loaded","loaded",-1246482293),true));
});})(link,temp__5718__auto__))
);
} else {
return fin.call(null,f_data);
}
});
figwheel.client.file_reloading.reload_css_files_STAR_ = (function figwheel$client$file_reloading$reload_css_files_STAR_(deferred,f_datas,on_cssload){
return figwheel.client.utils.liftContD.call(null,figwheel.client.utils.mapConcatD.call(null,deferred,figwheel.client.file_reloading.reload_css_file,f_datas),(function (f_datas_SINGLEQUOTE_,fin){
var loaded_f_datas_29090 = cljs.core.filter.call(null,new cljs.core.Keyword(null,"loaded","loaded",-1246482293),f_datas_SINGLEQUOTE_);
figwheel.client.file_reloading.on_cssload_custom_event.call(null,loaded_f_datas_29090);

if(cljs.core.fn_QMARK_.call(null,on_cssload)){
on_cssload.call(null,loaded_f_datas_29090);
} else {
}

return fin.call(null);
}));
});
figwheel.client.file_reloading.reload_css_files = (function figwheel$client$file_reloading$reload_css_files(p__29091,p__29092){
var map__29093 = p__29091;
var map__29093__$1 = (((((!((map__29093 == null))))?(((((map__29093.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__29093.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__29093):map__29093);
var on_cssload = cljs.core.get.call(null,map__29093__$1,new cljs.core.Keyword(null,"on-cssload","on-cssload",1825432318));
var map__29094 = p__29092;
var map__29094__$1 = (((((!((map__29094 == null))))?(((((map__29094.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__29094.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__29094):map__29094);
var files_msg = map__29094__$1;
var files = cljs.core.get.call(null,map__29094__$1,new cljs.core.Keyword(null,"files","files",-472457450));
if(figwheel.client.utils.html_env_QMARK_.call(null)){
var temp__5720__auto__ = cljs.core.not_empty.call(null,figwheel.client.file_reloading.distinctify.call(null,new cljs.core.Keyword(null,"file","file",-1269645878),files));
if(cljs.core.truth_(temp__5720__auto__)){
var f_datas = temp__5720__auto__;
return cljs.core.swap_BANG_.call(null,figwheel.client.file_reloading.reload_css_deferred_chain,figwheel.client.file_reloading.reload_css_files_STAR_,f_datas,on_cssload);
} else {
return null;
}
} else {
return null;
}
});

//# sourceMappingURL=file_reloading.js.map?rel=1591997117008
