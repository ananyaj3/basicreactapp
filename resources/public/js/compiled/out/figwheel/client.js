// Compiled by ClojureScript 1.10.520 {}
goog.provide('figwheel.client');
goog.require('cljs.core');
goog.require('goog.Uri');
goog.require('goog.userAgent.product');
goog.require('goog.object');
goog.require('cljs.reader');
goog.require('cljs.core.async');
goog.require('figwheel.client.socket');
goog.require('figwheel.client.utils');
goog.require('figwheel.client.heads_up');
goog.require('figwheel.client.file_reloading');
goog.require('clojure.string');
goog.require('cljs.repl');
figwheel.client._figwheel_version_ = "0.5.19";
figwheel.client.js_stringify = (((((typeof JSON !== 'undefined')) && ((!((JSON.stringify == null))))))?(function (x){
return ["#js ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(JSON.stringify(x,null," "))].join('');
}):(function (x){
try{return cljs.core.str.cljs$core$IFn$_invoke$arity$1(x);
}catch (e30300){if((e30300 instanceof Error)){
var e = e30300;
return "Error: Unable to stringify";
} else {
throw e30300;

}
}}));
figwheel.client.figwheel_repl_print = (function figwheel$client$figwheel_repl_print(var_args){
var G__30303 = arguments.length;
switch (G__30303) {
case 2:
return figwheel.client.figwheel_repl_print.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 1:
return figwheel.client.figwheel_repl_print.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

figwheel.client.figwheel_repl_print.cljs$core$IFn$_invoke$arity$2 = (function (stream,args){
figwheel.client.socket.send_BANG_.call(null,new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"figwheel-event","figwheel-event",519570592),"callback",new cljs.core.Keyword(null,"callback-name","callback-name",336964714),"figwheel-repl-print",new cljs.core.Keyword(null,"content","content",15833224),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"stream","stream",1534941648),stream,new cljs.core.Keyword(null,"args","args",1315556576),cljs.core.mapv.call(null,(function (p1__30301_SHARP_){
if(typeof p1__30301_SHARP_ === 'string'){
return p1__30301_SHARP_;
} else {
return figwheel.client.js_stringify.call(null,p1__30301_SHARP_);
}
}),args)], null)], null));

return null;
});

figwheel.client.figwheel_repl_print.cljs$core$IFn$_invoke$arity$1 = (function (args){
return figwheel.client.figwheel_repl_print.call(null,new cljs.core.Keyword(null,"out","out",-910545517),args);
});

figwheel.client.figwheel_repl_print.cljs$lang$maxFixedArity = 2;

figwheel.client.console_out_print = (function figwheel$client$console_out_print(args){
return console.log.apply(console,cljs.core.into_array.call(null,args));
});
figwheel.client.console_err_print = (function figwheel$client$console_err_print(args){
return console.error.apply(console,cljs.core.into_array.call(null,args));
});
figwheel.client.repl_out_print_fn = (function figwheel$client$repl_out_print_fn(var_args){
var args__4736__auto__ = [];
var len__4730__auto___30306 = arguments.length;
var i__4731__auto___30307 = (0);
while(true){
if((i__4731__auto___30307 < len__4730__auto___30306)){
args__4736__auto__.push((arguments[i__4731__auto___30307]));

var G__30308 = (i__4731__auto___30307 + (1));
i__4731__auto___30307 = G__30308;
continue;
} else {
}
break;
}

var argseq__4737__auto__ = ((((0) < args__4736__auto__.length))?(new cljs.core.IndexedSeq(args__4736__auto__.slice((0)),(0),null)):null);
return figwheel.client.repl_out_print_fn.cljs$core$IFn$_invoke$arity$variadic(argseq__4737__auto__);
});

figwheel.client.repl_out_print_fn.cljs$core$IFn$_invoke$arity$variadic = (function (args){
figwheel.client.console_out_print.call(null,args);

figwheel.client.figwheel_repl_print.call(null,new cljs.core.Keyword(null,"out","out",-910545517),args);

return null;
});

figwheel.client.repl_out_print_fn.cljs$lang$maxFixedArity = (0);

/** @this {Function} */
figwheel.client.repl_out_print_fn.cljs$lang$applyTo = (function (seq30305){
var self__4718__auto__ = this;
return self__4718__auto__.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq30305));
});

figwheel.client.repl_err_print_fn = (function figwheel$client$repl_err_print_fn(var_args){
var args__4736__auto__ = [];
var len__4730__auto___30310 = arguments.length;
var i__4731__auto___30311 = (0);
while(true){
if((i__4731__auto___30311 < len__4730__auto___30310)){
args__4736__auto__.push((arguments[i__4731__auto___30311]));

var G__30312 = (i__4731__auto___30311 + (1));
i__4731__auto___30311 = G__30312;
continue;
} else {
}
break;
}

var argseq__4737__auto__ = ((((0) < args__4736__auto__.length))?(new cljs.core.IndexedSeq(args__4736__auto__.slice((0)),(0),null)):null);
return figwheel.client.repl_err_print_fn.cljs$core$IFn$_invoke$arity$variadic(argseq__4737__auto__);
});

figwheel.client.repl_err_print_fn.cljs$core$IFn$_invoke$arity$variadic = (function (args){
figwheel.client.console_err_print.call(null,args);

figwheel.client.figwheel_repl_print.call(null,new cljs.core.Keyword(null,"err","err",-2089457205),args);

return null;
});

figwheel.client.repl_err_print_fn.cljs$lang$maxFixedArity = (0);

/** @this {Function} */
figwheel.client.repl_err_print_fn.cljs$lang$applyTo = (function (seq30309){
var self__4718__auto__ = this;
return self__4718__auto__.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq30309));
});

figwheel.client.enable_repl_print_BANG_ = (function figwheel$client$enable_repl_print_BANG_(){
cljs.core._STAR_print_newline_STAR_ = false;

cljs.core.set_print_fn_BANG_.call(null,figwheel.client.repl_out_print_fn);

cljs.core.set_print_err_fn_BANG_.call(null,figwheel.client.repl_err_print_fn);

return null;
});
figwheel.client.autoload_QMARK_ = (function figwheel$client$autoload_QMARK_(){
return figwheel.client.utils.persistent_config_get.call(null,new cljs.core.Keyword(null,"figwheel-autoload","figwheel-autoload",-2044741728),true);
});
figwheel.client.toggle_autoload = (function figwheel$client$toggle_autoload(){
var res = figwheel.client.utils.persistent_config_set_BANG_.call(null,new cljs.core.Keyword(null,"figwheel-autoload","figwheel-autoload",-2044741728),cljs.core.not.call(null,figwheel.client.autoload_QMARK_.call(null)));
figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"info","info",-317069002),"Toggle autoload deprecated! Use (figwheel.client/set-autoload! false)");

figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"info","info",-317069002),["Figwheel autoloading ",(cljs.core.truth_(figwheel.client.autoload_QMARK_.call(null))?"ON":"OFF")].join(''));

return res;
});
goog.exportSymbol('figwheel.client.toggle_autoload', figwheel.client.toggle_autoload);
/**
 * Figwheel by default loads code changes as you work. Sometimes you
 *   just want to work on your code without the ramifications of
 *   autoloading and simply load your code piecemeal in the REPL. You can
 *   turn autoloading on and of with this method.
 * 
 *   (figwheel.client/set-autoload false)
 * 
 *   NOTE: This is a persistent setting, meaning that it will persist
 *   through browser reloads.
 */
figwheel.client.set_autoload = (function figwheel$client$set_autoload(b){
if(((b === true) || (b === false))){
} else {
throw (new Error("Assert failed: (or (true? b) (false? b))"));
}

return figwheel.client.utils.persistent_config_set_BANG_.call(null,new cljs.core.Keyword(null,"figwheel-autoload","figwheel-autoload",-2044741728),b);
});
goog.exportSymbol('figwheel.client.set_autoload', figwheel.client.set_autoload);
figwheel.client.repl_pprint = (function figwheel$client$repl_pprint(){
return figwheel.client.utils.persistent_config_get.call(null,new cljs.core.Keyword(null,"figwheel-repl-pprint","figwheel-repl-pprint",1076150873),true);
});
goog.exportSymbol('figwheel.client.repl_pprint', figwheel.client.repl_pprint);
/**
 * This method gives you the ability to turn the pretty printing of
 *   the REPL's return value on and off.
 * 
 *   (figwheel.client/set-repl-pprint false)
 * 
 *   NOTE: This is a persistent setting, meaning that it will persist
 *   through browser reloads.
 */
figwheel.client.set_repl_pprint = (function figwheel$client$set_repl_pprint(b){
if(((b === true) || (b === false))){
} else {
throw (new Error("Assert failed: (or (true? b) (false? b))"));
}

return figwheel.client.utils.persistent_config_set_BANG_.call(null,new cljs.core.Keyword(null,"figwheel-repl-pprint","figwheel-repl-pprint",1076150873),b);
});
goog.exportSymbol('figwheel.client.set_repl_pprint', figwheel.client.set_repl_pprint);
figwheel.client.repl_result_pr_str = (function figwheel$client$repl_result_pr_str(v){
if(cljs.core.truth_(figwheel.client.repl_pprint.call(null))){
return figwheel.client.utils.pprint_to_string.call(null,v);
} else {
return cljs.core.pr_str.call(null,v);
}
});
goog.exportSymbol('figwheel.client.repl_result_pr_str', figwheel.client.repl_result_pr_str);
figwheel.client.get_essential_messages = (function figwheel$client$get_essential_messages(ed){
if(cljs.core.truth_(ed)){
return cljs.core.cons.call(null,cljs.core.select_keys.call(null,ed,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"message","message",-406056002),new cljs.core.Keyword(null,"class","class",-2030961996)], null)),figwheel.client.get_essential_messages.call(null,new cljs.core.Keyword(null,"cause","cause",231901252).cljs$core$IFn$_invoke$arity$1(ed)));
} else {
return null;
}
});
figwheel.client.error_msg_format = (function figwheel$client$error_msg_format(p__30313){
var map__30314 = p__30313;
var map__30314__$1 = (((((!((map__30314 == null))))?(((((map__30314.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__30314.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__30314):map__30314);
var message = cljs.core.get.call(null,map__30314__$1,new cljs.core.Keyword(null,"message","message",-406056002));
var class$ = cljs.core.get.call(null,map__30314__$1,new cljs.core.Keyword(null,"class","class",-2030961996));
return [cljs.core.str.cljs$core$IFn$_invoke$arity$1(class$)," : ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(message)].join('');
});
figwheel.client.format_messages = cljs.core.comp.call(null,cljs.core.partial.call(null,cljs.core.map,figwheel.client.error_msg_format),figwheel.client.get_essential_messages);
figwheel.client.focus_msgs = (function figwheel$client$focus_msgs(name_set,msg_hist){
return cljs.core.cons.call(null,cljs.core.first.call(null,msg_hist),cljs.core.filter.call(null,cljs.core.comp.call(null,name_set,new cljs.core.Keyword(null,"msg-name","msg-name",-353709863)),cljs.core.rest.call(null,msg_hist)));
});
figwheel.client.reload_file_QMARK__STAR_ = (function figwheel$client$reload_file_QMARK__STAR_(msg_name,opts){
var or__4131__auto__ = new cljs.core.Keyword(null,"load-warninged-code","load-warninged-code",-2030345223).cljs$core$IFn$_invoke$arity$1(opts);
if(cljs.core.truth_(or__4131__auto__)){
return or__4131__auto__;
} else {
return cljs.core.not_EQ_.call(null,msg_name,new cljs.core.Keyword(null,"compile-warning","compile-warning",43425356));
}
});
figwheel.client.reload_file_state_QMARK_ = (function figwheel$client$reload_file_state_QMARK_(msg_names,opts){
var and__4120__auto__ = cljs.core._EQ_.call(null,cljs.core.first.call(null,msg_names),new cljs.core.Keyword(null,"files-changed","files-changed",-1418200563));
if(and__4120__auto__){
return figwheel.client.reload_file_QMARK__STAR_.call(null,cljs.core.second.call(null,msg_names),opts);
} else {
return and__4120__auto__;
}
});
figwheel.client.block_reload_file_state_QMARK_ = (function figwheel$client$block_reload_file_state_QMARK_(msg_names,opts){
return ((cljs.core._EQ_.call(null,cljs.core.first.call(null,msg_names),new cljs.core.Keyword(null,"files-changed","files-changed",-1418200563))) && (cljs.core.not.call(null,figwheel.client.reload_file_QMARK__STAR_.call(null,cljs.core.second.call(null,msg_names),opts))));
});
figwheel.client.warning_append_state_QMARK_ = (function figwheel$client$warning_append_state_QMARK_(msg_names){
return cljs.core._EQ_.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"compile-warning","compile-warning",43425356),new cljs.core.Keyword(null,"compile-warning","compile-warning",43425356)], null),cljs.core.take.call(null,(2),msg_names));
});
figwheel.client.warning_state_QMARK_ = (function figwheel$client$warning_state_QMARK_(msg_names){
return cljs.core._EQ_.call(null,new cljs.core.Keyword(null,"compile-warning","compile-warning",43425356),cljs.core.first.call(null,msg_names));
});
figwheel.client.rewarning_state_QMARK_ = (function figwheel$client$rewarning_state_QMARK_(msg_names){
return cljs.core._EQ_.call(null,new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"compile-warning","compile-warning",43425356),new cljs.core.Keyword(null,"files-changed","files-changed",-1418200563),new cljs.core.Keyword(null,"compile-warning","compile-warning",43425356)], null),cljs.core.take.call(null,(3),msg_names));
});
figwheel.client.compile_fail_state_QMARK_ = (function figwheel$client$compile_fail_state_QMARK_(msg_names){
return cljs.core._EQ_.call(null,new cljs.core.Keyword(null,"compile-failed","compile-failed",-477639289),cljs.core.first.call(null,msg_names));
});
figwheel.client.compile_refail_state_QMARK_ = (function figwheel$client$compile_refail_state_QMARK_(msg_names){
return cljs.core._EQ_.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"compile-failed","compile-failed",-477639289),new cljs.core.Keyword(null,"compile-failed","compile-failed",-477639289)], null),cljs.core.take.call(null,(2),msg_names));
});
figwheel.client.css_loaded_state_QMARK_ = (function figwheel$client$css_loaded_state_QMARK_(msg_names){
return cljs.core._EQ_.call(null,new cljs.core.Keyword(null,"css-files-changed","css-files-changed",720773874),cljs.core.first.call(null,msg_names));
});
figwheel.client.file_reloader_plugin = (function figwheel$client$file_reloader_plugin(opts){
var ch = cljs.core.async.chan.call(null);
var c__26309__auto___30393 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__26309__auto___30393,ch){
return (function (){
var f__26310__auto__ = (function (){var switch__26214__auto__ = ((function (c__26309__auto___30393,ch){
return (function (state_30365){
var state_val_30366 = (state_30365[(1)]);
if((state_val_30366 === (7))){
var inst_30361 = (state_30365[(2)]);
var state_30365__$1 = state_30365;
var statearr_30367_30394 = state_30365__$1;
(statearr_30367_30394[(2)] = inst_30361);

(statearr_30367_30394[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30366 === (1))){
var state_30365__$1 = state_30365;
var statearr_30368_30395 = state_30365__$1;
(statearr_30368_30395[(2)] = null);

(statearr_30368_30395[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30366 === (4))){
var inst_30318 = (state_30365[(7)]);
var inst_30318__$1 = (state_30365[(2)]);
var state_30365__$1 = (function (){var statearr_30369 = state_30365;
(statearr_30369[(7)] = inst_30318__$1);

return statearr_30369;
})();
if(cljs.core.truth_(inst_30318__$1)){
var statearr_30370_30396 = state_30365__$1;
(statearr_30370_30396[(1)] = (5));

} else {
var statearr_30371_30397 = state_30365__$1;
(statearr_30371_30397[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30366 === (15))){
var inst_30325 = (state_30365[(8)]);
var inst_30340 = new cljs.core.Keyword(null,"files","files",-472457450).cljs$core$IFn$_invoke$arity$1(inst_30325);
var inst_30341 = cljs.core.first.call(null,inst_30340);
var inst_30342 = new cljs.core.Keyword(null,"file","file",-1269645878).cljs$core$IFn$_invoke$arity$1(inst_30341);
var inst_30343 = ["Figwheel: Not loading code with warnings - ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(inst_30342)].join('');
var inst_30344 = figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"warn","warn",-436710552),inst_30343);
var state_30365__$1 = state_30365;
var statearr_30372_30398 = state_30365__$1;
(statearr_30372_30398[(2)] = inst_30344);

(statearr_30372_30398[(1)] = (17));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30366 === (13))){
var inst_30349 = (state_30365[(2)]);
var state_30365__$1 = state_30365;
var statearr_30373_30399 = state_30365__$1;
(statearr_30373_30399[(2)] = inst_30349);

(statearr_30373_30399[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30366 === (6))){
var state_30365__$1 = state_30365;
var statearr_30374_30400 = state_30365__$1;
(statearr_30374_30400[(2)] = null);

(statearr_30374_30400[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30366 === (17))){
var inst_30347 = (state_30365[(2)]);
var state_30365__$1 = state_30365;
var statearr_30375_30401 = state_30365__$1;
(statearr_30375_30401[(2)] = inst_30347);

(statearr_30375_30401[(1)] = (13));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30366 === (3))){
var inst_30363 = (state_30365[(2)]);
var state_30365__$1 = state_30365;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_30365__$1,inst_30363);
} else {
if((state_val_30366 === (12))){
var inst_30324 = (state_30365[(9)]);
var inst_30338 = figwheel.client.block_reload_file_state_QMARK_.call(null,inst_30324,opts);
var state_30365__$1 = state_30365;
if(inst_30338){
var statearr_30376_30402 = state_30365__$1;
(statearr_30376_30402[(1)] = (15));

} else {
var statearr_30377_30403 = state_30365__$1;
(statearr_30377_30403[(1)] = (16));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30366 === (2))){
var state_30365__$1 = state_30365;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_30365__$1,(4),ch);
} else {
if((state_val_30366 === (11))){
var inst_30325 = (state_30365[(8)]);
var inst_30330 = cljs.core.PersistentVector.EMPTY_NODE;
var inst_30331 = figwheel.client.file_reloading.reload_js_files.call(null,opts,inst_30325);
var inst_30332 = cljs.core.async.timeout.call(null,(1000));
var inst_30333 = [inst_30331,inst_30332];
var inst_30334 = (new cljs.core.PersistentVector(null,2,(5),inst_30330,inst_30333,null));
var state_30365__$1 = state_30365;
return cljs.core.async.ioc_alts_BANG_.call(null,state_30365__$1,(14),inst_30334);
} else {
if((state_val_30366 === (9))){
var inst_30325 = (state_30365[(8)]);
var inst_30351 = figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"warn","warn",-436710552),"Figwheel: code autoloading is OFF");
var inst_30352 = new cljs.core.Keyword(null,"files","files",-472457450).cljs$core$IFn$_invoke$arity$1(inst_30325);
var inst_30353 = cljs.core.map.call(null,new cljs.core.Keyword(null,"file","file",-1269645878),inst_30352);
var inst_30354 = ["Not loading: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(inst_30353)].join('');
var inst_30355 = figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"info","info",-317069002),inst_30354);
var state_30365__$1 = (function (){var statearr_30378 = state_30365;
(statearr_30378[(10)] = inst_30351);

return statearr_30378;
})();
var statearr_30379_30404 = state_30365__$1;
(statearr_30379_30404[(2)] = inst_30355);

(statearr_30379_30404[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30366 === (5))){
var inst_30318 = (state_30365[(7)]);
var inst_30320 = [new cljs.core.Keyword(null,"compile-warning","compile-warning",43425356),null,new cljs.core.Keyword(null,"files-changed","files-changed",-1418200563),null];
var inst_30321 = (new cljs.core.PersistentArrayMap(null,2,inst_30320,null));
var inst_30322 = (new cljs.core.PersistentHashSet(null,inst_30321,null));
var inst_30323 = figwheel.client.focus_msgs.call(null,inst_30322,inst_30318);
var inst_30324 = cljs.core.map.call(null,new cljs.core.Keyword(null,"msg-name","msg-name",-353709863),inst_30323);
var inst_30325 = cljs.core.first.call(null,inst_30323);
var inst_30326 = figwheel.client.autoload_QMARK_.call(null);
var state_30365__$1 = (function (){var statearr_30380 = state_30365;
(statearr_30380[(8)] = inst_30325);

(statearr_30380[(9)] = inst_30324);

return statearr_30380;
})();
if(cljs.core.truth_(inst_30326)){
var statearr_30381_30405 = state_30365__$1;
(statearr_30381_30405[(1)] = (8));

} else {
var statearr_30382_30406 = state_30365__$1;
(statearr_30382_30406[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30366 === (14))){
var inst_30336 = (state_30365[(2)]);
var state_30365__$1 = state_30365;
var statearr_30383_30407 = state_30365__$1;
(statearr_30383_30407[(2)] = inst_30336);

(statearr_30383_30407[(1)] = (13));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30366 === (16))){
var state_30365__$1 = state_30365;
var statearr_30384_30408 = state_30365__$1;
(statearr_30384_30408[(2)] = null);

(statearr_30384_30408[(1)] = (17));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30366 === (10))){
var inst_30357 = (state_30365[(2)]);
var state_30365__$1 = (function (){var statearr_30385 = state_30365;
(statearr_30385[(11)] = inst_30357);

return statearr_30385;
})();
var statearr_30386_30409 = state_30365__$1;
(statearr_30386_30409[(2)] = null);

(statearr_30386_30409[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30366 === (8))){
var inst_30324 = (state_30365[(9)]);
var inst_30328 = figwheel.client.reload_file_state_QMARK_.call(null,inst_30324,opts);
var state_30365__$1 = state_30365;
if(cljs.core.truth_(inst_30328)){
var statearr_30387_30410 = state_30365__$1;
(statearr_30387_30410[(1)] = (11));

} else {
var statearr_30388_30411 = state_30365__$1;
(statearr_30388_30411[(1)] = (12));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__26309__auto___30393,ch))
;
return ((function (switch__26214__auto__,c__26309__auto___30393,ch){
return (function() {
var figwheel$client$file_reloader_plugin_$_state_machine__26215__auto__ = null;
var figwheel$client$file_reloader_plugin_$_state_machine__26215__auto____0 = (function (){
var statearr_30389 = [null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_30389[(0)] = figwheel$client$file_reloader_plugin_$_state_machine__26215__auto__);

(statearr_30389[(1)] = (1));

return statearr_30389;
});
var figwheel$client$file_reloader_plugin_$_state_machine__26215__auto____1 = (function (state_30365){
while(true){
var ret_value__26216__auto__ = (function (){try{while(true){
var result__26217__auto__ = switch__26214__auto__.call(null,state_30365);
if(cljs.core.keyword_identical_QMARK_.call(null,result__26217__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__26217__auto__;
}
break;
}
}catch (e30390){if((e30390 instanceof Object)){
var ex__26218__auto__ = e30390;
var statearr_30391_30412 = state_30365;
(statearr_30391_30412[(5)] = ex__26218__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_30365);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e30390;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__26216__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__30413 = state_30365;
state_30365 = G__30413;
continue;
} else {
return ret_value__26216__auto__;
}
break;
}
});
figwheel$client$file_reloader_plugin_$_state_machine__26215__auto__ = function(state_30365){
switch(arguments.length){
case 0:
return figwheel$client$file_reloader_plugin_$_state_machine__26215__auto____0.call(this);
case 1:
return figwheel$client$file_reloader_plugin_$_state_machine__26215__auto____1.call(this,state_30365);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
figwheel$client$file_reloader_plugin_$_state_machine__26215__auto__.cljs$core$IFn$_invoke$arity$0 = figwheel$client$file_reloader_plugin_$_state_machine__26215__auto____0;
figwheel$client$file_reloader_plugin_$_state_machine__26215__auto__.cljs$core$IFn$_invoke$arity$1 = figwheel$client$file_reloader_plugin_$_state_machine__26215__auto____1;
return figwheel$client$file_reloader_plugin_$_state_machine__26215__auto__;
})()
;})(switch__26214__auto__,c__26309__auto___30393,ch))
})();
var state__26311__auto__ = (function (){var statearr_30392 = f__26310__auto__.call(null);
(statearr_30392[(6)] = c__26309__auto___30393);

return statearr_30392;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__26311__auto__);
});})(c__26309__auto___30393,ch))
);


return ((function (ch){
return (function (msg_hist){
cljs.core.async.put_BANG_.call(null,ch,msg_hist);

return msg_hist;
});
;})(ch))
});
figwheel.client.truncate_stack_trace = (function figwheel$client$truncate_stack_trace(stack_str){
return cljs.core.take_while.call(null,(function (p1__30414_SHARP_){
return cljs.core.not.call(null,cljs.core.re_matches.call(null,/.*eval_javascript_STAR__STAR_.*/,p1__30414_SHARP_));
}),clojure.string.split_lines.call(null,stack_str));
});
figwheel.client.get_ua_product = (function figwheel$client$get_ua_product(){
if(figwheel.client.utils.node_env_QMARK_.call(null)){
return new cljs.core.Keyword(null,"chrome","chrome",1718738387);
} else {
if(cljs.core.truth_(goog.userAgent.product.SAFARI)){
return new cljs.core.Keyword(null,"safari","safari",497115653);
} else {
if(cljs.core.truth_(goog.userAgent.product.CHROME)){
return new cljs.core.Keyword(null,"chrome","chrome",1718738387);
} else {
if(cljs.core.truth_(goog.userAgent.product.FIREFOX)){
return new cljs.core.Keyword(null,"firefox","firefox",1283768880);
} else {
if(cljs.core.truth_(goog.userAgent.product.IE)){
return new cljs.core.Keyword(null,"ie","ie",2038473780);
} else {
return null;
}
}
}
}
}
});
var base_path_30420 = figwheel.client.utils.base_url_path.call(null);
figwheel.client.eval_javascript_STAR__STAR_ = ((function (base_path_30420){
return (function figwheel$client$eval_javascript_STAR__STAR_(code,opts,result_handler){
try{var sb = (new goog.string.StringBuffer());
var _STAR_print_newline_STAR__orig_val__30416 = cljs.core._STAR_print_newline_STAR_;
var _STAR_print_fn_STAR__orig_val__30417 = cljs.core._STAR_print_fn_STAR_;
var _STAR_print_newline_STAR__temp_val__30418 = true;
var _STAR_print_fn_STAR__temp_val__30419 = ((function (_STAR_print_newline_STAR__orig_val__30416,_STAR_print_fn_STAR__orig_val__30417,_STAR_print_newline_STAR__temp_val__30418,sb,base_path_30420){
return (function (x){
return sb.append(x);
});})(_STAR_print_newline_STAR__orig_val__30416,_STAR_print_fn_STAR__orig_val__30417,_STAR_print_newline_STAR__temp_val__30418,sb,base_path_30420))
;
cljs.core._STAR_print_newline_STAR_ = _STAR_print_newline_STAR__temp_val__30418;

cljs.core._STAR_print_fn_STAR_ = _STAR_print_fn_STAR__temp_val__30419;

try{var result_value = figwheel.client.utils.eval_helper.call(null,code,opts);
var result_value__$1 = (((!(typeof result_value === 'string')))?cljs.core.pr_str.call(null,result_value):result_value);
return result_handler.call(null,new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"status","status",-1997798413),new cljs.core.Keyword(null,"success","success",1890645906),new cljs.core.Keyword(null,"out","out",-910545517),cljs.core.str.cljs$core$IFn$_invoke$arity$1(sb),new cljs.core.Keyword(null,"ua-product","ua-product",938384227),figwheel.client.get_ua_product.call(null),new cljs.core.Keyword(null,"value","value",305978217),result_value__$1], null));
}finally {cljs.core._STAR_print_fn_STAR_ = _STAR_print_fn_STAR__orig_val__30417;

cljs.core._STAR_print_newline_STAR_ = _STAR_print_newline_STAR__orig_val__30416;
}}catch (e30415){if((e30415 instanceof Error)){
var e = e30415;
return result_handler.call(null,new cljs.core.PersistentArrayMap(null, 5, [new cljs.core.Keyword(null,"status","status",-1997798413),new cljs.core.Keyword(null,"exception","exception",-335277064),new cljs.core.Keyword(null,"value","value",305978217),cljs.core.pr_str.call(null,e),new cljs.core.Keyword(null,"ua-product","ua-product",938384227),figwheel.client.get_ua_product.call(null),new cljs.core.Keyword(null,"stacktrace","stacktrace",-95588394),clojure.string.join.call(null,"\n",figwheel.client.truncate_stack_trace.call(null,e.stack)),new cljs.core.Keyword(null,"base-path","base-path",495760020),base_path_30420], null));
} else {
var e = e30415;
return result_handler.call(null,new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"status","status",-1997798413),new cljs.core.Keyword(null,"exception","exception",-335277064),new cljs.core.Keyword(null,"ua-product","ua-product",938384227),figwheel.client.get_ua_product.call(null),new cljs.core.Keyword(null,"value","value",305978217),cljs.core.pr_str.call(null,e),new cljs.core.Keyword(null,"stacktrace","stacktrace",-95588394),"No stacktrace available."], null));

}
}});})(base_path_30420))
;
/**
 * The REPL can disconnect and reconnect lets ensure cljs.user exists at least.
 */
figwheel.client.ensure_cljs_user = (function figwheel$client$ensure_cljs_user(){
if(cljs.core.truth_(cljs.user)){
return null;
} else {
return cljs.user = ({});
}
});
figwheel.client.repl_plugin = (function figwheel$client$repl_plugin(p__30421){
var map__30422 = p__30421;
var map__30422__$1 = (((((!((map__30422 == null))))?(((((map__30422.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__30422.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__30422):map__30422);
var opts = map__30422__$1;
var build_id = cljs.core.get.call(null,map__30422__$1,new cljs.core.Keyword(null,"build-id","build-id",1642831089));
return ((function (map__30422,map__30422__$1,opts,build_id){
return (function (p__30424){
var vec__30425 = p__30424;
var seq__30426 = cljs.core.seq.call(null,vec__30425);
var first__30427 = cljs.core.first.call(null,seq__30426);
var seq__30426__$1 = cljs.core.next.call(null,seq__30426);
var map__30428 = first__30427;
var map__30428__$1 = (((((!((map__30428 == null))))?(((((map__30428.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__30428.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__30428):map__30428);
var msg = map__30428__$1;
var msg_name = cljs.core.get.call(null,map__30428__$1,new cljs.core.Keyword(null,"msg-name","msg-name",-353709863));
var _ = seq__30426__$1;
if(cljs.core._EQ_.call(null,new cljs.core.Keyword(null,"repl-eval","repl-eval",-1784727398),msg_name)){
figwheel.client.ensure_cljs_user.call(null);

return figwheel.client.eval_javascript_STAR__STAR_.call(null,new cljs.core.Keyword(null,"code","code",1586293142).cljs$core$IFn$_invoke$arity$1(msg),opts,((function (vec__30425,seq__30426,first__30427,seq__30426__$1,map__30428,map__30428__$1,msg,msg_name,_,map__30422,map__30422__$1,opts,build_id){
return (function (res){
return figwheel.client.socket.send_BANG_.call(null,new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"figwheel-event","figwheel-event",519570592),"callback",new cljs.core.Keyword(null,"callback-name","callback-name",336964714),new cljs.core.Keyword(null,"callback-name","callback-name",336964714).cljs$core$IFn$_invoke$arity$1(msg),new cljs.core.Keyword(null,"content","content",15833224),res], null));
});})(vec__30425,seq__30426,first__30427,seq__30426__$1,map__30428,map__30428__$1,msg,msg_name,_,map__30422,map__30422__$1,opts,build_id))
);
} else {
return null;
}
});
;})(map__30422,map__30422__$1,opts,build_id))
});
figwheel.client.css_reloader_plugin = (function figwheel$client$css_reloader_plugin(opts){
return (function (p__30430){
var vec__30431 = p__30430;
var seq__30432 = cljs.core.seq.call(null,vec__30431);
var first__30433 = cljs.core.first.call(null,seq__30432);
var seq__30432__$1 = cljs.core.next.call(null,seq__30432);
var map__30434 = first__30433;
var map__30434__$1 = (((((!((map__30434 == null))))?(((((map__30434.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__30434.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__30434):map__30434);
var msg = map__30434__$1;
var msg_name = cljs.core.get.call(null,map__30434__$1,new cljs.core.Keyword(null,"msg-name","msg-name",-353709863));
var _ = seq__30432__$1;
if(cljs.core._EQ_.call(null,msg_name,new cljs.core.Keyword(null,"css-files-changed","css-files-changed",720773874))){
return figwheel.client.file_reloading.reload_css_files.call(null,opts,msg);
} else {
return null;
}
});
});
figwheel.client.compile_fail_warning_plugin = (function figwheel$client$compile_fail_warning_plugin(p__30436){
var map__30437 = p__30436;
var map__30437__$1 = (((((!((map__30437 == null))))?(((((map__30437.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__30437.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__30437):map__30437);
var on_compile_warning = cljs.core.get.call(null,map__30437__$1,new cljs.core.Keyword(null,"on-compile-warning","on-compile-warning",-1195585947));
var on_compile_fail = cljs.core.get.call(null,map__30437__$1,new cljs.core.Keyword(null,"on-compile-fail","on-compile-fail",728013036));
return ((function (map__30437,map__30437__$1,on_compile_warning,on_compile_fail){
return (function (p__30439){
var vec__30440 = p__30439;
var seq__30441 = cljs.core.seq.call(null,vec__30440);
var first__30442 = cljs.core.first.call(null,seq__30441);
var seq__30441__$1 = cljs.core.next.call(null,seq__30441);
var map__30443 = first__30442;
var map__30443__$1 = (((((!((map__30443 == null))))?(((((map__30443.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__30443.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__30443):map__30443);
var msg = map__30443__$1;
var msg_name = cljs.core.get.call(null,map__30443__$1,new cljs.core.Keyword(null,"msg-name","msg-name",-353709863));
var _ = seq__30441__$1;
var pred__30445 = cljs.core._EQ_;
var expr__30446 = msg_name;
if(cljs.core.truth_(pred__30445.call(null,new cljs.core.Keyword(null,"compile-warning","compile-warning",43425356),expr__30446))){
return on_compile_warning.call(null,msg);
} else {
if(cljs.core.truth_(pred__30445.call(null,new cljs.core.Keyword(null,"compile-failed","compile-failed",-477639289),expr__30446))){
return on_compile_fail.call(null,msg);
} else {
return null;
}
}
});
;})(map__30437,map__30437__$1,on_compile_warning,on_compile_fail))
});
figwheel.client.auto_jump_to_error = (function figwheel$client$auto_jump_to_error(opts,error){
if(cljs.core.truth_(new cljs.core.Keyword(null,"auto-jump-to-source-on-error","auto-jump-to-source-on-error",-960314920).cljs$core$IFn$_invoke$arity$1(opts))){
return figwheel.client.heads_up.auto_notify_source_file_line.call(null,error);
} else {
return null;
}
});
figwheel.client.heads_up_plugin_msg_handler = (function figwheel$client$heads_up_plugin_msg_handler(opts,msg_hist_SINGLEQUOTE_){
var msg_hist = figwheel.client.focus_msgs.call(null,new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"compile-failed","compile-failed",-477639289),null,new cljs.core.Keyword(null,"compile-warning","compile-warning",43425356),null,new cljs.core.Keyword(null,"files-changed","files-changed",-1418200563),null], null), null),msg_hist_SINGLEQUOTE_);
var msg_names = cljs.core.map.call(null,new cljs.core.Keyword(null,"msg-name","msg-name",-353709863),msg_hist);
var msg = cljs.core.first.call(null,msg_hist);
var c__26309__auto__ = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__26309__auto__,msg_hist,msg_names,msg){
return (function (){
var f__26310__auto__ = (function (){var switch__26214__auto__ = ((function (c__26309__auto__,msg_hist,msg_names,msg){
return (function (state_30535){
var state_val_30536 = (state_30535[(1)]);
if((state_val_30536 === (7))){
var inst_30455 = (state_30535[(2)]);
var state_30535__$1 = state_30535;
if(cljs.core.truth_(inst_30455)){
var statearr_30537_30584 = state_30535__$1;
(statearr_30537_30584[(1)] = (8));

} else {
var statearr_30538_30585 = state_30535__$1;
(statearr_30538_30585[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30536 === (20))){
var inst_30529 = (state_30535[(2)]);
var state_30535__$1 = state_30535;
var statearr_30539_30586 = state_30535__$1;
(statearr_30539_30586[(2)] = inst_30529);

(statearr_30539_30586[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30536 === (27))){
var inst_30525 = (state_30535[(2)]);
var state_30535__$1 = state_30535;
var statearr_30540_30587 = state_30535__$1;
(statearr_30540_30587[(2)] = inst_30525);

(statearr_30540_30587[(1)] = (24));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30536 === (1))){
var inst_30448 = figwheel.client.reload_file_state_QMARK_.call(null,msg_names,opts);
var state_30535__$1 = state_30535;
if(cljs.core.truth_(inst_30448)){
var statearr_30541_30588 = state_30535__$1;
(statearr_30541_30588[(1)] = (2));

} else {
var statearr_30542_30589 = state_30535__$1;
(statearr_30542_30589[(1)] = (3));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30536 === (24))){
var inst_30527 = (state_30535[(2)]);
var state_30535__$1 = state_30535;
var statearr_30543_30590 = state_30535__$1;
(statearr_30543_30590[(2)] = inst_30527);

(statearr_30543_30590[(1)] = (20));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30536 === (4))){
var inst_30533 = (state_30535[(2)]);
var state_30535__$1 = state_30535;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_30535__$1,inst_30533);
} else {
if((state_val_30536 === (15))){
var inst_30531 = (state_30535[(2)]);
var state_30535__$1 = state_30535;
var statearr_30544_30591 = state_30535__$1;
(statearr_30544_30591[(2)] = inst_30531);

(statearr_30544_30591[(1)] = (4));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30536 === (21))){
var inst_30484 = (state_30535[(2)]);
var inst_30485 = new cljs.core.Keyword(null,"exception-data","exception-data",-512474886).cljs$core$IFn$_invoke$arity$1(msg);
var inst_30486 = figwheel.client.auto_jump_to_error.call(null,opts,inst_30485);
var state_30535__$1 = (function (){var statearr_30545 = state_30535;
(statearr_30545[(7)] = inst_30484);

return statearr_30545;
})();
var statearr_30546_30592 = state_30535__$1;
(statearr_30546_30592[(2)] = inst_30486);

(statearr_30546_30592[(1)] = (20));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30536 === (31))){
var inst_30514 = figwheel.client.css_loaded_state_QMARK_.call(null,msg_names);
var state_30535__$1 = state_30535;
if(inst_30514){
var statearr_30547_30593 = state_30535__$1;
(statearr_30547_30593[(1)] = (34));

} else {
var statearr_30548_30594 = state_30535__$1;
(statearr_30548_30594[(1)] = (35));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30536 === (32))){
var inst_30523 = (state_30535[(2)]);
var state_30535__$1 = state_30535;
var statearr_30549_30595 = state_30535__$1;
(statearr_30549_30595[(2)] = inst_30523);

(statearr_30549_30595[(1)] = (27));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30536 === (33))){
var inst_30510 = (state_30535[(2)]);
var inst_30511 = new cljs.core.Keyword(null,"message","message",-406056002).cljs$core$IFn$_invoke$arity$1(msg);
var inst_30512 = figwheel.client.auto_jump_to_error.call(null,opts,inst_30511);
var state_30535__$1 = (function (){var statearr_30550 = state_30535;
(statearr_30550[(8)] = inst_30510);

return statearr_30550;
})();
var statearr_30551_30596 = state_30535__$1;
(statearr_30551_30596[(2)] = inst_30512);

(statearr_30551_30596[(1)] = (32));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30536 === (13))){
var inst_30469 = figwheel.client.heads_up.clear.call(null);
var state_30535__$1 = state_30535;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_30535__$1,(16),inst_30469);
} else {
if((state_val_30536 === (22))){
var inst_30490 = new cljs.core.Keyword(null,"message","message",-406056002).cljs$core$IFn$_invoke$arity$1(msg);
var inst_30491 = figwheel.client.heads_up.append_warning_message.call(null,inst_30490);
var state_30535__$1 = state_30535;
var statearr_30552_30597 = state_30535__$1;
(statearr_30552_30597[(2)] = inst_30491);

(statearr_30552_30597[(1)] = (24));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30536 === (36))){
var inst_30521 = (state_30535[(2)]);
var state_30535__$1 = state_30535;
var statearr_30553_30598 = state_30535__$1;
(statearr_30553_30598[(2)] = inst_30521);

(statearr_30553_30598[(1)] = (32));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30536 === (29))){
var inst_30501 = (state_30535[(2)]);
var inst_30502 = new cljs.core.Keyword(null,"message","message",-406056002).cljs$core$IFn$_invoke$arity$1(msg);
var inst_30503 = figwheel.client.auto_jump_to_error.call(null,opts,inst_30502);
var state_30535__$1 = (function (){var statearr_30554 = state_30535;
(statearr_30554[(9)] = inst_30501);

return statearr_30554;
})();
var statearr_30555_30599 = state_30535__$1;
(statearr_30555_30599[(2)] = inst_30503);

(statearr_30555_30599[(1)] = (27));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30536 === (6))){
var inst_30450 = (state_30535[(10)]);
var state_30535__$1 = state_30535;
var statearr_30556_30600 = state_30535__$1;
(statearr_30556_30600[(2)] = inst_30450);

(statearr_30556_30600[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30536 === (28))){
var inst_30497 = (state_30535[(2)]);
var inst_30498 = new cljs.core.Keyword(null,"message","message",-406056002).cljs$core$IFn$_invoke$arity$1(msg);
var inst_30499 = figwheel.client.heads_up.display_warning.call(null,inst_30498);
var state_30535__$1 = (function (){var statearr_30557 = state_30535;
(statearr_30557[(11)] = inst_30497);

return statearr_30557;
})();
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_30535__$1,(29),inst_30499);
} else {
if((state_val_30536 === (25))){
var inst_30495 = figwheel.client.heads_up.clear.call(null);
var state_30535__$1 = state_30535;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_30535__$1,(28),inst_30495);
} else {
if((state_val_30536 === (34))){
var inst_30516 = figwheel.client.heads_up.flash_loaded.call(null);
var state_30535__$1 = state_30535;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_30535__$1,(37),inst_30516);
} else {
if((state_val_30536 === (17))){
var inst_30475 = (state_30535[(2)]);
var inst_30476 = new cljs.core.Keyword(null,"exception-data","exception-data",-512474886).cljs$core$IFn$_invoke$arity$1(msg);
var inst_30477 = figwheel.client.auto_jump_to_error.call(null,opts,inst_30476);
var state_30535__$1 = (function (){var statearr_30558 = state_30535;
(statearr_30558[(12)] = inst_30475);

return statearr_30558;
})();
var statearr_30559_30601 = state_30535__$1;
(statearr_30559_30601[(2)] = inst_30477);

(statearr_30559_30601[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30536 === (3))){
var inst_30467 = figwheel.client.compile_refail_state_QMARK_.call(null,msg_names);
var state_30535__$1 = state_30535;
if(inst_30467){
var statearr_30560_30602 = state_30535__$1;
(statearr_30560_30602[(1)] = (13));

} else {
var statearr_30561_30603 = state_30535__$1;
(statearr_30561_30603[(1)] = (14));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30536 === (12))){
var inst_30463 = (state_30535[(2)]);
var state_30535__$1 = state_30535;
var statearr_30562_30604 = state_30535__$1;
(statearr_30562_30604[(2)] = inst_30463);

(statearr_30562_30604[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30536 === (2))){
var inst_30450 = (state_30535[(10)]);
var inst_30450__$1 = figwheel.client.autoload_QMARK_.call(null);
var state_30535__$1 = (function (){var statearr_30563 = state_30535;
(statearr_30563[(10)] = inst_30450__$1);

return statearr_30563;
})();
if(cljs.core.truth_(inst_30450__$1)){
var statearr_30564_30605 = state_30535__$1;
(statearr_30564_30605[(1)] = (5));

} else {
var statearr_30565_30606 = state_30535__$1;
(statearr_30565_30606[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30536 === (23))){
var inst_30493 = figwheel.client.rewarning_state_QMARK_.call(null,msg_names);
var state_30535__$1 = state_30535;
if(inst_30493){
var statearr_30566_30607 = state_30535__$1;
(statearr_30566_30607[(1)] = (25));

} else {
var statearr_30567_30608 = state_30535__$1;
(statearr_30567_30608[(1)] = (26));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30536 === (35))){
var state_30535__$1 = state_30535;
var statearr_30568_30609 = state_30535__$1;
(statearr_30568_30609[(2)] = null);

(statearr_30568_30609[(1)] = (36));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30536 === (19))){
var inst_30488 = figwheel.client.warning_append_state_QMARK_.call(null,msg_names);
var state_30535__$1 = state_30535;
if(inst_30488){
var statearr_30569_30610 = state_30535__$1;
(statearr_30569_30610[(1)] = (22));

} else {
var statearr_30570_30611 = state_30535__$1;
(statearr_30570_30611[(1)] = (23));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30536 === (11))){
var inst_30459 = (state_30535[(2)]);
var state_30535__$1 = state_30535;
var statearr_30571_30612 = state_30535__$1;
(statearr_30571_30612[(2)] = inst_30459);

(statearr_30571_30612[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30536 === (9))){
var inst_30461 = figwheel.client.heads_up.clear.call(null);
var state_30535__$1 = state_30535;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_30535__$1,(12),inst_30461);
} else {
if((state_val_30536 === (5))){
var inst_30452 = new cljs.core.Keyword(null,"autoload","autoload",-354122500).cljs$core$IFn$_invoke$arity$1(opts);
var state_30535__$1 = state_30535;
var statearr_30572_30613 = state_30535__$1;
(statearr_30572_30613[(2)] = inst_30452);

(statearr_30572_30613[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30536 === (14))){
var inst_30479 = figwheel.client.compile_fail_state_QMARK_.call(null,msg_names);
var state_30535__$1 = state_30535;
if(inst_30479){
var statearr_30573_30614 = state_30535__$1;
(statearr_30573_30614[(1)] = (18));

} else {
var statearr_30574_30615 = state_30535__$1;
(statearr_30574_30615[(1)] = (19));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30536 === (26))){
var inst_30505 = figwheel.client.warning_state_QMARK_.call(null,msg_names);
var state_30535__$1 = state_30535;
if(inst_30505){
var statearr_30575_30616 = state_30535__$1;
(statearr_30575_30616[(1)] = (30));

} else {
var statearr_30576_30617 = state_30535__$1;
(statearr_30576_30617[(1)] = (31));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30536 === (16))){
var inst_30471 = (state_30535[(2)]);
var inst_30472 = new cljs.core.Keyword(null,"exception-data","exception-data",-512474886).cljs$core$IFn$_invoke$arity$1(msg);
var inst_30473 = figwheel.client.heads_up.display_exception.call(null,inst_30472);
var state_30535__$1 = (function (){var statearr_30577 = state_30535;
(statearr_30577[(13)] = inst_30471);

return statearr_30577;
})();
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_30535__$1,(17),inst_30473);
} else {
if((state_val_30536 === (30))){
var inst_30507 = new cljs.core.Keyword(null,"message","message",-406056002).cljs$core$IFn$_invoke$arity$1(msg);
var inst_30508 = figwheel.client.heads_up.display_warning.call(null,inst_30507);
var state_30535__$1 = state_30535;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_30535__$1,(33),inst_30508);
} else {
if((state_val_30536 === (10))){
var inst_30465 = (state_30535[(2)]);
var state_30535__$1 = state_30535;
var statearr_30578_30618 = state_30535__$1;
(statearr_30578_30618[(2)] = inst_30465);

(statearr_30578_30618[(1)] = (4));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30536 === (18))){
var inst_30481 = new cljs.core.Keyword(null,"exception-data","exception-data",-512474886).cljs$core$IFn$_invoke$arity$1(msg);
var inst_30482 = figwheel.client.heads_up.display_exception.call(null,inst_30481);
var state_30535__$1 = state_30535;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_30535__$1,(21),inst_30482);
} else {
if((state_val_30536 === (37))){
var inst_30518 = (state_30535[(2)]);
var state_30535__$1 = state_30535;
var statearr_30579_30619 = state_30535__$1;
(statearr_30579_30619[(2)] = inst_30518);

(statearr_30579_30619[(1)] = (36));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30536 === (8))){
var inst_30457 = figwheel.client.heads_up.flash_loaded.call(null);
var state_30535__$1 = state_30535;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_30535__$1,(11),inst_30457);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__26309__auto__,msg_hist,msg_names,msg))
;
return ((function (switch__26214__auto__,c__26309__auto__,msg_hist,msg_names,msg){
return (function() {
var figwheel$client$heads_up_plugin_msg_handler_$_state_machine__26215__auto__ = null;
var figwheel$client$heads_up_plugin_msg_handler_$_state_machine__26215__auto____0 = (function (){
var statearr_30580 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_30580[(0)] = figwheel$client$heads_up_plugin_msg_handler_$_state_machine__26215__auto__);

(statearr_30580[(1)] = (1));

return statearr_30580;
});
var figwheel$client$heads_up_plugin_msg_handler_$_state_machine__26215__auto____1 = (function (state_30535){
while(true){
var ret_value__26216__auto__ = (function (){try{while(true){
var result__26217__auto__ = switch__26214__auto__.call(null,state_30535);
if(cljs.core.keyword_identical_QMARK_.call(null,result__26217__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__26217__auto__;
}
break;
}
}catch (e30581){if((e30581 instanceof Object)){
var ex__26218__auto__ = e30581;
var statearr_30582_30620 = state_30535;
(statearr_30582_30620[(5)] = ex__26218__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_30535);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e30581;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__26216__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__30621 = state_30535;
state_30535 = G__30621;
continue;
} else {
return ret_value__26216__auto__;
}
break;
}
});
figwheel$client$heads_up_plugin_msg_handler_$_state_machine__26215__auto__ = function(state_30535){
switch(arguments.length){
case 0:
return figwheel$client$heads_up_plugin_msg_handler_$_state_machine__26215__auto____0.call(this);
case 1:
return figwheel$client$heads_up_plugin_msg_handler_$_state_machine__26215__auto____1.call(this,state_30535);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
figwheel$client$heads_up_plugin_msg_handler_$_state_machine__26215__auto__.cljs$core$IFn$_invoke$arity$0 = figwheel$client$heads_up_plugin_msg_handler_$_state_machine__26215__auto____0;
figwheel$client$heads_up_plugin_msg_handler_$_state_machine__26215__auto__.cljs$core$IFn$_invoke$arity$1 = figwheel$client$heads_up_plugin_msg_handler_$_state_machine__26215__auto____1;
return figwheel$client$heads_up_plugin_msg_handler_$_state_machine__26215__auto__;
})()
;})(switch__26214__auto__,c__26309__auto__,msg_hist,msg_names,msg))
})();
var state__26311__auto__ = (function (){var statearr_30583 = f__26310__auto__.call(null);
(statearr_30583[(6)] = c__26309__auto__);

return statearr_30583;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__26311__auto__);
});})(c__26309__auto__,msg_hist,msg_names,msg))
);

return c__26309__auto__;
});
figwheel.client.heads_up_plugin = (function figwheel$client$heads_up_plugin(opts){
var ch = cljs.core.async.chan.call(null);
figwheel.client.heads_up_config_options_STAR__STAR_ = opts;

var c__26309__auto___30650 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__26309__auto___30650,ch){
return (function (){
var f__26310__auto__ = (function (){var switch__26214__auto__ = ((function (c__26309__auto___30650,ch){
return (function (state_30636){
var state_val_30637 = (state_30636[(1)]);
if((state_val_30637 === (1))){
var state_30636__$1 = state_30636;
var statearr_30638_30651 = state_30636__$1;
(statearr_30638_30651[(2)] = null);

(statearr_30638_30651[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30637 === (2))){
var state_30636__$1 = state_30636;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_30636__$1,(4),ch);
} else {
if((state_val_30637 === (3))){
var inst_30634 = (state_30636[(2)]);
var state_30636__$1 = state_30636;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_30636__$1,inst_30634);
} else {
if((state_val_30637 === (4))){
var inst_30624 = (state_30636[(7)]);
var inst_30624__$1 = (state_30636[(2)]);
var state_30636__$1 = (function (){var statearr_30639 = state_30636;
(statearr_30639[(7)] = inst_30624__$1);

return statearr_30639;
})();
if(cljs.core.truth_(inst_30624__$1)){
var statearr_30640_30652 = state_30636__$1;
(statearr_30640_30652[(1)] = (5));

} else {
var statearr_30641_30653 = state_30636__$1;
(statearr_30641_30653[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30637 === (5))){
var inst_30624 = (state_30636[(7)]);
var inst_30626 = figwheel.client.heads_up_plugin_msg_handler.call(null,opts,inst_30624);
var state_30636__$1 = state_30636;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_30636__$1,(8),inst_30626);
} else {
if((state_val_30637 === (6))){
var state_30636__$1 = state_30636;
var statearr_30642_30654 = state_30636__$1;
(statearr_30642_30654[(2)] = null);

(statearr_30642_30654[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30637 === (7))){
var inst_30632 = (state_30636[(2)]);
var state_30636__$1 = state_30636;
var statearr_30643_30655 = state_30636__$1;
(statearr_30643_30655[(2)] = inst_30632);

(statearr_30643_30655[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30637 === (8))){
var inst_30628 = (state_30636[(2)]);
var state_30636__$1 = (function (){var statearr_30644 = state_30636;
(statearr_30644[(8)] = inst_30628);

return statearr_30644;
})();
var statearr_30645_30656 = state_30636__$1;
(statearr_30645_30656[(2)] = null);

(statearr_30645_30656[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
});})(c__26309__auto___30650,ch))
;
return ((function (switch__26214__auto__,c__26309__auto___30650,ch){
return (function() {
var figwheel$client$heads_up_plugin_$_state_machine__26215__auto__ = null;
var figwheel$client$heads_up_plugin_$_state_machine__26215__auto____0 = (function (){
var statearr_30646 = [null,null,null,null,null,null,null,null,null];
(statearr_30646[(0)] = figwheel$client$heads_up_plugin_$_state_machine__26215__auto__);

(statearr_30646[(1)] = (1));

return statearr_30646;
});
var figwheel$client$heads_up_plugin_$_state_machine__26215__auto____1 = (function (state_30636){
while(true){
var ret_value__26216__auto__ = (function (){try{while(true){
var result__26217__auto__ = switch__26214__auto__.call(null,state_30636);
if(cljs.core.keyword_identical_QMARK_.call(null,result__26217__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__26217__auto__;
}
break;
}
}catch (e30647){if((e30647 instanceof Object)){
var ex__26218__auto__ = e30647;
var statearr_30648_30657 = state_30636;
(statearr_30648_30657[(5)] = ex__26218__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_30636);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e30647;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__26216__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__30658 = state_30636;
state_30636 = G__30658;
continue;
} else {
return ret_value__26216__auto__;
}
break;
}
});
figwheel$client$heads_up_plugin_$_state_machine__26215__auto__ = function(state_30636){
switch(arguments.length){
case 0:
return figwheel$client$heads_up_plugin_$_state_machine__26215__auto____0.call(this);
case 1:
return figwheel$client$heads_up_plugin_$_state_machine__26215__auto____1.call(this,state_30636);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
figwheel$client$heads_up_plugin_$_state_machine__26215__auto__.cljs$core$IFn$_invoke$arity$0 = figwheel$client$heads_up_plugin_$_state_machine__26215__auto____0;
figwheel$client$heads_up_plugin_$_state_machine__26215__auto__.cljs$core$IFn$_invoke$arity$1 = figwheel$client$heads_up_plugin_$_state_machine__26215__auto____1;
return figwheel$client$heads_up_plugin_$_state_machine__26215__auto__;
})()
;})(switch__26214__auto__,c__26309__auto___30650,ch))
})();
var state__26311__auto__ = (function (){var statearr_30649 = f__26310__auto__.call(null);
(statearr_30649[(6)] = c__26309__auto___30650);

return statearr_30649;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__26311__auto__);
});})(c__26309__auto___30650,ch))
);


figwheel.client.heads_up.ensure_container.call(null);

return ((function (ch){
return (function (msg_hist){
cljs.core.async.put_BANG_.call(null,ch,msg_hist);

return msg_hist;
});
;})(ch))
});
figwheel.client.enforce_project_plugin = (function figwheel$client$enforce_project_plugin(opts){
return (function (msg_hist){
if(((1) < cljs.core.count.call(null,cljs.core.set.call(null,cljs.core.keep.call(null,new cljs.core.Keyword(null,"project-id","project-id",206449307),cljs.core.take.call(null,(5),msg_hist)))))){
figwheel.client.socket.close_BANG_.call(null);

console.error("Figwheel: message received from different project. Shutting socket down.");

if(cljs.core.truth_(new cljs.core.Keyword(null,"heads-up-display","heads-up-display",-896577202).cljs$core$IFn$_invoke$arity$1(opts))){
var c__26309__auto__ = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__26309__auto__){
return (function (){
var f__26310__auto__ = (function (){var switch__26214__auto__ = ((function (c__26309__auto__){
return (function (state_30664){
var state_val_30665 = (state_30664[(1)]);
if((state_val_30665 === (1))){
var inst_30659 = cljs.core.async.timeout.call(null,(3000));
var state_30664__$1 = state_30664;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_30664__$1,(2),inst_30659);
} else {
if((state_val_30665 === (2))){
var inst_30661 = (state_30664[(2)]);
var inst_30662 = figwheel.client.heads_up.display_system_warning.call(null,"Connection from different project","Shutting connection down!!!!!");
var state_30664__$1 = (function (){var statearr_30666 = state_30664;
(statearr_30666[(7)] = inst_30661);

return statearr_30666;
})();
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_30664__$1,inst_30662);
} else {
return null;
}
}
});})(c__26309__auto__))
;
return ((function (switch__26214__auto__,c__26309__auto__){
return (function() {
var figwheel$client$enforce_project_plugin_$_state_machine__26215__auto__ = null;
var figwheel$client$enforce_project_plugin_$_state_machine__26215__auto____0 = (function (){
var statearr_30667 = [null,null,null,null,null,null,null,null];
(statearr_30667[(0)] = figwheel$client$enforce_project_plugin_$_state_machine__26215__auto__);

(statearr_30667[(1)] = (1));

return statearr_30667;
});
var figwheel$client$enforce_project_plugin_$_state_machine__26215__auto____1 = (function (state_30664){
while(true){
var ret_value__26216__auto__ = (function (){try{while(true){
var result__26217__auto__ = switch__26214__auto__.call(null,state_30664);
if(cljs.core.keyword_identical_QMARK_.call(null,result__26217__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__26217__auto__;
}
break;
}
}catch (e30668){if((e30668 instanceof Object)){
var ex__26218__auto__ = e30668;
var statearr_30669_30671 = state_30664;
(statearr_30669_30671[(5)] = ex__26218__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_30664);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e30668;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__26216__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__30672 = state_30664;
state_30664 = G__30672;
continue;
} else {
return ret_value__26216__auto__;
}
break;
}
});
figwheel$client$enforce_project_plugin_$_state_machine__26215__auto__ = function(state_30664){
switch(arguments.length){
case 0:
return figwheel$client$enforce_project_plugin_$_state_machine__26215__auto____0.call(this);
case 1:
return figwheel$client$enforce_project_plugin_$_state_machine__26215__auto____1.call(this,state_30664);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
figwheel$client$enforce_project_plugin_$_state_machine__26215__auto__.cljs$core$IFn$_invoke$arity$0 = figwheel$client$enforce_project_plugin_$_state_machine__26215__auto____0;
figwheel$client$enforce_project_plugin_$_state_machine__26215__auto__.cljs$core$IFn$_invoke$arity$1 = figwheel$client$enforce_project_plugin_$_state_machine__26215__auto____1;
return figwheel$client$enforce_project_plugin_$_state_machine__26215__auto__;
})()
;})(switch__26214__auto__,c__26309__auto__))
})();
var state__26311__auto__ = (function (){var statearr_30670 = f__26310__auto__.call(null);
(statearr_30670[(6)] = c__26309__auto__);

return statearr_30670;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__26311__auto__);
});})(c__26309__auto__))
);

return c__26309__auto__;
} else {
return null;
}
} else {
return null;
}
});
});
figwheel.client.enforce_figwheel_version_plugin = (function figwheel$client$enforce_figwheel_version_plugin(opts){
return (function (msg_hist){
var temp__5720__auto__ = new cljs.core.Keyword(null,"figwheel-version","figwheel-version",1409553832).cljs$core$IFn$_invoke$arity$1(cljs.core.first.call(null,msg_hist));
if(cljs.core.truth_(temp__5720__auto__)){
var figwheel_version = temp__5720__auto__;
if(cljs.core.not_EQ_.call(null,figwheel_version,figwheel.client._figwheel_version_)){
figwheel.client.socket.close_BANG_.call(null);

console.error("Figwheel: message received from different version of Figwheel.");

if(cljs.core.truth_(new cljs.core.Keyword(null,"heads-up-display","heads-up-display",-896577202).cljs$core$IFn$_invoke$arity$1(opts))){
var c__26309__auto__ = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__26309__auto__,figwheel_version,temp__5720__auto__){
return (function (){
var f__26310__auto__ = (function (){var switch__26214__auto__ = ((function (c__26309__auto__,figwheel_version,temp__5720__auto__){
return (function (state_30679){
var state_val_30680 = (state_30679[(1)]);
if((state_val_30680 === (1))){
var inst_30673 = cljs.core.async.timeout.call(null,(2000));
var state_30679__$1 = state_30679;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_30679__$1,(2),inst_30673);
} else {
if((state_val_30680 === (2))){
var inst_30675 = (state_30679[(2)]);
var inst_30676 = ["Figwheel Client Version <strong>",figwheel.client._figwheel_version_,"</strong> is not equal to ","Figwheel Sidecar Version <strong>",cljs.core.str.cljs$core$IFn$_invoke$arity$1(figwheel_version),"</strong>",".  Shutting down Websocket Connection!","<h4>To fix try:</h4>","<ol><li>Reload this page and make sure you are not getting a cached version of the client.</li>","<li>You may have to clean (delete compiled assets) and rebuild to make sure that the new client code is being used.</li>","<li>Also, make sure you have consistent Figwheel dependencies.</li></ol>"].join('');
var inst_30677 = figwheel.client.heads_up.display_system_warning.call(null,"Figwheel Client and Server have different versions!!",inst_30676);
var state_30679__$1 = (function (){var statearr_30681 = state_30679;
(statearr_30681[(7)] = inst_30675);

return statearr_30681;
})();
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_30679__$1,inst_30677);
} else {
return null;
}
}
});})(c__26309__auto__,figwheel_version,temp__5720__auto__))
;
return ((function (switch__26214__auto__,c__26309__auto__,figwheel_version,temp__5720__auto__){
return (function() {
var figwheel$client$enforce_figwheel_version_plugin_$_state_machine__26215__auto__ = null;
var figwheel$client$enforce_figwheel_version_plugin_$_state_machine__26215__auto____0 = (function (){
var statearr_30682 = [null,null,null,null,null,null,null,null];
(statearr_30682[(0)] = figwheel$client$enforce_figwheel_version_plugin_$_state_machine__26215__auto__);

(statearr_30682[(1)] = (1));

return statearr_30682;
});
var figwheel$client$enforce_figwheel_version_plugin_$_state_machine__26215__auto____1 = (function (state_30679){
while(true){
var ret_value__26216__auto__ = (function (){try{while(true){
var result__26217__auto__ = switch__26214__auto__.call(null,state_30679);
if(cljs.core.keyword_identical_QMARK_.call(null,result__26217__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__26217__auto__;
}
break;
}
}catch (e30683){if((e30683 instanceof Object)){
var ex__26218__auto__ = e30683;
var statearr_30684_30686 = state_30679;
(statearr_30684_30686[(5)] = ex__26218__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_30679);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e30683;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__26216__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__30687 = state_30679;
state_30679 = G__30687;
continue;
} else {
return ret_value__26216__auto__;
}
break;
}
});
figwheel$client$enforce_figwheel_version_plugin_$_state_machine__26215__auto__ = function(state_30679){
switch(arguments.length){
case 0:
return figwheel$client$enforce_figwheel_version_plugin_$_state_machine__26215__auto____0.call(this);
case 1:
return figwheel$client$enforce_figwheel_version_plugin_$_state_machine__26215__auto____1.call(this,state_30679);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
figwheel$client$enforce_figwheel_version_plugin_$_state_machine__26215__auto__.cljs$core$IFn$_invoke$arity$0 = figwheel$client$enforce_figwheel_version_plugin_$_state_machine__26215__auto____0;
figwheel$client$enforce_figwheel_version_plugin_$_state_machine__26215__auto__.cljs$core$IFn$_invoke$arity$1 = figwheel$client$enforce_figwheel_version_plugin_$_state_machine__26215__auto____1;
return figwheel$client$enforce_figwheel_version_plugin_$_state_machine__26215__auto__;
})()
;})(switch__26214__auto__,c__26309__auto__,figwheel_version,temp__5720__auto__))
})();
var state__26311__auto__ = (function (){var statearr_30685 = f__26310__auto__.call(null);
(statearr_30685[(6)] = c__26309__auto__);

return statearr_30685;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__26311__auto__);
});})(c__26309__auto__,figwheel_version,temp__5720__auto__))
);

return c__26309__auto__;
} else {
return null;
}
} else {
return null;
}
} else {
return null;
}
});
});
figwheel.client.default_on_jsload = cljs.core.identity;
figwheel.client.file_line_column = (function figwheel$client$file_line_column(p__30688){
var map__30689 = p__30688;
var map__30689__$1 = (((((!((map__30689 == null))))?(((((map__30689.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__30689.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__30689):map__30689);
var file = cljs.core.get.call(null,map__30689__$1,new cljs.core.Keyword(null,"file","file",-1269645878));
var line = cljs.core.get.call(null,map__30689__$1,new cljs.core.Keyword(null,"line","line",212345235));
var column = cljs.core.get.call(null,map__30689__$1,new cljs.core.Keyword(null,"column","column",2078222095));
var G__30691 = "";
var G__30691__$1 = (cljs.core.truth_(file)?[G__30691,"file ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(file)].join(''):G__30691);
var G__30691__$2 = (cljs.core.truth_(line)?[G__30691__$1," at line ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(line)].join(''):G__30691__$1);
if(cljs.core.truth_((function (){var and__4120__auto__ = line;
if(cljs.core.truth_(and__4120__auto__)){
return column;
} else {
return and__4120__auto__;
}
})())){
return [G__30691__$2,", column ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(column)].join('');
} else {
return G__30691__$2;
}
});
figwheel.client.default_on_compile_fail = (function figwheel$client$default_on_compile_fail(p__30692){
var map__30693 = p__30692;
var map__30693__$1 = (((((!((map__30693 == null))))?(((((map__30693.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__30693.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__30693):map__30693);
var ed = map__30693__$1;
var exception_data = cljs.core.get.call(null,map__30693__$1,new cljs.core.Keyword(null,"exception-data","exception-data",-512474886));
var cause = cljs.core.get.call(null,map__30693__$1,new cljs.core.Keyword(null,"cause","cause",231901252));
var message_30696 = (function (){var G__30695 = cljs.core.apply.call(null,cljs.core.str,"Figwheel: Compile Exception ",figwheel.client.format_messages.call(null,exception_data));
if(cljs.core.truth_(new cljs.core.Keyword(null,"file","file",-1269645878).cljs$core$IFn$_invoke$arity$1(exception_data))){
return [cljs.core.str.cljs$core$IFn$_invoke$arity$1(G__30695)," Error on ",figwheel.client.file_line_column.call(null,exception_data)].join('');
} else {
return G__30695;
}
})();
figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"warn","warn",-436710552),message_30696);

return ed;
});
figwheel.client.default_on_compile_warning = (function figwheel$client$default_on_compile_warning(p__30697){
var map__30698 = p__30697;
var map__30698__$1 = (((((!((map__30698 == null))))?(((((map__30698.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__30698.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__30698):map__30698);
var w = map__30698__$1;
var message = cljs.core.get.call(null,map__30698__$1,new cljs.core.Keyword(null,"message","message",-406056002));
figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"warn","warn",-436710552),["Figwheel: Compile Warning - ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"message","message",-406056002).cljs$core$IFn$_invoke$arity$1(message))," in ",figwheel.client.file_line_column.call(null,message)].join(''));

return w;
});
figwheel.client.default_before_load = (function figwheel$client$default_before_load(files){
figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"debug","debug",-1608172596),"Figwheel: notified of file changes");

return files;
});
figwheel.client.default_on_cssload = (function figwheel$client$default_on_cssload(files){
figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"debug","debug",-1608172596),"Figwheel: loaded CSS files");

figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"info","info",-317069002),cljs.core.pr_str.call(null,cljs.core.map.call(null,new cljs.core.Keyword(null,"file","file",-1269645878),files)));

return files;
});
if((typeof figwheel !== 'undefined') && (typeof figwheel.client !== 'undefined') && (typeof figwheel.client.config_defaults !== 'undefined')){
} else {
figwheel.client.config_defaults = cljs.core.PersistentHashMap.fromArrays([new cljs.core.Keyword(null,"on-compile-warning","on-compile-warning",-1195585947),new cljs.core.Keyword(null,"on-jsload","on-jsload",-395756602),new cljs.core.Keyword(null,"reload-dependents","reload-dependents",-956865430),new cljs.core.Keyword(null,"on-compile-fail","on-compile-fail",728013036),new cljs.core.Keyword(null,"debug","debug",-1608172596),new cljs.core.Keyword(null,"heads-up-display","heads-up-display",-896577202),new cljs.core.Keyword(null,"websocket-url","websocket-url",-490444938),new cljs.core.Keyword(null,"auto-jump-to-source-on-error","auto-jump-to-source-on-error",-960314920),new cljs.core.Keyword(null,"before-jsload","before-jsload",-847513128),new cljs.core.Keyword(null,"load-warninged-code","load-warninged-code",-2030345223),new cljs.core.Keyword(null,"eval-fn","eval-fn",-1111644294),new cljs.core.Keyword(null,"retry-count","retry-count",1936122875),new cljs.core.Keyword(null,"autoload","autoload",-354122500),new cljs.core.Keyword(null,"on-cssload","on-cssload",1825432318)],[new cljs.core.Var(function(){return figwheel.client.default_on_compile_warning;},new cljs.core.Symbol("figwheel.client","default-on-compile-warning","figwheel.client/default-on-compile-warning",584144208,null),cljs.core.PersistentHashMap.fromArrays([new cljs.core.Keyword(null,"ns","ns",441598760),new cljs.core.Keyword(null,"name","name",1843675177),new cljs.core.Keyword(null,"file","file",-1269645878),new cljs.core.Keyword(null,"end-column","end-column",1425389514),new cljs.core.Keyword(null,"column","column",2078222095),new cljs.core.Keyword(null,"line","line",212345235),new cljs.core.Keyword(null,"end-line","end-line",1837326455),new cljs.core.Keyword(null,"arglists","arglists",1661989754),new cljs.core.Keyword(null,"doc","doc",1913296891),new cljs.core.Keyword(null,"test","test",577538877)],[new cljs.core.Symbol(null,"figwheel.client","figwheel.client",-538710252,null),new cljs.core.Symbol(null,"default-on-compile-warning","default-on-compile-warning",-18911586,null),"resources/public/js/compiled/out/figwheel/client.cljs",33,1,362,362,cljs.core.list(new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"keys","keys",1068423698),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"message","message",1234475525,null)], null),new cljs.core.Keyword(null,"as","as",1148689641),new cljs.core.Symbol(null,"w","w",1994700528,null)], null)], null)),null,(cljs.core.truth_(figwheel.client.default_on_compile_warning)?figwheel.client.default_on_compile_warning.cljs$lang$test:null)])),figwheel.client.default_on_jsload,true,new cljs.core.Var(function(){return figwheel.client.default_on_compile_fail;},new cljs.core.Symbol("figwheel.client","default-on-compile-fail","figwheel.client/default-on-compile-fail",1384826337,null),cljs.core.PersistentHashMap.fromArrays([new cljs.core.Keyword(null,"ns","ns",441598760),new cljs.core.Keyword(null,"name","name",1843675177),new cljs.core.Keyword(null,"file","file",-1269645878),new cljs.core.Keyword(null,"end-column","end-column",1425389514),new cljs.core.Keyword(null,"column","column",2078222095),new cljs.core.Keyword(null,"line","line",212345235),new cljs.core.Keyword(null,"end-line","end-line",1837326455),new cljs.core.Keyword(null,"arglists","arglists",1661989754),new cljs.core.Keyword(null,"doc","doc",1913296891),new cljs.core.Keyword(null,"test","test",577538877)],[new cljs.core.Symbol(null,"figwheel.client","figwheel.client",-538710252,null),new cljs.core.Symbol(null,"default-on-compile-fail","default-on-compile-fail",-158814813,null),"resources/public/js/compiled/out/figwheel/client.cljs",30,1,355,355,cljs.core.list(new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"keys","keys",1068423698),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"exception-data","exception-data",1128056641,null),new cljs.core.Symbol(null,"cause","cause",1872432779,null)], null),new cljs.core.Keyword(null,"as","as",1148689641),new cljs.core.Symbol(null,"ed","ed",2076825751,null)], null)], null)),null,(cljs.core.truth_(figwheel.client.default_on_compile_fail)?figwheel.client.default_on_compile_fail.cljs$lang$test:null)])),false,true,["ws://",cljs.core.str.cljs$core$IFn$_invoke$arity$1(((figwheel.client.utils.html_env_QMARK_.call(null))?location.host:"localhost:3449")),"/figwheel-ws"].join(''),false,figwheel.client.default_before_load,false,false,(100),true,figwheel.client.default_on_cssload]);
}
figwheel.client.handle_deprecated_jsload_callback = (function figwheel$client$handle_deprecated_jsload_callback(config){
if(cljs.core.truth_(new cljs.core.Keyword(null,"jsload-callback","jsload-callback",-1949628369).cljs$core$IFn$_invoke$arity$1(config))){
return cljs.core.dissoc.call(null,cljs.core.assoc.call(null,config,new cljs.core.Keyword(null,"on-jsload","on-jsload",-395756602),new cljs.core.Keyword(null,"jsload-callback","jsload-callback",-1949628369).cljs$core$IFn$_invoke$arity$1(config)),new cljs.core.Keyword(null,"jsload-callback","jsload-callback",-1949628369));
} else {
return config;
}
});
figwheel.client.fill_url_template = (function figwheel$client$fill_url_template(config){
if(figwheel.client.utils.html_env_QMARK_.call(null)){
return cljs.core.update_in.call(null,config,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"websocket-url","websocket-url",-490444938)], null),(function (x){
return clojure.string.replace.call(null,clojure.string.replace.call(null,x,"[[client-hostname]]",location.hostname),"[[client-port]]",location.port);
}));
} else {
return config;
}
});
figwheel.client.base_plugins = (function figwheel$client$base_plugins(system_options){
var base = new cljs.core.PersistentArrayMap(null, 6, [new cljs.core.Keyword(null,"enforce-project-plugin","enforce-project-plugin",959402899),figwheel.client.enforce_project_plugin,new cljs.core.Keyword(null,"enforce-figwheel-version-plugin","enforce-figwheel-version-plugin",-1916185220),figwheel.client.enforce_figwheel_version_plugin,new cljs.core.Keyword(null,"file-reloader-plugin","file-reloader-plugin",-1792964733),figwheel.client.file_reloader_plugin,new cljs.core.Keyword(null,"comp-fail-warning-plugin","comp-fail-warning-plugin",634311),figwheel.client.compile_fail_warning_plugin,new cljs.core.Keyword(null,"css-reloader-plugin","css-reloader-plugin",2002032904),figwheel.client.css_reloader_plugin,new cljs.core.Keyword(null,"repl-plugin","repl-plugin",-1138952371),figwheel.client.repl_plugin], null);
var base__$1 = (((!(figwheel.client.utils.html_env_QMARK_.call(null))))?cljs.core.select_keys.call(null,base,new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"file-reloader-plugin","file-reloader-plugin",-1792964733),new cljs.core.Keyword(null,"comp-fail-warning-plugin","comp-fail-warning-plugin",634311),new cljs.core.Keyword(null,"repl-plugin","repl-plugin",-1138952371)], null)):base);
var base__$2 = ((new cljs.core.Keyword(null,"autoload","autoload",-354122500).cljs$core$IFn$_invoke$arity$1(system_options) === false)?cljs.core.dissoc.call(null,base__$1,new cljs.core.Keyword(null,"file-reloader-plugin","file-reloader-plugin",-1792964733)):base__$1);
if(cljs.core.truth_((function (){var and__4120__auto__ = new cljs.core.Keyword(null,"heads-up-display","heads-up-display",-896577202).cljs$core$IFn$_invoke$arity$1(system_options);
if(cljs.core.truth_(and__4120__auto__)){
return figwheel.client.utils.html_env_QMARK_.call(null);
} else {
return and__4120__auto__;
}
})())){
return cljs.core.assoc.call(null,base__$2,new cljs.core.Keyword(null,"heads-up-display-plugin","heads-up-display-plugin",1745207501),figwheel.client.heads_up_plugin);
} else {
return base__$2;
}
});
figwheel.client.add_message_watch = (function figwheel$client$add_message_watch(key,callback){
return cljs.core.add_watch.call(null,figwheel.client.socket.message_history_atom,key,(function (_,___$1,___$2,msg_hist){
return callback.call(null,cljs.core.first.call(null,msg_hist));
}));
});
figwheel.client.add_json_message_watch = (function figwheel$client$add_json_message_watch(key,callback){
return figwheel.client.add_message_watch.call(null,key,cljs.core.comp.call(null,callback,cljs.core.clj__GT_js));
});
goog.exportSymbol('figwheel.client.add_json_message_watch', figwheel.client.add_json_message_watch);
figwheel.client.add_plugins = (function figwheel$client$add_plugins(plugins,system_options){
var seq__30700 = cljs.core.seq.call(null,plugins);
var chunk__30701 = null;
var count__30702 = (0);
var i__30703 = (0);
while(true){
if((i__30703 < count__30702)){
var vec__30710 = cljs.core._nth.call(null,chunk__30701,i__30703);
var k = cljs.core.nth.call(null,vec__30710,(0),null);
var plugin = cljs.core.nth.call(null,vec__30710,(1),null);
if(cljs.core.truth_(plugin)){
var pl_30716 = plugin.call(null,system_options);
cljs.core.add_watch.call(null,figwheel.client.socket.message_history_atom,k,((function (seq__30700,chunk__30701,count__30702,i__30703,pl_30716,vec__30710,k,plugin){
return (function (_,___$1,___$2,msg_hist){
return pl_30716.call(null,msg_hist);
});})(seq__30700,chunk__30701,count__30702,i__30703,pl_30716,vec__30710,k,plugin))
);
} else {
}


var G__30717 = seq__30700;
var G__30718 = chunk__30701;
var G__30719 = count__30702;
var G__30720 = (i__30703 + (1));
seq__30700 = G__30717;
chunk__30701 = G__30718;
count__30702 = G__30719;
i__30703 = G__30720;
continue;
} else {
var temp__5720__auto__ = cljs.core.seq.call(null,seq__30700);
if(temp__5720__auto__){
var seq__30700__$1 = temp__5720__auto__;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__30700__$1)){
var c__4550__auto__ = cljs.core.chunk_first.call(null,seq__30700__$1);
var G__30721 = cljs.core.chunk_rest.call(null,seq__30700__$1);
var G__30722 = c__4550__auto__;
var G__30723 = cljs.core.count.call(null,c__4550__auto__);
var G__30724 = (0);
seq__30700 = G__30721;
chunk__30701 = G__30722;
count__30702 = G__30723;
i__30703 = G__30724;
continue;
} else {
var vec__30713 = cljs.core.first.call(null,seq__30700__$1);
var k = cljs.core.nth.call(null,vec__30713,(0),null);
var plugin = cljs.core.nth.call(null,vec__30713,(1),null);
if(cljs.core.truth_(plugin)){
var pl_30725 = plugin.call(null,system_options);
cljs.core.add_watch.call(null,figwheel.client.socket.message_history_atom,k,((function (seq__30700,chunk__30701,count__30702,i__30703,pl_30725,vec__30713,k,plugin,seq__30700__$1,temp__5720__auto__){
return (function (_,___$1,___$2,msg_hist){
return pl_30725.call(null,msg_hist);
});})(seq__30700,chunk__30701,count__30702,i__30703,pl_30725,vec__30713,k,plugin,seq__30700__$1,temp__5720__auto__))
);
} else {
}


var G__30726 = cljs.core.next.call(null,seq__30700__$1);
var G__30727 = null;
var G__30728 = (0);
var G__30729 = (0);
seq__30700 = G__30726;
chunk__30701 = G__30727;
count__30702 = G__30728;
i__30703 = G__30729;
continue;
}
} else {
return null;
}
}
break;
}
});
figwheel.client.start = (function figwheel$client$start(var_args){
var G__30731 = arguments.length;
switch (G__30731) {
case 1:
return figwheel.client.start.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 0:
return figwheel.client.start.cljs$core$IFn$_invoke$arity$0();

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

figwheel.client.start.cljs$core$IFn$_invoke$arity$1 = (function (opts){
if((goog.dependencies_ == null)){
return null;
} else {
if((typeof figwheel !== 'undefined') && (typeof figwheel.client !== 'undefined') && (typeof figwheel.client.__figwheel_start_once__ !== 'undefined')){
return null;
} else {
return (
figwheel.client.__figwheel_start_once__ = setTimeout((function (){
var plugins_SINGLEQUOTE_ = new cljs.core.Keyword(null,"plugins","plugins",1900073717).cljs$core$IFn$_invoke$arity$1(opts);
var merge_plugins = new cljs.core.Keyword(null,"merge-plugins","merge-plugins",-1193912370).cljs$core$IFn$_invoke$arity$1(opts);
var system_options = figwheel.client.fill_url_template.call(null,figwheel.client.handle_deprecated_jsload_callback.call(null,cljs.core.merge.call(null,figwheel.client.config_defaults,cljs.core.dissoc.call(null,opts,new cljs.core.Keyword(null,"plugins","plugins",1900073717),new cljs.core.Keyword(null,"merge-plugins","merge-plugins",-1193912370)))));
var plugins = (cljs.core.truth_(plugins_SINGLEQUOTE_)?plugins_SINGLEQUOTE_:cljs.core.merge.call(null,figwheel.client.base_plugins.call(null,system_options),merge_plugins));
figwheel.client.utils._STAR_print_debug_STAR_ = new cljs.core.Keyword(null,"debug","debug",-1608172596).cljs$core$IFn$_invoke$arity$1(opts);

figwheel.client.enable_repl_print_BANG_.call(null);

figwheel.client.add_plugins.call(null,plugins,system_options);

figwheel.client.file_reloading.patch_goog_base.call(null);

var seq__30732_30737 = cljs.core.seq.call(null,new cljs.core.Keyword(null,"initial-messages","initial-messages",2057377771).cljs$core$IFn$_invoke$arity$1(system_options));
var chunk__30733_30738 = null;
var count__30734_30739 = (0);
var i__30735_30740 = (0);
while(true){
if((i__30735_30740 < count__30734_30739)){
var msg_30741 = cljs.core._nth.call(null,chunk__30733_30738,i__30735_30740);
figwheel.client.socket.handle_incoming_message.call(null,msg_30741);


var G__30742 = seq__30732_30737;
var G__30743 = chunk__30733_30738;
var G__30744 = count__30734_30739;
var G__30745 = (i__30735_30740 + (1));
seq__30732_30737 = G__30742;
chunk__30733_30738 = G__30743;
count__30734_30739 = G__30744;
i__30735_30740 = G__30745;
continue;
} else {
var temp__5720__auto___30746 = cljs.core.seq.call(null,seq__30732_30737);
if(temp__5720__auto___30746){
var seq__30732_30747__$1 = temp__5720__auto___30746;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__30732_30747__$1)){
var c__4550__auto___30748 = cljs.core.chunk_first.call(null,seq__30732_30747__$1);
var G__30749 = cljs.core.chunk_rest.call(null,seq__30732_30747__$1);
var G__30750 = c__4550__auto___30748;
var G__30751 = cljs.core.count.call(null,c__4550__auto___30748);
var G__30752 = (0);
seq__30732_30737 = G__30749;
chunk__30733_30738 = G__30750;
count__30734_30739 = G__30751;
i__30735_30740 = G__30752;
continue;
} else {
var msg_30753 = cljs.core.first.call(null,seq__30732_30747__$1);
figwheel.client.socket.handle_incoming_message.call(null,msg_30753);


var G__30754 = cljs.core.next.call(null,seq__30732_30747__$1);
var G__30755 = null;
var G__30756 = (0);
var G__30757 = (0);
seq__30732_30737 = G__30754;
chunk__30733_30738 = G__30755;
count__30734_30739 = G__30756;
i__30735_30740 = G__30757;
continue;
}
} else {
}
}
break;
}

return figwheel.client.socket.open.call(null,system_options);
})))
;
}
}
});

figwheel.client.start.cljs$core$IFn$_invoke$arity$0 = (function (){
return figwheel.client.start.call(null,cljs.core.PersistentArrayMap.EMPTY);
});

figwheel.client.start.cljs$lang$maxFixedArity = 1;

figwheel.client.watch_and_reload_with_opts = figwheel.client.start;
figwheel.client.watch_and_reload = (function figwheel$client$watch_and_reload(var_args){
var args__4736__auto__ = [];
var len__4730__auto___30762 = arguments.length;
var i__4731__auto___30763 = (0);
while(true){
if((i__4731__auto___30763 < len__4730__auto___30762)){
args__4736__auto__.push((arguments[i__4731__auto___30763]));

var G__30764 = (i__4731__auto___30763 + (1));
i__4731__auto___30763 = G__30764;
continue;
} else {
}
break;
}

var argseq__4737__auto__ = ((((0) < args__4736__auto__.length))?(new cljs.core.IndexedSeq(args__4736__auto__.slice((0)),(0),null)):null);
return figwheel.client.watch_and_reload.cljs$core$IFn$_invoke$arity$variadic(argseq__4737__auto__);
});

figwheel.client.watch_and_reload.cljs$core$IFn$_invoke$arity$variadic = (function (p__30759){
var map__30760 = p__30759;
var map__30760__$1 = (((((!((map__30760 == null))))?(((((map__30760.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__30760.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__30760):map__30760);
var opts = map__30760__$1;
return figwheel.client.start.call(null,opts);
});

figwheel.client.watch_and_reload.cljs$lang$maxFixedArity = (0);

/** @this {Function} */
figwheel.client.watch_and_reload.cljs$lang$applyTo = (function (seq30758){
var self__4718__auto__ = this;
return self__4718__auto__.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq30758));
});

figwheel.client.fetch_data_from_env = (function figwheel$client$fetch_data_from_env(){
try{return cljs.reader.read_string.call(null,goog.object.get(window,"FIGWHEEL_CLIENT_CONFIGURATION"));
}catch (e30765){if((e30765 instanceof Error)){
var e = e30765;
cljs.core._STAR_print_err_fn_STAR_.call(null,"Unable to load FIGWHEEL_CLIENT_CONFIGURATION from the environment");

return new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"autoload","autoload",-354122500),false], null);
} else {
throw e30765;

}
}});
figwheel.client.console_intro_message = "Figwheel has compiled a temporary helper application to your :output-file.\n\nThe code currently in your configured output file does not\nrepresent the code that you are trying to compile.\n\nThis temporary application is intended to help you continue to get\nfeedback from Figwheel until the build you are working on compiles\ncorrectly.\n\nWhen your ClojureScript source code compiles correctly this helper\napplication will auto-reload and pick up your freshly compiled\nClojureScript program.";
figwheel.client.bad_compile_helper_app = (function figwheel$client$bad_compile_helper_app(){
cljs.core.enable_console_print_BANG_.call(null);

var config = figwheel.client.fetch_data_from_env.call(null);
cljs.core.println.call(null,figwheel.client.console_intro_message);

figwheel.client.heads_up.bad_compile_screen.call(null);

if(cljs.core.truth_(goog.dependencies_)){
} else {
goog.dependencies_ = true;
}

figwheel.client.start.call(null,config);

return figwheel.client.add_message_watch.call(null,new cljs.core.Keyword(null,"listen-for-successful-compile","listen-for-successful-compile",-995277603),((function (config){
return (function (p__30766){
var map__30767 = p__30766;
var map__30767__$1 = (((((!((map__30767 == null))))?(((((map__30767.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__30767.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__30767):map__30767);
var msg_name = cljs.core.get.call(null,map__30767__$1,new cljs.core.Keyword(null,"msg-name","msg-name",-353709863));
if(cljs.core._EQ_.call(null,msg_name,new cljs.core.Keyword(null,"files-changed","files-changed",-1418200563))){
return location.href = location.href;
} else {
return null;
}
});})(config))
);
});

//# sourceMappingURL=client.js.map?rel=1591997118746
